// Registration/Revisit

function layer_kpsearch() {
    document.getElementById('layer_kpsearch').className = 'animated layerwindow fadeIn';
    document.getElementById('layer_kpsearch').style.display = 'block';

    document.getElementById('layer_kpsearch_kpuuidoutput').style.display = 'block';
    document.getElementById('btn_register_new_kp_client').style.display = 'none';
    document.getElementById('layer_kpsearch_kpuuidsearchoutput').style.display = 'none';

    localStorage.removeItem('uuid_entry');
    localStorage.removeItem('scancardreturn');
}
function layer_kpsearch_close() {
    document.getElementById('layer_kpsearch').className = 'animated layerwindow fadeOut';
    setTimeout(function () {
        document.getElementById('layer_kpsearch').style.display = 'none';
    }, 700);
    document.getElementById('layer_kpsearch_search_results_ajax').innerHTML = '<div class="kp_search_results_box animated">No KP Client data to display.</div>';

    // document.getElementById('btn_bookforservice').style.display = 'none';
    document.getElementById('layer_kpsearch_kpuuidoutput').style.display = 'block';
    document.getElementById('layer_kpsearch_kpuuidsearchoutput').style.display = 'none';
    document.getElementById('btn_register_new_kp_client').style.display = 'none';
}
function registernewkpclient() {
    window.location.href = 'main.php?currentview=screening_tool';
}
function layer_kpbookforservice_close() {
    document.getElementById('layer_kpbookforservice').className = 'animated layerwindow fadeOut';
    setTimeout(function () {
        document.getElementById('layer_kpbookforservice').style.display = 'none';
    }, 600);
}
function layer_kpbookforservice_existing_client_close() {
    document.getElementById('layer_kpbookforservice_existing_client').className = 'animated layerwindow fadeOut';
    setTimeout(function () {
        document.getElementById('layer_kpbookforservice_existing_client').style.display = 'none';
    }, 600);
}
function layer_kpbookforservice() {
    var currentuuid = localStorage.getItem('currentuuid_prep');

    $.get("database/kp_active/checkuuid.php?&task=checkuuid&uuid=" + currentuuid + "", function (data) {
        // alert(data);

        if (data == "uuid_does_exist") {
            $.get("database/kp_active/checkuuid.php?&task=checkifactive&uuid=" + currentuuid + "", function (data) {
                // alert(data);

                if (data == "uuid_is_active") {
                    alert('This client is currently in the clinic and can not be added again.');
                    loadingindicator_stop();
                }
                else if (data == "uuid_is_not_active") {
                    layer_kpbookforservice_existing_client_open();
                }
            });

        }
        else if (data == "uuid_does_not_exist") {
            document.getElementById('book_client_name').innerHTML = localStorage.getItem('currentclientname_prep');
            layer_kpbookforservice_open();
        }
    });
}
function layer_kpbookforservice_open() {
    document.getElementById('layer_kpbookforservice').className = 'animated layerwindow fadeIn';
    document.getElementById('layer_kpbookforservice').style.display = 'block';
    loadingindicator_stop();
}
function layer_kpbookforservice_existing_client_open() {
    document.getElementById('layer_kpbookforservice_existing_client').className = 'animated layerwindow fadeIn';
    document.getElementById('layer_kpbookforservice_existing_client').style.display = 'block';

    var currentuuid = localStorage.getItem('currentuuid_prep');

    $("#existing_client_clinical_visitation_history_box").load("/emarps/database/kp_active/active_kp_clinical_visitation.php?uuid=" + currentuuid + "");

    $.get("database/kp_active/checkuuid.php?&task=lastvisit&uuid=" + currentuuid + "", function (data) {
        document.getElementById('existing_client_last_visit_details').innerHTML = data;
        get_partner_and_clinic();
    });
}

function get_partner_and_clinic() {
    loadingindicator_stop();
    partner_name_from_id = document.getElementById('partner_name_from_id').innerHTML;
    clinic_name_from_id = document.getElementById('clinic_name_from_id').innerHTML;
    // alert(''+partner_name_from_id+' '+clinic_name_from_id+'');
    $.get("database/kp_active/checkuuid.php?&task=get_partner&partner_id=" + partner_name_from_id + "", function (data) {
        document.getElementById('partner_name_from_id').innerHTML = data;
    });
    $.get("database/kp_active/checkuuid.php?&task=get_clinic&clinic_id=" + clinic_name_from_id + "", function (data) {
        document.getElementById('clinic_name_from_id').innerHTML = data;
    });
}

function uuid_entry_search() {
    uuid_entry = document.getElementById('uuid_entry').value;
    // alert(uuid_entry);
    if (uuid_entry < 1) {
        alert('Please enter UUID to search for KP Clients.');
        return;
    }
    localStorage.setItem('uuid_entry', uuid_entry);
    document.getElementById('layer_kpsearch_uuid_view').innerHTML = uuid_entry;
    $("#layer_kpsearch_search_results_ajax").load("/emarps/database/registration_revisit.php?uuid=" + uuid_entry + "");
    setTimeout(function () {
        var resultsfromajax = document.getElementById('layer_kpsearch_search_results_ajax').innerHTML;
// alert(resultsfromajax);
        if (resultsfromajax == 'No results to display. Please check UUID.') {
            // alert('Error');
        }
        else {

        }
    }, 1000);
    layer_kpsearch();
}

// Screening Tool A

$(document).ready(function () {

    $("input:radio[name='radio_alreadyregisteredinclinic']").click(function () {
        if ($(this).attr("value") == "Yes") {
            $("#selectclinicswrapper").fadeIn();
            $('#txt_nameclientusedinclinic').prop('required', true);
            $('#txt_patientdiscdicenumber').prop('required', true);
        }
        if ($(this).attr("value") == "No") {
            $("#selectclinicswrapper").fadeOut();
            $('#txt_nameclientusedinclinic').prop('required', false);
            $('#txt_patientdiscdicenumber').prop('required', false);
        }
    });

    $("input:radio[name='radio_recievedsexforfavours']").click(function () {
        if ($(this).attr("value") == "Yes") {
            $("#txt_recievedsexforfavours").fadeIn();
            $('#txt_recievedsexforfavours').prop('required', true);
        }
        if ($(this).attr("value") == "No") {
            $("#txt_recievedsexforfavours").fadeOut();
            $('#txt_recievedsexforfavours').prop('required', false);
        }
    });

    $("input:radio[name='radio_givensexforfavours']").click(function () {
        if ($(this).attr("value") == "Yes") {
            $("#txt_givensexforfavours").fadeIn();
            $('#txt_givensexforfavours').prop('required', true);
        }
        if ($(this).attr("value") == "No") {
            $("#txt_givensexforfavours").fadeOut();
            $('#txt_givensexforfavours').prop('required', false);
        }
    });

    $("input:radio[name='radio_sexwithcasualpartnerinlast3months']").click(function () {
        if ($(this).attr("value") == "Yes") {
            $("#txt_sexwithcasualpartnerinlast3months").fadeIn();
            $('#txt_sexwithcasualpartnerinlast3months').prop('required', true);
        }
        if ($(this).attr("value") == "No") {
            $("#txt_sexwithcasualpartnerinlast3months").fadeOut();
            $('#txt_sexwithcasualpartnerinlast3months').prop('required', false);
        }
    });

    $("input:radio[name='radio_doyoueverinjectyourselfwithdrugs']").click(function () {
        if ($(this).attr("value") == "Yes") {
            $("#probefutherifclientisidu").fadeIn();
        }
        if ($(this).attr("value") == "No") {
            $("#probefutherifclientisidu").fadeOut();
        }
    });

});

function layer_sourceofincome() {
    document.getElementById('layer_sourceofincome').className = 'animated layerwindow fadeIn';
    document.getElementById('layer_sourceofincome').style.display = 'block';
}
function layer_sourceofincome_close() {
    document.getElementById('layer_sourceofincome').className = 'animated layerwindow fadeOut';
    setTimeout(function () {
        document.getElementById('layer_sourceofincome').style.display = 'none';
    }, 600);
}
function layer_typeofsyndrome() {
    document.getElementById('layer_typeofsyndrome').className = 'animated layerwindow fadeIn';
    document.getElementById('layer_typeofsyndrome').style.display = 'block';
}
function layer_typeofsyndrome_close() {
    document.getElementById('layer_typeofsyndrome').className = 'animated layerwindow fadeOut';
    setTimeout(function () {
        document.getElementById('layer_typeofsyndrome').style.display = 'none';
    }, 600);
}
function layer_drugprescription() {
    document.getElementById('layer_drugprescription').className = 'animated layerwindow fadeIn';
    document.getElementById('layer_drugprescription').style.display = 'block';
}
function layer_drugprescription_close() {
    document.getElementById('layer_drugprescription').className = 'animated layerwindow fadeOut';
    setTimeout(function () {
        document.getElementById('layer_drugprescription').style.display = 'none';
    }, 600);
}

$(function () {

    $('#screening_tool_form_a').submit(function (e) {
        e.preventDefault();

        // alert('validation OK!');
        generateuuid();
    });

});

function screening_tool_b() {
    document.getElementById('screening_tool_a').style.display = 'none';
    document.getElementById('screening_tool_b').style.display = 'block';
}

function marps_selected() {
    document.getElementById('marps_selected').style.display = 'block';
    document.getElementById('non_marps_selected').style.display = 'none';
}

function non_marps_selected() {
    document.getElementById('marps_selected').style.display = 'none';
    document.getElementById('non_marps_selected').style.display = 'block';
}

// Partner Registration

$(document).ready(function () {
    if (window.location.href.indexOf("?currentview=clinic_registration") > -1) {

        var ac_config = {
            source: "database/ajax.php",
            select: function (event, ui) {
                $("#clinic_name").val(ui.item.facility_name);
                $("#county").val(ui.item.county);
                $("#facility_id").val(ui.item.facility_id);
                $("#facility_code").val(ui.item.facility_code);
            },
            minLength: 1
        };
        $("#clinic_name").autocomplete(ac_config);




        $('.partner_name').change(function () {
            option = $(this).find('option:selected').val();
            //alert(option);
            $.ajax({
                type: "GET",
                url: "database/ajax2.php/?region_partner_id=" + option,
                dataType: "json",
                success: function (data) {
                    // $('#user_clinic_name').val('');
                    $("#clinic_region_name").empty().append('<option value="">-Select-</option>');
                    for (i = 0; i < data.length; i++) {


                        $('#clinic_region_name').append("<option value=" + data[i].region_id + ">" + data[i].region_name + "</option>");
                    }

                    //$('#user_clinic_name').append(toAppend);

                }, error: function () {

                }
            });
        });






    } else if (window.location.href.indexOf("?currentview=partner_registration") > -1) {
        var rowCount = 1;





    } else if (window.location.href.indexOf("?currentview=user_registration") > -1) {


        $('.user_partner_name').change(function () {
            option = $(this).find('option:selected').val();
            //alert(option);
            $.ajax({
                type: "GET",
                url: "database/ajax2.php/?partner_id=" + option,
                dataType: "json",
                success: function (data) {
                    // $('#user_clinic_name').val('');
                    $("#user_clinic_name").empty().append('<option value="">-Select-</option>');
                    for (i = 0; i < data.length; i++) {

                        $('#user_clinic_name').append("<option value=" + data[i].facility_code + ">" + data[i].facility_name + "</option>");
                    }

                    //$('#user_clinic_name').append(toAppend);

                }, error: function () {

                }
            });
        });







    } else if (window.location.href.indexOf("?currentview=hot_spot_registration") > -1) {

        $('#partner_name_hot_spot').change(function () {
            option = $(this).find('option:selected').val();

            $.ajax({
                type: "GET",
                url: "database/ajax2.php/?partner_id=" + option,
                dataType: "json",
                success: function (data) {
                    // $('#user_clinic_name').val('');
                    $("#clinic_name_hot_spot").empty().append('<option value="">-Select-</option>');
                    for (i = 0; i < data.length; i++) {

                        $('#clinic_name_hot_spot').append("<option value=" + data[i].facility_code + ">" + data[i].facility_name + "</option>");
                    }

                    //$('#user_clinic_name').append(toAppend);

                }, error: function () {

                }
            });
        });




    } else if (window.location.href.indexOf("?currentview=edit_hot_spots_name&edit_id=3") > -1) {



        $('#partner_name_hot_spot').change(function () {
            option = $(this).find('option:selected').val();
            alert('fvfgbvdf');
            $.ajax({
                type: "GET",
                url: "database/ajax2.php/?partner_id=" + option,
                dataType: "json",
                success: function (data) {
                    // $('#user_clinic_name').val('');
                    $("#clinic_name_hot_spot").empty().append('<option value="">-Select-</option>');
                    for (i = 0; i < data.length; i++) {

                        $('#clinic_name_hot_spot').append("<option value=" + data[i].facility_code + ">" + data[i].facility_name + "</option>");
                    }

                    //$('#user_clinic_name').append(toAppend);

                }, error: function () {

                }
            });
        });



    } else if (window.location.href.indexOf("?currentview=trial") > -1) {



        //Get
        //var bla = $('#txt_name').val();

        //Set
        // $('#txt_name').val('bla');




        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];




        $('#search_filter_button').click(function (e) {
            e.preventDefault(); // Stop form submission

            var county = $('#filter_county').val();
            var kp_type = $('#filter_kp_type').val();
            var partner = $('#filter_partner').val();
            var date_from = $('#filter_date_from').val();
            var date_to = $('#filter_date_to').val();
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];


            html_tr = '';
            $.ajax({
                type: 'GET',
                url: baseUrl + "/reports/get_report.php?county=" + county + "&kp_type=" + kp_type + "&partner=" + partner + "&date_from=" + date_from + "&date_to=" + date_to + "&partner_name=" + partner,
                dataType: "JSON",
                success: function (data) {
                    var one = 1;
                    for (i = 0; i < data.length; i++) {

                        html_tr += '<tr>\n\
<td>' + one + '</td>\n\
<td>' + data[i].name + '</td>\n\
<td>' + data[i].Abbrv + '</td>\n\\n\
<td>' + data[i].partner_name + '</td>\n\\n\
<td>' + data[i].facility_name + '</td>\n\\n\
<td>' + data[i].county + '</td>\n\\n\
<td>' + data[i].no_kps + '</td>\n\\n\
<td>' + data[i].activity_stamp + '</td>\n\</tr>';
                        one++;
                    }

                    $('#tbody_append').empty();
                    $('#tbody_append').append(html_tr);
                    $("#trial_table_div").css("display", "inline");
                    $("#trial_table_1div").css("display", "none");

                }, error: function (data) {
                    alert(data);
                }
            });
        });







        $('#trial_submit_1').click(function () {

            //get
            $(".div_pharmacy_make_payment").hide("slow");
            var visit_id = $(this).closest('li').find('input[name="visit_id_list"]').val();
            //alert(visit_id);
            var patient_id = $(this).closest('li').find('input[name="patient_id_list"]').val();
            //alert(patient_id);
            html1 = '';
            htmlhead1 = '';
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>cashier/pharmacy_service_payments_details/" + visit_id + "/" + patient_id,
                dataType: "JSON",
                success: function (data) {
                    for (i = 0; i < data.length; i++) {

                        // alert(data[i].stock_id);
                        html1 += '<div>\n\
                        <div class = "form-group">\n\
            \n\
                        <textarea required=""  class = "form-control" style="width:100px" readonly="" name="description[]" id = "description"  >' + data[i].description + '</textarea>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="amount_charged[]" id = "amount_charged" value="' + data[i].amount_charged + '" placeholder = "Amount Charged">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control amount_owed' + data[i].patient_visit_statement_id + '" name="amount_owed[]" readonly="" style="width:80px" id = "amount_owed' + data[i].patient_visit_statement_id + '" value="' + data[i].amount_owed + '" placeholder = "Amount Paid">\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control amount_paid' + data[i].patient_visit_statement_id + '" name="amount_paid[]"  style="width:80px" id = "amount_paid' + data[i].patient_visit_statement_id + '"  placeholder = "Amount Paid">\n\
            </div>\n\
             <div class = "form-group">\n\
            \n\
            <input type = "text" required="" class = "form-control" style="width:80px" readonly="" name="quantity[]" id = "quantity" value="' + data[i].quantity + '" placeholder = "Quantity">\n\
            <input type="hidden" required="" readonly="" name="patient_visit_statement_id[]" id="patient_visit_statement_id" value="' + data[i].patient_visit_statement_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_id[]" id="patient_id" value="' + data[i].patient_id + '">\n\
            <input type="hidden" required="" readonly="" name="visit_id[]" id="visit_id" value="' + data[i].visit_id + '">\n\
            <input type="hidden" required="" readonly="" name="patient_payment_id[]" id="patient_payment_id" value="' + data[i].patient_payment_id + '">\n\
            <input type="hidden" required="" readonly="" name="payment_prescription_tracker[]" id="payment_prescription_tracker" value="' + data[i].prescription_tracker + '">\n\<input type="hidden" required="" readonly="" name="amount_paid_td[]" id="amount_paid_td" value="' + data[i].amount_paid + '">\n\
            \n\
            \n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            \n\
            <select required="" name="payment_method[]" class="form-control" id="payment_method">\n\
            <option value="' + data[i].payment_method + '">' + data[i].payment_method + '<option>\n\
            <option value="Cash">Cash</option>\n\
            <option value="M-Pesa">M-Pesa</option>\n\
            <option value="Airtel Money">Airtel Money</option>\n\
            <option value="Orange Money">Orange Money</option>\n\
            <option value="PDQ">PDQ </option></select>\n\
            </div>\n\
            <div class = "form-group">\n\
            \n\
            <textarea type = "text" style="width:80px" required="" class = "form-control" name="payment_code[]" id = "payment_code" value="' + data[i].payment_code + '" placeholder = "Payment Code">' + data[i].payment_code + '</textarea>\n\
            </div>\n\
                        </div>';



                    }

                    htmlhead1 += '<div>\n\
                        <div class = "form-group">\n\
            <label class = "label label-info" for = "description">Description</label>\n\
            </div>\n\
            <div class = "form-group">\n\
             <label class = "label label-info" for = "amount_charged">Amount Charged</label>\n\
            </div>\n\
            \n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "balance_remaining">Balance Remaining</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "amount_paid">Amount Paid</label>\n\
            </div>\n\
             <div class = "form-group">\n\
            <label class = "label label-info" for = "quantity">Quantity</label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_method">Payment Method </label>\n\
            </div>\n\
            <div class = "form-group">\n\
            <label class = "label label-info" for = "payment_code">Payment Code </label>\n\
            </div>\n\
                        </div>';
                    $('#patient_pharmacy_service_payment_div').empty();
                    $('#patient_pharmacy_service_payment_div').append(htmlhead1);
                    $('#patient_pharmacy_service_payment_div').append(html1);
                },
                error: function (data) {

                }
            });
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>cashier/get_patient_name/" + visit_id + "/" + patient_id,
                dataType: "JSON",
                success: function (response) {
                    $('#pharmacy_service_patient_name').val(response[0].patient_name);
                }
            });
        });









    }
    else if (window.location.href.indexOf("?currentview=estimated_kps_area_wrk") > -1) {



        //Get
        //var bla = $('#txt_name').val();

        //Set
        // $('#txt_name').val('bla');






        $('#search_filter_button').click(function (e) {
            e.preventDefault(); // Stop form submission

            var county = $('#filter_county').val();
            var kp_type = $('#filter_kp_type').val();
            var partner = $('#filter_partner').val();
            var date_from = $('#filter_date_from').val();
            var date_to = $('#filter_date_to').val();
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];


            html_tr = '';
            $.ajax({
                type: 'GET',
                url: baseUrl + "/reports/estimated_kps_area_wrk.php?county=" + county + "&kp_type=" + kp_type + "&date_from=" + date_from + "&date_to=" + date_to,
                dataType: "JSON",
                success: function (data) {
                    var one = 1;
                    for (i = 0; i < data.length; i++) {

                        html_tr += '<tr>\n\
<td>' + one + '</td>\n\
<td>' + data[i].name + '</td>\n\
<td>' + data[i].kp_target + '</td>\n\\n\
<td>' + data[i].kp_type_contacted + '</td>\n\\n\
<td>' + data[i].kp_type_enrolled + '</td>\n\\n\
<td>' + data[i].kp_type_rcving_svrces + '</td>\n\\n\
<td>' + data[i].region_name + '</td>\n\\n\
\n\</tr>';
                        one++;
                    }

                    $('#tbody_append').empty();
                    $('#tbody_append').append(html_tr);
                    $("#estimatd_kps_area_wrk_div_2").css("display", "inline");
                    $("#estimated_kp_area_wrk_div").css("display", "none");

                }, error: function (data) {
                    alert(data);
                }
            });
        });




    }
    else if (window.location.href.indexOf("?currentview=kps_planned_covered") > -1) {
        //Get
        //var bla = $('#txt_name').val();

        //Set
        // $('#txt_name').val('bla');


        $('#search_filter_button').click(function (e) {
            e.preventDefault(); // Stop form submission

            var county = $('#filter_county').val();
            var kp_type = $('#filter_kp_type').val();
            var partner = $('#filter_partner').val();
            var date_from = $('#filter_date_from').val();
            var date_to = $('#filter_date_to').val();
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];


            html_tr = '';
            $.ajax({
                type: 'GET',
                url: baseUrl + "/reports/kps_planned_covered.php?county=" + county + "&kp_type=" + kp_type + "&partner=" + partner + "&date_from=" + date_from + "&date_to=" + date_to + "&partner_name=" + partner,
                dataType: "JSON",
                success: function (data) {
                    var one = 1;
                    for (i = 0; i < data.length; i++) {

                        html_tr += '<tr>\n\
<td>' + one + '</td>\n\
<td>' + data[i].name + '</td>\n\
<td>' + data[i].kp_target + '</td>\n\\n\
<td>' + data[i].no_kp_type_contacted + '</td>\n\\n\
<td>' + data[i].partner_name + '</td>\n\\n\
<td>' + data[i].region_name + '</td>\n\\n\\n\</tr>';
                        one++;
                    }

                    $('#kps_planned_covered_tbody').empty();
                    $('#kps_planned_covered_tbody').append(html_tr);
                    $("#kps_planned_covered_div_2").css("display", "inline");
                    $("#kps_planned_covered_div_1").css("display", "none");

                }, error: function (data) {
                    alert(data);
                }
            });
        });



    }
    else if (window.location.href.indexOf("?currentview=site_kps_operate") > -1) {
        //Get
        //var bla = $('#txt_name').val();

        //Set
        // $('#txt_name').val('bla');


        $('#search_filter_button').click(function (e) {
            e.preventDefault(); // Stop form submission

            var county = $('#filter_county').val();
            var kp_type = $('#filter_kp_type').val();
            var partner = $('#filter_partner').val();
            var date_from = $('#filter_date_from').val();
            var date_to = $('#filter_date_to').val();
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];


            html_tr = '';
            $.ajax({
                type: 'GET',
                url: baseUrl + "/reports/site_kps_operate.php?county=" + county + "&kp_type=" + kp_type + "&partner=" + partner + "&date_from=" + date_from + "&date_to=" + date_to + "&partner_name=" + partner,
                dataType: "JSON",
                success: function (data) {
                    var one = 1;
                    for (i = 0; i < data.length; i++) {

                        html_tr += '<tr>\n\
<td>' + one + '</td>\n\
<td>' + data[i].name + '</td>\n\
<td>' + data[i].abbrv + '</td>\n\\n\
<td>' + data[i].partner_name + '</td>\n\\n\
<td>' + data[i].region_name + '</td>\n\\n\
<td>' + data[i].no_of_sites + '</td>\n\\n\\n\</tr>';
                        one++;
                    }

                    $('#site_kps_operate_tbody').empty();
                    $('#site_kps_operate_tbody').append(html_tr);
                    $("#site_kps_operate_div_2").css("display", "inline");
                    $("#site_kps_operate_div_1").css("display", "none");

                }, error: function (data) {
                    alert(data);
                }
            });
        });



    }
    else if (window.location.href.indexOf("?currentview=site_intervtn_area") > -1) {
        //Get
        //var bla = $('#txt_name').val();

        //Set
        // $('#txt_name').val('bla');


        $('#search_filter_button').click(function (e) {
            e.preventDefault(); // Stop form submission

            var county = $('#filter_county').val();
            var kp_type = $('#filter_kp_type').val();
            var partner = $('#filter_partner').val();
            var date_from = $('#filter_date_from').val();
            var date_to = $('#filter_date_to').val();
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];


            html_tr = '';
            $.ajax({
                type: 'GET',
                url: baseUrl + "/reports/site_intervtn_area.php?county=" + county + "&kp_type=" + kp_type + "&partner=" + partner + "&date_from=" + date_from + "&date_to=" + date_to + "&partner_name=" + partner,
                dataType: "JSON",
                success: function (data) {
                    var one = 1;
                    for (i = 0; i < data.length; i++) {

                        html_tr += '<tr>\n\
<td>' + one + '</td>\n\
<td>' + data[i].name + '</td>\n\
<td>' + data[i].abbrv + '</td>\n\\n\
<td>' + data[i].partner_name + '</td>\n\\n\
<td>' + data[i].region_name + '</td>\n\\n\
<td>' + data[i].no_of_sites + '</td>\n\\n\\n\</tr>';
                        one++;
                    }

                    $('#site_intervation_area_tbody_2').empty();
                    $('#site_intervation_area_tbody_2').append(html_tr);
                    $("#site_intertn_area_div_2").css("display", "inline");
                    $("#site_intertn_area_div_1").css("display", "none");

                }, error: function (data) {
                    alert(data);
                }
            });
        });



    }
    else if (window.location.href.indexOf("?currentview=kps_contacted") > -1) {
        //Get
        //var bla = $('#txt_name').val();

        //Set
        // $('#txt_name').val('bla');


        $('#search_filter_button').click(function (e) {
            e.preventDefault(); // Stop form submission

            var county = $('#filter_county').val();
            var kp_type = $('#filter_kp_type').val();
            var partner = $('#filter_partner').val();
            var date_from = $('#filter_date_from').val();
            var date_to = $('#filter_date_to').val();
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];


            html_tr = '';
            $.ajax({
                type: 'GET',
                url: baseUrl + "/reports/kps_contacted.php?county=" + county + "&kp_type=" + kp_type + "&partner=" + partner + "&date_from=" + date_from + "&date_to=" + date_to + "&partner_name=" + partner,
                dataType: "JSON",
                success: function (data) {
                    var one = 1;
                    for (i = 0; i < data.length; i++) {

                        html_tr += '<tr>\n\
<td>' + one + '</td>\n\
<td>' + data[i].name + '</td>\n\
<td>' + data[i].Abbrv + '</td>\n\\n\
<td>' + data[i].partner_name + '</td>\n\\n\
<td>' + data[i].facility_name + '</td>\n\\n\
<td>' + data[i].county + '</td>\n\\n\\n\\n\
<td>' + data[i].no_kps + '</td>\n\
<td>' + data[i].activity_stamp + '</td></tr>';
                        one++;
                    }

                    $('#kps_contacted_tbody_2').empty();
                    $('#kps_contacted_tbody_2').append(html_tr);
                    $("#kps_contacted_div_2").css("display", "inline");
                    $("#kps_contacted_div_1").css("display", "none");

                }, error: function (data) {
                    alert(data);
                }
            });
        });



    }
    else if (window.location.href.indexOf("?currentview=new_kps_contacted") > -1) {
        //Get
        //var bla = $('#txt_name').val();

        //Set
        // $('#txt_name').val('bla');


        $('#search_filter_button').click(function (e) {
            e.preventDefault(); // Stop form submission

            var county = $('#filter_county').val();
            var kp_type = $('#filter_kp_type').val();
            var partner = $('#filter_partner').val();
            var date_from = $('#filter_date_from').val();
            var date_to = $('#filter_date_to').val();
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];


            html_tr = '';
            $.ajax({
                type: 'GET',
                url: baseUrl + "/reports/new_kps_contacted.php?county=" + county + "&kp_type=" + kp_type + "&partner=" + partner + "&date_from=" + date_from + "&date_to=" + date_to + "&partner_name=" + partner,
                dataType: "JSON",
                success: function (data) {
                    var one = 1;
                    for (i = 0; i < data.length; i++) {

                        html_tr += '<tr>\n\
<td>' + one + '</td>\n\
<td>' + data[i].name + '</td>\n\
<td>' + data[i].Abbrv + '</td>\n\\n\
<td>' + data[i].partner_name + '</td>\n\\n\
<td>' + data[i].facility_name + '</td>\n\\n\
<td>' + data[i].county + '</td>\n\\n\\n\\n\
<td>' + data[i].no_kps + '</td>\n\
<td>' + data[i].activity_stamp + '</td></tr>';
                        one++;
                    }

                    $('#new_kps_contacted_tbody_2').empty();
                    $('#new_kps_contacted_tbody_2').append(html_tr);
                    $("#new_kps_contacted_div_2").css("display", "inline");
                    $("#new_kps_contacted_div_1").css("display", "none");

                }, error: function (data) {
                    alert(data);
                }
            });
        });



    }
    else if (window.location.href.indexOf("?currentview=cnsllng_svrcs_uptke") > -1) {
        //Get
        //var bla = $('#txt_name').val();

        //Set
        // $('#txt_name').val('bla');


        $('#search_filter_button').click(function (e) {
            e.preventDefault(); // Stop form submission

            var county = $('#filter_county').val();
            var kp_type = $('#filter_kp_type').val();
            var partner = $('#filter_partner').val();
            var date_from = $('#filter_date_from').val();
            var date_to = $('#filter_date_to').val();
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];


            html_tr = '';
            $.ajax({
                type: 'GET',
                url: baseUrl + "/reports/cnsllng_svrcs_uptke.php?county=" + county + "&kp_type=" + kp_type + "&partner=" + partner + "&date_from=" + date_from + "&date_to=" + date_to + "&partner_name=" + partner,
                dataType: "JSON",
                success: function (data) {
                    var one = 1;
                    for (i = 0; i < data.length; i++) {

                        html_tr += '<tr>\n\
<td>' + one + '</td>\n\
<td>' + data[i].name + '</td>\n\
<td>' + data[i].abbrv + '</td>\n\\n\
<td>' + data[i].partner_name + '</td>\n\\n\
<td>' + data[i].facility_name + '</td>\n\\n\
<td>' + data[i].region_name + '</td>\n\\n\\n\\n\
<td>' + data[i].no_kps_counselled + '</td>\n\
<td>' + data[i].activity_time_stamp + '</td></tr>';
                        one++;
                    }

                    $('#cnsllng_svrcs_uptke_tbody_2').empty();
                    $('#cnsllng_svrcs_uptke_tbody_2').append(html_tr);
                    $("#cnsllng_svrcs_uptke_div_2").css("display", "inline");
                    $("#cnsllng_svrcs_uptke_div_1").css("display", "none");

                }, error: function (data) {
                    alert(data);
                }
            });
        });



    }
        else if (window.location.href.indexOf("?currentview=kps_rcvd_pep_srvcs") > -1) {
        //Get
        //var bla = $('#txt_name').val();

        //Set
        // $('#txt_name').val('bla');


        $('#search_filter_button').click(function (e) {
            e.preventDefault(); // Stop form submission

            var county = $('#filter_county').val();
            var kp_type = $('#filter_kp_type').val();
            var partner = $('#filter_partner').val();
            var date_from = $('#filter_date_from').val();
            var date_to = $('#filter_date_to').val();
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];


            html_tr = '';
            $.ajax({
                type: 'GET',
                url: baseUrl + "/reports/cnsllng_svrcs_uptke.php?county=" + county + "&kp_type=" + kp_type + "&partner=" + partner + "&date_from=" + date_from + "&date_to=" + date_to + "&partner_name=" + partner,
                dataType: "JSON",
                success: function (data) {
                    var one = 1;
                    for (i = 0; i < data.length; i++) {

                        html_tr += '<tr>\n\
<td>' + one + '</td>\n\
<td>' + data[i].name + '</td>\n\
<td>' + data[i].abbrv + '</td>\n\\n\
<td>' + data[i].partner_name + '</td>\n\\n\
<td>' + data[i].facility_name + '</td>\n\\n\
<td>' + data[i].region_name + '</td>\n\\n\\n\\n\
<td>' + data[i].no_kps_counselled + '</td>\n\
<td>' + data[i].activity_time_stamp + '</td></tr>';
                        one++;
                    }

                    $('#cnsllng_svrcs_uptke_tbody_2').empty();
                    $('#cnsllng_svrcs_uptke_tbody_2').append(html_tr);
                    $("#cnsllng_svrcs_uptke_div_2").css("display", "inline");
                    $("#cnsllng_svrcs_uptke_div_1").css("display", "none");

                }, error: function (data) {
                    alert(data);
                }
            });
        });



    }
});



