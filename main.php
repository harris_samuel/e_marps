<?php
session_start();
include 'database/websrvc.php';
include 'database/db_connect.php';
include'config.php';
$websrvc = new Websrvc();

$uid = $_SESSION['uid'];
$sess_partner_id = $_SESSION['sess_partner_id'];
$sess_role_id = $_SESSION['sess_role_id'];
$sess_clinic_id = $_SESSION['sess_clinic_id'];

// echo '' . print_r($_SESSION) . '';

if (!$websrvc->get_session()) {
    header("location:login.php");
}

if (isset($_GET['q'])) {
    $websrvc->user_logout();
    header("location:login.php");
}
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
?>
<!doctype html>
<html>
    <head>
        <title>e-MARPS</title>

        <meta charset="utf-8">

        <link rel="shortcut icon" href="<?php echo $system_path ?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo $system_path ?>favicon.ico" type="image/x-icon">

        <script src="js/jquery.min.js"></script>
        <script src="js/tinybox.js"></script>
        <script src="js/conditional.js"></script>
        <script src="js/updatelog.js"></script>
        <script src="js/responsivevoice.js"></script>

        <link rel="stylesheet" type="text/css" href="tipr/tipr.css">
        <script type="text/javascript" src="tipr/tipr.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
        <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

        <link href="animate.min.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" href="fieldstyles.css" />

        <script>
            function loadingindicator_start() {
                document.getElementById('loadingindicator').style.display = 'block';
                document.getElementById('loadingindicator').className = 'loadingindicator animated fadeIn';
            }
            function loadingindicator_stop() {
                document.getElementById('loadingindicator').className = 'loadingindicator animated fadeOut';
                setTimeout(function () {
                    document.getElementById('loadingindicator').style.display = 'none';
                }, 600);
            }

            function checkfornewnotifications() {

                var usersettings_notifications = localStorage.getItem('usersettings_notifications');

                if (usersettings_notifications == 'Yes') {
                    $("#newupdate").load("<?php echo $system_path ?>database/audit_trail/notificationstatus.php?task=check&userid=<?php echo $uid ?>", function () {
                                });
                                var newupdate = document.getElementById('newupdate').innerHTML;
                                // alert(newupdate);
                                if (newupdate == "Yes") {
                                    document.getElementById('notificationsiframe').src = "/emarps/notifications.php";
                                    $("#newupdate").load("/emarps/database/audit_trail/notificationstatus.php?task=reset&userid=<?php echo $uid ?>", function () {
                                    });
                                }
                                else {
                                }



                            }
                            else {

                            }
                        }

                        $(document).ready(function () {

                            storeuserdata();

                            function storeuserdata() {
                                localStorage.setItem('sess_role_id', '<?php echo $sess_role_id ?>');
                                localStorage.setItem('sess_partner_id', '<?php echo $sess_partner_id ?>');
                                localStorage.setItem('sess_clinic_id', '<?php echo $sess_clinic_id ?>');
                                localStorage.setItem('currentuserid', '<?php echo $uid ?>');
                            }

                            Notification.requestPermission(function (status) {
                            });

                            setInterval(function () {
                                checkfornewnotifications();
                            }, 3000);

                            // alert(currentclientnameshowinbar);

                            document.getElementById('currentclientname').innerHTML = localStorage.getItem('currentclientname');
                            document.getElementById('currentuuid').innerHTML = localStorage.getItem('currentuuid');

                            welcomeaudio = localStorage.getItem('welcomeaudio');
                            if (welcomeaudio == 'Yes') {
                                return;
                            }
                            else {
                                localStorage.setItem('t2s', 'Welcome, loading your personal profile.');
                                speak();
                                localStorage.setItem('welcomeaudio', 'Yes');
                            }

                        });

        </script>

        <script type="application/javascript">
            function open_navigation(){
            document.getElementById('navigation').style.display = 'none';
            setTimeout(function(){document.getElementById('closenavigation').style.display = 'block';}, 1000);
            document.getElementById('opennavigation').style.display = 'block';
            document.getElementById('opennavigation').className = 'animated fadeInDown';
            document.getElementById('contentloader').className = 'blur';
            }
            function close_navigation(){
            setTimeout(function(){ document.getElementById('navigation').style.display = 'block'; }, 1000);
            setTimeout(function(){ document.getElementById('opennavigation').style.display = 'none'; }, 1000);
            document.getElementById('closenavigation').style.display = 'none';
            document.getElementById('opennavigation').className = 'animated fadeOutUp';
            document.getElementById('contentloader').className = 'noblur';
            }
            function open_reportsmenu(){
            document.getElementById('reportsmenu').style.display = 'none';
            setTimeout(function(){document.getElementById('closereportsmenu').style.display = 'block';}, 1000);
            document.getElementById('openreports').style.display = 'block';
            document.getElementById('openreports').className = 'animated fadeInDown';
            document.getElementById('contentloader').className = 'blur';
            }
            function close_reportsmenu(){
            setTimeout(function(){ document.getElementById('reportsmenu').style.display = 'block'; }, 1000);
            setTimeout(function(){ document.getElementById('openreports').style.display = 'none'; }, 1000);
            document.getElementById('closereportsmenu').style.display = 'none';
            document.getElementById('openreports').className = 'animated fadeOutUp';
            document.getElementById('contentloader').className = 'noblur';
            }
            function open_registersmenu(){
            document.getElementById('registersmenu').style.display = 'none';
            setTimeout(function(){document.getElementById('closeregistersmenu').style.display = 'block';}, 1000);
            document.getElementById('openregisters').style.display = 'block';
            document.getElementById('openregisters').className = 'animated fadeInDown';
            document.getElementById('contentloader').className = 'blur';
            }
            function close_registersmenu(){
            setTimeout(function(){ document.getElementById('registersmenu').style.display = 'block'; }, 1000);
            setTimeout(function(){ document.getElementById('openregisters').style.display = 'none'; }, 1000);
            document.getElementById('closeregistersmenu').style.display = 'none';
            document.getElementById('openregisters').className = 'animated fadeOutUp';
            document.getElementById('contentloader').className = 'noblur';
            }
            function sidebar1(){
            document.getElementById('sidebar1').className = 'active';
            }
            function sidebar2(){
            document.getElementById('sidebar2').className = 'active';
            }
            function sidebar3(){
            document.getElementById('sidebar3').className = 'active';
            }
            function sidebar4(){
            document.getElementById('sidebar4').className = 'active';
            }
            function sidebar5(){
            document.getElementById('sidebar5').className = 'active';
            }
            function sidebar6(){
            document.getElementById('sidebar6').className = 'active';
            }
            function sidebar7(){
            document.getElementById('sidebar7').className = 'active';
            }
            function sidebarclear(){
            document.getElementById('sidebar1').className = '';
            document.getElementById('sidebar2').className = '';
            document.getElementById('sidebar3').className = '';
            document.getElementById('sidebar4').className = '';
            document.getElementById('sidebar5').className = '';
            document.getElementById('sidebar6').className = '';
            document.getElementById('sidebar7').className = '';
            }
            function layer_kpbookactiveclient() {
            document.getElementById('layer_kpbookactiveclient').className = 'animated layerwindow fadeIn';
            document.getElementById('layer_kpbookactiveclient').style.display = 'block';
            }
            function layer_kpbookactiveclient_close() {
            document.getElementById('layer_kpbookactiveclient').className = 'animated layerwindow fadeOut';
            setTimeout(function(){ document.getElementById('layer_kpbookactiveclient').style.display = 'none'; }, 600);
            }


            function layer_kpsetinactiveclient() {
            document.getElementById('layer_kpsetinactiveclient').className = 'animated layerwindow fadeIn';
            document.getElementById('layer_kpsetinactiveclient').style.display = 'block';
            }
            function layer_kpsetinactiveclient_close() {
            document.getElementById('layer_kpsetinactiveclient').className = 'animated layerwindow fadeOut';
            setTimeout(function(){ document.getElementById('layer_kpsetinactiveclient').style.display = 'none'; }, 600);
            setTimeout(function(){ 
            document.location.href = 'main.php';
            }, 1000);
            }
        </script>

    </head>

    <body>

        <?php include'loadingindicator.php'; ?>

        <?php include'views/edit_active_kp.php'; ?>
        <?php include'views/set_inactive_kp.php'; ?>

        <?php include'systembar.php'; ?>

        <div id="contentloader">

            <?php include'sidebar.php'; ?>   

            <?php
// error_reporting(E_ALL ^ E_NOTICE);

            $currentview = $_REQUEST['currentview'];

            if (!empty($currentview)) {
                include "views/" . $currentview . ".php";
            } else {
                include "views/start.php";
            }
            ?>
        </div>

        <div id="newupdate" style="display:none;"></div>

        <iframe id="notificationsiframe" style="display:none;" src=""></iframe>

        <script>
            $(document).ready(function () {
                $('.tip').tipr({
                    'speed': 300,
                    'mode': 'top'
                });
            });
        </script>

        <div id="text2speech" style="display:none;">
            <textarea id="text2speecharea" cols="45" rows="3">Text not defined</textarea>

            <select id="voiceselection"></select> 

            <input 
                onclick="responsiveVoice.speak($('#text2speecharea').val(), $('#voiceselection').val());" 
                type="button" 
                value="Play" 
                />

            <script>
                function speak() {
                    var t2son = localStorage.getItem('t2son');

                    if (t2son == 'Yes') {

                        var t2s = localStorage.getItem('t2s');
                        document.getElementById('text2speecharea').value = t2s;
                        responsiveVoice.speak($('#text2speecharea').val(), $('#voiceselection').val());
                    }

                    //Populate voice selection dropdown
                    var voicelist = responsiveVoice.getVoices();

                    var vselect = $("#voiceselection");

                    $.each(voicelist, function () {
                        vselect.append($("<option />").val(this.name).text(this.name));
                    });

                }
            </script>
        </div>

    </body>
</html>