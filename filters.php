<style type="text/css">
#searchfilters {
	background-color: #FFFFFF;
	width: 700px;
	box-shadow: 0 0 15px 0px rgba(0,0,0,0.2);
	margin-top: 7px;
	margin-right: 20px;
	padding-top: 10px;
	padding-right: 10px;
	padding-left: 10px;
}
#searchfilters select {
	margin-right: 15px;
	margin-bottom: 15px;
}
.select_filters {
	transition: all 0.6s;
	background-size: 100% 100%;
	padding-top: 3px;
	padding-right: 5px;
	padding-bottom: 3px;
	padding-left: 5px;
	margin-top: 10px;
	margin-bottom: 10px;
	margin-left: 10px;
	border: 1px solid #999999;
}
</style>

<div id="searchfilters">
    <select class="select_filters">
    <option>
    Implementing Partner
    </option>
    </select>
    
    <select class="select_with_label">
    <option>
    County/Sub County
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    Site
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    Year
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    Month
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    Quarterly
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    KP/Type
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    Bridge Population
    </option>
    </select>
    
    <select class="select_filters">
    <option>
   Point of Collection
    </option>
    </select>
    
</div>


