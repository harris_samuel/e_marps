<style type="text/css">
</style>
<div id="titlebar"><div id="titlebartext">Registration/Revisit</div></div>

<script>
sidebar1();
</script>

<div id="forms">

<?php include'kp_registration_id.php';?>

</div>

<style>
#layer_kpsearch input[type="radio"] {
	display: none;
}
</style>

<div id="layer_kpsearch" class="layerwindow animated fadeIn">
                    <div class="inlayerwindow_large animated fadeInUp">
                        <input class="layerwindowclose" type="button" value="Close" onclick="layer_kpsearch_close();">
                        <div class="inlayerwindowscrollarea">
                        
                        <h2>KP Client Search</h2>
                        
                        
                        <div style="width:50%; float:left;">
                        <form>
                       
                        
<div style="clear:both"></div> 

<input class="fieldstyle"  id="txt_client_name" name="txt_client_name" type="text"  placeholder="Name">

<div style="clear:both"></div> 

<input name="radio_andor1" id="radio_andor1a" type="radio" value="and" /><input name="radio_andor1" type="radio" id="radio_andor1b" value="or" checked="checked" />

<div style="clear:both"></div> 

<label>Gender </label>
                <select onfocus="document.getElementById('radio_andor1a').checked = true;" class="select_with_label" id="select_gender" name="select_gender">
                    <option value="M">Male</option>
                    <option value="F" selected="selected">Female</option>
                    <option value="T">Trans-Gender</option>
                </select>
                
<div style="clear:both"></div>

<input name="radio_andor2" id="radio_andor2a" type="radio" value="and" /><input name="radio_andor2" type="radio" value="or" checked="checked" />

<div style="clear:both"></div> 
                     
<label>Date of birth</label>
<input onfocus="document.getElementById('radio_andor2a').checked = true;" class="fieldstyledate" id="txt_client_date_of_birth" name="txt_client_date_of_birth" type="date"  placeholder="Client Date of Birth">

<div style="clear:both"></div>

<input name="radio_andor3" id="radio_andor3a" type="radio" value="and" /><input name="radio_andor3" type="radio" value="or" checked="checked" />

<div style="clear:both"></div> 

<input onfocus="document.getElementById('radio_andor3a').checked = true;" class="fieldstyle" id="txt_client_mobile_number" name="txt_client_mobile_number" type="text"  placeholder="Client Mobile Number">

<div style="clear:both"></div>

<input name="radio_andor4" id="radio_andor4a" type="radio" value="and" /><input name="radio_andor4" type="radio" value="or" checked="checked" />

<div style="clear:both"></div> 

<input style="display:none"  onfocus="document.getElementById('radio_andor4a').checked = true;" required class="fieldstyle" id="txt_client_residential_area" name="txt_client_residential_area" type="text"  placeholder="Client Residential Area">

<div style="clear:both"></div>

<input name="radio_andor5" id="radio_andor5a" type="radio" value="and" /><input name="radio_andor5" type="radio" value="or" checked="checked" />

<div style="clear:both"></div> 

<input style="display:none" onfocus="document.getElementById('radio_andor5a').checked = true;" required class="fieldstyle" id="txt_client_residential_home" name="txt_client_residential_home" type="text"  placeholder="Client Home Area">

<br><br>

<input style="float:left; width:150px;" onclick="searchkpoptions();" class="small_button" type="button" value="Search KP Client">

<input name="Reset" type="reset" class="small_button" style="float:left; margin-left:10px; width:60px;" value="Reset">

<div style="clear:both"></div> 
</form>
</div>

<div style="width:50%; float:right;">

<div id="layer_kpsearch_kpuuidsearchoutput">
</div>

<div id="layer_kpsearch_kpuuidoutput">
<div style="height:15px;"></div>
 <div id="uuid_box">
                        UUID: <span id="layer_kpsearch_uuid_view">Unknown</span>
                        </div>


<div style="clear:both"></div>

<div id="layer_kpsearch_search_results"><span id="layer_kpsearch_search_results_ajax">No KP Client data to display.</span>
</div>

</div>

<input id="btn_register_new_kp_client" style="float:right; margin-top:10px; margin-right:2px; width:334px; display:none;" onclick="registernewkpclient();" class="small_button" type="button" value="No Match? Register new KP Client">

</div>
</div>
</div>



</div>

<div id="layer_kpbookforservice" class="layerwindow animated fadeIn">
	<div style="margin-top:200px !important;" class="inlayerwindow animated bounceIn">
    <div class="layer_close_x" onclick="layer_kpbookforservice_close()">x</div>
    
<script>


function updatenewnotificationstatusaddnew () {
$.get( "database/audit_trail/notificationstatus.php?task=addnew", function( data ) {
  document.location.href = '?currentview='+gotoloc+'';
});
}
function updatenewnotificationstatus () {
var currentuuid = localStorage.getItem('currentuuid');
var currentclientname = localStorage.getItem('currentclientname');
	
$.get( "database/audit_trail/updatelog.php?currentuser=<?php echo $uid ?>&useraction=Client "+currentclientname+" was added to "+currentservice+"", function( data ) {
  updatenewnotificationstatusaddnew();
});
}




function addtocounselling() {
var currentdate = new Date(); 
var currenttime = " "
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + "";
var currentuuid = localStorage.getItem('currentuuid');
var currentclientname = localStorage.getItem('currentclientname');

var clinic_id = localStorage.getItem('sess_clinic_id');
var partner_id = localStorage.getItem('sess_partner_id');
var currentclientgender = localStorage.getItem('currentclientgender');

$.get('/marps/database/kp_active/update.php?uuid='+currentuuid+'&clinic_id='+clinic_id+'&partner_id='+partner_id+'&currentclientgender='+currentclientgender+'&name='+currentclientname+'&entrytime='+currenttime+'&status=Counselling', function(data) {
gotoloc = 'counselling';
currentservice = 'Counselling';
updatenewnotificationstatus();
});
}

function addtoconsultation() {
var currentdate = new Date(); 
var currenttime = " "
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + "";
var currentuuid = localStorage.getItem('currentuuid');
var currentclientname = localStorage.getItem('currentclientname');

var clinic_id = localStorage.getItem('sess_clinic_id');
var partner_id = localStorage.getItem('sess_partner_id');
var currentclientgender = localStorage.getItem('currentclientgender');

$.get('/marps/database/kp_active/update.php?uuid='+currentuuid+'&clinic_id='+clinic_id+'&partner_id='+partner_id+'&currentclientgender='+currentclientgender+'&name='+currentclientname+'&entrytime='+currenttime+'&status=Consultation', function(data) {
gotoloc = 'consultation';
currentservice = 'Consultation';
updatenewnotificationstatus();
});
}

function addtocareandtreatment() {
var currentdate = new Date(); 
var currenttime = " "
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + "";
var currentuuid = localStorage.getItem('currentuuid');
var currentclientname = localStorage.getItem('currentclientname');

var clinic_id = localStorage.getItem('sess_clinic_id');
var partner_id = localStorage.getItem('sess_partner_id');
var currentclientgender = localStorage.getItem('currentclientgender');

$.get('/marps/database/kp_active/update.php?uuid='+currentuuid+'&clinic_id='+clinic_id+'&partner_id='+partner_id+'&currentclientgender='+currentclientgender+'&name='+currentclientname+'&entrytime='+currenttime+'&status=Care and Treatment', function(data) {
gotoloc = 'careandtreatment';
currentservice = 'Care and Treatment';
updatenewnotificationstatus();
});
}

function addtopharmacy() {
var currentdate = new Date(); 
var currenttime = " "
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + "";
var currentuuid = localStorage.getItem('currentuuid');
var currentclientname = localStorage.getItem('currentclientname');

var clinic_id = localStorage.getItem('sess_clinic_id');
var partner_id = localStorage.getItem('sess_partner_id');
var currentclientgender = localStorage.getItem('currentclientgender');

$.get('/marps/database/kp_active/update.php?uuid='+currentuuid+'&clinic_id='+clinic_id+'&partner_id='+partner_id+'&currentclientgender='+currentclientgender+'&name='+currentclientname+'&entrytime='+currenttime+'&status=Pharmacy', function(data) {
gotoloc = 'pharmacy';
currentservice = 'Pharmacy';
updatenewnotificationstatus();
});
}

function addtowaiting() {
var currentdate = new Date(); 
var currenttime = " "
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + "";
var currentuuid = localStorage.getItem('currentuuid');
var currentclientname = localStorage.getItem('currentclientname');

var clinic_id = localStorage.getItem('sess_clinic_id');
var partner_id = localStorage.getItem('sess_partner_id');
var currentclientgender = localStorage.getItem('currentclientgender');

$.get('/marps/database/kp_active/update.php?uuid='+currentuuid+'&clinic_id='+clinic_id+'&partner_id='+partner_id+'&currentclientgender='+currentclientgender+'&name='+currentclientname+'&entrytime='+currenttime+'&status=Pharmacy', function(data) {
gotoloc = 'waiting';
currentservice = 'Waiting';
updatenewnotificationstatus();
});
}


</script>
    <h2>Book for Service</h2>
 <table border="0" align="center" cellpadding="5">
  <tr>
    <td><input onClick="addtocounselling();" class="small_button" type="button" value="Counselling"><div class="waiting_button"></div></td>
  </tr></table>
    
    <div id="layer_kpbookforservice_more">▼ Other Options</div>
    <div id="layer_kpbookforservice_more_buttons" style="display:none">
    <center>
<br>
<input onClick="addtoconsultation();" class="small_button" type="button" value="Consultation"><div class="waiting_button"></div>
<br><br>
<input class="small_button" type="button" value="Care and Treatment" onClick="addtocareandtreatment();"><div class="waiting_button"></div>
<br><br>
<input class="small_button" type="button" value="Pharmacy" onClick="addtopharmacy();"><div class="waiting_button"></div>
</center>
  </div>
  </div>
</div>
                
<script>
scancardreturn = localStorage.getItem('scancardreturn');
// alert (scancardreturn);
if (scancardreturn == 1) {
var uuidresult = localStorage.getItem('uuid_entry');
document.getElementById('layer_kpsearch_uuid_view').innerHTML = localStorage.getItem('uuid_entry');
layer_kpsearch();
// alert(uuidresult);
$( "#layer_kpsearch_search_results_ajax" ).load( "/marps/database/registration_revisit.php?uuid="+uuidresult+"" );
document.getElementById('btn_bookforservice').style.display = 'block';
}
else {
	
}

function searchkpoptions() {
	document.getElementById('layer_kpsearch_kpuuidoutput').style.display = 'none';
	document.getElementById('btn_register_new_kp_client').style.display = 'block';
	document.getElementById('layer_kpsearch_kpuuidsearchoutput').style.display = 'block';
	
	var searchkpoptions_name = document.getElementById('txt_client_name').value;
	var searchkpoptions_gender = document.getElementById('select_gender').value;
	var searchkpoptions_date_of_birth = document.getElementById('txt_client_date_of_birth').value;
	var searchkpoptions_client_mobile_number = document.getElementById('txt_client_mobile_number').value;
		var searchkpoptions_client_residential_area = document.getElementById('txt_client_residential_area').value;

	var radio_andor1 = $("input[name=radio_andor1]:checked").val();
	var radio_andor2 = $("input[name=radio_andor2]:checked").val();
	var radio_andor3 = $("input[name=radio_andor3]:checked").val();
	var radio_andor4 = $("input[name=radio_andor4]:checked").val();
	var radio_andor5 = $("input[name=radio_andor5]:checked").val();
	
	$( "#layer_kpsearch_kpuuidsearchoutput" ).load( "/marps/database/registration_revisit_kpsearch.php?radio_andor1="+radio_andor1+"&radio_andor2="+radio_andor2+"&radio_andor3="+radio_andor3+"&radio_andor4="+radio_andor4+"&radio_andor5="+radio_andor5+"&name="+searchkpoptions_name+"&gender="+searchkpoptions_gender+"" );
}

$( "#layer_kpbookforservice_more" ).click(function() {
  $( "#layer_kpbookforservice_more_buttons" ).slideToggle( "slow", function() {
    // Animation complete.
  });
});

</script>