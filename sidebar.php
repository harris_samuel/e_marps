<style type="text/css">
    #sidebar1 {
        background-image: url(images/icons/LOGOS/Actions-user-group-new-icon.png);
        background-repeat: no-repeat;
        background-position: 5px center;
        background-size: auto 80%;
    }
    #sidebar3 {
        background-image: url(images/icons/LOGOS/counselling-icon-txt.png);
        background-repeat: no-repeat;
        background-position: 5px center;
        background-size: auto 80%;
    }
    #sidebar4 {
        background-image: url(images/icons/LOGOS/doctor-man-icon.png);
        background-repeat: no-repeat;
        background-position: 5px center;
        background-size: auto 80%;
    }
    #sidebar5 {
        background-image: url(images/icons/LOGOS/caretremnt.png);
        background-repeat: no-repeat;
        background-position: 5px center;
        background-size: auto 80%;
    }
    .activeclientbox {
        background-color: #EBEBEB;
        padding: 5px;
        margin-top: 10px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
    }
    .activeclientbox .activeclientfield {
        font-size: 12px;
    }
    .activeclientbutton {
        color: #000;
        text-decoration: none;
        background-color: #FFF;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #0070C0;
        font-size: 12px;
        width: 120px;
        margin-top: 5px;
        margin-right: auto;
        margin-bottom: 5px;
        margin-left: auto;
        cursor: pointer;
    }
    .activeclientbutton:hover, .activeclientremove:hover {
        color: #FFF;
        background-color: #0070C0;
    }
    .activeclientremove {
        font-size: 13px;
        background-color: #FFF;
        float: right;
        margin-top: 2px;
        margin-right: 2px;
        border: 1px solid #0070C0;
        padding-top: 0px;
        padding-right: 5px;
        padding-bottom: 0px;
        padding-left: 5px;
        cursor: pointer;
    }
    #sidebar6 {
        background-image: url(images/icons/LOGOS/Actions-user-group-new-icon.png);
        background-repeat: no-repeat;
        background-position: 5px center;
        background-size: auto 80%;
    }
    #sidebar7 {
        background-image: url(images/icons/LOGOS/phramacy.png);
        background-repeat: no-repeat;
        background-position: 5px center;
        background-size: auto 80%;
    }
    #activeclient {
        width: 177px;
        position: absolute;
        left: 10px;
        top: 357px;
        z-index: 100;
    }
    #activeclientlist {
        height: 400px;
        overflow: auto;
    }
    #activeclienttitle {
        color: #FFF;
        background-color: #0070C0;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
    }
</style>
<div id="sidebar">

<?php

?>
     
    <?php
    $user_id = $_SESSION['uid'];
    //echo 'User id is .....:'.$user_id.'<br>';
    $query = "select * from user_permission_list where user_id='$user_id' and location='sidebar' and permissions_status='Active'   ORDER BY level ASC";
    if ($result = mysqli_query($link, $query)) {
        ?>
        <?php
        while ($idresult = mysqli_fetch_row($result)) {
            $code = $idresult[6];
            echo $code;
        }
        ?>

    <?php
}
?>



<!--<div id="sidebar1" onClick="window.location.href = '?currentview=kp_registration';">Registration/Revisit</div>

<div id="sidebar3" onClick="window.location.href = '?currentview=counselling';">Counselling<br>(MOH901,903,915,920)</div>
<div id="sidebar4" onClick="window.location.href = '?currentview=consultation';">Consultation<br>(MOH904)</div>
<div id="sidebar5" onClick="window.location.href = '?currentview=careandtreatment';">Care & Treatment<br>(MOH902)</div>
<div id="sidebar6" style="display:none;" onClick="window.location.href = '?currentview=pmtct';">PMTCT</div>
<div id="sidebar7" onClick="window.location.href = '?currentview=pharmacy';">Pharmacy</div>-->

</div>

<script>
    $(document).ready(function () {
		
		var clinic_id = localStorage.getItem('sess_clinic_id');
		var partner_id = localStorage.getItem('sess_partner_id');

        setInterval(function () {
            $("#activeclientlist").load("database/kp_active/load_active_kp.php?partner_id="+partner_id+"&clinic_id="+clinic_id+"");
        }, 1000);
    });
    function activeclientremove() {
        var activeclientremove = localStorage.getItem('activeclientremove');
		
		layer_kpsetinactiveclient();

       // $.get('/emarps/database/kp_active/remove.php?id=' + activeclientremove + '', function (data) {
          //  alert('Client removed');
       // });

    }
    function openclienttransfer() {
        layer_kpbookactiveclient();
    }
</script>



<div id="activeclient">

<div class="small_button" style="width:175px; margin-bottom:12px;" onclick="document.location.href='?currentview=waiting';">Waiting Room</div>

    <div id="activeclienttitle">Active Clients</div>

    <div id="activeclientlist">
        <?php include'database/kp_active/load_active_kp.php'; ?>
    </div>

</div>