<?php

class ADMIN {

    public function __construct() {
        
    }

    /* Database Connection */

    public function db_connection() {
        $host_name = "localhost";
        $database = "db565263480";
        $user_name = "dbo565263480";
        $password = "emarps2015!";
        $link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        } else {
            return $link;
        }
    }

    // function for create
    // function for read
    public function read() {
        $conn = $this->db_connection();

        $clinic_id = $_SESSION['sess_clinic_id'];
        $partner_id = $_SESSION['sess_partner_id'];
        if (empty($clinic_id) and empty($partner_id)) {
            return mysqli_query($conn, "select users.user_id,users.title,users.user_name,users.status,users.email"
                    . ",users.phone_no,roles.role_id,roles.role_name from users inner join roles on users.role_id = roles.role_id ORDER BY users.user_id ASC");
        } elseif (empty($clinic_id) and ! empty($partner_id)) {
            return mysqli_query($conn, "select users.user_id,users.title,users.user_name,users.status,users.email"
                    . ",users.phone_no,roles.role_id,roles.role_name from users inner join roles on users.role_id = roles.role_id where users.partner_id='$partner_id' ORDER BY users.user_id ASC");
        } else if (!empty($partner_id) and ! empty($clinic_id)) {
            return mysqli_query($conn, "select users.user_id,users.title,users.user_name,users.status,users.email"
                    . ",users.phone_no,roles.role_id,roles.role_name from users inner join roles on users.role_id = roles.role_id where users.partner_id='$partner_id' and users.clinic_id='$clinic_id' ORDER BY users.user_id ASC");
        }
    }

    // function for read
    // // function for manage partner read
    public function manage_partner_read() {
        $conn = $this->db_connection();

        $clinic_id = $_SESSION['sess_clinic_id'];
        $partner_id = $_SESSION['sess_partner_id'];
        if (empty($clinic_id or $partner_id)) {
            
        } else {
            return mysqli_query($conn, "SELECT * FROM `partner` where status='Active' order by partner_id ASC");
        }
    }

    public function manage_partner_facility_read() {
        $conn = $this->db_connection();
        $clinic_id = $_SESSION['sess_clinic_id'];
        $partner_id = $_SESSION['sess_partner_id'];
        if (empty($partner_id)) {
            return mysqli_query($conn, "SELECT * FROM `partner_facility_site` order by partner_id ASC");
        } else {
            return mysqli_query($conn, "SELECT * FROM `partner_facility_site` where partner_id='$partner_id' order by partner_id ASC");
        }
    }

    //function to manage roles
    public function manage_roles_read() {
        $conn = $this->db_connection();
        return mysqli_query($conn, "SELECT * FROM `roles` where role_name!=''  order by role_id ASC");
    }

    public function unattended_clients() {
        $link = $this->db_connection();

        $clinic_id = $_SESSION['sess_clinic_id'];
        $partner_id = $_SESSION['sess_partner_id'];
        if (empty($clinic_id) and empty($partner_id)) {
            return mysqli_query($link, "SELECT * FROM `kp_active`  order by uuid ASC");
        } elseif (empty($clinic_id) and ! empty($partner_id)) {
            return mysqli_query($link, "SELECT * FROM `kp_active` where partner_id='$partner_id'  order by uuid ASC");
        } elseif (!empty($clinic_id) and ! empty($partner_id)) {
            return mysqli_query($link, "SELECT * FROM `kp_active` where partner_id='$partner_id' and clinic_id='$clinic_id'  order by uuid ASC");
        }
    }

    // function for read
    // function for manage peer educator read
    public function manage_peer_educator_read() {
        $conn = $this->db_connection();
        $clinic_id = $_SESSION['sess_clinic_id'];
        $partner_id = $_SESSION['sess_partner_id'];
        if (empty($partner_id)) {

            return mysqli_query($conn, "SELECT * FROM manage_peer_educator");
        } else if (empty($clinic_id) and ! empty($partner_id)) {
            return mysqli_query($conn, "SELECT * FROM manage_peer_educator where partner_id='$partner_id'");
        } elseif (!empty($partner_id) and ! empty($clinic_id)) {

            return mysqli_query($conn, "SELECT * FROM manage_peer_educator where partner_id='$partner_id' and clinic_id='$clinic_id'");
        }
    }

    // function for read
    //function for managing kp type
    public function manage_kp_type_read() {
        $conn = $this->db_connection();
        $clinic_id = $_SESSION['sess_clinic_id'];
        $partner_id = $_SESSION['sess_partner_id'];
        if (empty($partner_id) or empty($clinic_id)) {
            return mysqli_query($conn, "SELECT * FROM `kp_types` where status='Active' order by id ASC");
        } else {
            
        }
    }

    //function for managing hot spot
    public function manage_hot_spot_read() {
        $conn = $this->db_connection();
        $clinic_id = $_SESSION['sess_clinic_id'];
        $partner_id = $_SESSION['sess_partner_id'];
        if (empty($partner_id) or empty($clinic_id)) {
            return mysqli_query($conn, "SELECT * FROM `hot_spot_management`");
        } else if (!empty($partner_id) and ! empty($clinic_id)) {
            return mysqli_query($conn, "SELECT * FROM `hot_spot_management` where clinic_id='$clinic_id' and partner_id='$partner_id'");
        } else if (empty($partner_id) and ! empty($clinic_id)) {
            return mysqli_query($conn, "SELECT * FROM `hot_spot_management` where  partner_id='$partner_id'");
        }
    }

    //function for managing test kits
    public function manage_test_kits_read() {
        $conn = $this->db_connection();

        $clinic_id = $_SESSION['sess_clinic_id'];
        $partner_id = $_SESSION['sess_partner_id'];
        if (empty($clinic_id) and empty($partner_id)) {
            return mysqli_query($conn, "SELECT * FROM `test_kits` where status='Active' order by id ASC");
        } else {
            
        }
    }

    // function for delete
    public function delete($id) {
        $conn = $this->db_connection();
        mysqli_query($conn, "DELETE FROM users WHERE user_id=" . $id);
    }

    //function for delete roles
    public function delete_roles($role_id, $delete_id) {
        $conn = $this->db_connection();
        $query = "Update roles set status='In Active' where role_id='$role_id'";
        if (mysqli_query($conn, $query)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //function for edit roles
    public function edit_roles($role_id, $role_name, $role_status) {
        $conn = $this->db_connection();
        $query = "Update roles set status='$role_status',role_name='$role_name' where role_id='$role_id'";
        if (mysqli_query($conn, $query)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // function for delete partner
    public function delete_partner($partner_id, $delete_id) {
        $conn = $this->db_connection();
        $in_active = 'In Active';
        $sql = "UPDATE partner SET status='$in_active' WHERE partner_id='$delete_id'";

        if (mysqli_query($conn, $sql)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //function delete peer educator
    public function delete_peer_educator($peer_educator_id) {
        $conn = $this->db_connection();
        $in_active = 'In Active';
        $sql = "UPDATE peer_educator SET status='$in_active' WHERE id='$delete_id'";

        if (mysqli_query($conn, $sql)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // function for update
    public function update($user_name, $e_mail, $phone_no, $status, $role_name, $edit_id, $permissions, $date_added, $registers_permissions, $reports_permissions, $sidebar_permissions, $systembar_permissions) {
        $conn = $this->db_connection();



        $sql = "UPDATE users SET user_name='$user_name', email='$e_mail', phone_no='$phone_no',role_id='$role_name' ,status='$status' WHERE user_id='$edit_id'";
        $select_permision_sql = "Select permissions_id from permissions_trigger where user_id='$edit_id'";

        $result = mysqli_query($conn, $select_permision_sql);
        $user_data = mysqli_fetch_array($result, MYSQLI_BOTH);
        $count_row = mysqli_num_rows($result);
        if ($count_row >= 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
                $permission_id = $row["permissions_id"];
                $delete_query = "DELETE FROM permissions WHERE id='$permission_id'";
                //$delete_query = "Update permissions set status='In Active' where id='$permission_id'";
                mysqli_query($conn, $delete_query);
            }
            //$permissions_id = $user_data['id'];

            foreach ($registers_permissions as $value) {
                $insert_permissions = "INSERT INTO permissions (user_id,function_id,date_added) VALUES ('$edit_id','$value','$date_added')";
                mysqli_query($conn, $insert_permissions);
            }
            foreach ($systembar_permissions as $value) {
                $insert_permissions = "INSERT INTO permissions (user_id,function_id,date_added) VALUES ('$edit_id','$value','$date_added')";
                mysqli_query($conn, $insert_permissions);
            }
            foreach ($reports_permissions as $value) {
                $insert_permissions = "INSERT INTO permissions (user_id,function_id,date_added) VALUES ('$edit_id','$value','$date_added')";
                mysqli_query($conn, $insert_permissions);
            }
            foreach ($sidebar_permissions as $value) {
                $insert_permissions = "INSERT INTO permissions (user_id,function_id,date_added) VALUES ('$edit_id','$value','$date_added')";
                mysqli_query($conn, $insert_permissions);
            }
        }



        if (mysqli_query($conn, $sql)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //function update peer educator information
    public function update_peer_educator($peer_educator_id, $peer_educator_nme, $nick_name, $phone_no, $status) {
        $conn = $this->db_connection();
        $sql = "UPDATE peer_educator set peer_educator_nme='$peer_educator_nme', nick_name='$nick_name',phone_no='$phone_no',status='$status' where id='$peer_educator_id'";
        if (mysqli_query($conn, $sql)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // function for update partner 
    public function update_partner($partner_id, $partner_name, $contact_person_name, $contact_person_phone, $contact_person_email, $status, $kp_target) {
        $conn = $this->db_connection();



        $sql = "UPDATE partner SET partner_name='$partner_name',kp_target='$kp_target', contact_person='$contact_person_name', contact_person_phone='$contact_person_phone',contact_person_email='$contact_person_email' ,status='$status' WHERE partner_id='$partner_id'";

        if (mysqli_query($conn, $sql)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // function for update partner 
    public function update_hot_spot($hot_spot_id, $hot_spot_name, $region_name, $parter_name, $clinic_name) {
        $conn = $this->db_connection();



        $sql = "UPDATE partner SET partner_name='$partner_name',kp_target='$kp_target', contact_person='$contact_person_name', contact_person_phone='$contact_person_phone',contact_person_email='$contact_person_email' ,status='$status' WHERE partner_id='$partner_id'";

        if (mysqli_query($conn, $sql)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // function for update user password
    public function update_password($user_password, $new_password, $edit_id) {
        $conn = $this->db_connection();


        if ($user_password === $new_password) {
            $user_password = md5($user_password);

            $sql = "UPDATE users SET password='$user_password' WHERE user_id='$edit_id'";

            if (mysqli_query($conn, $sql)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    // function for update
}

?>