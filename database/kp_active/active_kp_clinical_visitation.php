<?php

header("Access-Control-Allow-Origin: *");
?>
<?php

include '../db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

$uuid = $_GET['uuid'];

// get the records from the database
if ($result = $mysqli->query("SELECT * FROM clinical_visit where uuid='$uuid' ORDER BY activity_timestamp DESC LIMIT 1")) {
    // display records if there are records to display
    if ($result->num_rows > 0) {


        while ($row = $result->fetch_object()) {
            // set up a row for each record
  
  			echo "Date: " . $row->date_added . "";
			echo "<br>";
            echo "Service given: " . $row->service_given . "";
			echo "<br>";
			

$screened = $row->screened;

$placeholders = array('1', '0');

$replacewith = array('Yes', 'No');

//male string
$malestr = str_replace($placeholders, $replacewith, $screened);
			
			echo "Screened: " . $malestr . "";
			echo "<br>";
			echo "Referred to: " . $row->reffered_to . "";
	  
        }
    }
    // if there are no records in the database, display an alert message
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No clinical visitation data found";
        echo "</div>";
    }
}
// show an error if there is an issue with the database query
else {
    echo "Error: " . $mysqli->error;
}

// close database connection
$mysqli->close();
?>