<?php

header("Access-Control-Allow-Origin: *");
?>
<?php

include '../db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

$clinic_id = $_GET['clinic_id'];
$partner_id = $_GET['partner_id'];
$task = $_GET['task'];
$group = $_GET['group'];

if($task == "inroom") {

if($group == "counselling") {
if ($result = $mysqli->query("SELECT * FROM kp_active where is_active='Yes' and partner_id='$partner_id' and clinic_id='$clinic_id' and status='Counselling' ORDER BY entrytime ASC")) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            echo "<div class=\"activeclientbox\">";
            echo "<div onclick=\"localStorage.setItem('activeclientremove', '" . $row->id . "');activeclientremove();\" class=\"activeclientremove\">x</div><div class=\"activeclientfield uuidfield\">UUID: " . $row->UUID . "</div>";
            echo "<div class=\"activeclientfield namefield\">Name: " . $row->name . "</div>";
            echo "<div class=\"activeclientfield genderfield\">Gender: " . $row->gender . "</div>";
            echo "<div class=\"activeclientfield statusfield\">Status: " . $row->status . "</div>";
            
			echo "<div class=\"activeclientfield datefield\">Date: " . $row->entrydate . "</div>";
			echo "<div class=\"activeclientfield timefield\">Time: " . $row->entrytime . "</div>";
			
            echo "<a href=\"clinichistory.php?uuid=" . $row->UUID . "\"><div class=\"activeclientbutton\">History</div></a><div class=\"activeclientbutton\" onclick=\"localStorage.setItem('activeclientidstatusupdate','" . $row->id . "');localStorage.setItem('currentuuid','" . $row->UUID . "');localStorage.setItem('currentclientname','" . $row->name . "');openclienttransfer();\";>Transfer</div>";
            echo "</div>";
        }
    }
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No client in Counselling";
        echo "</div>";
    }
}
else {
    echo "Error: " . $mysqli->error;
}
$mysqli->close();
}



if($group == "consultation") {
if ($result = $mysqli->query("SELECT * FROM kp_active where is_active='Yes' and partner_id='$partner_id' and clinic_id='$clinic_id' and status='Consultation' ORDER BY entrytime ASC")) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            echo "<div class=\"activeclientbox\">";
            echo "<div onclick=\"localStorage.setItem('activeclientremove', '" . $row->id . "');activeclientremove();\" class=\"activeclientremove\">x</div><div class=\"activeclientfield uuidfield\">UUID: " . $row->UUID . "</div>";
            echo "<div class=\"activeclientfield namefield\">Name: " . $row->name . "</div>";
            echo "<div class=\"activeclientfield genderfield\">Gender: " . $row->gender . "</div>";
            echo "<div class=\"activeclientfield statusfield\">Status: " . $row->status . "</div>";
            
			echo "<div class=\"activeclientfield datefield\">Date: " . $row->entrydate . "</div>";
			echo "<div class=\"activeclientfield timefield\">Time: " . $row->entrytime . "</div>";
			
            echo "<a href=\"clinichistory.php?uuid=" . $row->UUID . "\"><div class=\"activeclientbutton\">History</div></a><div class=\"activeclientbutton\" onclick=\"localStorage.setItem('activeclientidstatusupdate','" . $row->id . "');localStorage.setItem('currentuuid','" . $row->UUID . "');localStorage.setItem('currentclientname','" . $row->name . "');openclienttransfer();\";>Transfer</div>";
            echo "</div>";
        }
    }
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No client in Consultation";
        echo "</div>";
    }
}
else {
    echo "Error: " . $mysqli->error;
}
$mysqli->close();
}

if($group == "careandtreatment") {
if ($result = $mysqli->query("SELECT * FROM kp_active where is_active='Yes' and partner_id='$partner_id' and clinic_id='$clinic_id' and status='Care and Treatment' ORDER BY entrytime ASC")) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            echo "<div class=\"activeclientbox\">";
            echo "<div onclick=\"localStorage.setItem('activeclientremove', '" . $row->id . "');activeclientremove();\" class=\"activeclientremove\">x</div><div class=\"activeclientfield uuidfield\">UUID: " . $row->UUID . "</div>";
            echo "<div class=\"activeclientfield namefield\">Name: " . $row->name . "</div>";
            echo "<div class=\"activeclientfield genderfield\">Gender: " . $row->gender . "</div>";
            echo "<div class=\"activeclientfield statusfield\">Status: " . $row->status . "</div>";
            
			echo "<div class=\"activeclientfield datefield\">Date: " . $row->entrydate . "</div>";
			echo "<div class=\"activeclientfield timefield\">Time: " . $row->entrytime . "</div>";
			
            echo "<a href=\"clinichistory.php?uuid=" . $row->UUID . "\"><div class=\"activeclientbutton\">History</div></a><div class=\"activeclientbutton\" onclick=\"localStorage.setItem('activeclientidstatusupdate','" . $row->id . "');localStorage.setItem('currentuuid','" . $row->UUID . "');localStorage.setItem('currentclientname','" . $row->name . "');openclienttransfer();\";>Transfer</div>";
            echo "</div>";
        }
    }
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No client in Care & Treatment";
        echo "</div>";
    }
}
else {
    echo "Error: " . $mysqli->error;
}
$mysqli->close();
}

if($group == "pharmacy") {
if ($result = $mysqli->query("SELECT * FROM kp_active where is_active='Yes' and partner_id='$partner_id' and clinic_id='$clinic_id' and status='Pharmacy' ORDER BY entrytime ASC")) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            echo "<div class=\"activeclientbox\">";
            echo "<div onclick=\"localStorage.setItem('activeclientremove', '" . $row->id . "');activeclientremove();\" class=\"activeclientremove\">x</div><div class=\"activeclientfield uuidfield\">UUID: " . $row->UUID . "</div>";
            echo "<div class=\"activeclientfield namefield\">Name: " . $row->name . "</div>";
            echo "<div class=\"activeclientfield genderfield\">Gender: " . $row->gender . "</div>";
            echo "<div class=\"activeclientfield statusfield\">Status: " . $row->status . "</div>";
            
			echo "<div class=\"activeclientfield datefield\">Date: " . $row->entrydate . "</div>";
			echo "<div class=\"activeclientfield timefield\">Time: " . $row->entrytime . "</div>";
			
            echo "<a href=\"clinichistory.php?uuid=" . $row->UUID . "\"><div class=\"activeclientbutton\">History</div></a><div class=\"activeclientbutton\" onclick=\"localStorage.setItem('activeclientidstatusupdate','" . $row->id . "');localStorage.setItem('currentuuid','" . $row->UUID . "');localStorage.setItem('currentclientname','" . $row->name . "');openclienttransfer();\";>Transfer</div>";
            echo "</div>";
        }
    }
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No client in Pharmacy";
        echo "</div>";
    }
}
else {
    echo "Error: " . $mysqli->error;
}
$mysqli->close();
}


}


















if($task == "queue") {

if($group == "counselling") {
if ($result = $mysqli->query("SELECT * FROM kp_active where is_active='Yes' and partner_id='$partner_id' and clinic_id='$clinic_id' and status='Counselling - Queue' ORDER BY entrytime ASC")) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            echo "<div class=\"activeclientbox\">";
            echo "<div onclick=\"localStorage.setItem('activeclientremove', '" . $row->id . "');activeclientremove();\" class=\"activeclientremove\">x</div><div class=\"activeclientfield uuidfield\">UUID: " . $row->UUID . "</div>";
            echo "<div class=\"activeclientfield namefield\">Name: " . $row->name . "</div>";
            echo "<div class=\"activeclientfield genderfield\">Gender: " . $row->gender . "</div>";
            echo "<div class=\"activeclientfield statusfield\">Status: " . $row->status . "</div>";
            
			echo "<div class=\"activeclientfield datefield\">Date: " . $row->entrydate . "</div>";
			echo "<div class=\"activeclientfield timefield\">Time: " . $row->entrytime . "</div>";
			
            echo "<a href=\"clinichistory.php?uuid=" . $row->UUID . "\"><div class=\"activeclientbutton\">History</div></a><div class=\"activeclientbutton\" onclick=\"localStorage.setItem('activeclientidstatusupdate','" . $row->id . "');localStorage.setItem('currentuuid','" . $row->UUID . "');localStorage.setItem('currentclientname','" . $row->name . "');openclienttransfer();\";>Transfer</div>";
            echo "</div>";
        }
    }
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No clients in Queue";
        echo "</div>";
    }
}
else {
    echo "Error: " . $mysqli->error;
}
$mysqli->close();
}	

if($group == "consultation") {
if ($result = $mysqli->query("SELECT * FROM kp_active where is_active='Yes' and partner_id='$partner_id' and clinic_id='$clinic_id' and status='Consultation - Queue' ORDER BY entrytime ASC")) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            echo "<div class=\"activeclientbox\">";
            echo "<div onclick=\"localStorage.setItem('activeclientremove', '" . $row->id . "');activeclientremove();\" class=\"activeclientremove\">x</div><div class=\"activeclientfield uuidfield\">UUID: " . $row->UUID . "</div>";
            echo "<div class=\"activeclientfield namefield\">Name: " . $row->name . "</div>";
            echo "<div class=\"activeclientfield genderfield\">Gender: " . $row->gender . "</div>";
            echo "<div class=\"activeclientfield statusfield\">Status: " . $row->status . "</div>";
            
			echo "<div class=\"activeclientfield datefield\">Date: " . $row->entrydate . "</div>";
			echo "<div class=\"activeclientfield timefield\">Time: " . $row->entrytime . "</div>";
			
            echo "<a href=\"clinichistory.php?uuid=" . $row->UUID . "\"><div class=\"activeclientbutton\">History</div></a><div class=\"activeclientbutton\" onclick=\"localStorage.setItem('activeclientidstatusupdate','" . $row->id . "');localStorage.setItem('currentuuid','" . $row->UUID . "');localStorage.setItem('currentclientname','" . $row->name . "');openclienttransfer();\";>Transfer</div>";
            echo "</div>";
        }
    }
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No clients in Queue";
        echo "</div>";
    }
}
else {
    echo "Error: " . $mysqli->error;
}
$mysqli->close();
}	

if($group == "careandtreatment") {
if ($result = $mysqli->query("SELECT * FROM kp_active where is_active='Yes' and partner_id='$partner_id' and clinic_id='$clinic_id' and status='Care and Treatment - Queue' ORDER BY entrytime ASC")) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            echo "<div class=\"activeclientbox\">";
            echo "<div onclick=\"localStorage.setItem('activeclientremove', '" . $row->id . "');activeclientremove();\" class=\"activeclientremove\">x</div><div class=\"activeclientfield uuidfield\">UUID: " . $row->UUID . "</div>";
            echo "<div class=\"activeclientfield namefield\">Name: " . $row->name . "</div>";
            echo "<div class=\"activeclientfield genderfield\">Gender: " . $row->gender . "</div>";
            echo "<div class=\"activeclientfield statusfield\">Status: " . $row->status . "</div>";
            
			echo "<div class=\"activeclientfield datefield\">Date: " . $row->entrydate . "</div>";
			echo "<div class=\"activeclientfield timefield\">Time: " . $row->entrytime . "</div>";
			
            echo "<a href=\"clinichistory.php?uuid=" . $row->UUID . "\"><div class=\"activeclientbutton\">History</div></a><div class=\"activeclientbutton\" onclick=\"localStorage.setItem('activeclientidstatusupdate','" . $row->id . "');localStorage.setItem('currentuuid','" . $row->UUID . "');localStorage.setItem('currentclientname','" . $row->name . "');openclienttransfer();\";>Transfer</div>";
            echo "</div>";
        }
    }
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No clients in Queue";
        echo "</div>";
    }
}
else {
    echo "Error: " . $mysqli->error;
}
$mysqli->close();
}	
	
if($group == "pharmacy") {
if ($result = $mysqli->query("SELECT * FROM kp_active where is_active='Yes' and partner_id='$partner_id' and clinic_id='$clinic_id' and status='Pharmacy - Queue' ORDER BY entrytime ASC")) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {

            echo "<div class=\"activeclientbox\">";
            echo "<div onclick=\"localStorage.setItem('activeclientremove', '" . $row->id . "');activeclientremove();\" class=\"activeclientremove\">x</div><div class=\"activeclientfield uuidfield\">UUID: " . $row->UUID . "</div>";
            echo "<div class=\"activeclientfield namefield\">Name: " . $row->name . "</div>";
            echo "<div class=\"activeclientfield genderfield\">Gender: " . $row->gender . "</div>";
            echo "<div class=\"activeclientfield statusfield\">Status: " . $row->status . "</div>";
            
			echo "<div class=\"activeclientfield datefield\">Date: " . $row->entrydate . "</div>";
			echo "<div class=\"activeclientfield timefield\">Time: " . $row->entrytime . "</div>";
			
            echo "<a href=\"clinichistory.php?uuid=" . $row->UUID . "\"><div class=\"activeclientbutton\">History</div></a><div class=\"activeclientbutton\" onclick=\"localStorage.setItem('activeclientidstatusupdate','" . $row->id . "');localStorage.setItem('currentuuid','" . $row->UUID . "');localStorage.setItem('currentclientname','" . $row->name . "');openclienttransfer();\";>Transfer</div>";
            echo "</div>";
        }
    }
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No clients in Queue";
        echo "</div>";
    }
}
else {
    echo "Error: " . $mysqli->error;
}
$mysqli->close();
}



}
?>