<?php

header("Access-Control-Allow-Origin: *");
?>
<?php

include '../db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

//values to be inserted in database table
$uuid = $_GET["uuid"];
$name = $_GET["name"];
$status = $_GET["status"];
$gender = $_GET['gender'];
$entrytime = $_GET["entrytime"];
$entrydate = $_GET["entrydate"];
$clinic_id = $_GET['clinic_id'];
$partner_id = $_GET['partner_id'];

$query = "INSERT INTO kp_active (
UUID,
name,
entrytime,
entrydate,
gender,
partner_id,
clinic_id,
status
) 
VALUES(?,?,?,?,?,?,?,?)";
$statement = $mysqli->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('sssssiis', $uuid, $name, $entrytime, $entrydate, $gender, $partner_id, $clinic_id, $status);

if ($statement->execute()) {
    //  header("Location: /whiskysundays/admin/admin.php?categoryid=$categoryid");
} else {
    die('Error : (' . $mysqli->errno . ') ' . $mysqli->error);
}
$statement->close();
?>