<?php
header("Access-Control-Allow-Origin: *");
?>
<?php
include '../db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

$status = $_GET['statusupdate'];
$activeclientidstatusupdate = $_GET['activeclientidstatusupdate'];

$query = "UPDATE kp_active SET status=? WHERE id=?";
$statement = $mysqli->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$results =  $statement->bind_param('si', $status, $activeclientidstatusupdate);

//execute query
$statement->execute();

?>