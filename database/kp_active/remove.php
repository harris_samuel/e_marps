<?php
include '../db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

$id = $_GET['id'];
$unattended = $_GET['unattended'];
$unattendedother = $_GET['unattendedother'];
$isactive = 'No';

$query = "UPDATE kp_active SET is_active=?, unattended=?, unattended_other=?  WHERE id=?";
$statement = $mysqli->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$results =  $statement->bind_param('sssi', $isactive, $unattended, $unattendedother,  $id);

//execute query
$statement->execute();

?>