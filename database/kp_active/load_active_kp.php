<?php

header("Access-Control-Allow-Origin: *");
?>
<?php

include '../db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

$clinic_id = $_GET['clinic_id'];
$partner_id = $_GET['partner_id'];

// get the records from the database
if ($result = $mysqli->query("SELECT * FROM kp_active where is_active='Yes' and partner_id='$partner_id' and clinic_id='$clinic_id' ORDER BY id DESC")) {
    // display records if there are records to display
    if ($result->num_rows > 0) {


        while ($row = $result->fetch_object()) {
            // set up a row for each record

            echo "<div class=\"activeclientbox\">";
            echo "<div onclick=\"localStorage.setItem('activeclientremove', '" . $row->id . "');activeclientremove();\" class=\"activeclientremove\">x</div><div class=\"activeclientfield uuidfield\">UUID: " . $row->UUID . "</div>";
            echo "<div class=\"activeclientfield namefield\">Name: " . $row->name . "</div>";
			
            echo "<div class=\"activeclientfield genderfield\">Gender: " . $row->gender . "</div>";
            echo "<div class=\"activeclientfield statusfield\">Status: " . $row->status . "</div>";
			
            echo "<div class=\"activeclientfield datefield\">Date: " . $row->entrydate . "</div>";
			echo "<div class=\"activeclientfield timefield\">Time: " . $row->entrytime . "</div>";
			
            echo "<a href=\"clinichistory.php?uuid=" . $row->UUID . "\"><div class=\"activeclientbutton\">History</div></a><div class=\"activeclientbutton\" onclick=\"localStorage.setItem('activeclientidstatusupdate','" . $row->id . "');localStorage.setItem('currentuuid','" . $row->UUID . "');localStorage.setItem('currentclientname','" . $row->name . "');openclienttransfer();\";>Transfer</div>";
            echo "</div>";
        }
    }
    // if there are no records in the database, display an alert message
    else {

        echo "<div class=\"activeclientbox\">";
        echo "No active clients";
        echo "</div>";
    }
}
// show an error if there is an issue with the database query
else {
    echo "Error: " . $mysqli->error;
}

// close database connection
$mysqli->close();
?>