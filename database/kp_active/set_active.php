<?php
header("Access-Control-Allow-Origin: *");
?>
<?php
include '../db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

$is_active = 'Yes';
$uuid = $_GET['uuid'];
$status = $_GET["status"];
$entrytime = $_GET["entrytime"];
$entrydate = $_GET["entrydate"];
$clinic_id = $_GET['clinic_id'];
$partner_id = $_GET['partner_id'];

$query = "UPDATE kp_active SET is_active=?, status=?, entrytime=?, entrydate=?, clinic_id=?, partner_id=? WHERE UUID=?";
$statement = $mysqli->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$results =  $statement->bind_param('ssssiis', $is_active, $status, $entrytime, $entrydate, $clinic_id, $partner_id, $uuid);

//execute query
$statement->execute();

?>