<?php

class Websrvc {

    function __construct() {
        
    }

    public function db_connection() {
        $host_name = "localhost";
        $database = "db565263480";
        $user_name = "dbo565263480";
        $password = "emarps2015!";
        $link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        } else {
            return $link;
        }
    }

    /* get url parameters */

    function url() {
        return sprintf("%s://%s%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME'], dirname($_SERVER['PHP_SELF']));
    }

    /* Select data from table county */

    public function select_counties() {
        $link = $this->db_connection();
        $query = "SELECT * FROM `region`";
        $result = mysqli_query($link, $query);
        $county_data = mysqli_fetch_array($result, MYSQLI_BOTH);
        $count_row = mysqli_num_rows($result);
        if ($count_row === 1) {
            return $result;
        }
    }

    /*     * * for login process ** */

    public function check_login($emailusername, $password) {
        $link = $this->db_connection();
        $enc_password = md5($password);

        //checking if the username is available in the table
        $result = mysqli_query($link, "SELECT first_access, user_id,user_name,role_id,status,partner_id,clinic_id from users WHERE email='$emailusername' or user_name='$emailusername' and password='$enc_password'");
        $user_data = mysqli_fetch_array($result, MYSQLI_BOTH);
        $count_row = mysqli_num_rows($result);
        $base_url = $this->url();
        if ($count_row == 1) {
            $first_access = $user_data['first_access'];

            if ($first_access === "Yes") {

                header("Location: recover_password.php"); /* Redirect browser to  password recovery/reset page */
            } else {
                $clinic_id = $user_data['clinic_id'];
                $_SESSION['login'] = true; // this login var will use for the session thing
                $_SESSION['uid'] = $user_data['user_id'];
                $_SESSION['sess_partner_id'] = $user_data['partner_id'];
				$_SESSION['sess_clinic_id'] = $user_data['clinic_id'];
                $_SESSION['sess_role_id'] = $user_data['role_id'];
                $query = "Select facility_id from clinic where clinic_id='$clinic_id'";
                $query_2 = mysqli_query($link, $query);
                $clinic_data = mysqli_fetch_array($query_2, MYSQLI_BOTH);
                $count_clinic_row = mysqli_num_rows($query_2);
                if ($count_clinic_row == 1) {
                    $facilty_id = $clinic_data['facility_id'];
                    $_SESSION['sess_clinic_id'] = $facilty_id;
                }



                $this->clear_prev_clients();

                return true;
            }
        } else {
            return false;
        }
    }

    public function clear_prev_clients() {
        $link = $this->db_connection();

        $clinic_id = $_SESSION['sess_clinic_id'];
        $partner_id = $_SESSION['sess_partner_id'];
        $update_kp_active = "UPDATE `kp_active` SET `is_active` = 'No' WHERE `activity_timestamp` < CURDATE() and partner_id='$partner_id' and clinic_id='$clinic_id' ;";
        if (mysqli_query($link, $update_kp_active)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /* Recover password */

    public function recover_pasword($email) {
        $link = $this->db_connection();
        $result = mysqli_query($link, "Select email, status, user_name,user_id from users where email='$email' ");
        $user_data = mysqli_fetch_array($result, MYSQLI_BOTH);
        $count_row = mysqli_num_rows($result);
        if ($count_row === 1) {
            $email = $user_data['email'];
            // $full_name = $user_data['fullname'];
            $user_id = $user_data['user_id'];
            $username = $user_data['user_name'];

            $base_url = $this->url();
            //set the random id length 
            $random_id_length = 10;
            $today = date("Y-m-d H:i:s");
            //generate a random id encrypt it and store it in $rnd_id 
            $rnd_id = crypt(uniqid($today, 1));
            //to remove any slashes that might have come 
            $rnd_id = strip_tags(stripslashes($rnd_id));

            //Removing any . or / and reversing the string 
            $rnd_id = str_replace(".", "", $rnd_id);
            $rnd_id = strrev(str_replace("/", "", $rnd_id));

            //finally I take the first 10 characters from the $rnd_id 
            $random_key = substr($rnd_id, 0, $random_id_length);


            $headers = "From: webmaster@emarps.org" . "\r\n" .
                    "CC: harrisdindisamuel@gmail.com";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $subject = "Reset User Account Password";
            // the message
            $msg = '<html><head>
<title>User Account Password Reset </title>
</head>
<body>
                                                                           ---------------------
                                                                                Hey: ' . $username . '!
                                                                                    <fieldset>
                                                                                We recently received a request for resetting your EMARPS  ACCOUNT Password. You can reset your   Personal Account Password 
                                                                                through the link below:
                                                                                <hr>
                                                                                ------------------------
                                                                                Please click this link to activate your account:<a href="' . $base_url . '/reset_password.php?uq=' . $random_key . '">Reset</a>
                                                                                ------------------------ 
                                                                                <hr>
                                                                                If you did not request for pasword reset please follow this link to restore your E-Marps Access Credentials : <a href="' . $base_url . '/restore_account.php?uq=' . $random_key . '">Restore</a>

                                                                                ------------------------
</body>                                                                                
</html>';


            // use wordwrap() if lines are longer than 70 characters
            // $msg = wordwrap($msg, 70);
            // send email
            mail($email, $subject, $msg, $headers);
            $expiry_time = date("Y-m-d H:i:s", strtotime($today . ' +2 hour'));

            $update_q = mysqli_query($link, "Update users set token_key = '$random_key' , expiry_time='$expiry_time' , status='In Active' where user_id='$user_id'");
            if ($update_q) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    /*     * *  Reset password process ** */

    public function reset_password($password, $password_2, $token_key) {
        $link = $this->db_connection();
        //get user details from the database based on the  token key assigned
        $select_query = "Select user_id,activity_timestamp ,expiry_time from users where token_key = '$token_key' and status = 'In Active'";
        $result = mysqli_query($link, $select_query);
        $user_data = mysqli_fetch_array($result, MYSQLI_BOTH);
        $count_row = mysqli_num_rows($result);
        if ($count_row === 1) {
            $user_id = $user_data['user_id'];
            $expiry_time = $user_data['expiry_time'];
            $activity_timestamp = $user_data['activity_timestamp'];
            $active = 'Active';
            $today = date("Y-m-d H:i:s");
            $token_expiry_time = date("Y-m-d H:i:s", strtotime($activity_timestamp . ' +2 hour'));
            //check again if password match 
            if ($password === $password_2) {
                //Update user details and set user account to be active again
                $enc_password = md5($password);
                $update_user_query = "UPDATE `users` SET `password` = '$enc_password',first_access='No', `status` = '$active' WHERE `users`.`token_key` = '$token_key'";
                if (mysqli_query($link, $update_user_query)) {
                    //return true if user account update was successfull
                    return TRUE;
                } else {
                    //return false if user account update was unsuccessful 
                    return FALSE;
                }
            }
        }
    }

    /*     * * for showing the username or fullname ** */

    public function get_fullname($uid) {
        $link = $this->db_connection();
        $result = mysqli_query($link, "SELECT concat(users.title,' ',users.f_name,' ',users.s_name,' ',users.o_name) as fullname FROM users WHERE uid = $uid");
        $user_data = mysqli_fetch_array($result, MYSQLI_BOTH);
        echo $user_data['fullname'];
    }

    /*     * * starting the session ** */

    public function get_session() {

        return $_SESSION['login'];
    }

    public function user_logout() {
        $_SESSION['login'] = FALSE;
        session_destroy();
    }

    function getuserroles() {
        $link = $this->db_connection();
        $result = mysqli_query("SELECT DISTINCT partner_id,partner_name FROM partner")
                or die(mysql_error());

        while ($tier = mysql_fetch_array($result)) {
            echo '<option value="' . $tier['role_id'] . '">' . $tier['role_name'] . '</option>';
        }
    }

    function role_registration($role_name, $user_id, $status, $datetime) {
        $link = $this->db_connection();
        $query = "INSERT INTO `db565263480`.`roles` (`role_id`, `role_name`, `date_added`, `activity_stamp`, `user_id`, `status`)"
                . " VALUES ('', '$role_name', '$datetime', CURRENT_TIMESTAMP, '$user_id', '$status');";
        if (mysqli_query($link, $query)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function peer_educators_registration($peer_educator_name, $user_id, $nick_name, $phone_no, $status, $datetime) {
        $link = $this->db_connection();


        $query = mysqli_query($link, "Select * from users where user_id='$user_id'") or die('[{"id":"' . mysqli_error($link) . '"}]');
        if (mysqli_num_rows($query) > 0) {

            while ($row = $query->fetch_assoc()) {

                $clinic_id = $row["clinic_id"];
                $partner_id = $row["partner_id"];


                $peer_educator = mysqli_query($link, "INSERT INTO `peer_educator` (`id`, `peer_educator_nme`, `nick_name`, `phone_no`, `date_joined`, `partner_id`, `clinic_id`, `user_id`, `status`) "
                        . "VALUES ('', '$peer_educator_name ', '$nick_name', '$phone_no',  '$datetime', '$partner_id', '$clinic_id', '$user_id', '$status')");

                if ($peer_educator) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }
    }

    function test_kit_registration($kit_name, $user_id, $datetime, $kit_type, $status) {
        $link = $this->db_connection();


        $query = mysqli_query($link, "Select * from users where user_id='$user_id'") or die('[{"id":"' . mysqli_error($link) . '"}]');
        if (mysqli_num_rows($query) > 0) {

            while ($row = $query->fetch_assoc()) {

                $clinic_id = $row["clinic_id"];
                $partner_id = $row["partner_id"];


                $test_kits = mysqli_query($link, "INSERT INTO `test_kits` (`id`, `kit_name`,  `kit_type`, `user_id`, `date_added`, `partner_id`, `clinic_id`, `status`) "
                        . "VALUES ('', '$kit_name ', '$kit_type',  '$user_id', '$datetime', '$partner_id', '$clinic_id', '$status')");

                if ($test_kits) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }
    }

    function peer_educator_members_registration($member_name, $nick_name, $phone_no) {
        //check DB Connection

        $link = $this->db_connection();

// Perform queries 

        $date_added = date('Y-m-d H:i:s');
        $no = "No";
        $q = mysqli_query($link, "INSERT INTO peer_educator_members (name,phone,nick_name,date_joined,activity_stamp,is_screened) 
VALUES ('" . $member_name . "','" . $phone_no . "','" . $nick_name . "','" . $date_added . "','','" . $no . "')");

        if (mysqli_affected_rows($q)) {

            return TRUE;
        } else {

            printf("Errormessage: %s\n", mysqli_error($con));
        }
    }

    public function user_registration($user_name, $user_email, $user_partner_name, $user_clinic_name, $user_status, $user_roles, $user_phone, $sidebar_permissions, $systembar_permissions, $reports_permissions, $registers_permissions) {
        $mysqli = $this->db_connection();

// Perform queries 
        $password = "123456";
        $enc_password = $password;
        $encrypted_password = md5($password);

        $date_added = date('Y-m-d H:i:s');
        $user_status = 'In Active';
        $q = mysqli_query($mysqli, "INSERT INTO users (user_name,password,status,partner_id,clinic_id,date_added,email,phone_no,role_id) 
VALUES ('" . $user_name . "','" . $encrypted_password . "','" . $user_status . "','" . $user_partner_name . "',"
                . "'" . $user_clinic_name . "','" . $date_added . "','" . $user_email . "','" . $user_phone . "','" . $user_roles . "')");
        $last_id = mysqli_insert_id($mysqli);
        if ($q) {
            foreach ($sidebar_permissions as $value) {
                $permission_id = $value;
                $permission_query = "INSERT INTO permissions (user_id,function_id,date_added) VALUES ('$last_id','$permission_id','$date_added')";
                $permission_sql = mysqli_query($mysqli, $permission_query);
            }

            foreach ($systembar_permissions as $value) {
                $permission_id = $value;
                $permission_query = "INSERT INTO permissions (user_id,function_id,date_added) VALUES ('$last_id','$permission_id','$date_added')";
                $permission_sql = mysqli_query($mysqli, $permission_query);
            }

            foreach ($reports_permissions as $value) {
                $permission_id = $value;
                $permission_query = "INSERT INTO permissions (user_id,function_id,date_added) VALUES ('$last_id','$permission_id','$date_added')";
                $permission_sql = mysqli_query($mysqli, $permission_query);
            }

            foreach ($registers_permissions as $value) {
                $permission_id = $value;
                $permission_query = "INSERT INTO permissions (user_id,function_id,date_added) VALUES ('$last_id','$permission_id','$date_added')";
                $permission_sql = mysqli_query($mysqli, $permission_query);
            }

            $base_url = $this->url();
            //set the random id length 
            $random_id_length = 10;
            $today = date("Y-m-d H:i:s");
            //generate a random id encrypt it and store it in $rnd_id 
            $rnd_id = crypt(uniqid($today, 1));
            //to remove any slashes that might have come 
            $rnd_id = strip_tags(stripslashes($rnd_id));

            //Removing any . or / and reversing the string 
            $rnd_id = str_replace(".", "", $rnd_id);
            $rnd_id = strrev(str_replace("/", "", $rnd_id));

            //finally I take the first 10 characters from the $rnd_id 
            $random_key = substr($rnd_id, 0, $random_id_length);


            $headers = "From: webmaster@emarps.org" . "\r\n" .
                    "CC: harrisdindisamuel@gmail.com";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $subject = " E-Marps User Account Login Credentials";
            // the message
            $msg = '<html><head>
<title>User Account Password Reset </title>
</head>
<body>
                                                                           ---------------------
                                                                                Hey :' . $user_name . '!
                                                                                    <fieldset>
                                                                                Your  E-MARPS Personal Account has been created. Please use the  following credentials to access your account : 
                                                                                User name : ' . $user_name . ' <br>
                                                                                E Mail Address : ' . $user_email . '<br>
                                                                                Password : ' . $password . ' <br>
                                                                                    You will be forced to reset you account password on your first itme login. 
                                                                                You can reset your password using  the link below:
                                                                                <hr>
                                                                                ------------------------
                                                                                Please click this link to activate your account:<a href="' . $base_url . '/reset_password.php?uq=' . $random_key . '">Reset</a>
                                                                                

                                                                                ------------------------
</body>                                                                                
</html>';


            // use wordwrap() if lines are longer than 70 characters
            // $msg = wordwrap($msg, 70);
            // send email
            mail($user_email, $subject, $msg, $headers);
            $update_query = "Update users set token_key ='$random_key' where user_id='$last_id'";
            if (mysqli_query($mysqli, $update_query)) {
                return TRUE;
            } else {
                echo mysqli_error($link);
                return FALSE;
            }
            return TRUE;
        } else {
            echo mysqli_error($mysqli);
            return FALSE;
        }
    }

    function clinic_registration($clinic_name, $county, $facility_code, $facility_id, $partner_name, $clinic_region_name, $date_today, $kp_target, $kp_typess, $kp_targetss) {

        $user_id = 1;
        $mysqli = $this->db_connection();
        $q = mysqli_query($mysqli, "INSERT INTO clinic (facility_code,facility_id,partner_id,user_id,kp_target) VALUES ('" . $facility_code . "','" . $facility_id . "','" . $partner_name . "','" . $user_id . "','" . $kp_target . "')");
        $last_clinic_id = mysqli_insert_id($mysqli);
        $q_2 = mysqli_query($mysqli, "INSERT INTO partner_clinic (partner_id,clinic_id,user_id) VALUE ('" . $partner_name . "','" . $facility_id . "','" . $user_id . "')");
        $q_2 = mysqli_query($mysqli, "UPDATE `facility` SET `is_registered` = 'Yes' WHERE `facility`.`facility_id` = '$facility_id'");

        foreach ($clinic_region_name as $value) {
            $region_id = $value;

            //$clinic_region_query = mysqli_query($mysqli, "INSERT INTO clinic_region(region_id,clinic_id,facility_code,date_added) VALUES ('" . $region_id . "','" . $last_clinic_id . "','" . $facility_code . "','" . $date_today . "')  ");
            //$query = "INSERT INTO clinic_region (`region_id`, `clinic_id`, `facility_code`, `date_added`, `activity_stamp`) VALUES ( '$region_id', '$last_clinic_id', '$facility_code', '$date_today', CURRENT_TIMESTAMP)";
            $clinic_region_query = mysqli_query($mysqli, "INSERT INTO clinic_region (region_id, clinic_id, facility_code, date_added) VALUES ( '$region_id', '$last_clinic_id', '$facility_code', '$date_today')");
        }

        $temp = $kp_typess;
        for ($i = 0; $i < count($temp); $i++) {
            $kp_type = $kp_typess[$i];
            $kp_target = $kp_targetss[$i];

            $clinic_kp_types_target = mysqli_query($mysqli, "INSERT INTO `clinic_kp_type` ( `clinic_id`, `kp_type_id`, `target`, `partner_id`, `date_added`, `activity_stamp`, `user_id`) "
                    . "VALUES ( '$last_clinic_id', '$kp_type', '$kp_target', '$partner_name', '$date_today', CURRENT_TIMESTAMP, '$user_id')");
        }


        if ($clinic_region_query) {
            return TRUE;
        } else {
            echo mysqli_error($mysqli);
        }
    }

    function kp_type_registration($kp_type_name, $abbrv) {
        $mysqli = $this->db_connection();
        $user_id = 1;

        $date_added = date("Y-m-d H:i:s");
        $query = "INSERT INTO `kp_types` (`id`, `Name`, `Abbrv`, `date_added`, `activity_timestamp`, `user_id`) VALUES (NULL, '$kp_type_name', '$abbrv', '', '$date_added', '')";

        $add_new_kp_type = mysqli_query($mysqli, $query);
        if ($add_new_kp_type) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function hot_spot_registration($hot_spot_name, $user_partner_name, $user_clinic_name, $date_added, $region_name) {
        $link = $this->db_connection();
        $user_id = $_SESSION['uid'];
        $query = "INSERT INTO hot_spot (`name`,`region_id`,`partner_id`,`clinic_id`,`user_id`,`date_added`)"
                . " VALUES ('$hot_spot_name','$region_name','$user_partner_name','$user_clinic_name','$user_id','$date_added')";
        if (mysqli_query($link, $query)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function partner_registration($kp_types, $date_today, $partner_kp_target, $partner_region_name, $partner, $contactpersonname, $contactpersonphone, $contactpersonemail, $kp_typess, $kp_targetss, $region_namess) {
        $mysqli = $this->db_connection();
        $user_id = 1;
        $my_date = date("Y-m-d H:i:s");
        $q = mysqli_query($mysqli, "INSERT INTO partner (partner_name,contact_person,contact_person_phone,contact_person_email,user_id, date,kp_target) "
                . "VALUES ('" . $partner . "','" . $contactpersonname . "','" . $contactpersonphone . "','" . $contactpersonemail . "','" . $user_id . "','$date_today','$partner_kp_target')");
        $last_partner_id = mysqli_insert_id($mysqli);
        $temp = $kp_typess;
        for ($i = 0; $i < count($temp); $i++) {
            $kp_type = $kp_typess[$i];
            $kp_target = $kp_targetss[$i];
            $region_names = $region_namess[$i];
            $partner_region_kps = mysqli_query($mysqli, "INSERT INTO `partner_region_kps` (`id`, `partner_id`, `region_id`, `kp_type_id`, `kp_target`, `date_added`, `activity_stamp`) VALUES (NULL, '$last_partner_id', '$region_names', '$kp_type', '$kp_target', '$date_today', CURRENT_TIMESTAMP);");
        }

        foreach ($partner_region_name as $value) {
            $region_id = $value;
            $region = mysqli_query($mysqli, "INSERT INTO partner_region(partner_id,region_id,user_id) VALUES ('" . $last_partner_id . "','" . $region_id . "','" . $user_id . "')  ");
        }

        foreach ($kp_types as $value) {
            $kp_id = $value;
            $kp = mysqli_query($mysqli, "INSERT INTO kp_partner (partner_id,kp_id,user_id) VALUES ('" . $last_partner_id . "','" . $kp_id . "','" . $user_id . "')");
            if ($kp) {
                return TRUE;
            } else {

                return FALSE;
            }
        }




        mysqli_close($mysqli);
    }

    public static function kp_registration($kp_type, $client_name, $d_o_b, $tel, $gender, $education_level, $disc_number, $unique_id, $place_of_birth, $marital_status, $clinic_name, $facility_id, $name_used_in_clinic) {
        $mysqli = $this->db_connection();

        global $kp;
        global $fnme;
        global $lnme;
        global $gen;
        global $dob;
        global $tel;
        $kp = $kp_type;
        $fnme = $client_name;
        $lnme = $client_name;
        $gen = $gender;
        $dob = $d_o_b;
        $tel = $tel;

        $kabrv = "";
        $uid = "";

//Get the KP addrev
        $query = "SELECT Abbrv FROM kp_types WHERE id = '$kp'";

        $result = mysqli_query($mysqli, $query) or die('[{"Abbrv":"' . mysqli_error($mysqli) . '"}]');

        if (mysqli_num_rows($result) > 0) {
            while ($post = mysqli_fetch_assoc($result)) {
//$posts[] = $post;
                $kabrv = $post["Abbrv"];

//echo "KP_Abbrv" . $kabrv;
            }

//Derive First Letter from Last Name
//$ln = $lnme[0];
//Register Patient into database

            $query = "INSERT INTO patients (F_Name,L_Name,Gender,Dob,Tel) 
VALUES('$fnme','$lnme','$gen','$dob','$tel') ";

            mysqli_query($mysqli, $query) or die("'[{'Rslt : " . mysqli_error($mysqli) . "'}]'");

            if (mysqli_affected_rows($mysqli) > 0) {
//Get the unique record id
                $uid = mysqli_insert_id($mysqli);
//echo "Record id " . $uid;
//Generate the incremental id
//1.Get the Last Record unique id
                $ud = intval($uid - 1);
// echo "Previous record".$ud;
                $query = "SELECT Key_Id FROM patients WHERE id = '$ud'";

//echo "Query " . $query;

                $result = mysqli_query($mysqli, $query) or die('[{"Key_Id":"' . mysqli_error($mysqli) . '"}]');

                if (mysqli_num_rows($result) > 0) {
//Extract unique id
                    $unqid = "";
                    while ($post = mysqli_fetch_assoc($result)) {
                        $unqid = explode("-", $post["Key_Id"]);
//echo "Unique ID ".$unqid[1]; 
                    }
//echo "Record Id".$uid;            
                    if (intval($uid) > 1) {
//See if the year is current
                        $cy = date("y");
//Extract unique id from date
//echo "Current Year ". $cy;
//echo "Date comparator ".substr($unqid[1], 0, -1);
                        $cmpr = explode(".", $unqid[1]);
                        if ($cy == $cmpr[0]) {
//Simply Increment the current id by 1
//echo "Number to increment".$cmpr[1];
                            $newid = intval($cmpr[1]) + 1;
                            $dmnth = explode("-", $dob);
//Generate the New Key Identifyer
                            $kid = $kabrv . $lnme[0] . "-" . $cy . "." . $newid . "-" . $dmnth[1] . substr($dmnth[0], -2);
//Update record with Kip
//echo '[{"kid":"' . $kid . '"}]';
                            $query = "UPDATE patients SET Key_Id = '$kid' WHERE id = '$uid'";
                            $result = mysqli_query($mysqli, $query) or die('[{"Key_Id":"' . mysqli_error($mysqli) . '"}]');
                            if (mysqli_affected_rows($mysqli) > 0) {
                                include "phpqrcode/qrlib.php";

// create a QR Code with this text and display it
                                QRcode::png("$kid", "generated_images/$kid.png", "L", 4, 4);
                                echo '[{"kid":"' . $kid . '"}]';
                            } else {
                                echo '[{"kid":"0"}]';
                            }
                        } else {
                            $newid = "1";
                            $dmnth = explode("-", $dob);
//Generate the New Key Identifyer
                            $kid = $kabrv . $lnme[0] . "-" . $cy . "." . $newid . "-" . $dmnth[1] . substr($dmnth[0], -2);
//Update record with Kip
//echo '[{"kid":"' . $kid . '"}]';
                            $query = "UPDATE patients SET Key_Id = '$kid' WHERE id = '$uid'";
                            $result = mysqli_query($mysqli, $query) or die('[{"Key_Id":"' . mysqli_error($mysqli) . '"}]');
                            if (mysqli_affected_rows($mysqli) > 0) {
                                include "phpqrcode/qrlib.php";

// create a QR Code with this text and display it
                                QRcode::png("$kid", "generated_images/$kid.png", "L", 4, 4);
                                echo '[{"kid":"' . $kid . '"}]';
                            } else {
                                echo '[{"kid":"0"}]';
                            }
                        }
                    } else {
                        $newid = "1";
                        $dmnth = explode("-", $dob);
//Generate the New Key Identifyer
                        $kid = $kabrv . $lnme[0] . "-" . $cy . "." . $newid . "-" . $dmnth[1] . substr($dmnth[0], -2);
//Update record with Kip
//echo '[{"kid":"' . $kid . '"}]';
                        $query = "UPDATE patients SET Key_Id = '$kid' WHERE id = '$uid'";
                        $result = mysqli_query($mysqli, $query) or die('[{"Key_Id":"' . mysqli_error($mysqli) . '"}]');
                        if (mysqli_affected_rows($mysqli) > 0) {


//include "../phpqrcode/qrlib.php";
                            include "phpqrcode/qrlib.php";

// create a QR Code with this text and display it
                            QRcode::png("$kid", "generated_images/$kid.png", "L", 4, 4);
//return TRUE;
                            echo 'KP ID' . $kid . '</br>';
                        } else {
                            echo '[{"kid":"0"}]';
                            return FALSE;
                        }
                    }
                }
            }
        } else {
            echo '[{"kid":"0"}]';

//Ensure to erase this
        }
    }

    public function generate_qr() {
        
    }

    function register() {

        $fnme = $_POST['Fnme'];

        $lnme = $_POST['Lnme'];

        $gen = $_POST['Gender'];

        $dob = $_POST['Dob'];

        $tel = $_POST['Tel'];

        $ms = $_POST['MS'];

        $edu = $_POST['EduLvl'];

        $kp = $_POST['KP'];

        $pic = $_POST['Pic'];

        $usr = $_POST['Usr'];

        $admin = $_POST['Admn'];

        $updte = $_POST['Updte'];

        $rid = $_POST['Rid'];

        $role = $_POST['Role'];

        $rdata = $_POST['RData'];

        $tme = $_POST['Time'];

        $dte1 = $_POST['Dte1'];

        $dte2 = $_POST['Dte2'];

        $clnc = $_POST['Clinic'];

        $nmeusd = $_POST['NmeUsd'];

        $dscno = $_POST['Dscno'];

        $kpuid = $_POST['UUID'];



        if (function_exists($_POST['task'])) {

//connect to the db
            $user = 'roots';
            $pswd = 'root';
            $db = 'e_marps';
            $conn = mysql_connect('localhost', $user, $pswd);
            mysql_select_db($db, $conn);

            $_POST['task']();
        } else {

            echo '["rslt":"No such function"]';
        }
    }

    function getClinics() {

//Load the clinics

        $query = "SELECT * FROM facility LIMIT 50";

        $result = mysql_query($query) or die('[{"id":"' . mysql_error() . '"}]');

        if (mysql_num_rows($result) > 0) {

            while ($post = mysql_fetch_assoc($result)) { //
                $posts[] = $post;
            }

            echo json_encode($posts);
        } else {

            echo '["id":"0"]';
        }
    }

    function regKPClnc() {

//Register the KP clinic/welness center visit disc no and clinic name
        global $clnc;
        global $dscno;
        global $nmeusd;
        global $kpuid;

        $query = "INSERT INTO clinic_visit (uuid,wellness_center_visit,disc_no,kp_name) 

VALUES('$kpuid','$clnc','$dscno','$nmeusd') "; //remeber to add nme used in clinic to table

        mysql_query($query) or die("'[{'Rslt : " . mysql_error() . "'}]'");
    }

    function getKp() {

//Get the bio data from the partner databases and generate a key id field for it
    }

    function getKps() {

//Get the KP choices

        $query = "SELECT * FROM kp_types ";



        $result = mysql_query($query) or die('[{"id":"' . mysql_error() . '"}]');
        if (mysql_num_rows($result) > 0) {

            while ($post = mysql_fetch_assoc($result)) { //
                $posts[] = $post;
            }

            echo json_encode($posts);
        } else {

            echo '["id":"0"]';
        }
    }

    function genKI() {

//Generate a key identifier for bio data

        global $kp;

        global $fnme;

        global $lnme;

        global $gen;

        global $dob;

        global $tel;

        global $ms;

        global $edu;

        global $kpuid;
        $kabrv = "";

        $uid = "";

//Get the KP addrev

        $query = "SELECT Abbrv FROM kp_types WHERE id = '$kp'";
        $result = mysql_query($query) or die('[{"Abbrv":"' . mysql_error() . '"}]');
        if (mysql_num_rows($result) > 0) {

            while ($post = mysql_fetch_assoc($result)) {
                $kabrv = $post["Abbrv"];
            }


            $query = "INSERT INTO kp_bio_data (firstname,lastname,gender,dob,tel,marital_status,education_level) 

VALUES('$fnme','$lnme','$gen','$dob','$tel','$ms','$edu') ";
            mysql_query($query) or die("'[{'Rslt : " . mysql_error() . "'}]'");
            if (mysql_affected_rows() > 0) {

//Get the unique record id

                $uid = mysql_insert_id();

//echo "Record id " . $uid;
//Generate the incremental id
//1.Get the Last Record unique id

                $ud = intval($uid - 1);

// echo "Previous record".$ud;

                $query = "SELECT uuid FROM kp_bio_data WHERE id = '$ud'";

//echo "Query " . $query;
                $result = mysql_query($query) or die('[{"Key_Id":"' . mysql_error() . '"}]');
                if (mysql_num_rows($result) > 0) {

//Extract unique id

                    $unqid = "";

                    while ($post = mysql_fetch_assoc($result)) {

                        $unqid = explode("-", $post["uuid"]);

//echo "Unique ID ".$unqid[1]; 
                    }

//echo "Record Id".$uid;            

                    if (intval($uid) > 1) {

//See if the year is current

                        $cy = date("y");

//Extract unique id from date
//echo "Current Year ". $cy;
//echo "Date comparator ".substr($unqid[1], 0, -1);

                        $cmpr = explode(".", $unqid[1]);

                        if ($cy == $cmpr[0]) {

//Simply Increment the current id by 1
//echo "Number to increment".$cmpr[1];

                            $newid = intval($cmpr[1]) + 1;

                            $dmnth = explode("-", $dob);

//Generate the New Key Identifyer

                            $kid = $kabrv . $lnme[0] . "-" . $cy . "." . $newid . "-" . $dmnth[1] . substr($dmnth[0], -2);

//Update record with Kip
//echo '[{"kid":"' . $kid . '"}]';

                            $query = "UPDATE kp_bio_data SET uuid = '$kid' WHERE id = '$uid'";

                            $result = mysql_query($query) or die('[{"Key_Id":"' . mysql_error() . '"}]');

                            if (mysql_affected_rows() > 0) {

                                echo '[{"kid":"' . $kid . '"}]';

                                $kpuid = $kid;

                                regKPClnc();
                            } else {

                                echo '[{"kid":"0"}]';
                            }
                        } else {

                            $newid = "1";

                            $dmnth = explode("-", $dob);

//Generate the New Key Identifyer

                            $kid = $kabrv . $lnme[0] . "-" . $cy . "." . $newid . "-" . $dmnth[1] . substr($dmnth[0], -2);

//Update record with Kip
//echo '[{"kid":"' . $kid . '"}]';

                            $query = "UPDATE kp_bio_data SET uuid = '$kid' WHERE id = '$uid'";

                            $result = mysql_query($query) or die('[{"Key_Id":"' . mysql_error() . '"}]');

                            if (mysql_affected_rows() > 0) {

                                echo '[{"kid":"' . $kid . '"}]';

                                $kpuid = $kid;

                                regKPClnc();
                            } else {

                                echo '[{"kid":"0"}]';
                            }
                        }
                    } else {

                        $newid = "1";

                        $dmnth = explode("-", $dob);

//Generate the New Key Identifyer

                        $kid = $kabrv . $lnme[0] . "-" . $cy . "." . $newid . "-" . $dmnth[1] . substr($dmnth[0], -2);

//Update record with Kip
//echo '[{"kid":"' . $kid . '"}]';

                        $query = "UPDATE kp_bio_data SET uuid = '$kid' WHERE id = '$uid'";

                        $result = mysql_query($query) or die('[{"Key_Id":"' . mysql_error() . '"}]');

                        if (mysql_affected_rows() > 0) {

                            echo '[{"kid":"' . $kid . '"}]';

                            $kpuid = $kid;

                            regKPClnc();
                        } else {

                            echo '[{"kid":"0"}]';
                        }
                    }
                }
            }
        } else {

            echo '[{"kid":"0"}]';



//Ensure to erase this
        }
    }

}

?>