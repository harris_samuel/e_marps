<?php

include 'db_connect.php';

$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$facilities = mysqli_query($link, "SELECT facility_name, facility_id,facility_code,county FROM facility where is_registered='No' ");


// Cleaning up the term
$term = trim(strip_tags($_GET['term']));
// Rudimentary search
$matches = array();


while ($facility = mysqli_fetch_array($facilities, MYSQLI_BOTH)) {
    if (stripos($facility['facility_name'], $term) !== false) {
        // Add the necessary "value" and "label" fields and append to result set
        $facility['value'] = $facility['facility_name'];
        $facility['label'] = "{$facility['facility_name']}, {$facility['facility_code']} {$facility['facility_id']}{$facility['county']}";
        $matches[] = $facility;
    }
}
// Truncate, encode and return the results
$matches = array_slice($matches, 0, 5);
print json_encode($matches);
?>