<?php
include 'db_connect.php';

$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$name = $_GET["name"];

$pieces = explode(" ", $name);

$firstname = $pieces[0]; // piece1
$lastname = $pieces[1]; // piece2

$namesearch = "firstname LIKE '%".$firstname."%' AND lastname LIKE '%".$lastname."%'";

if($firstname == ""){
	echo "<div class=\"kp_search_results_box\">Please remove any whitespace characters before the name and try again.</div>";
	exit;
}

if($lastname == ""){
	$nolastname = "Last name: Not specified. Searching for any occurrence of the name given.";
	$namesearch = "(firstname LIKE '%".$firstname."%' OR lastname LIKE '%".$firstname."%')";
}

$gender = $_GET["gender"];
$dob = $_GET["dob"];
$mobile = $_GET["mobile"];
$residential_area = $_GET["residential_area"];
$home_area = $_GET["home_area"];

$radio_andor1 = $_GET["radio_andor1"];
$radio_andor2 = $_GET["radio_andor2"];
$radio_andor3 = $_GET["radio_andor3"];
$radio_andor4 = $_GET["radio_andor4"];
$radio_andor5 = $_GET["radio_andor5"];

if ($dob > 1) {
	$dobsearch = "AND dob LIKE '%".$dob."%'";
}
else {
	$dobsearch = "";
}

if(strlen(trim($mobile)) > 1){
	$mobilesearch = "AND tel LIKE '%".$mobile."%'";
}
else {
	$mobilesearch = "";
}

if(strlen(trim($residential_area)) > 1){
	$residentialsearch = "AND residence LIKE '%".$residential_area."%'";
}
else {
	$residentialsearch = "";
}

if(strlen(trim($home_area)) > 1){
	$homesearch = "AND place_of_birth LIKE '%".$home_area."%'";
}
else {
	$homesearch = "";
}

$i = 0;

if ($result = $link->query("SELECT * FROM kp_bio_data WHERE ".$namesearch." ".$dobsearch." ".$mobilesearch." ".$residentialsearch." ".$homesearch." AND gender = '".$gender."'") or die(mysqli_error($link))) { $row_cnt = $result->num_rows; if (mysqli_num_rows($result)) { include"kp_search_criteria.php"; while ($row = $result->fetch_object()) { include"kp_search_results_box.php"; } } else { include"kp_search_criteria.php"; echo "<div class=\"kp_search_results_box\">No results to display. Please search again.</div>"; } } else { echo "Error: " . $link->error; }

$link->close();
?>