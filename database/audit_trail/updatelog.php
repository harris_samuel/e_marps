<?php
header("Access-Control-Allow-Origin: *");
?>
<?php
include '../db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

//values to be inserted in database table
$currentuser = $_GET["currentuser"];
$useraction = $_GET["useraction"];

$query = "INSERT INTO audit_trail (
user_id,
action
) 
VALUES(?, ?)";
$statement = $mysqli->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('is', $currentuser, $useraction);

if($statement->execute()){

}
else{
     die('Error : ('. $mysqli->errno .') '. $mysqli->error);
}
$statement->close();
?>