<?php
header("Access-Control-Allow-Origin: *");
?>
<?php
include '../db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

                    // get the records from the database
                    if ($result = $mysqli->query("SELECT * FROM audit_trail ORDER BY date_time DESC LIMIT 100"))
                    {
                            // display records if there are records to display
                            if ($result->num_rows > 0)
                            {
                                  

                                    while ($row = $result->fetch_object())
                                    {
                                            // set up a row for each record
											echo "<div onclick=\"document.location.href ='?currentview=userprofile&uid=" . $row->user_id . "'\" class=\"logentry\">";
											echo "<div class=\"datetimelog\">" . $row->date_time . "</div>";
											echo "<div class=\"logentryidanddate\">User ID: " . $row->user_id . "</div>";
											
											echo "<div>Action: " . $row->action . "</div>";
											echo "</div>";
                                    }

     
                            }
                            // if there are no records in the database, display an alert message
                            else
                            {
                                    echo "No results to display!";
                            }
                    }
                    // show an error if there is an issue with the database query
                    else
                    {
                            echo "Error: " . $mysqli->error;
                    }

                    // close database connection
                    $mysqli->close();

?>