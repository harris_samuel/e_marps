<?php
include 'db_connect.php';

$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

 // get the records from the database
                    if ($result = $link->query("SELECT * FROM kp_types") or die(mysqli_error($link)))
                    {
						$row_cnt = $result->num_rows;
						// echo $row_cnt;
                            // display records if there are records to display
                            if (mysqli_num_rows($result))
                            {

                                    // set table headers
echo "<select name=\"load_kp_types\" class=\"select_with_label\" id=\"load_kp_types\">";

                while ($row = $result->fetch_object())
                                    {
				echo "<option value=\"" . $row->id . "\">" . $row->Name . "</option>";
                                    }

echo "<option value=\"Unknown\">Unknown</option></select>"; 
                            }
                            // if there are no records in the database, display an alert message
                            else
                            {
                                    echo "No results to display.";
                            }
                    }
                    // show an error if there is an issue with the database query
                    else
                    {
                            echo "Error: " . $link->error;
                    }

                    // close database connection
                    $link->close();

?>
