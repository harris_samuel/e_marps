<?php
include 'database/db_connect.php';

$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
?><?php
session_start();
include 'database/websrvc.php';

$websrvc = new Websrvc();

if (isset($_REQUEST['submit'])) {
    extract($_REQUEST);

    $login = $websrvc->check_login($emailusername, $password);
    if ($login) {
        header("location:logged_in_update_audit_trail.php");
    } else {
        // Registration Failed
        echo 'Wrong username/email or password';
    }
}
?>
<!DOCTYPE html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-MARPS </title>
        
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

        <script src="js/jquery.min.js"></script>
        <script src="js/tinybox.js"></script>
        <script src="js/user_authentication.js"></script>
        <script src="js/conditional.js"></script>
        
<script>
$( document ).ready(function() {
    window.localStorage.clear();
});
</script>

        <link href="animate.min.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" href="style.css" />
            <link rel="stylesheet" href="fieldstyles.css" />
            <script language="javascript" type="text/javascript">

                function submitlogin() {
                    var form = document.login;
                    if (form.emailusername.value == "") {
                        alert("Enter email or username.");
                        return false;
                    }
                    else if (form.password.value == "") {
                        alert("Enter password.");
                        return false;
                    }
                }
            </script>
    </head>

    <body>
        <div id="form" class="form animated fadeInUp" style="width:500px;">
            <h2>Login to e-Marps</h2>
            <form action="" method="post" name="login">
                <table width="100%">
                    <tr>
                        <th class="loginfielddesc" align="left">Username or Email: </th>
                        <td><input class="fieldstyle" type="text" name="emailusername" required></td>
                    </tr>
                    <tr>
                        <th class="loginfielddesc" align="left">Password:</th>
                        <td><input class="fieldstyle" type="password" name="password" required></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input class="large_button" type="submit" name="submit" value="Login" onclick="return(submitlogin());"></td>
                    </tr>

                    <tr>
                        <td>
                            <a href="recover_password.php">Forgot Password ? </a>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>