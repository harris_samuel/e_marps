<script>

confirmmessage_new = "The client you are adding will be set as active. Continue?";

function seteditclient_new() {
updatenewnotificationstatus2_new();
}

function updatenewnotificationstatus2_new () {

var currentservice = localStorage.getItem('currentservice');
var currentuuid = localStorage.getItem('currentuuid');
var currentclientname = localStorage.getItem('currentclientname');
var activeclientidstatusupdate = localStorage.getItem('activeclientidstatusupdate');

// alert(activeclientidstatusupdate);
	
$.get( "database/audit_trail/updatelog.php?currentuser=<?php echo $uid ?>&useraction=Client "+currentclientname+" was moved to "+currentservice+"", function( data ) {
var activeclientidstatusupdate = localStorage.getItem('activeclientidstatusupdate');
var currentservice = localStorage.getItem('currentservice');

var statusupdateurl = "database/kp_active/updatestatus.php?statusupdate="+currentservice+"&activeclientidstatusupdate="+activeclientidstatusupdate+"";
// alert(statusupdateurl);

$.get( ""+statusupdateurl+"", function( data ) {
console.log('Status was changed');

$.get( "database/audit_trail/updatelog.php?currentuser=<?php echo $uid ?>&useraction=Client "+currentclientname+" was added to "+currentservice+"", function( data ) {

  updatenewnotificationstatusaddnew2_new();
  
 });
});
});
}

function updatenewnotificationstatusaddnew2_new () {

// redirect url?
var gotoloc = localStorage.getItem('gotoloc');
var host_name = window.location.host;
$.get( host_name+"marps/database/audit_trail/notificationstatus.php?task=addnew", function( data ) {
  setTimeout(function(){
window.location.href = '?currentview='+gotoloc+'';
}, 2000);
});

}

// movetocounsellingqueue
function movetocounsellingqueue_new() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'waiting';
currentservice = 'Counselling - Queue';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient_new();

}

// movetoconsultationqueue
function movetoconsultationqueue() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'waiting';
currentservice = 'Consultation - Queue';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient_new();

}

// movetocareandtreatmentqueue
function movetocareandtreatmentqueue() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'waiting';
currentservice = 'Care and Treatment - Queue';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient_new();

}

// movetopharmacyqueue
function movetopharmacyqueue() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'waiting';
currentservice = 'Pharmacy - Queue';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient_new();

}

// addtocounselling
function movetocounselling() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'counselling';
currentservice = 'Counselling';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient_new();

}


// addtoconsultation

function movetoconsultation_new() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'consultation';
currentservice = 'Consultation';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient_new();

}


// addtocareandtreatment

function movetocareandtreatment_new() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'careandtreatment';
currentservice = 'Care and Treatment';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient_new();

}

// addtopharmacy

function movetopharmacy_new() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'pharmacy';
currentservice = 'Pharmacy';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient_new();

}
</script>

 <table border="0" align="center" cellpadding="5">
  <tr>
    <td>

<input onClick="movetocounselling_new();" class="small_button" type="button" value="Counselling">
<div onClick="movetocounsellingqueue_new();" data-tip="Move to Counselling Queue" class="waiting_button tip"></div>

</td>
</tr>
<tr><td>
<input onClick="movetoconsultation_new();" class="small_button" type="button" value="Consultation">
<div onClick="movetoconsultationqueue_new();" data-tip="Move to Consultation Queue" class="waiting_button tip"></div>
</td>
</tr>
<tr><td>
<input onClick="movetocareandtreatment_new();" class="small_button" type="button" value="Care and Treatment">
<div onClick="movetocareandtreatmentqueue_new();" data-tip="Move to Care and Treatment Queue" class="waiting_button tip"></div>
</td>
</tr>
<tr><td>
<input onClick="movetopharmacy_new();" class="small_button" type="button" value="Pharmacy">
<div onClick="movetopharmacyqueue_new();" data-tip="Move to Pharmacy Queue" class="waiting_button tip"></div>
</td>
  </tr>
</table>