<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>E-MARPS </title>

        <script src="js/jquery.min.js"></script>
        <script src="js/tinybox.js"></script>
        <script src="js/user_authentication.js"></script>
        <script src="js/conditional.js"></script>

        <link href="animate.min.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" href="style.css" />
            <link rel="stylesheet" href="fieldstyles.css" />
            <script language="javascript" type="text/javascript">

                function submitlogin() {
                    var form = document.reset_password;
                    if (form.password_2.value !== form.password.value) {
                        alert("Your password did not match .");
                        return false;
                    } else if (form.password.value === "") {
                        alert("Please enter your new password .");
                        return false;
                    } else if (form.password_2.value === "") {
                        alert("Please re-enter your new password.");
                        return false;
                    }
                }
            </script>



    </head>
    <?php
    include 'database/db_connect.php';

    $link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    ?><?php
    session_start();
    include 'database/websrvc.php';
    $uid = $_SESSION['uid'];
    $websrvc = new Websrvc();

    if (isset($_REQUEST['submit'])) {
        extract($_REQUEST);
        $token_key = $_GET['uq'];

        $reset_password = $websrvc->reset_password($password, $password_2, $token_key);
        ?>


        <?php
        if ($reset_password) {
            // Reset Password Success
            ?>
            <script src="js/jquery.min.js"></script>

            <script type="text/javascript">
                        $(document).ready(function () {
                            var base_url = "/emarps/login.php";
                            setInterval(function () {

                                $(location).attr('href', base_url);
                            }, 3000);
                        });</script>
            <!-- noty -->
            <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
            <script type="text/javascript">
                        $(document).ready(function () {
                            noty({text: 'Password reset successful...you will redirected to Login page in 5 Seconds .'});
                        });
            </script>
            <?php
            header("refresh:5;url=login.php");
        } else {
            // Reset Password Failed
            ?>

            <script type="text/javascript">
                $(document).ready(function () {
                    noty({text: 'Oops ...Something went wrong, please try again or  contact help desk for assistance .'});
                });
            </script>
            <?php
        }
    }
    ?>
    <body>
        <div id="form animated fadeIn" class="form animated fadeIn">
            <h1>E-Marps / Please enter your  Password</h1>
            <form action="" method="post" name="reset_password">
                <table class="table " width="400">
                    <tr>

                        <th> <label class="fieldstyle_with_label"> Reset Password   </label> </th>
                        <td><input type="password" name="password" required></td>
                        <td><input type="password" name="password_2" required/></td>

                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td><input class="button" type="submit" name="submit" value="Reset Password" onclick="return(submitlogin());"/></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <a href="login.php" class="button">Login</a>
                        </td>

                    </tr>

                </table>
            </form>
        </div>
    </body>
</html>