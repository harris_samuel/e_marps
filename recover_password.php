<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-MARPS</title>

        <script src="js/jquery.min.js"></script>
        <script src="js/tinybox.js"></script>
        <script src="js/user_authentication.js"></script>
        <script src="js/conditional.js"></script>

        <link href="animate.min.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" href="style.css" />
            <link rel="stylesheet" href="fieldstyles.css" />
            <script language="javascript" type="text/javascript">

                function submitlogin() {
                    var form = document.recovery_mail;
                    if (form.email.value == "") {
                        alert("Please enter your Email.");
                        return false;
                    }
                }
            </script>



    </head>
    <?php
    include 'database/db_connect.php';

    $link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    ?><?php
    session_start();
    include 'database/websrvc.php';

    $websrvc = new Websrvc();

    if (isset($_REQUEST['submit'])) {
        extract($_REQUEST);

        $recover_pasword = $websrvc->recover_pasword($email);
        ?>

        <?php
        if ($recover_pasword) {
            // Registration Success
            ?>

            <script src="js/jquery.min.js"></script>


            <!-- noty -->
            <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
            <script type="text/javascript">
                        $(document).ready(function () {
                            noty({text: 'Email has been sent to your account with a detailed explaination on how to reset your password.'});
                        });
            </script>
            <?php
        } else {
            // Registration Failed
            ?>

            <script type="text/javascript">
                $(document).ready(function () {
                    noty({text: 'Oops ...Something went wrong, please try again or  contact help desk for assistance .'});
                });
            </script>
            <?php
        }
    }
    ?>

    <body>
        <div id="form" class="form animated fadeInUp" style="width:500px;">
            <h2>Reset Password</h2>

            <form action="" method="post" name="recovery_mail">
                <table class="table " width="100%">
                    <tr>
                        <th align="left" class="loginfielddesc">  Email    </th>
                      <td><input class="fieldstyle" type="email" name="email" required="" /></td>
                    </tr>

                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input class="large_button" type="submit" name="submit" value="Send Password Recovery Email" onclick="return(submitlogin());"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <a href="login.php" class="button">Login</a>
                        </td>

                    </tr>

                </table>
            </form>
        </div>
    </body>
</html>