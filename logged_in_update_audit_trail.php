<?php
session_start();
include 'database/websrvc.php';
include 'database/db_connect.php';
$websrvc = new Websrvc();

$uid = $_SESSION['uid'];

if (!$websrvc->get_session()) {
    header("location:login.php");
}

if (isset($_GET['q'])) {
    $websrvc->user_logout();
    header("location:login.php");
}
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
?>
<!doctype html>
<html>
<head>
<title>e-MARPS</title>
<meta charset="utf-8">
<script src="js/jquery.min.js"></script>
<script>
function updatenewnotificationstatus () {
$.get( "database/audit_trail/notificationstatus.php?task=addnew", function( data ) {
  document.location.href = 'main.php';
});
}
$( document ).ready(function() {
$.get( "database/audit_trail/updatelog.php?currentuser=<?php echo $uid ?>&useraction=User ID <?php echo $uid ?> signed in", function( data ) {
  updatenewnotificationstatus();
});
});
</script>
</head>

<body>
</body>
</html>