<?php include'config.php';?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome to eMARPS</title>
<style type="text/css">
body {
	background-image: url(loginscreen_background.png);
	background-size: cover;
	text-align: center;
	margin: 0px;
	padding: 0px;
	font-family: sans-serif;
}
#logo {
	margin-top: 100px;
	height: 200px;
}
.large_button {
	line-height: 40px;
	background-image: url(largebutton.png);
	background-repeat: repeat-x;
	text-align: center;
	width: 200px;
	border: 1px solid #ACACAC;
	cursor: pointer;
	transition: 0.5s;
	background-color: #FFF;
}
#versioncode {
	font-size: 14px;
	color: #000;
	background-color: #FFF;
	text-align: center;
	width: 400px;
	margin-right: auto;
	margin-left: auto;
	padding-top: 3px;
	padding-bottom: 3px;
	cursor: default;
}
.inlayerwindow .layerwindowclose:hover, .inlayerwindow_large .layerwindowclose:hover, .large_button:hover, .small_button:hover, .layer_close_x:hover {
	background-position: 0px 100px;
	border: 1px solid #0070C0;
	transition: 0.5s;
	background-color: #0070C0;
	color: #FFF;
}
:focus {outline:none;}
#organisations {
	background-color: #FFF;
	width: 100%;
}
</style>
<link href="animate.min.css" rel="stylesheet" type="text/css">

<script src="js/jquery.min.js"></script>

<script type="text/javascript">
$( document ).ready(function() {
	
$.get( "/emarps_updates/newversion.php?versioncode=<?php echo $emarps_version ?>&installationid=<?php echo $installation_id ?>", function( data ) {
  
if (data == "up_to_date") {
	 $( "#version_check_output" ).html('Update check: No update available.');
}
  
if (data == "update_available") {
	 $( "#version_check_output" ).html('Update available. Please contact support!');
	 alert('Update available. Please contact support!');
} 
  
});

});
</script>

<link rel="shortcut icon" href="/emarps/favicon.ico" type="image/x-icon">
<link rel="icon" href="/emarps/favicon.ico" type="image/x-icon">

</head>

<body>
<img class="animated fadeIn" id="logo" src="logo_login.png">

<br>
<br>
<br>
<br>
<br>

<div class="animated fadeIn andel3" id="buttons">

<button class="large_button" onClick="
document.getElementById('logo').className = 'animated fadeOutUp';
document.getElementById('organisations').className = 'animated fadeOut';
document.getElementById('versioncode').className = 'animated fadeOut';
document.getElementById('buttons').className = 'animated fadeOutDown';
setTimeout(function(){location.href = 'main.php';},1000);">
Start
</button>




</div>

<br>
<br>
<br>
<div class="animated fadeIn andel6" id="versioncode">
  <p>Current network status: <span id="status">Checking...</span><br>
    Installed eMARPS version: <?php echo $emarps_version ?></p>
  
<img align="absmiddle" src="images/system-software-update-3.png" width="40" style="padding-right:5px;"> 
  
<span id="version_check_output"></span>
<br><br>

<script>
var addEvent = (function () {
  if (document.addEventListener) {
    return function (el, type, fn) {
      if (el && el.nodeName || el === window) {
        el.addEventListener(type, fn, false);
      } else if (el && el.length) {
        for (var i = 0; i < el.length; i++) {
          addEvent(el[i], type, fn);
        }
      }
    };
  } else {
    return function (el, type, fn) {
      if (el && el.nodeName || el === window) {
        el.attachEvent('on' + type, function () { return fn.call(el, window.event); });
      } else if (el && el.length) {
        for (var i = 0; i < el.length; i++) {
          addEvent(el[i], type, fn);
        }
      }
    };
  }
})();

var statusElem  = document.getElementById('status'),
    state               = document.getElementById('state');

function online(event) {
  statusElem.className = navigator.onLine ? 'Online' : 'Offline';
  statusElem.innerHTML = navigator.onLine ? 'Online' : 'Offline';
}

addEvent(window, 'Online', online);
addEvent(window, 'Offline', online);
online({ type: 'ready' });
</script>

</div>
<br>
<br>
<br>
<br>

<div class="animated fadeIn andel6" id="organisations">
  <img src="organisations.png" height="70">
</div>

</body>
</html>