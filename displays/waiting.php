<script src="../js/jquery.min.js"></script>

<style type="text/css">
#waitingroom {
	width: 900px;
}
#waitingroom td {
	background-color: #FFF;
	padding: 2px;
}
.waitingroomthead {
	color: #FFF;
	background-color: #999 !important;
	text-align: center;
}
.activeroomthead {
	color: #FFF;
	background-color: #0070C0 !important;
	text-align: center;
}
</style>

<script type="text/javascript">
$( document ).ready(function() {

setInterval(function(){
       checkqueue();
}, 3000);

});
function checkqueue() {
	
	clinic_id="8284"; 
	partner_id="1"; 
	
	$( "#group_counselling" ).load( "../database/kp_active/load_active_kp_queue.php?task=queue&partner_id="+partner_id+"&clinic_id="+clinic_id+"&group=counselling" , function() {
	});
		$( "#group_consultation" ).load( "../database/kp_active/load_active_kp_queue.php?task=queue&partner_id="+partner_id+"&clinic_id="+clinic_id+"&group=consultation" , function() {
	});
		$( "#group_careandtreatment" ).load( "../database/kp_active/load_active_kp_queue.php?task=queue&partner_id="+partner_id+"&clinic_id="+clinic_id+"&group=careandtreatment" , function() {
	});
		$( "#group_pharmacy" ).load( "../database/kp_active/load_active_kp_queue.php?task=queue&partner_id="+partner_id+"&clinic_id="+clinic_id+"&group=pharmacy" , function() {
	});
	
	
	
	
	
	
	$( "#group_counselling_inroom" ).load( "../database/kp_active/load_active_kp_queue.php?task=inroom&partner_id="+partner_id+"&clinic_id="+clinic_id+"&group=counselling" , function() {
	});
		$( "#group_consultation_inroom" ).load( "../database/kp_active/load_active_kp_queue.php?task=inroom&partner_id="+partner_id+"&clinic_id="+clinic_id+"&group=consultation" , function() {
	});
		$( "#group_careandtreatment_inroom" ).load( "../database/kp_active/load_active_kp_queue.php?task=inroom&partner_id="+partner_id+"&clinic_id="+clinic_id+"&group=careandtreatment" , function() {
	});
		$( "#group_pharmacy_inroom" ).load( "../database/kp_active/load_active_kp_queue.php?task=inroom&partner_id="+partner_id+"&clinic_id="+clinic_id+"&group=pharmacy" , function() {
	});
}
</script>

<div id="titlebar"><div id="titlebartext">Waiting Room Queue</div></div>

<table id="waitingroom" border="0">
  <tr>
    <td class="activeroomthead" width="25%">Counselling</td>
    <td class="activeroomthead" width="25%">Consultation</td>
    <td class="activeroomthead" width="25%">Care &amp; Treatment</td>
    <td class="activeroomthead" width="25%">Pharmacy</td>
  </tr>
  <tr>
    <td valign="top" id="group_counselling_inroom">Loading...</td>
    <td valign="top" id="group_consultation_inroom">Loading...</td>
    <td valign="top" id="group_careandtreatment_inroom">Loading...</td>
    <td valign="top" id="group_pharmacy_inroom">Loading...</td>
  </tr>
</table>

<table id="waitingroom" border="0">
  <tr>
    <td class="waitingroomthead" width="25%">Counselling - Queue</td>
    <td class="waitingroomthead" width="25%">Consultation - Queue</td>
    <td class="waitingroomthead" width="25%">Care &amp; Treatment - Queue</td>
    <td class="waitingroomthead" width="25%">Pharmacy - Queue</td>
  </tr>
  <tr>
    <td valign="top" id="group_counselling">Loading...</td>
    <td valign="top" id="group_consultation">Loading...</td>
    <td valign="top" id="group_careandtreatment">Loading...</td>
    <td valign="top" id="group_pharmacy">Loading...</td>
  </tr>
</table>