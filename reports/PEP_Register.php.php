<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<h1>Post Exposure Prophylaxis Register</h1>
<body>

<table border="1" style="width:100%" cellspacing="0" cellpadding="0" width="894">
  <tr>
    <td width="78" rowspan="2" valign="top"><p><strong>Date</strong></p></td>
    <td width="79" rowspan="2" valign="top"><p><strong>UUID</strong></p></td>
    <td width="76" rowspan="2" valign="top"><p><strong>Sex</strong></p></td>
    <td width="53" rowspan="2" valign="top"><p><strong>Age </strong></p></td>
    <td width="75" valign="top"><p><strong>Date of Exposure</strong></p></td>
    <td width="81" rowspan="2" valign="top"><p><strong>Nature of Exposure</strong></p></td>
    <td width="75" valign="top"><p><strong>Date of current initiation PEP</strong></p></td>
    <td width="77" rowspan="2" valign="top"><p><strong>PEP Regimen for current    exposure</strong></p></td>
    <td width="84" rowspan="2" valign="top"><p><strong>Date of completion of PEP</strong></p></td>
    <td width="106" valign="top"><p><strong>HIV status at 6 weeks</strong></p></td>
    <td width="111" rowspan="2" valign="top"><p><strong>HIV status at 6 months</strong></p></td>
  </tr>
  <tr>
    <td width="75" valign="top"><p><strong>Time of Exposure</strong></p></td>
    <td width="75" valign="top"><p><strong>Time of current initiation PEP</strong></p></td>
    <td width="106" valign="top"><p><strong>HIV status at 3 months</strong></p></td>
  </tr>
  <tr>
    <td><input name="Date"></td>
    <td><input name="UUID"></td>
    <td><input name="Sex"></td>
    <td><input name="Age"></td>
    <td><input name="Date_of_exposure/Time_of_exposure"></td>
    <td><input name="Nature_of_exposure"></td>
    <td><input name="Date_of_current_initiation_PEP/Time_of_current_initiation_PEP"></td>
    <td><input name="PEP_regimen_for_current_exposure"></td>
    <td><input name="Date_of_completion_of_PEP"></td>
    <td><input name="HIV_status_at_6_weeks/HIV_status_at_3_months"></td>
    <td><input name="HIV_status_at_6_months"></td>
  </tr>
</table>
<br><input value="Export in MSExcel" id="submit" class="export_in_excel" type="button"><br>
</body>
</html>
