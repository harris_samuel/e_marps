<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>MoH Registers</title>
</head>
<h1>HIV Testing and Counselling Register</h1>

<body>
<table border="1" cellspacing="0" cellpadding="0" width="1026">
  <tr>
    <td width="48" valign="top"><p><strong>Date</strong></p></td>
    <td width="48" valign="top"><p><strong>UUID</strong></p></td>
    <td width="42" valign="top"><p><strong>Age</strong></p></td>
    <td width="48" valign="top"><p><strong>Sex</strong></p></td>
    <td width="66" valign="top"><p><strong>strategy</strong></p></td>
    <td width="60" valign="top"><p><strong>Tested before</strong></p></td>
    <td width="54" valign="top"><p><strong>If yes, the result</strong></p></td>
    <td width="72" valign="top"><p><strong>When last tested</strong><br>
      <strong>(_month ago)</strong></p></td>
    <td width="60" valign="top"><p><strong>MARPS</strong></p></td>
    <td width="72" valign="top"><p><strong>Disability</strong></p></td>
    <td width="66" valign="top"><p><strong>Consent</strong></p></td>
    <td width="54" valign="top"><p><strong>Client tested as</strong></p></td>
    <td width="54" valign="top"><p><strong>Final result</strong></p></td>
    <td width="84" valign="top"><p><strong>Couple discordant</strong></p></td>
    <td width="72" valign="top"><p><strong>Quality control DBS collected</strong></p></td>
    <td width="54" valign="top"><p><strong>DBS result</strong></p></td>
    <td width="72" valign="top"><p><strong>TB screening</strong></p></td>
  </tr>
  <tr>
    <td><input name="Date"></td>
    <td><input name="UUID"></td>
    <td><input name="Age"></td>
    <td><input name="Sex"></td>
    <td><input name="Strategy"></td>
    <td><input name="Tested before"></td>
    <td><input name="If_Yes_the_result"></td>
    <td><input name="When_last_tested"></td>
    <td><input name="MARPS"></td>
    <td><input name="Disability"></td>
    <td><input name="Cosnsent"></td>
    <td><input name="Client_Tested_as"></td>
    <td><input name="Final_Result"></td>
    <td><input name="Couple_discordant"></td>
    <td><input name="Quality_control_DBS_collected"></td>
    <td><input name="DBS_result"></td>
    <td><input name="TB_screening"></td>
  </tr>
</table>
<br><input value="Export in MSExcel" id="submit" class="export_in_excel" type="button"><br>
</body>
</html>
