<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<h1>Cervical Cancer Register</h1>
<body>
<table border="1" style="width:100%" cellspacing="0" cellpadding="0" width="969">
  <tr>
    <td width="77" rowspan="2" valign="top"><p><strong>Date</strong></p></td>
    <td width="80" rowspan="2" valign="top"><p><strong>UUID</strong></p></td>
    <td width="72" valign="top"><p><strong>Sex</strong></p></td>
    <td width="72" rowspan="2" valign="top"><p><strong>Age</strong></p></td>
    <td width="101" rowspan="2" valign="top"><p><strong>Initial screening visit</strong></p></td>
    <td width="85" rowspan="2" valign="top"><p><strong>1 yr. follow up visit</strong></p></td>
    <td width="102" rowspan="2" valign="top"><p><strong>Screening Test VIA/VILI (specify)</strong></p></td>
    <td width="44" rowspan="2" valign="top"><p><strong>-Ve</strong></p></td>
    <td width="49" rowspan="2" valign="top"><p><strong>+Ve</strong></p></td>
    <td width="94" rowspan="2" valign="top"><p><strong>Other screening (Pap Smear)</strong></p></td>
    <td width="87" rowspan="2" valign="top"><p><strong>Pap Smear Result</strong><br>
      <strong>(specify)</strong></p></td>
    <td width="105" colspan="2" valign="top"><p><strong>HIV Status</strong></p></td>
  </tr>
  <tr>
    <td width="72" valign="top"></td>
    <td width="50" valign="top"><p><strong>-Ve</strong></p></td>
    <td width="55" valign="top"><p><strong>+Ve</strong></p></td>
  </tr>
  <tr>
    <td><input name="Date"></td>
    <td><input name="UUID"></td>
    <td><input name="Sex"></td>
    <td><input name="Age"></td>
    <td><input name="Initial_screening_visit"></td>
    <td><input name="1yr._follow_up_visit"></td>
    <td><input name="Screening_test_VIA/VILI_(specify)"></td>
    <td><input name="-Ve"></td>
    <td><input name="+Ve"></td>
    <td><input name="Other_screening(Pap_smear)"></td>
    <td><input name="Pap_smear_result(specify)"></td>
    <td><input name="-Ve"></td>
    <td><input name="+Ve"></td>
  </tr>
</table>

<br><input value="Export in MSExcel" id="submit" class="export_in_excel" type="button"><br>
</body>
</html>
