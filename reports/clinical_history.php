<?php
include 'database/reports.php';

$reports = new Reports();
?>


<script type="text/javascript"  src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css" type="text/css" />
<script type="text/javascript">

    $(document).ready(function () {
        $('#clinical_history').DataTable();
    });
</script>
<style type="text/css">
    .clinical_history {
        width: 80%;
    }
</style>

<h2>Clinical History Report</h2>

<div class="form animated fadeIn">



    <table id="clinical_history" class="clinical_history">
        <thead>
            <tr>
                <th>No</th>
                <th>Implementing Partner</th>
                <th>Date</th>
                <th>Health Worker</th>
                <th>Service Type</th>
                <th>Screened</th>
                <th>Treated</th>
                <th>Referred</th>
                <th>Facility</th>
                <th>UUID</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Implementing Partner</th>
                <th>Date</th>
                <th>Health Worker</th>
                <th>Service Type</th>
                <th>Screened</th>
                <th>Treated</th>
                <th>Referred</th>
                <th>Facility</th>
                <th>UUID</th>
            </tr>
        </tfoot>
        <tbody>


            <?php
            $uuid = $_GET['uuid'];
            $res = $reports->clinical_history($uuid);
            if (mysqli_num_rows($res) > 0) {
                $i = 1;
                while ($row = mysqli_fetch_array($res, MYSQLI_BOTH)) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['implementing_partneR']; ?></td>
                        <td><?php echo $row['Date']; ?></td>
                        <td><?php echo $row['Health_worker']; ?></td>
                        <td><?php echo $row['Service_type']; ?></td>
                        <td><?php echo $row['screened']; ?></td>
                        <td><?php echo $row['treated']; ?></td>
                        <td><?php echo $row['referred']; ?></td>
                        <td><?php echo $row['facility']; ?></td>
                        <td><?php echo $row['uuid']; ?></td>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
            } else {
                ?><tr>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>

                </tr><?php
            }
            ?>
        </tbody>
    </table>


</div>
