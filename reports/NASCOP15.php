<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<h1>Tests Per Gender and Age</h1>
<body>
<table border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="144" rowspan="2" valign="top"><p><strong>Indicators </strong><br>
      <strong>(Data Source)</strong></p></td>
    <td width="144" colspan="2" valign="top"><p align="center"><strong>&lt;15</strong></p></td>
    <td width="144" colspan="2" valign="top"><p align="center"><strong>15-24</strong></p></td>
    <td width="144" colspan="2" valign="top"><p align="center"><strong>25-49</strong></p></td>
    <td width="144" colspan="2" valign="top"><p align="center"><strong>50+</strong></p></td>
    <td width="144" rowspan="2" valign="top"><p align="center"><strong>TOTAL</strong></p></td>
  </tr>
  <tr>
    <td width="74" valign="top"><p><strong>Male</strong></p></td>
    <td width="70" valign="top"><p><strong>Female</strong></p></td>
    <td width="72" valign="top"><p><strong>Male</strong></p></td>
    <td width="72" valign="top"><p><strong>Female</strong></p></td>
    <td width="72" valign="top"><p><strong>Male</strong></p></td>
    <td width="72" valign="top"><p><strong>Female</strong></p></td>
    <td width="72" valign="top"><p><strong>Male</strong></p></td>
    <td width="72" valign="top"><p><strong>Female</strong></p></td>
  </tr>
  <tr>
    <td width="144" valign="top"><p>HTC Register</p></td>
    <td width="74" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="70" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="144" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="144" valign="top"><p>PEP Register</p></td>
    <td width="74" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="70" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="144" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="144" valign="top"><p>HIV Care and Treatment Reg.</p></td>
    <td width="74" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="70" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="144" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="144" valign="top"><p>Pre ART Register</p></td>
    <td width="74" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="70" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="144" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="144" valign="top"><p>ART Register</p></td>
    <td width="74" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="70" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="144" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="144" valign="top"><p>Cervical Cancer Register</p></td>
    <td width="74" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="70" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="144" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
  <tr>
    <td width="144" valign="top"><p>Family Planning Register</p></td>
    <td width="74" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="70" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="72" valign="top"><p><strong>&nbsp;</strong></p></td>
    <td width="144" valign="top"><p><strong>&nbsp;</strong></p></td>
  </tr>
</table>
<br><input value="Export in MSExcel" id="submit" class="export_in_excel" type="button"><br>
</body>
</html>
