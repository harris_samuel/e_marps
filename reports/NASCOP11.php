<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<h1>Pre-ART Register</h1>

<body>

<table  border="1" style="width:45%" cellspacing="0" cellpadding="0" width="1026">
  <tr>
    <td width="76" rowspan="2" valign="top"><p><strong>Date start Chronic HIV Care</strong></p></td>
    <td width="53" rowspan="2" valign="top"><p><strong>UUID</strong></p></td>
    <td width="83" rowspan="2" valign="top"><p><strong>Age at Enrollment</strong></p></td>
    <td width="36" rowspan="2" valign="top"><p><strong>Sex</strong></p></td>
    <td width="62" rowspan="2" valign="top"><p><strong>Entry Point: From Where?</strong></p></td>
    <td width="81" rowspan="2" valign="top"><p><strong>Confirmed HIV+ </strong><br>
      <strong>Date</strong></p></td>
    <td width="268" colspan="3" valign="top"><p align="center"><strong>Prophylaxis</strong></p></td>
    <td width="85" rowspan="2" valign="top"><p><strong>Follow Up(indicate lost/dead +    date)</strong></p></td>
    <td width="96" rowspan="2" valign="top"><p><strong>WHO Clinical Stage at    Enrollment +</strong><br>
      <strong>Date</strong></p></td>
    <td width="107" rowspan="2" valign="top"><p><strong>Date medically eligible for    ARVs</strong></p></td>
    <td width="79" valign="top"><p><strong>Date</strong></p></td>
  </tr>
  <tr>
    <td width="84" valign="top"><p><strong>PEP (Start/Stop Date)</strong></p></td>
    <td width="96" valign="top"><p><strong>Reason for PEP/Nature of    Exposure</strong></p></td>
    <td width="89" valign="top"><p><strong>CTX (Start/Stop Date)</strong></p></td>
    <td width="79" valign="top"><p><strong>ART Started</strong></p></td>
  </tr>
  <tr>
    <td><input name="Date_start_chronic_HIV_care"></td>
    <td><input name="UUID"></td>
    <td><input name="Age_at_enrollment"></td>
    <td><input name="Sex"></td>
    <td><input name="Entry_point_from_where"></td>
    <td><input name="Confirmed_HIV+_date"></td>
    <td><input name="PEP_(Start/Stop_Date)"></td>
    <td><input name="Reason_for_PEP/Nature_of_exposure"></td>
    <td><input name="CTX_(Start/Stop_date)"></td>
    <td><input name="Follow_up_(indicate_lost/dead_+_date)"></td>
    <td><input name="WHO_clinical_stage_at_enrollment_+_date"></td>
    <td><input name="Date_medically_eligible_for_ARVs"></td>
    <td><input name="Date_ART_Started"></td>
  </tr>
</table>
<br><input value="Export in MSExcel" id="submit" class="export_in_excel" type="button"><br>
</body>
</html>
