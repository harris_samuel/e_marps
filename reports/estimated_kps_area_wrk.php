<?php

header("Access-Control-Allow-Origin: *");
?>
<?php

include '../database/db_connect.php';

$mysqli = mysqli_connect($host_name, $user_name, $password, $database);

$filter_county = $_GET['county'];
$filter_kp_type = $_GET['kp_type'];
$filter_partner = $_GET['partner'];
$filter_date_from = $_GET['date_from'];
$filter_date_to = $_GET['date_to'];


$sql = "SELECT * FROM `no_estimated_kps` where 1 ";

if (!empty($filter_county) and ! empty($filter_kp_type) and ! empty($filter_date_from) and ! empty($filter_date_to)) {
    $sql .= "  and region_name='$filter_county' and name='$filter_kp_type' and activity_stamp between '$filter_date_from' and '$filter_date_to' ";
} elseif (empty($filter_county) and empty($filter_kp_type) and ! empty($filter_date_from) and ! empty($filter_date_to)) {
    $sql .= " and activity_stamp between '$filter_date_from' and '$filter_date_to'";
} elseif (!empty($filter_county) and empty($filter_kp_type) and ! empty($filter_date_from) and ! empty($filter_date_to)) {
    $sql .= " and region_name='$filter_county' and activity_stamp between '$filter_date_from' and '$filter_date_to'";
} elseif (!empty($filter_county) and empty($filter_kp_type) and empty($filter_date_from) and empty($filter_date_to)) {
    $sql .= " and region_name='$filter_county'";
} elseif (empty($filter_county) and ! empty($filter_kp_type) and empty($filter_date_from) and empty($filter_date_to)) {
    $sql .= " and name='$filter_kp_type'";
} elseif (!empty($filter_county) and ! empty($filter_kp_type) and empty($filter_date_from) and empty($filter_date_to)) {
    $sql .= " and name='$filter_kp_type' and region_name='$filter_county'";
} else {
    $sql.= " ";
}
$result = mysqli_query($mysqli, $sql);
$data = [];

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $data[] = $row;
}

echo json_encode($data);


// close database connection
mysqli_close($mysqli);
?>