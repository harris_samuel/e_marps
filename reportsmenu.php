<div id="reportsnavoptions">
    <div class="navoptions">
        <div class="reportsdescription"> Programme Coverage Reports</div>
        <?php
        $user_id = $_SESSION['uid'];

        $query = "select * from user_permission_list where user_id='$user_id' and location='reports' and level='1' and permissions_status='Active'  ";
        if ($result = mysqli_query($link, $query)) {
            ?>
            <?php
            $count_row = mysqli_num_rows($result);
            if ($count_row > 0) {
                while ($idresult = mysqli_fetch_row($result)) {
                    $code = $idresult[6];
                    echo $code;
                }
            } else {
                ?>
<div class="subitems"  > You Don't have access to this Report </div>
                
                <?php
             
            }
            ?>

            <?php
        }
        ?>
        <!--
                <div class="subitems" onClick="window.location.href = '?currentview=estimated_kps_area_wrk';" > Estimated KPs in area of work </div>
                <div class="subitems" onClick="window.location.href = '?currentview=kps_planned_covered';" > No. of KPs planned to be covered </div>
                <div class="subitems" onClick="window.location.href = '?currentview=site_kps_operate';" > No. of Sites/Spots KPs operate in </div>
                <div class="subitems" onClick="window.location.href = '?currentview=site_intervtn_area';" > Estimated Sites/Spots in the intervention area </div>-->


        <div class="reportsdescription"> Outreach Activities Reports</div>

        <?php
        $user_id = $_SESSION['uid'];

        $query = "select * from user_permission_list where user_id='$user_id' and location='reports' and level='2' and permissions_status='Active'  ";
       if ($result = mysqli_query($link, $query)) {
            ?>
            <?php
            $count_row = mysqli_num_rows($result);
            if ($count_row > 0) {
                while ($idresult = mysqli_fetch_row($result)) {
                    $code = $idresult[6];
                    echo $code;
                }
            } else {
                ?>
<div class="subitems"  > You Don't have access to this Report </div>
                
                <?php
             
            }
            ?>

            <?php
        }
        ?>

        <!--
                <div class="subitems" onClick="window.location.href = '?currentview=kps_contacted';" > KPs contacted </div>
                <div class="subitems" onClick="window.location.href = '?currentview=new_kps_contacted';" > Newly KPs contacted </div>
                <div class="subitems" onClick="window.location.href = '?currentview=grp_mtng_orgnzd';" > Group meetings/Events organized by KPs </div>-->


        <div class="reportsdescription">Biomedical Services Reports</div>

        <?php
        $user_id = $_SESSION['uid'];

        $query = "select * from user_permission_list where user_id='$user_id' and location='reports' and level='3' and permissions_status='Active'  ";
         if ($result = mysqli_query($link, $query)) {
            ?>
            <?php
            $count_row = mysqli_num_rows($result);
            if ($count_row > 0) {
                while ($idresult = mysqli_fetch_row($result)) {
                    $code = $idresult[6];
                    echo $code;
                }
            } else {
                ?>
<div class="subitems"  > You Don't have access to this Report </div>
                
                <?php
              
            }
            ?>

            <?php
        }
        ?>

        <!--        <div class="subitems" onClick="window.location.href = '?currentview=bio_mdcl_svrcs_uptke';" > Biomedical services uptake </div>
                <div class="subitems" onClick="window.location.href = '?currentview=cnsllng_svrcs_uptke';" > Counselling services uptake</div>
                <div class="subitems" onClick="window.location.href = '?currentview=kps_rcvd_htc_art_srvcs';" > Individual KPs received HTC and ART services</div>
                <div class="subitems" onClick="window.location.href = '?currentview=kps_rcvd_sti_scrng_trtment';" > Individual KPs received STI screening and treatment</div>
                <div class="subitems" onClick="window.location.href = '?currentview=kps_rcvd_pep_srvcs';"> Individual KPs received PEP services</div>
                <div class="subitems" onClick="window.location.href = '?currentview=kps_trtd_abscss';" > Individual KPs treated for Abscess</div>
                <div class="subitems" onClick="window.location.href = '?currentview=no_hlth_cmps_orgnzed';" > No. of Health camps organized</div>
                <div class="subitems" onClick="window.location.href = '?currentview=no_hlth_cmps_orgnzed_brdg_ppn';" > No. of Health camps organized for Bridge populations</div>
        -->


        <div class="reportsdescription"> Commodities Reports</div>

        <?php
        $user_id = $_SESSION['uid'];

        $query = "select * from user_permission_list where user_id='$user_id' and location='reports' and level='4' and permissions_status='Active'  ";
         if ($result = mysqli_query($link, $query)) {
            ?>
            <?php
            $count_row = mysqli_num_rows($result);
            if ($count_row > 0) {
                while ($idresult = mysqli_fetch_row($result)) {
                    $code = $idresult[6];
                    echo $code;
                }
            } else {
                ?>
<div class="subitems"  > You Don't have access to this Report </div>
                
                <?php
              
            }
            ?>

            <?php
        }
        ?>

        <!--        <div class="subitems" onClick="window.location.href = '?currentview=kps_rcvd_cmmdties_outrch';" > Individual KPs received commodities from Outreach Team</div>
                <div class="subitems" onClick="window.location.href = '?currentview=ndle_srnge_rtrn';" > Needle and Syringe Return</div>
        -->

        <div class="reportsdescription">Structural Activities/Interventions Reports</div>


        <?php
        $user_id = $_SESSION['uid'];

        $query = "select * from user_permission_list where user_id='$user_id' and location='reports' and level='5' and permissions_status='Active'  ";
         if ($result = mysqli_query($link, $query)) {
            ?>
            <?php
            $count_row = mysqli_num_rows($result);
            if ($count_row > 0) {
                while ($idresult = mysqli_fetch_row($result)) {
                    $code = $idresult[6];
                    echo $code;
                }
            } else {
                ?>
<div class="subitems"  > You Don't have access to this Report </div>
                
                <?php
            
            }
            ?>

            <?php
        }
        ?>

        <!--        <div class="subitems" onClick="window.location.href = '?currentview=no_advccy_wrkshps';" > No. of Advocacy Workshops</div>
                <div class="subitems" onClick="window.location.href = '?currentview=indvdl_kps_attndd';" > Individual KPs attended</div>
                <div class="subitems" onClick="window.location.href = '?currentview=vlnce_incdnts_rprtd';" > Violence Incidents reported</div>
                <div class="subitems" onClick="window.location.href = '?currentview=vlnce_incdnts_addrsd';" > Violence incidents addressed</div>-->


    </div>
</div>
