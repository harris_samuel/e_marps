<style type="text/css">
#getqrfromgoogle {
	position: absolute;
	z-index: 900;
	height: 200px;
	width: 200px;
	top: 120px;
	right: 40px;
	box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.2);
	transition: all 1s;
	display: none;
}
</style>
<div id="systembar"><a href="?currentview=start"><img src="logo.png" height="40"></a>

	<div id="navigation" onclick="open_navigation();">
	<img src="images/navigation.png" align="absmiddle" width="4">
	Open Menu</div>

	<div id="closenavigation" onclick="close_navigation();" style="display:			none;"><img src="images/navigation.png" align="absmiddle" width="4">
	Close Menu</div>

	<div id="reportsmenu" onclick="open_reportsmenu();"><img src="images/navigation.png" align="absmiddle" width="4">
	Open Reports</div>

	<div id="closereportsmenu" onclick="close_reportsmenu();" style="display:none;"><img src="images/navigation.png" align="absmiddle" width="4">
	Close Reports</div>
    
    <div id="registersmenu" onclick="open_registersmenu();"><img src="images/navigation.png" align="absmiddle" width="4">
	Open Registers</div>

	<div id="closeregistersmenu" onclick="close_registersmenu();" style="display:none;"><img src="images/navigation.png" align="absmiddle" width="4">
	Close Registers</div>
    
    <script>
    function helpmenu() {
    location.href = 'help/';
    }
    </script>
    
    <div id="helpmenu" onclick="helpmenu();">
    <img src="images/help.svg" align="absmiddle" height="30">
    Help
    </div>

<div id="accountholder">
<img title="View Profile" onclick="window.location.href='?currentview=userprofile&uid=<?php echo $uid ?>';" src="images/profile.png" style="position:absolute; height:25px; margin-left:-30px; margin-top:-5px; cursor:pointer;">
   
    


<script>
$( document ).ready(function() {
	
var currentusercheck = localStorage.getItem('currentuser');

if (currentusercheck != null) {
document.getElementById('currentuser').innerHTML = localStorage.getItem('currentuser');
}
else {
loadusername();
}

});

function loadusername() {
$( "#currentuser" ).load( "/emarps/database/getuserdetails.php?uid=<?php echo $uid ?>", function() {
  var currentuser = document.getElementById('currentuser').innerHTML;
  localStorage.setItem('currentuser', currentuser);
});
}

</script>

<span id="currentuser" style="cursor:pointer; padding-right:15px;" onclick="window.location.href='?currentview=userprofile&uid=<?php echo $uid ?>';">

</span>

<a href="signout.php"><span class="buttonc2h">Sign out</span></a></div>
</div>

</div>

<div id="opennavigation">

<?php include'mainmenu.php';?>

</div>

<div id="openreports">

<?php include'reportsmenu.php';?>

</div>

<div id="openregisters">

<?php include'registersmenu.php';?>

</div>

<script type="text/javascript">
function getqrfromgoogle() {
	
$( "#getqrfromgoogle" ).fadeIn( "slow", function() {
});
	
var codedatafromuuid = localStorage.getItem('currentuuid');

$('#getqrfromgoogle').html('<img src="https://chart.googleapis.com/chart?cht=qr&chs=200x200&chl='+codedatafromuuid+'" width="200" height="200" title="'+codedatafromuuid+'" />');

document.getElementById('getqrfromgoogle').style.display = 'block';

}
function getqrfromgooglehide() {
	
$( "#getqrfromgoogle" ).fadeOut( "slow", function() {
});

}
</script>

<div id="getqrfromgoogle"></div>

<div id="currenteditclient" onmouseover="getqrfromgoogle();" onmouseout="getqrfromgooglehide();">

<div id="currentclientname">
No active client
</div>

<div id="currentuuid">
</div>

</div>