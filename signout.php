<?php
    session_start();
	
	$uid = $_SESSION['uid'];
	
    $_SESSION = array();
    session_destroy();
?>
<!doctype html>
<html>
<head>
<title>e-MARPS</title>
<meta charset="utf-8">
<script src="js/jquery.min.js"></script>
<script>
function updatenewnotificationstatus () {
$.get( "database/audit_trail/notificationstatus.php?task=addnew", function( data ) {
  document.location.href = 'login.php';
});
}
$( document ).ready(function() {
$.get( "database/audit_trail/updatelog.php?currentuser=<?php echo $uid ?>&useraction=User ID <?php echo $uid ?> signed out", function( data ) {
  updatenewnotificationstatus();
});
});
</script>
</head>

<body>
</body>
</html>