<?php
include 'database/reports.php';

$reports = new Reports();
?>


<script type="text/javascript"  src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css" type="text/css" />
<script type="text/javascript">

    $(document).ready(function () {
        $('#clinical_history').DataTable();
    });
</script>
<style type="text/css">
    .clinical_history {
        width: 80%;
    }
</style>

<h2>Clinical History Report</h2>

<div class="form animated fadeIn">



    <table id="clinical_history" class="clinical_history">
        <thead>
            <tr>
                <th>No</th>
                <th>No of SW</th>
                <th>No of TRK</th>
                <th>No of MSM</th>
                <th>No of PWUD</th>
                <th>No of FF</th>
                <th>No of FFK</th>
                <th>No of PWUD</th>

            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>No of SW</th>
                <th>No of TRK</th>
                <th>No of MSM</th>
                <th>No of PWUD</th>
                <th>No of FF</th>
                <th>No of FFK</th>
                <th>No of PWUD</th>

            </tr>
        </tfoot>
        <tbody>


            <?php
            $res = $reports->no_indvdl_kps_contacted();
            if (mysqli_num_rows($res) > 0) {
                $i = 1;
                while ($row = mysqli_fetch_array($res, MYSQLI_BOTH)) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['Total_no_SW']; ?></td>
                        <td><?php echo $row['Total_no_TRK']; ?></td>
                        <td><?php echo $row['Total_no_MSM']; ?></td>
                        <td><?php echo $row['Total_no_PWUD']; ?></td>
                        <td><?php echo $row['Total_no_FF']; ?></td>
                        <td><?php echo $row['Total_no_FFK']; ?></td>
                        <td><?php echo $row['Total_no_PWIDU']; ?></td>
                     
                    </tr>
                    <?php
                    $i++;
                }
            } else {
                ?><tr>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                    <td >Nothing here...</td>
                   
                </tr><?php
        }
            ?>
        </tbody>
    </table>


</div>
