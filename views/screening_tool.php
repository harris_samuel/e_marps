<style type="text/css">
    #uuidresultwindow {
        width: 100%;
        position: fixed;
        z-index: 10000;
        top: 0px;
        display: none;
        left: 0px;
        height: 100%;
        background-color: rgba(0,0,0,0.2);
    }
    #inuuidresultwindow {
        background-color: #FFF;
        width: 450px;
        margin-right: auto;
        margin-left: auto;
        height: 300px;
        padding: 8px;
        -webkit-box-shadow: 5px 5px 30px 15px rgba(0,0,0,0.2);
        box-shadow: 5px 5px 30px 15px rgba(0,0,0,0.2);
        margin-top: 100px;
        z-index: 10000;
    }
    #uuidresulttext {
        font-size: 24px;
        margin-top: 10px;
    }
    #before_biometrics_buttons {
        margin-top:60px;
    }
    #before_biometrics_spinner {
        background-image: url(images/ring-alt.gif);
        background-repeat: no-repeat;
        background-position: center center;
        background-size: 100px 100px;
        width: 400px;
        height: 300px;
        background-color: #FFF;
        -webkit-box-shadow: 5px 5px 30px 15px rgba(0,0,0,0.2);
        box-shadow: 5px 5px 30px 15px rgba(0,0,0,0.2);
        display:none;
    }
</style>

<script>
    loadingindicator_start();
</script>

<div id="titlebar"><div id="titlebartext">Registration/Revisit</div></div>

<div id="uuidresultwindow">
    <div id="inuuidresultwindow">
        <div style="display:none;" id="uuidresultconvert"></div>

        <center>
            <div id="uuidresulttext"><span id="uuidresult"></span> - <span id="uuidresultname"></span></div>


            <div id="before_biometrics_buttons">

                <div onmouseover="document.getElementById('before_biometrics_checked_img').src = 'images/checked_white.svg';"
                     onmouseout="document.getElementById('before_biometrics_checked_img').src = 'images/checked.svg';"
                     class="small_button" style="width: 300px; text-align:left;" onclick="before_biometrics_enroll();"><img id="before_biometrics_checked_img" src="images/checked.svg" style="height:50px;" align="absmiddle"> Enroll Fingerprint</div>

                <div onmouseover="document.getElementById('before_biometrics_cancel_img').src = 'images/cancel_white.svg';"
                     onmouseout="document.getElementById('before_biometrics_cancel_img').src = 'images/cancel.svg';"
                     class="small_button" style="width: 300px; text-align:left; margin-top: 10px;" onclick="before_biometrics_decline();"><img id="before_biometrics_cancel_img" src="images/cancel.svg" style="height:50px;" align="absmiddle"> Decline</div>

            </div>


            <script>
                function before_biometrics_decline() {
                    document.getElementById('hide_before_biometrics').style.display = 'block';
                    document.getElementById('before_biometrics_buttons').style.display = 'none';
                }
                function before_biometrics_enroll() {
                    // alert('Start enrollment');
                    var currentuuid = localStorage.getItem('currentuuid');
                    before_biometrics_url = 'efs://reg/' + currentuuid + '|0';
                    // alert(before_biometrics_url);
                    location.href = before_biometrics_url;
                    document.getElementById('before_biometrics_buttons').style.display = 'none';
                    document.getElementById('before_biometrics_spinner').style.display = 'block';


                    after_biometrics_enroll_timer = setInterval(function () {
                        after_biometrics_enroll();
                    }, 2000);

                }
                function after_biometrics_enroll() {

                    var currentuuid = localStorage.getItem('currentuuid');

                    $.get("database/check_biometrics_enrollment.php?&task=checkifexists&uuid=" + currentuuid + "", function (data) {

                        if (data == "template_exists") {
                            // alert('Fingerprint successfully enrolled.');
                            document.getElementById('before_biometrics_spinner').style.display = 'none';
                            document.getElementById('hide_before_biometrics').style.display = 'block';
                        }
                        else if (data == "template_does_not_exist") {
                            // alert('This client has no fingerprint template.');
                        }
                    });

                }
            </script>

            <div id="before_biometrics_spinner" class="animated fadeIn">
                <div style="padding:15px;">Communicating with Fingerprint Scanner... Please wait...</div>
            </div>

            <div id="hide_before_biometrics" style="display:none;" class="animated fadeIn">

                <br><h2>Book for Service</h2>

                <?php
                include 'views/add_new_kp.php';
                ?>

            </div>

        </center>

    </div>
</div>

<?php
include 'database/db_connect.php';

$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
} else {
    ?>


    <script>
        sidebar1();
    </script>



    <div id="screening_tool_a" class="animated fadeIn">

        <script>
            function selectreferralsource() {
                referralsource = document.getElementById('select_referral_source').value;
                // alert (referralsource);

                $("#select_referral_source_results").load("/emarps/database/select_referral_source_results.php?peer_educator_id=" + referralsource + "");

            }
            function select_referral_source_ok() {
                document.getElementById("select_referral_source_ok").style.display = "block";
            }

            function referral_types() {

                var option = document.getElementById("referral_source_type").value;

                if (option == "0") {
                    document.getElementById("select_referral_source").style.display = "none";
                    document.getElementById("select_referral_source_label").style.display = "none";

                    document.getElementById("txt_referral_source_other").style.display = "none";

                    document.getElementById("txt_referral_source_other").removeAttribute("required");

                }
                if (option == "1") {
                    document.getElementById("select_referral_source").style.display = "none";
                    document.getElementById("select_referral_source_label").style.display = "none";

                    document.getElementById("txt_referral_source_other").style.display = "none";

                    document.getElementById("txt_referral_source_other").removeAttribute("required");

                }
                if (option == "2") {
                    document.getElementById("select_referral_source").style.display = "block";
                    document.getElementById("select_referral_source_label").style.display = "block";

                    document.getElementById("txt_referral_source_other").style.display = "none";

                    document.getElementById("txt_referral_source_other").removeAttribute("required");

                }
                if (option == "3") {
                    document.getElementById("select_referral_source").style.display = "none";
                    document.getElementById("select_referral_source_label").style.display = "none";

                    document.getElementById("txt_referral_source_other").style.display = "block";

                    document.getElementById("txt_referral_source_other").setAttribute("required", "required");




                }
                if (option == "4") {
                    document.getElementById("select_referral_source").style.display = "none";
                    document.getElementById("select_referral_source_label").style.display = "none";
                }

            }

            $(document).ready(function () {

                loadingindicator_stop();

                document.getElementById("load_kp_types").onchange = function () {
                    var kptypes = document.getElementById("load_kp_types").value;

                    if (kptypes == "Unknown") {
                        document.getElementById('kp_screening').style.display = 'block';
                        document.getElementById('register_uuid_button').style.display = 'none';
                    }
                    else if (kptypes != "Unknown") {
                        document.getElementById('kp_screening').style.display = 'none';
                    }
                }

            });

            function generateuuid() {

                var screening_tool_form_a = document.getElementById('screening_tool_form_a');

                if (!screening_tool_form_a.checkValidity || screening_tool_form_a.checkValidity()) {

                }
                else {
                    document.getElementById('screening_tool_form_a').submit();
                    return;
                }

                kpnamesplit();
                convertdate();

                var pid = localStorage.getItem('sess_partner_id');
                var clnc = localStorage.getItem('sess_clinic_id');

                var Fnme = document.getElementById('splitnamef1').value;
                var Lnme = document.getElementById('splitnamef2').value;
                var NckNme = document.getElementById('txt_client_nickname').value;
                var KP = document.getElementById('load_kp_types').value;
                var Dob = convertdatenoleadingzero;
                var Tel = document.getElementById('txt_client_mobile_number').value;
                var Gender = document.getElementById('select_gender').value;
                var EduLvl = document.getElementById('select_client_education_level').value;
                var Cntry = document.getElementById('select_country').value;
                var MS = document.getElementById('select_marital_status').value;
                var pob = document.getElementById('txt_client_residential_home').value;
                var residence = document.getElementById('txt_client_residential_area').value;
                var colpnt = 'Clinical';

                var clinicselection = localStorage.getItem('clinicselection');

                // alert(clinicselection);

                if (clinicselection == 'Yes') {
                    var clinic = document.getElementById('clinic_source').value;
                }
                else {
                    var clinic = '';
                    alert('No Clinic selected. UUID will be generated without.');
                }

                var uri = '/marps/database/Emarps/websrvc/websrvc.php?task=genKI&pid=' + pid + 'clnc=' + clnc + '&Fnme=' + Fnme + '&Lnme=' + Lnme + '&Edit=0&KP=' + KP + '&Dob=' + Dob + '&Tel=' + Tel + '&Gender=' + Gender + '&MS=' + MS + '&EduLvl=' + EduLvl + '&NckNme=' + NckNme + '&Cntry=' + Cntry + '&pob=' + pob + '&res=' + residence + '&clinic=' + clinic + '';
                var res = encodeURI(uri);

                // alert(res);

                alert('Data to Server: \r\n\Partner ID: ' + pid + '\nClinic ID: ' + clnc + '\nFirst Name: ' + Fnme + '\nLast Name: ' + Lnme + ' \nKP Type: ' + KP + ' \nDate of Birth: ' + Dob + ' \nTel: ' + Tel + ' \nGender: ' + Gender + '\nMarital Status: ' + MS + ' \nEducation Level: ' + EduLvl + ' \nNickname: ' + NckNme + ' \nCountry: ' + Cntry + '\nPlace of birth: ' + pob + '\nResidence: ' + residence + '\nClinic: ' + clinic + '\nCollection Point: ' + colpnt + '');

                document.getElementById('uuidresultwindow').style.display = 'block';

                $('#uuidresultconvert').load('' + res + '', function () {
                    var uuidresultconvert = document.getElementById('uuidresultconvert').innerHTML;

                    var specialChars = '!@#$^&%*()+=[]\/{}|:<>?,"';
                    for (var i = 0; i < specialChars.length; i++) {
                        uuidresultconvert = uuidresultconvert.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
                    }

                    var uuidresult = uuidresultconvert.replace("kid", "");

                    // alert(uuidresult);

                    document.getElementById('uuidresult').innerHTML = uuidresult;
                    document.getElementById('uuidresultname').innerHTML = '' + Fnme + ' ' + Lnme + '';

                    localStorage.setItem("currentuuid", uuidresult);
                    localStorage.setItem("currentclientname", '' + Fnme + ' ' + Lnme + '');
                });



            }

            function convertdate() {
                var convertdate = document.getElementById('txt_client_date_of_birth').value;

                convertdatenoleadingzero = convertdate.replace(/\b0(?=\d)/g, '')



            }

        </script>

        <form action="" id="screening_tool_form_a" name="screening_tool_form_a" class="formstyle">

            <table width="930" border="0" cellpadding="6" cellspacing="0">
                <tr>
                    <td valign="top">

                        <label>Select referral source</label>

                        <select id="referral_source_type" name="referral_source_type" class="select_with_label" onchange="referral_types();">
                            <option value="1">Self-referral</option>
                            <option value="2">Peer Educator</option>
                            <option value="3">Other</option>
                        </select>

                    </td>
                    <td valign="top"> <label style="display:none;" id="select_referral_source_label">Select Peer to show  members</label><?php
                        $query = "SELECT id, peer_educator_nme, nick_name, phone_no FROM peer_educator ORDER BY date_joined, id ASC";
                        if ($result = mysqli_query($link, $query)) {

                            //
                            //SELECT THE REFERAL SOURCE
                            ?><select onchange="selectreferralsource();" id="select_referral_source" style="display:none" class="select_with_label" name="select_referral_source"><option>Please Select</option>
                            <?php
                            while ($idresult = mysqli_fetch_row($result)) {
                                $id = $idresult[0];
                                $peer_educator_nme = $idresult[1];
                                $nick_name = $idresult[2];
                                $phone_no = $idresult[3];
                                ?>
                                    <option value="<?php echo $id; ?>"><?php echo $peer_educator_nme . '&nbsp;(' . $nick_name . ')'; ?></option>   <?php
                                }
                                ?>
                            </select>
                            <?php
                        }
                    }
                    ?>

                    <input type="text" name="txt_referral_source_other" id="txt_referral_source_other" class="fieldstyle" placeholder="Specifiy" style="display:none;" />

                </td>
                <td align="left" valign="top"><div id="select_referral_source_results"></div></td>
            </tr>
            <tr>
                <td valign="top">

                    <label>Select KP Type</label>

                    <?php include('database/getkptypes.php'); ?>


                </td>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td valign="top"  width="33%">


                    <input onkeyup="nametocaps();" id="txt_client_name" required class="fieldstyle"  name="txt_client_name" type="text"  placeholder="Enter Client Name">
                    <div id="splitnamewrapper" style="display:none;">       
                        <input onclick="kpnamesplit();" type="button" value="Name Split" />



                        <script>
                            function nametocaps() {

                                var txt_client_name = document.getElementById('txt_client_name').value.toUpperCase();

                                document.getElementById('txt_client_name').value = txt_client_name;
                            }

        // Registration generate KP

                            function kpnamesplit() {
                                var kpname = document.getElementById('txt_client_name').value;

                                var value = kpname.split(/[ ]+/);

                                var blkstr = [];
                                $.each(value, function (idx2, val2) {
                                    var str = idx2 + ":" + val2;
                                    blkstr.push(str);
                                });

        // alert(blkstr);

                                var one = blkstr[0],
                                        two = blkstr[1];

                                var one = one.replace("0:", "");
                                var two = two.replace("1:", "");

                                document.getElementById('splitnamef1').value = one;
                                document.getElementById('splitnamef2').value = two;

                            }

                        </script>

                        <input type="text" name="splitnamef1" id="splitnamef1" />
                        <input type="text" name="splitnamef2" id="splitnamef2" />
                    </div>

                </td>
                <td valign="top"  width="33%"><label>Date of birth</label>
                    <input required class="fieldstyledate"   name="txt_client_date_of_birth" id="txt_client_date_of_birth" type="date"  placeholder="Client Date of Birth">
                    <input style="display:none;" onclick="convertdate();" type="button" value="Test" />

                </td>
                <td valign="top"  width="33%">  

                    <label>Gender </label>

                    <select class="select_with_label" name="select_gender" id="select_gender">

                        <option value="Male">Male</option>
                        <option value="Female" selected="selected">Female</option>

                    </select>

                </td>
            </tr>
            <tr>
                <td valign="top" >
                    <input required class="fieldstyle"  name="txt_client_nickname" id="txt_client_nickname" type="text"  placeholder="Enter Client Nickname">
                </td>
                <td valign="top" >
                    <label>Marital Status</label>
                    <select class="select_with_label" name="select_marital_status" id="select_marital_status">
                        <option value="Single">Single</option>
                        <option value="Engaged">Engaged</option>
                        <option value="Married">Married</option>
                        <option value="Divorced">Divorced</option>
                        <option value="Separeted">Separated</option>
                        <option value="Widowed">Widowed</option>
                    </select>
                </td>
                <td valign="top" >
                    <input required class="fieldstyle" name="txt_client_mobile_number" id="txt_client_mobile_number" type="text"  placeholder="Client Mobile Number">
                </td>
            </tr>
            <tr>
                <td valign="top" >
                    <label>Select Country</label>
                    <select class="select_with_label" name="select_country" id="select_country">
                        <option value="Kenya">Kenya</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Uganda">Uganda</option>
                    </select>
                </td>

                <td valign="top" >
                    <input required class="fieldstyle"
                           name="txt_client_residential_area"
                           id="txt_client_residential_area"
                           type="text"  
                           placeholder="Residential Area">
                </td>

                <td valign="top" >
                    <input required class="fieldstyle"
                           name="txt_client_residential_home"
                           id="txt_client_residential_home"
                           type="text"
                           placeholder="Home District">
                </td>

            </tr>
            <tr>
                <td valign="top" >
                    <label>Highest Level of Education</label>
                    <select class="select_with_label" name="select_client_education_level" id="select_client_education_level">
                        <option value="Completed Primary">Completed Primary</option>
                        <option value="Not Completed Primary">Not Completed Primary</option>
                        <option value="Completed Secondary">Completed Secondary</option>
                        <option value="Not Completed Secondary">Not Completed Secondary</option>
                        <option value="Completed Tertiary Level">Completed Tertiary Level</option>
                        <option value="Not Completed Tertiary Level">Not Completed Tertiary Level</option>
                    </select>
                </td>



                <td>

                    <label>Have you registered in any other clinic / wellness center before?</label>
                    <input onclick="localStorage.setItem('clinicselection', 'Yes');" required name="radio_alreadyregisteredinclinic" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input onclick="localStorage.setItem('clinicselection', 'No');" name="radio_alreadyregisteredinclinic" type="radio" required class="wellness_center radiostyle" value="No" checked="checked"><div class="radiolabel">No</div>
                    <div class="clearafter"></div>

                </td><td valign="top">

                    <style type="text/css">
                        #selectclinics select {
                            width: 250px !important;
                        }
                    </style>

                    <div id="selectclinicswrapper" style="display:none;">
                        <div id="selectclinics">
                            <?php include"database/getclinics.php"; ?>
                        </div>

                        <input class="fieldstyle"  name="txt_nameclientusedinclinic" type="text"  placeholder="Enter name client used in clinic" />

                        <input class="fieldstyle"  name="txt_patientdiscdicenumber" type="text"  placeholder="Enter patient Disc / Dice number" />

                    </div>

                </td>

            </tr>
        </table>

        <input id="register_uuid_button" name="Button2" type="submit" class="large_button" value="Register" />

    </form>

    <div id="kp_screening" style="display:none;" class="animated fadeIn">
        <table><tr>
                <td valign="top" >
                    <?php include"screening_tool_source_of_income.php"; ?>
                    <label>Source of Income</label><input class="select_with_label" style="cursor:pointer" onclick="layer_sourceofincome();" type="button" value="Select">
                </td>
                <td valign="top" >&nbsp;</td>
            </tr>
            <tr>
                <td valign="top" >  <label>Have you ever given sex for money?</label>
                    <input required name="radio_givensexformoney" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input required name="radio_givensexformoney" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div>  
                </td>
                <td valign="top" >
                    <label>Have you ever given sex for fish?</label>
                    <input required name="radio_sexforfish" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input required name="radio_sexforfish" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div>
                </td>
                <td valign="top" >
                    <label>Have you ever given sex for favours?</label>
                    <input required name="radio_givensexforfavours" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input required name="radio_givensexforfavours" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div>
                    <input class="fieldstyle fieldhidden" name="txt_givensexforfavours" id="txt_givensexforfavours" type="text" value="" placeholder="Specify favour">
                </td>
            </tr>
            <tr>
                <td valign="top" > <label>Have you recieved sex for money?</label>
                    <input required name="radio_recievedsexformoney" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input required name="radio_recievedsexformoney" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div></td>
                <td valign="top" >

                    <label>Have you recieved sex for fish?</label>
                    <input required name="radio_recievedsexforfish" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input required name="radio_recievedsexforfish" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div>

                </td>
                <td valign="top" > <label>Have you ever recieved sex for favours?</label>
                    <input required name="radio_recievedsexforfavours" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input required name="radio_recievedsexforfavours" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div>
                    <input id="txt_recievedsexforfavours" class="fieldstyle fieldhidden" name="txt_recievedsexforfavours" type="text" value="" placeholder="Specify favour"></td>
            </tr>
            <tr>
                <td valign="top" ><label>Did you have sex with a casual partner(s) in the last 3 months?</label>
                    <input required name="radio_sexwithcasualpartnerinlast3months" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input required name="radio_sexwithcasualpartnerinlast3months" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div>
                    <div class="clearafter"></div>
                    <textarea name="txt_sexwithcasualpartnerinlast3months" id="txt_sexwithcasualpartnerinlast3months" rows="3" class="fieldstyle fieldhidden" placeholder="Enter number of casual clients/sexual partners in last 3 months"></textarea></td>
                <td valign="top" >

                    <label>Condom used in every casual encounter with both sex partner/client?</label>
                    <input required name="radio_condomusedineverycasualencounterwithbothsexpartnerclient" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input required name="radio_condomusedineverycasualencounterwithbothsexpartnerclient" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div>

                </td>
                <td valign="top" ><label>What Gender are your sex partners?</label>
                    <input name="checkbox_whatgenderareyoursexpartners_female" type="checkbox" class="checkboxstyle" value="Female">Female<br>
                    <input name="checkbox_whatgenderareyoursexpartners_male" type="checkbox" class="checkboxstyle" value="Male">Male<br>
                    <input name="checkbox_whatgenderareyoursexpartners_transgender" type="checkbox" class="checkboxstyle" value="Male">Trans-G.

                </td>
            </tr>
            <tr>
                <td valign="top" ><label>Do you ever inject yourself with drugs?</label>
                    <input required name="radio_doyoueverinjectyourselfwithdrugs" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
                    <input required name="radio_doyoueverinjectyourselfwithdrugs" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div>
                </td>
                <td valign="top" id="probefutherifclientisidu" style="display:none;"><label>Probe further if client is IDU?</label>
                    <input class="checkboxstyle" name="checkbox_probefurtherifclientisidu?" type="checkbox" value="Therapeutic">Therapeutic<br>

                    <input class="checkboxstyle" name="checkbox_probefurtherifclientisidu" type="checkbox" value="Non Therapeutic">Non-T.</td>
                <td valign="top">
                </td>
            </tr>
        </table>
        <br>

        <input name="Submit" type="submit" class="large_button" value="Next">
    </div>

    <br />

</div>



<div id="screening_tool_b" class="animated fadeInUp" style="display:none">

    <table width="930" border="0" cellpadding="5" cellspacing="0"><tr><td><label>Client Category</label>

                <input onclick="marps_selected();" name="radio_kpmarpstype" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">MARPs</div>

                <input onclick="non_marps_selected();" name="radio_kpmarpstype" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">Non-MARPs</div>

                <div style="clear"></div>



                <input id="non_marps_selected" style="display:none;" class="fieldstyle"  name="txt_kpmarpstype_gp" type="text"  placeholder="Specify occupation">
            </td></tr></table>

    <input name="Button" type="button" class="large_button" value="Finish" onclick="generateuuid();">

</div>