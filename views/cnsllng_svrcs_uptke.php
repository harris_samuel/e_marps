<?php
include 'database/reports.php';
include 'database/configuration.php';
$reports = new Reports();
$configuration = new Configuration();
$server_url = $configuration->get_server_name();
?>

<script type="text/javascript"  src="<?php echo $server_url; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo $server_url; ?>/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo $server_url; ?>/css/jquery.dataTables.css" type="text/css" />


<link rel="stylesheet" href="<?php echo $server_url; ?>/css/jquery-ui.css">
<script src="<?php echo $server_url; ?>/js/jquery-ui.js"></script>
<script type="text/javascript">
    var j = jQuery.noConflict();
    j(document).ready(function () {
        j('#cnsllng_svrcs_uptke_table_1').DataTable({
        });

        j('#cnsllng_svrcs_uptke_table_2').DataTable({
        });

        j("#filter_date_from").datepicker({
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });

        j("#filter_date_to").datepicker({
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });
    });
</script>






<style type="text/css">
    .user_report {width: 80%;}
    .dataTables_filter input {width:60px}
</style>


<h2>Counselling Services Uptake Report</h2>

<div class="form animated fadeIn">


    <style type="text/css">
        #searchfilters {
            background-color: #FFFFFF;
            width: 700px;
            box-shadow: 0 0 15px 0px rgba(0,0,0,0.2);
            margin-top: 7px;
            margin-right: 20px;
            padding-top: 10px;
            padding-right: 10px;
            padding-left: 10px;
        }
        #searchfilters select {
            margin-right: 15px;
            margin-bottom: 15px;
        }
        .select_filters {
            transition: all 0.6s;
            background-size: 100% 100%;
            padding-top: 3px;
            padding-right: 5px;
            padding-bottom: 3px;
            padding-left: 5px;
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 10px;
            border: 1px solid #999999;
        }
    </style>

    <div id="searchfilters">

        <!-- Filter form start -->
        <form class="search_filter_form" id="search_filter_form" method="POST">
            <!-- Partner filter start-->
            <?php
            $query = "SELECT partner_id,partner_name,contact_person FROM partner ORDER BY  partner_id ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>
                <?php
                ?>

                <select class="select_filters" id="filter_partner" name="filter_partner">
                    <option value="">
                        Implementing Partner
                    </option>
                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $partner_id = $idresult[0];
                        $partner_name = $idresult[1];
                        $contact_person = $idresult[2];
                        ?>
                        <option id="partner_id"  value="<?php echo $partner_name; ?>"><?php echo $partner_name . '&nbsp;(' . $contact_person . ')'; ?></option>   <?php
                    }
                }
                ?>
            </select>
            <!-- Partner filter end -->
            <!-- County filter start -->
            <?php
            $query = "SELECT * FROM `region`
ORDER BY `region`.`region_name` ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>
                <?php
                ?>
                <select class="select_with_label" id="filter_county" name="filter_county">
                    <option value="">
                        County/Sub County
                    </option>
                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $region_id = $idresult[0];
                        $region_name = $idresult[1];
                        ?>
                        <option id="partner_id"  value="<?php echo $region_name; ?>"><?php echo $region_name; ?></option>   <?php
                    }
                }
                ?>
            </select>

            <!-- County filter end -->

            <input type="text" id="filter_date_from" class="select_filters" name="filter_date_from"/>
            <input type="text" id="filter_date_to" class="select_filters" name="filter_date_to"/>

            <!-- KP Type filter start -->


            <?php
            $query = "SELECT * FROM `kp_types`
ORDER BY `kp_types`.`name` ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>
                <?php
                ?>
                <select class="select_with_label" id="filter_kp_type" name="filter_kp_type">
                    <option value="">
                        KP Type
                    </option>
                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $kp_type_id = $idresult[0];
                        $kp_type_name = $idresult[1];
                        ?>
                        <option id="partner_id"  value="<?php echo $kp_type_name; ?>"><?php echo $kp_type_name; ?></option>   <?php
                    }
                }
                ?>
            </select>

            <!-- KP Type filter start -->
            <input type="submit" name="search_filter_button" id="search_filter_button" class="search_filter_button" value="Filter Report"/>

        </form>
    </div>



    <div id="cnsllng_svrcs_uptke_div_1" class="cnsllng_svrcs_uptke_div_1">

        <table id="cnsllng_svrcs_uptke_table_1" class="cnsllng_svrcs_uptke_div_1">
            <thead>
                <tr>
                    <th>No</th>
                    <th> KP Name</th>
                    <th>Abbrv</th>
                    <th>Partner Name</th>
                    <th>Facility Name </th>
                    <th>Region Name </th>
                    <th>No of KPs Counselled </th>
                    <th>Date </th>

                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th> KP Name</th>
                    <th>Abbrv</th>
                    <th>Partner Name</th>
                    <th>Facility Name </th>
                    <th>Region Name </th>
                    <th>No of KPs Counselled </th>
                    <th>Date </th>

                </tr>
            </tfoot>
            <tbody id="cnsllng_svrcs_uptke_tbody_1" class="cnsllng_svrcs_uptke_tbody_1">


                <?php
                $res = $reports->counselling_srvces_uptake();
                if (mysqli_num_rows($res) > 0) {
                    $i = 1;
                    while ($row = mysqli_fetch_array($res, MYSQLI_BOTH)) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['abbrv']; ?></td>
                            <td><?php echo $row['partner_name']; ?></td>
                            <td><?php echo $row['facility_name']; ?></td>
                            <td><?php echo $row['county']; ?></td>
                            <td><?php echo $row['no_kps_counselled']; ?></td>
                            <td><?php echo $row['activity_time_stamp']; ?></td>

                        </tr>
                        <?php
                        $i++;
                    }
                } else {
                    ?><tr>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                    </tr><?php
                }
                ?>
            </tbody>
        </table>


    </div>

    <div id="cnsllng_svrcs_uptke_div_2" class="cnsllng_svrcs_uptke_div_2" style="display: none;">
        <table id="cnsllng_svrcs_uptke_table_2" class="cnsllng_svrcs_uptke_table_2">
            <thead>
                <tr>
                    <th>No</th>
                    <th> KP Name</th>
                    <th>Abbrv</th>
                    <th>Partner Name</th>
                    <th>Facility Name </th>
                    <th>Region Name </th>
                    <th>No of KPs Counselled </th>
                    <th>Date </th>

                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th> KP Name</th>
                    <th>Abbrv</th>
                    <th>Partner Name</th>
                    <th>Facility Name </th>
                    <th>Region Name </th>
                    <th>No of KPs Counselled </th>
                    <th>Date </th>

                </tr>
            </tfoot>
            <tbody id="cnsllng_svrcs_uptke_tbody_2" class="cnsllng_svrcs_uptke_tbody_2">

            </tbody>
        </table>
    </div>


</div>
