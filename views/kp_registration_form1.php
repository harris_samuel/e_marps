<center>

<?php
include 'views/websrvc.php';
$websrvc = new Websrvc();

if (isset($_REQUEST['register'])) {
    extract($_REQUEST);
    $succes = $websrvc->kp_registration($kp_type, $client_name, $d_o_b, $tel, $gender, $education_level, $disc_number, $unique_id, $place_of_birth, $marital_status, $clinic_name, $facility_id, $name_used_in_clinic);
    if ($succes) {
     
    } else {
      
    }
}
?>
</center>

<link href="../fieldstyles.css" rel="stylesheet" type="text/css" />

<form action="" method="POST" name="kp_registration" class="kp_registration" id="kp_registration">
    <div id="form1" class="form animated fadeIn">
        <div class="left">

            <label>KP Client Type</label>
            <select class="select_with_label" name="kp_type">
                <option value="1">SW</option>
                <option value="2">TRK</option>
                <option value="3">MSM</option>
                <option value="4">PWUD</option>
                <option value="5">FF</option>
            </select>
            <div class="clearafter"></div>

            <input class="fieldstyle"  name="client_name" type="text"  placeholder="Enter Client Name (First and Last Names)">

            <input class="fieldstyle"   name="d_o_b" type="text"  placeholder="Date of Birth">

            <input class="fieldstyle"   name="tel" type="text"  placeholder="Tel">
            
			<label>Gender </label>
            <select class="select_with_label" name="gender">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Transgender">Trans-Gender</option>
            </select>
            <div class="clearafter"></div>

            <label>Education Level</label>
            <select class="select_with_label" name="education_level">
                <option value="Completed Primary">Completed Primary</option>
                <option value="Not Completed Primary">Not Completed Primary</option>
                <option value="Completed Secondary">Completed Secondary</option>
                <option value="Not Completed Secondary">Not Completed Secondary</option>
                <option value="Completed Tertiary Level">Completed Tertiary Level</option>
                <option value="Not Completed Tertiary Level">Not Completed Tertiary Level</option>
            </select>
            
           

        </div>
        <div class="right">

            <input class="fieldstyle"  name="unique_id" type="text"  placeholder="Unique ID">

            <input class="fieldstyle"  name="place_of_birth" type="text"  placeholder="Place of Birth">

            <label>Marital Status</label>
            <select class="select_with_label" name="marital_status">
                <option value="Single">Single</option>
                <option value="Engaged">Engaged</option>
                <option value="Married">Married</option>
                <option value="Divorced">Divorced</option>
                <option value="Separeted">Separated</option>
                <option value="Widowed">Widowed</option>
            </select>
            <div class="clearafter"></div>

            <label>Wellness Center Visit in last 6 Months?</label>
            <input name="wellness_center" type="radio" class="wellness_center radiostyle" value="Yes"><div class="radiolabel">Yes</div>
            <input name="wellness_center" type="radio" class="wellness_center radiostyle" value="No"><div class="radiolabel">No</div>  
            
            <div class="clearafter"></div>

<div id="wellnesscentervisit" style="display:none;">

<input class="fieldstyle" placeholder="Name used in Clinic"  name="name_used_in_clinic" type="text"  value="">
            <label>Clinic Name</label>
            <input class="fieldstyle_with_label clinic_name" placeholder="Clinic Name" id="clinic_name"  name="clinic_name" type="text"  value="">
            <input class="facility_id" id="facility_id" name="facility_id" type="hidden" />

            <div class="clearafter"></div>

            
            
             <label>Enter DISC Number</label>
            <input class="fieldstyle_with_label"  name="disc_number" type="text"  placeholder="">
            <div class="clearafter"></div>
            
 </div>

        </div>

        <div class="next_button_container">
            <button id="register" name="register" class="large_button register">Register</button>
        </div>

    </div>

</form>



</div>
<div style="clear:both"></div>