<?php
include 'database/class.admin.php';
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$admin = new ADMIN();
if (isset($_GET['edit_id'])) {
    $edit_id = $_GET['edit_id'];

    $select_query = "select * from users inner join roles on users.role_id = roles.role_id WHERE users.user_id='$edit_id'";
    $res = mysqli_query($link, $select_query);
    if ($res) {
        $row = mysqli_fetch_array($res, MYSQLI_BOTH);
    } else {

        echo 'Error' . mysqli_errno($link);
    }
}
if (isset($_REQUEST['update_user'])) {
    extract($_REQUEST);
    // echo 'Details ' . '<br>' . $user_name . '<br>' . $edit_id . '<br>' . $e_mail . '<br>' . $phone_no . '<br>' . $status . '<br>' . $role_name . '<br>';
    $success = $admin->update_password($user_password, $new_password, $edit_id);
    if ($success) {
        echo 'User Information Updated Successfully';
    } else {
        echo 'Error please try again ...';
    }
}
?> 
<div class="form animated fadeIn">
    <script type="text/javascript">
        function goBack() {
            window.history.back()
        }

    </script>
    <script language="javascript" type="text/javascript">

        function submitresetpasswrd() {
            var form = document.reset_password;
            if (form.user_password.value !== form.new_password.value) {
                alert("The password did not match .");
                return false;
            } else if (form.user_password.value === "") {
                alert("Please enter the new password .");
                return false;
            } else if (form.new_password.value === "") {
                alert("Please re-enter the new password.");
                return false;
            }
        }
    </script>
    <form method="post" name="update_user_password_form" class="update_user_password_form" id="update_user_password_form">
        <table id="dataview">
            <?php
            $count_row = mysqli_num_rows($res);
            if ($count_row === 1) {
                ?>   
                <input type="hidden" name="user_id" value="<?php echo $edit_id; ?>"/>
                <tr><td><input type="password" name="user_password" placeholder="Password"  /><br /></td></tr>
                <tr><td><input type="password" name="new_password" placeholder="Repeat Password"  /></td></tr>

                <tr><td>
                        <input type="submit" name="update_user" value="Update User Password" class="update_user button" id="update_user" onclick="return(submitresetpasswrd());"/>
                    </td><td>
                        <button class="button" onclick="goBack()">Cancel</button>
                    </td></tr>
                <?php
            } else {
                
            }
            ?>
        </table>
    </form>
</table>
</div>