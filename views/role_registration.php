<?php
$counties = $websrvc->select_counties();
if (isset($_REQUEST['save'])) {
    extract($_REQUEST);
    $succes = $websrvc->role_registration($role_name, $user_id, $status, $datetime);
    if ($succes) {
        ?>
        <script src="js/jquery.min.js"></script>


        <!-- noty -->
        <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                noty({text: 'New Role Added Succesfully...'});
            });
        </script>


        <?php
    } else {

        echo '<div class="animated bounceIn warning">Wrong information. Please try again</div>';
    }
}
?>

<h2>User Role Registration Form</h2>





<div class="form animated fadeIn" style="width:400px;">

    <center>
        <div>

            <form method="POST" autocomplete="off" action="" name="user_role_registration" id="form_data user_role_registration"  accept-charset="UTF-8">


                <div class="left">


                    <input name="role_name"  placeholder="Role  Name" class="fieldstyle" type="text" required=""/>


                    <input name="user_id" type="hidden" value="<?php
                    $user_id = $_SESSION['uid'];
                    echo $user_id;
                    ?>"/>
                    <input type="hidden" value="<?php
                    $today = date('Y-m-d H:i:s');
                    echo $today;
                    ?>" name="datetime"/>


                    <select name="status" id="" class="status">

                        <option value="Active">Active</option>
                        <option value="In Active">In Active</option>


                    </select>


                    <div class="next_button_container">
                        <input class="large_button" type="submit" value="Add New Role" name="save" />

                    </div>
                </div>

            </form>




        </div></center>

</div>