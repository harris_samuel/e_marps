<?php
include 'database/class.admin.php';
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$admin = new ADMIN();
if (isset($_GET['edit_id'])) {
    $edit_id = $_GET['edit_id'];

    $select_query = "SELECT * FROM `partner_facility_site`  WHERE clinic_id='$edit_id'";
    $res = mysqli_query($link, $select_query);
    if ($res) {
        $row = mysqli_fetch_array($res, MYSQLI_BOTH);
    } else {

        echo 'Error' . mysqli_errno($link);
    }
}
if (isset($_REQUEST['update_partner_clinic'])) {
    extract($_REQUEST);
    // echo 'Details ' . '<br>' . $user_name . '<br>' . $edit_id . '<br>' . $e_mail . '<br>' . $phone_no . '<br>' . $status . '<br>' . $role_name . '<br>';
    $success = $admin->update_partner_facilty($clinic_id);
    if ($success) {
        echo 'Partner Information Updated Successfully';
    } else {
        echo 'Error please try again ...';
    }
}
?> 
<div class="form animated fadeIn">
    <script type="text/javascript">
        function goBack() {
            window.history.back()
        }

    </script>

    <form method="post" name="update_partner_form" class="update_partner_form" id="update_partner_form">
        <table id="dataview">
            <?php
            $count_row = mysqli_num_rows($res);
            if ($count_row === 1) {
                ?>   
                <input type="hidden" name="clinic_id" value="<?php echo $row['clinic_id']; ?>"/>
                <tr><td>
                        <select name="partner_name">
                            <option value="<?php echo $row['partner_id']; ?>"><?php echo $row['partner_name']; ?></option>
                            <?php
                            $query = "SELECT * from partner order by partner_id ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>  <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $partner_id = $idresult[0];
                                    $partner_name = $idresult[1];
                                    ?>
                                    <option id="partner_id"  value="<?php echo $partner_id; ?>"><?php echo $partner_name; ?></option>   <?php
                                }
                                ?>
                                <?php
                            }
                            ?>
                        </select>
                    </td></tr>
                <tr><td><input type="text" name="facility_name" placeholder="Facility Name" value="<?php echo $row['facility_name'] ?>" /><br /></td></tr>
                <tr><td><input type="text" name="facility_code" readonly="" placeholder="Facility Code " value="<?php echo $row['facility_code'] ?>" /></td></tr>
                <tr><td><input type="text" name="facility_type" readonly="" placeholder="Facility Type" value="<?php echo $row['facility_type'] ?>" /></td></tr>
                <tr><td><input type="text" name="operational_status" readonly="" placeholder="Operational tatus" value="<?php echo $row['operational_status'] ?>" /></td></tr>


                <tr><td>
                        <input type="submit" name="update_partner_clinic" value="Update Clinic Details" class="update_partner_clinic button" id="update_partner_clinic"/>
                    </td><td>
                        <button class="button" onclick="goBack()">Cancel</button>
                    </td></tr>
                <?php
            } else {
                
            }
            ?>
        </table>
    </form>
</table>
</div>