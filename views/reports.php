<style type="text/css">
.chartsholder {
	background-color: #FFF;
	padding-top: 10px;
	float: left;
	margin: 5px;
	box-shadow: 0 0 15px 0px rgba(0,0,0,0.2);
}
#allcharts {
	width: 1220px;
	margin-right: auto;
	margin-left: auto;
}
.chartsholder h1 {
	font-size: 18px;
	color: #333333;
	text-align: center;
	font-weight: normal;
	margin: 0px;
	padding: 0px;
}
.reports_topnav_date {
	height: 20px;
	margin-top: -5px;
}
body {
	background-color: #E1F2FF !important;
}
#reportsnavigation {
	background-color: #FFF;
	height: 80px;
	width: 100%;
	top: 0px;
	position: absolute;
	box-shadow: 0 0 25px 1px rgba(0,0,0,0.3);
	left: 0px;
	z-index: 1;
}
#reportsnavigationoptions {
	text-align: center;
	margin-top: 51px;
	font-family: sans-serif;
	position: absolute;
	margin-left: 230px;
}
#reportsnavigationoptions div {
	display: inline;
	transition: all 0.7s;
	cursor: pointer;
	margin-right: 15px;
	margin-left: 15px;
	background-image: url(images/linkline.png);
	background-position: -450px bottom;
	background-repeat: no-repeat;
}
#reportsnavigationoptions div:hover {
	background-image: url(images/linkline.png);
	transition: all 0.7s;
	background-position: 0px bottom;
}
#reportsnavigationoptions_partner,
#reportsnavigationoptions_county,
#reportsnavigationoptions_kptype,
#reportsnavigationoptions_year,
#reportsnavigationoptions_gender {
	background-color: #FFF;
	width: 250px;
	position: absolute;
	box-shadow: 3px 3px 8px 3px rgba(0,0,0,0.2);
	display: none;
	z-index: 1000;
	top: 100px;
	padding-top: 15px;
	padding-bottom: 15px;
	cursor: pointer;
}
.arrowup {
	height: 15px;
	width: 15px;
	margin-top: -27px;
	margin-left: 120px;
	position: absolute;
}
#reportsnavigationoptions_partner div,
#reportsnavigationoptions_county div,
#reportsnavigationoptions_kptype div,
#reportsnavigationoptions_year div,
#reportsnavigationoptions_gender div {
	text-align: center;
	width: 100%;
	padding-top: 6px;
	padding-bottom: 6px;
}
#reportsnavigationoptions_partner div:hover,
#reportsnavigationoptions_county div:hover,
#reportsnavigationoptions_kptype div:hover,
#reportsnavigationoptions_year div:hover,
#reportsnavigationoptions_gender div:hover {
	text-decoration: underline;
}
</style>

<script>
function reportsnavigationoptions_closeall() {
document.getElementById('reportsnavigationoptions_partner').style.display='none';
document.getElementById('reportsnavigationoptions_county').style.display='none';
document.getElementById('reportsnavigationoptions_kptype').style.display='none';
document.getElementById('reportsnavigationoptions_year').style.display='none';
document.getElementById('reportsnavigationoptions_gender').style.display='none';
}
function reportsnavigationoptions_partner() {
document.getElementById('reportsnavigationoptions_partner').style.display = 'block';
document.getElementById('reportsnavigationoptions_partner').className = 'animated fadeInUp';

document.getElementById('reportsnavigationoptions_county').style.display='none';
document.getElementById('reportsnavigationoptions_kptype').style.display='none';
document.getElementById('reportsnavigationoptions_year').style.display='none';
document.getElementById('reportsnavigationoptions_gender').style.display='none';
}
function reportsnavigationoptions_county() {
document.getElementById('reportsnavigationoptions_county').style.display = 'block';
document.getElementById('reportsnavigationoptions_county').className = 'animated fadeInUp';

document.getElementById('reportsnavigationoptions_partner').style.display='none';
document.getElementById('reportsnavigationoptions_kptype').style.display='none';
document.getElementById('reportsnavigationoptions_year').style.display='none';
document.getElementById('reportsnavigationoptions_gender').style.display='none';
}
function reportsnavigationoptions_kptype() {
document.getElementById('reportsnavigationoptions_kptype').style.display = 'block';
document.getElementById('reportsnavigationoptions_kptype').className = 'animated fadeInUp';

document.getElementById('reportsnavigationoptions_partner').style.display='none';
document.getElementById('reportsnavigationoptions_county').style.display='none';
document.getElementById('reportsnavigationoptions_year').style.display='none';
document.getElementById('reportsnavigationoptions_gender').style.display='none';
}
function reportsnavigationoptions_year() {
document.getElementById('reportsnavigationoptions_year').style.display = 'block';
document.getElementById('reportsnavigationoptions_year').className = 'animated fadeInUp';

document.getElementById('reportsnavigationoptions_partner').style.display='none';
document.getElementById('reportsnavigationoptions_county').style.display='none';
document.getElementById('reportsnavigationoptions_kptype').style.display='none';
document.getElementById('reportsnavigationoptions_gender').style.display='none';
}
function reportsnavigationoptions_gender() {
document.getElementById('reportsnavigationoptions_gender').style.display = 'block';
document.getElementById('reportsnavigationoptions_gender').className = 'animated fadeInUp';

document.getElementById('reportsnavigationoptions_partner').style.display='none';
document.getElementById('reportsnavigationoptions_county').style.display='none';
document.getElementById('reportsnavigationoptions_kptype').style.display='none';
document.getElementById('reportsnavigationoptions_year').style.display='none';
}

</script>

<div id="reportsnavigationoptions_partner" style="margin-left:-110px;">
<img class="arrowup" src="images/arrowup.png">
<div>All implementing partners</div>
<div>IRDO</div>
<div>IMC</div>
<div>University of Manitoba</div>
<div>University of Nairobi</div>
<div>Northstar Alliance</div>
<div>KASH</div>
</div>

<div id="reportsnavigationoptions_county" style="margin-left:75px;">
<img class="arrowup" src="images/arrowup.png">
<div>Mombasa</div>
<div>Nairobi</div>
<div>Kiambu</div>
<div>Nakuru</div>
<div>Kisumu</div>
<div>Naivasha</div>
<div>Kericho</div>
<div>Kisii</div>
<div>Migori</div>
</div>

<div id="reportsnavigationoptions_kptype" style="margin-left:-20px;">
<img class="arrowup" src="images/arrowup.png">
<div>FSW</div>
<div>MSW</div>
<div>MSM</div>
<div>PWUD</div>
<div>TRK</div>
<div>FF</div>
</div>

<div id="reportsnavigationoptions_gender" style="margin-left:165px;">
<img class="arrowup" src="images/arrowup.png">
<div>Male</div>
<div>Female</div>
<div>Transgender</div>
</div>

<div id="reportsnavigationoptions_year" style="margin-left:245px;">
<img class="arrowup" src="images/arrowup.png">
<div>2010</div>
<div>2011</div>
<div>2012</div>
<div>2013</div>
<div>2014</div>
<div>2015</div>
</div>


<div class="animated fadeInDown andel4" id="reportsnavigation">

<div id="reportsnavigationoptions">

<div onclick="reportsnavigationoptions_partner()">Partner</div>
 | 
<div onclick="reportsnavigationoptions_kptype()">KP Type</div>
 | 
<div onclick="reportsnavigationoptions_county()">County</div>
 | 
<div onclick="reportsnavigationoptions_gender()">Gender</div>
 | 
<div onclick="reportsnavigationoptions_year()">Year</div>

<input class="reports_topnav_date" type="date"> - <input class="reports_topnav_date" type="date" style="margin-right:20px;">
  

 | 
<div onclick="TINY.box.show({url:'views/advancedfilters.php',width:600,height:400,maskid:'bluemask',maskopacity:40})">Advanced Filters</div>
</div>

</div>

<div onclick="reportsnavigationoptions_closeall()">



<br><br><br>

<div class="fadeIn animated" id="allcharts">

<div class="chartsholder">
<h1>Outreach Level Activities</h1>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Outreach Level Activities'],
          ['PEP',     11],
          ['Violence Reported',      2],
          ['Program Contact',  2],
          ['Health education', 2],
          ['Counseling',    7]
        ]);

        var options = {
         title: 'KP types FSW/MSW/MSM/TG'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>


    <div id="piechart" style="width: 600px; height: 400px;"></div>
    
</div>

<div class="chartsholder">
<h1>Summary of Condoms and Lubes</h1>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Week', 'Male Condoms', 'Female Condoms', 'Lubes'],
          ['Week1',  1000,      400,      500],
          ['Week2',  1170,      460,      400],
          ['Week3',  660,       1120,      700],
          ['Week4',  1030,      540,      400]
        ]);

        var options = {
          title: 'KP Type: FSW/MSW/MSM/TG',
          vAxis: {title: 'Week',  titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.BarChart(document.getElementById('barchart'));

        chart.draw(data, options);
      }
    </script>


  <div id="barchart" style="width: 600px; height: 400px;"></div>
    
</div>

<div class="chartsholder">
<h1>Gender</h1>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Gender'],
          ['Male',     11],
          ['Female',      89]
        ]);

        var options = {
         title: ''
        };



        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

        chart.draw(data, options);
      }
    </script>


    <div id="piechart2" style="width: 600px; height: 400px;"></div>
    
</div>

<div class="chartsholder">
<h1>Client Enrollment</h1>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Year 2011');
      data.addColumn('number', 'Year 2012');
	  data.addColumn('number', 'Year 2013');
	  

      data.addRows([
        [10, 65, 78, 79],   [20, 55, 64, 66],  [30, 70, 76, 70],
        [33, 59, 51, 55], [34, 62, 54, 60], [35, 65, 57, 54],
        [36, 62, 54, 54], [37, 58, 50, 67], [38, 55, 47, 55],
        [39, 61, 53, 56], [45, 69, 61, 66], [46, 69, 61, 68], [47, 70, 62, 45],
        [48, 72, 64, 50], [49, 68, 60, 55], [50, 66, 58, 70],
        [51, 65, 57, 80], [52, 67, 59, 77], [53, 70, 62, 80],
        [54, 71, 63, 76], [55, 72, 64, 74], [56, 73, 65, 72],
        [57, 75, 67, 80], [62, 65, 57, 55],
        [63, 67, 59, 44], [70, 68, 60, 65], [80, 90, 79, 85],
        [90, 70, 62, 75], [100, 72, 64, 72], [110, 75, 67, 80],
        [120, 80, 90, 92]
      ]);


           var options = {
        width: 600,
        height: 400,
        hAxis: {
		  ticks: [
		   {v:10, f:'Jan'},
		   {v:20, f:'Feb'},
		   {v:30, f:'Mar'},
		   {v:40, f:'Apr'},
		   {v:50, f:'May'},
		   {v:60, f:'Jun'},
		   {v:70, f:'Jul'},
		   {v:80, f:'Aug'},
		   {v:90, f:'Sept'},
		   {v:100, f:'Oct'},
		   {v:110, f:'Nov'},
		   {v:120, f:'Dec'}
		   ],
          title: 'Month'
        },
        vAxis: {
          title: 'Enrollment Level'
        },
        series: {
          
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('ex2'));

      chart.draw(data, options);
    }
    </script>


    <div id="ex2" style="width: 600px; height: 400px;"></div>
    
</div>

</div>

<div class="clearafter"></div>

<br><br>

</div>