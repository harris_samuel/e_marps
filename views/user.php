<?php
include 'database/class.admin.php';

$admin = new ADMIN();
?>


<script type="text/javascript"  src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css" type="text/css" />
<script type="text/javascript">

    $(document).ready(function () {
        $('#user_report').DataTable();
    });
</script>
<style type="text/css">
    .user_report {
        width: 80%;
    }
</style>

<h2>User Management Form</h2>

<div class="form animated fadeIn">



    <table id="user_report" class="user_report">
        <thead>
            <tr>
                <th>No</th>
                <th>User Name</th>
                <th>E-Mail</th>
                <th>Phone Number</th>
                <th>Role</th>
                <th>Status</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>User Name</th>
                <th>E-Mail</th>
                <th>Phone Number</th>
                <th>Role</th>
                <th>Status</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
        <tbody>


            <?php
            $clinic_id = $_SESSION['sess_clinic_id'];
            $partner_id = $_SESSION['sess_partner_id'];
            echo 'Clinic ID : '.$clinic_id.'</br>'.'Partner Id : '.$partner_id.'</br>';
            $res = $admin->read();
            if (mysqli_num_rows($res) > 0) {
                $i = 1;
                while ($row = mysqli_fetch_array($res, MYSQLI_BOTH)) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['user_name']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['phone_no']; ?></td>
                        <td><?php echo $row['role_name']; ?></td>
                        <td><?php echo $row['status']; ?></td>

                        <td><a href="?currentview=reset_password&edit_id=<?php echo $row['user_id']; ?>">Reset Password</a>
                            |<a href="?currentview=edit_user&edit_id=<?php echo $row['user_id']; ?>">Edit</a></td>
                        <td><a href="?currentview=delete_user&delete_id=<?php echo $row['user_id']; ?>">Delete</a>

                        </td>
                    </tr>
                    <?php
                    $i++;
                }
            } else {
                ?><tr>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                </tr><?php
            }
            ?>
        </tbody>
    </table>


</div>
