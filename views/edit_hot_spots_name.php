<?php
include 'database/class.admin.php';
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$admin = new ADMIN();
if (isset($_GET['edit_id'])) {
    $edit_id = $_GET['edit_id'];

    $select_query = "SELECT * FROM `hot_spot_management` where id='$edit_id'";
    $res = mysqli_query($link, $select_query);
    if ($res) {
        $row = mysqli_fetch_array($res, MYSQLI_BOTH);
    } else {

        echo 'Error' . mysqli_errno($link);
    }
}
if (isset($_REQUEST['update_host_spot'])) {
    extract($_REQUEST);
    // echo 'Details ' . '<br>' . $user_name . '<br>' . $edit_id . '<br>' . $e_mail . '<br>' . $phone_no . '<br>' . $status . '<br>' . $role_name . '<br>';
    $success = $admin->update_hot_spot($hot_spot_id, $hot_spot_name, $region_name, $parter_name, $clinic_name);
    if ($success) {
        echo 'Hot Spot Information Updated Successfully';
    } else {
        echo 'Error please try again ...';
    }
}
?> 
<div class="form animated fadeIn">
    <script type="text/javascript">
        function goBack() {
            window.history.back()
        }

    </script>

    <form method="post" name="update_hot_spot_form" class="update_hot_spot_form" id="update_hot_spot_form">
        <table id="dataview">
            <?php
            $count_row = mysqli_num_rows($res);
            if ($count_row === 1) {
                ?>   
                <input type="hidden" name="hot_spot_id" value="<?php echo $row['id']; ?>"/>
                <tr><td><input type="text" name="hot_spot_name" placeholder="Hot Spot Name" value="<?php echo $row['name'] ?>" /><br /></td></tr>
                <tr><td>

                        <?php
                        $query = "SELECT * FROM `region` ORDER BY region_name ASC";
                        if ($result = mysqli_query($link, $query)) {
                            ?>
                            <select  class="region_name fieldstyle_with_label"  required="" name="region_name" id="region_name">
                                <option value="<?php echo $row['region_id']; ?>"><?php echo $row['regin_name']; ?></option>
                                <option id="region_name"  class="region_name fieldstyle_with_label" value="<?php echo $row['region_id']; ?>"><?php echo $row['region_name'] . '&nbsp;'; ?></option>   <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $region_id = $idresult[0];
                                    $region_name = $idresult[1];
                                    ?>
                                    <option id="region_name"  class="region_name fieldstyle_with_label" value="<?php echo $region_id; ?>"><?php echo $region_name . '&nbsp;'; ?></option>   <?php
                                }
                                ?>
                            </select>
                            <?php
                        }
                        ?>


                <tr>
                    <td>

                        <?php
// GET LIST AND DISPLAY IN FORM
// check connection
                        ?>
                        <label>Partner Name : </label>

                        <?php
                        $query = "SELECT partner_id,partner_name,contact_person FROM partner ORDER BY  partner_id ASC";
                        if ($result = mysqli_query($link, $query)) {
                            ?>
                            <select onchange="document.getElementById('clinic_name_holder').style.display = 'block';" class="partner_name_hot_spot fieldstyle_with_label" id="partner_name_hot_spot" required="" name="user_partner_name">
                                <option id="" value="">Please select a partner</option>
                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $partner_id = $idresult[0];
                                    $partner_name = $idresult[1];
                                    $contact_person = $idresult[2];
                                    ?>
                                    <option id="partner_name_hot_spot"  value="<?php echo $partner_id; ?>"><?php echo $partner_name . '&nbsp;(' . $contact_person . ')'; ?></option>   <?php
                                }
                                ?>
                            </select>

                            <div style="clear:both"></div>

                            <?php
                        }
                        ?>

                    </td>
                </tr>

                <tr>
                    <td>

                        <div id="clinic_name_holder" style="display:none;">

                            <label>Clinic Name : </label>
                            <select class="clinic_name_hot_spot fieldstyle_with_label" required="" name="user_clinic_name" id="clinic_name_hot_spot">

                            </select>

                        </div>

                    </td>
                </tr>




                <tr><td><input type="text" name="partner_name" placeholder="Partner Number" value="<?php echo $row['partner_name'] ?>" /></td></tr>
                <tr><td><input type="text" name="clinic_name" placeholder="Clinic Name" value="<?php echo $row['clinic_name'] ?>" /></td></tr>

                <tr>
                    <td>

                        <input type="submit" name="update_host_spot" value="Update Hot Spot  Details" class="update_host_spot button" id="update_host_spot"/>
                    </td><td>
                        <button class="button" onclick="goBack()">Cancel</button>
                    </td></tr>
                <?php
            } else {
                
            }
            ?>
        </table>
    </form>
</table>
</div>