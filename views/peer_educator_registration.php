<?php
$counties = $websrvc->select_counties();
if (isset($_REQUEST['save'])) {
    extract($_REQUEST);
    $succes = $websrvc->peer_educators_registration($peer_educator_name, $user_id,$nick_name, $phone_no, $status,$datetime);
    if ($succes) {
        ?>
        <script src="js/jquery.min.js"></script>


        <!-- noty -->
        <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                noty({text: 'New Peer Educator Added Succesfully...'});
            });
        </script>


        <?php
    } else {

        echo '<div class="animated bounceIn warning">Wrong information. Please try again</div>';
    }
}
?>

<h2>Peer Educator Registration</h2>





<div class="form animated fadeIn" style="width:400px;">

    <center>
        <div>

            <form method="POST" autocomplete="off" action="" name="kp_type_registration" id="form_data"  accept-charset="UTF-8">


                <div class="left">


                    <input name="peer_educator_name"  placeholder="Peer Educator's Name" class="fieldstyle" type="text" required=""/>

                    <input name="nick_name" placeholder="Nick  Name" class="fieldstyle" type="text" required=""/>
                    <input name="user_id" type="hidden" value="<?php
                    $user_id = $_SESSION['uid'];
                    echo $user_id;
                    ?>"/>
                    <input type="hidden" value="<?php
                    $today = date('Y-m-d H:i:s');
                    echo $today;
                    ?>" name="datetime"/>

                    <input name="phone_no" placeholder="Phone  Number" class="fieldstyle" type="text" required=""/>

                    <select name="status" id="" class="status">

                        <option value="Active">Active</option>
                        <option value="Dead">Dead</option>
                        <option value="Deleted">Deleted</option>
                        <option value="Discontinued">Discontinued</option>
                        <option value="Lost">Lost</option>
                        <option value="On Going">On Going</option>
                        <option value="Pending">Pending</option>
                        <option value="Transfer" >Transfer</option>
                        <option value="#N/A" >#N/A</option>

                    </select>


                    <div class="next_button_container">
                        <input class="large_button" type="submit" value="Add Peer Educator" name="save" />

                    </div>
                </div>

            </form>




        </div></center>

</div>