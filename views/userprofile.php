<div id="titlebar"><div id="titlebartext">User Profile</div></div>

<div class="form animated fadeIn" style="margin-top:150px;">
    <br><br><br><br>

    <?php
    include 'db_connect.php';

    $link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $uid = $_GET["uid"];

    if ($result = $link->query("SELECT * FROM user_account_information WHERE user_id = '" . $uid . "'") or die(mysqli_error($link))) {
        $row_cnt = $result->num_rows;

        if (mysqli_num_rows($result)) {

            while ($row = $result->fetch_object()) {

                echo "<table border=\"0\" align=\"center\" cellpadding=\"5\">
  <tr>
    <td width=\"50%\">Name:</td>
    <td width=\"50%\">ID " . $row->user_id . " | " . $row->user_name . "</td>
  </tr>
  <tr>
    <td>E-Mail:</td>
    <td>" . $row->email . "</td>
  </tr>
  <tr>
    <td>Phone:</td>
    <td>" . $row->phone_no . "</td>
  </tr>
  <tr>
    <td>Implementing Partner:</td>
    <td>" . $row->partner_name . "</td>
  </tr>
  <tr>
    <td>Role / Access Level:</td>
    <td>" . $row->role_name . "</td>
  </tr>
  <tr>
    <td>Facility Name:</td>
    <td>" . $row->facility_name . "</td>
  </tr>
  <tr>
    <td></td>
    <td><br><br><br><div class=\"small_button\" onclick=\"document.location.href='?currentview=usersettings&uid=" . $row->user_id . "';\">Settings/Preferences</div></td>
  </tr>
</table>";
            }
        }
    }
    ?>

    <br><br><br><br>
</div>