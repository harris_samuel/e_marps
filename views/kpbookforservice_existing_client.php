<h2>Returning Client - Book for Service</h2>

<div id="existing_client_last_visit_details"></div>

<div id="existing_client_clinical_visitation_history" style="margin-top:10px;">
<div id="clinical_visitation_box" style="height: 163px;">
<div id="clinical_visitation_box_title">Clinical Visitation History</div>

<div id="existing_client_clinical_visitation_history_box"></div>

</div>
</div>

<script>
function activate_existing_active_client() {
	localStorage.setItem('activate_existing_client','Yes');
	document.getElementById('book_client_name').innerHTML = localStorage.getItem('currentclientname_prep');
	layer_kpbookforservice_open();
}
</script>

<div class="small_button" style="margin-top:10px;" onclick="activate_existing_active_client();">Book for Service</div>