<?php
include 'database/class.admin.php';
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$admin = new ADMIN();
if (isset($_GET['edit_id'])) {

    $edit_id = $_GET['edit_id'];

    $select_query = "select * from roles WHERE role_id='$edit_id'";
    $res = mysqli_query($link, $select_query);
    if ($res) {
        $row = mysqli_fetch_array($res, MYSQLI_BOTH);
    } else {

        echo 'Error' . mysqli_errno($link);
    }
}
if (isset($_REQUEST['edit_role'])) {
    extract($_REQUEST);
    $success = $admin->edit_roles($role_id, $role_name, $role_status);
    if ($success) {
        echo 'Role   Deativated Succesfully ';
    } else {
        echo 'Error please try again ...';
    }
}
?> 
<div class="form animated fadeIn">
    <script type="text/javascript">
        function goBack() {
            window.history.back()
        }

    </script>
    <form method="post" name="edit_role_form" class="edit_role_form" id="edit_role_form">
        <table id="dataview">
            <?php
            $count_row = mysqli_num_rows($res);
            if ($count_row === 1) {
                ?>   
                <input type="hidden" name="role_id" value="<?php echo $row['role_id']; ?>"/>
                <tr><td><input type="text" readonly="" name="role_name" placeholder="Role Name" value="<?php echo $row['role_name'] ?>" /><br /></td></tr>
                <tr><td>
                        <select name="role_status" id="role_status">
                            <option value="<?php echo $row['status'] ?>"><?php echo $row['status']; ?></option>
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select>
                    </td></tr>

                <tr><td>
                        <input type="submit" value="Edit Role Entry" name="edit_role" class="button"/>
                        <button class="button" onclick="goBack()">Cancel</button>
                    </td></tr>
                <?php
            } else {
                
            }
            ?>
        </table>
    </form>
</table>
</div>