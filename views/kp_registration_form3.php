<div id="form3" class="form animated fadeIn">
<div class="left">

<label>Have you ever been tested for HIV?</label>
<input name="tested" type="radio" class="radiostyle" value=""><div class="radiolabel">Yes</div>
<input class="radiostyle" name="tested" type="radio" value=""><div class="radiolabel">No</div>

<div style="clear:both"></div>

  <label>Where have you been tested?</label>
<select class="select_with_label" name="">
  <option></option>
</select>
<div class="clearafter"></div>

  <label>What did you do with the Kit?</label>
<input class="fieldstyle_with_label"  name="textfield" type="text"  value="">
<div class="clearafter"></div>

<label>Was it difficult to administer?</label>
<input name="administer" type="radio" class="radiostyle" value=""><div class="radiolabel">Yes</div>
<input name="administer" type="radio" class="radiostyle" value=""><div class="radiolabel">No</div>
<div class="clearafter"></div>

<label>Would you recommend the self help kit to others?</label>
<input name="recommend" type="radio" class="radiostyle" value=""><div class="radiolabel">Yes</div>
<input name="recommend" type="radio" class="radiostyle" value=""><div class="radiolabel">No</div>
<div class="clearafter"></div>

<label>If tested would you share the results?</label>
<div class="radiocontainer">
<input class="testedhiv radiostyle" name="share_test_results" type="radio" value="positiveyes"><div class="radiolabel" onClick=""="tested_result_positive_yes">Yes</div>
<input name="share_test_results" type="radio" class="testedhiv radiostyle" value="positiveno"><div class="radiolabel" onClick="tested_result_positive_yes">No</div>
<div style="display:none" id="tested_results_yes">
<input class="radiostyle" name="share_test_results_positive" type="radio" value=""><div class="radiolabel">+ve</div>
<input class="radiostyle" name="share_test_results_positive" type="radio" value=""><div class="radiolabel">-ve</div>
</div>
</div>


</div>
<div class="right">

  <label>When where you last tested for HIV?</label>
<input class="fieldstyle_with_label"  name="textfield" type="text"  value="">
<div class="clearafter"></div>

<label>Have you ever tested yourself with a self test kit?</label>
<div class="radiocontainer">
<input name="selftest" type="radio" class="radiostyle" value=""><div class="radiolabel">Yes</div>
<input class="radiostyle" name="selftest" type="radio" value=""><div class="radiolabel">No</div>
</div>
<div class="clearafter"></div>

  <label>How many days after receiving the self test kit did you admister it?</label>
<input class="fieldstyle_with_label"  name="textfield" type="text"  value="">
<div class="clearafter"></div>

<label>Where you able to understand the results?</label>
<div class="radiocontainer">
<input name="understandresults" type="radio" class="radiostyle" value=""><div class="radiolabel">Yes</div>
<input class="radiostyle" name="understandresults" type="radio" value=""><div class="radiolabel">No</div>
</div>
<div class="clearafter"></div>

<label>Have you disclosed your HIV status to anybody?</label>
<div class="radiocontainer">
<input name="disclosed" type="radio" class="radiostyle" value=""><div class="radiolabel">Yes</div>
<input class="radiostyle" name="disclosed" type="radio" value=""><div class="radiolabel">No</div>
</div>
<div class="clearafter"></div>

</div>

<div class="next_button_container">
<button class="large_button" onClick="form3form4();">Next</button>
</div>

</div>
