<script>
    $(document).ready(function () {
        var currentuuid = localStorage.getItem('currentuuid');
        $.ajax({
            type: "GET",
            url: "database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=stitrtmnt",
            dataType: "JSON",
            success: function (response) {
                $.each(response, function (i, resp) {
                    

                    //Lab Reference STATUS GAPS CHECKER
                    $("input[name=Lab_Ref]").prop('checked', false); //uncheck all
                    $("input[name=Lab_Ref][value=" + resp.ref_lab_invest + "]").prop('checked', true);

                    $("input[name=Lab_Ref_No]").val(resp.lab_ref_no);

                    //Health Referral GAPS CHECKER
                    $("input[name=Health_Ref]").prop('checked', false); //uncheck all
                    $("input[name=Health_Ref][value=" + resp.ref_hlth_fac + "]").prop('checked', true);

                    $("input[name=Health_Ref_Nme]").val(resp.ref_hlth_fac_nme);


                    //CD Given GAPS CHECKER
                    $("input[name=Cd_Gvn]").prop('checked', false); //uncheck all
                    $("input[name=Cd_Gvn][value=" + resp.condom_given + "]").prop('checked', true);

                    $("input[name=No_CD_Gvn]").val(resp.no_of_condom_given);


                    //Lubes Given GAPS CHECKER
                    $("input[name=Lub_Gvn]").prop('checked', false); //uncheck all
                    $("input[name=Lub_Gvn][value=" + resp.lubs_given + "]").prop('checked', true);

                    $("input[name=No_Lub_Gvn]").val(resp.no_of_lubs_given);


                    //Partner Referral  GAPS CHECKER
                    $("input[name=Partnr_Ref]").prop('checked', false); //uncheck all
                    $("input[name=Partnr_Ref][value=" + resp.partner_ref + "]").prop('checked', true);

                    $("input[name=Partnr_Id]").val(resp.partner);

                    //Internal Examination  GAPS CHECKER
                    $("input[name=Intrnl_Exam]").prop('checked', false); //uncheck all
                    $("input[name=Intrnl_Exam][value=" + resp.internal_exam_given + "]").prop('checked', true);

                    
                    //NEXT APPOINTMENT DATE GAPS CHECKER 
                    $("textarea[name=Comments]").val(resp.clinic_off_comments);
                    //Get value of the  insert or update
                    var one = "1";
                    $("input[name=Edit]").val(one);
                    
                    
                    //Row ID to be Updated GAPS CHECKER
                    $("input[name=Rid]").val(resp.id);

                });

            }
        });


    });
</script>
<div id="titlebar"><div id="titlebartext">STI Treatment MOH902</div></div>

<script>
    sidebar5();</script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>



<form id="sti_treatment_form" class="sti_treatment_form" action="javascript:sti_treatment();">


    <div style="display:none;">
        <input  name="task" value="regSTIData">
        <input id="UUIDTOSEND" name="UUID" value="">
        <input name="Edit" value="0">
        <input name="Pid" id="Pid" class="Pid" value="<?php echo $_SESSION["sess_partner_id"] ?>" />
        <input name="Clinic" id="Clinic" class="Clinic" value="<?php echo $_SESSION["sess_clinic_id"] ?>" />
        <input type="text" id="CollPnt" class="CollPnt" value="Clinical" name="CollPnt"/>
        <input type="text" id="Rid" class="Rid" value="0" name="Rid"/>
    </div>

    <div id="forms">

        <label>Reason for Visit</label>
        <select class="select_with_label RsnVst" name="RsnVst" id="RsnVst">
            <option value="Asymptomatic">Asymptomatic</option>
            <option value="Symptomatic">Symptomatic</option>
            <option value="Quartlery">Quartlery</option>
            <option value="STI Screening checkup">STI Screening checkup</option>
            <option value="Follow up">Follow up</option>
        </select>
        <div class="clearafter"></div>

        <?php include"careandtreatment_typeofsyndrome.php"; ?>
        <label>Type of Syndrome</label><input class="select_with_label" style="cursor:pointer; width:250px;" onclick="layer_typeofsyndrome();" type="button" value="Select">

        <div class="clearafter"></div>

        <?php include"careandtreatment_drugprescription.php"; ?>
        <label>Drug Prescription</label><input class="select_with_label" style="cursor:pointer; width:250px;" onclick="layer_drugprescription();" type="button" value="Select">

        <div class="clearafter"></div>

        <label>Referred for Lab Investigation</label>
        <input required name="Lab_Ref" type="radio" class="wellness_center radiostyle Lab_Ref" id="Lab_Ref" value="1" onclick="enterlabrefno_show();">
        <div class="radiolabel">Yes</div>
        <input name="Lab_Ref" type="radio" required class="wellness_center radiostyle Lab_Ref" id="Lab_Ref" value="0" checked="checked" onclick="enterlabrefno_hide();" >
        <div class="radiolabel">No</div>
        <div id="Lab_Ref_No_Div" class="Lab_Ref_No_Div" style="display: none;">
            <input type="text" placeholder="Lab Ref No : " class="Lab_Ref_No fieldstyle" id="Lab_Ref_No" name="Lab_Ref_No" />

        </div>

        <div class="clearafter"></div>

        <label>Referred to Health Facility</label>
        <input required name="Health_Ref" type="radio" class="wellness_center radiostyle Health_Ref" id="Health_Ref" onclick="enterhospitalreferredto_show();" value="1">
        <div class="radiolabel">Yes</div>
        <input name="Health_Ref" type="radio" required class="wellness_center radiostyle Health_Ref" id="Health_Ref" onclick="enterhospitalreferredto_hide();" value="0" checked="checked">
        <div class="radiolabel">No</div>
        <div class="Health_Ref_Nme_Div" id="Health_Ref_Nme_Div" style="display: none;">
            <input type="text" placeholder="Enter Hospital Name :" class="Health_Ref_Nme fieldstyle" name="Health_Ref_Nme" id="Health_Ref_Nme" />
            <input type="text" placeholder="Enter Hospital ID :" class="Health_Ref_Nmes fieldstyle" name="Health_Ref_Nmes" readonly="" id="Health_Ref_Nmes" />

        </div>
        <div class="clearafter"></div>

        <label>Condoms Given</label>
        <input required name="Cd_Gvn" type="radio" class="wellness_center radiostyle Cd_Gvn" id="Cd_Gvn" value="1" onclick="enternoofcdgiven_show();">
        <div class="radiolabel">Yes</div>
        <input name="Cd_Gvn" type="radio" required class="wellness_center radiostyle Cd_Gvn" id="Cd_Gvn" value="0" checked="checked" onclick="enternoofcdgiven_hide();">
        <div class="radiolabel">No</div>
        <div id="No_CD_Gvn_Div" class="No_CD_Gvn_Div" style="display: none;">
            <input type="text" name="No_CD_Gvn" id="No_CD_Gvn" class="No_CD_Gvn fieldstyle" placeholder="Enter No of Condoms Given : "/>

        </div>

        <div class="clearafter"></div>

        <label>Lubricants Given</label>
        <input required name="Lub_Gvn" type="radio" class="wellness_center radiostyle Lub_Gvn" id="Lub_Gvn" value="1" onclick="enternooflubegiven_show();">
        <div class="radiolabel">Yes</div>
        <input name="Lub_Gvn" type="radio" required class="wellness_center radiostyle Lub_Gvn" id="Lub_Gvn" value="0" checked="checked" onclick="enternooflubegiven_hide();">
        <div class="radiolabel">No</div>
        <div id="No_Lub_Gvn_Div" class="No_Lub_Gvn_Div" style="display: none;">
            <input type="text" name="No_Lub_Gvn" id="No_Lub_Gvn" class="No_Lub_Gvn fieldstyle" placeholder="Enter No of Lubricants Given : "/>

        </div>
        <div class="clearafter"></div>


        <label>Partner Referral Done  </label>
        <input required name="Partnr_Ref" type="radio" class="wellness_center radiostyle Partnr_Ref" id="Partnr_Ref" value="1" onclick="enterpartnerref_show();">
        <div class="radiolabel">Yes</div>
        <input name="Partnr_Ref" type="radio" required class="wellness_center radiostyle Partnr_Ref" id="Partnr_Ref" value="0" checked="checked" onclick="enterpartnerref_hide();">
        <div class="radiolabel">No</div>
        <div id="Partner_Ref_Div" class="Partner_Ref_Div" style="display: none;">

            <select name="Partnr_Id" class="Partnr_Id_list select_with_label" id="Partnr_Id_list">

            </select>

        </div>
        <div class="clearafter"></div>



        <label>Internal Examinations Done:</label>
        <input required name="Intrnl_Exam" type="radio" class="wellness_center radiostyle Intrnl_Exam" id="Intrnl_Exam" value="1" >
        <div class="radiolabel">Yes</div>
        <input name="Intrnl_Exam" type="radio" required class="wellness_center radiostyle Intrnl_Exam" id="Intrnl_Exam" value="0" checked="checked" >
        <div class="radiolabel">No</div>

        <div class="clearafter"></div>
        &nbsp;
        <div class="clearafter"></div>

        <div>
            <textarea class="textarea Comments" cols="20" rows="5" id="Comments" placeholder="Clinical Officers Comments" name="Comments"></textarea>
        </div>

        <div class="clearafter"></div>
        &nbsp;
        <div class="clearafter"></div>

        <button class="large_button" > Register Data</button>



    </div>

</form>


<script>
    function sti_treatment() {

        var thestring = $("#sti_treatment_form").serialize();
        console.log(thestring);
        loadingindicator_start();
        $.post("database/Emarps/websrvc/websrvc.php", $("#sti_treatment_form").serialize())
                .done(function (data) {

                    loadingindicator_stop();
                    noty({
                        text: " STI Treatment Data Updated Successfully!",
                        type: 'success',
                        dismissQueue: true,
                        timeout: 5000,
                        closeWith: ['click'],
                        layout: 'center',
                        theme: 'defaultTheme',
                        maxVisible: 10

                    });

                });
    }

    function enterlabrefno_show() {

        document.getElementById("Lab_Ref_No_Div").style.display = "block";
    }
    function enterlabrefno_hide() {

        document.getElementById("Lab_Ref_No_Div").style.display = "none";
        document.getElementById("Lab_Ref_No").value = "";
    }

    function enterhospitalreferredto_show() {

        document.getElementById("Health_Ref_Nme_Div").style.display = "block";
    }
    function enterhospitalreferredto_hide() {

        document.getElementById("Health_Ref_Nme_Div").style.display = "none";
        document.getElementById("Health_Ref_Nme").value = "";
    }


    function enternoofcdgiven_show() {

        document.getElementById("No_CD_Gvn_Div").style.display = "block";
    }
    function enternoofcdgiven_hide() {

        document.getElementById("No_CD_Gvn_Div").style.display = "none";
        document.getElementById("No_CD_Gvn").value = "";
    }
    function enternooflubegiven_show() {

        document.getElementById("No_Lub_Gvn_Div").style.display = "block";
    }
    function enternooflubegiven_hide() {

        document.getElementById("No_Lub_Gvn_Div").style.display = "none";
        document.getElementById("No_Lub_Gvn").value = "";
    }
    function enterpartnerref_show() {

        document.getElementById("Partner_Ref_Div").style.display = "block";
    }
    function enterpartnerref_hide() {

        document.getElementById("Partner_Ref_Div").style.display = "none";
        document.getElementById("Partner_Ref_Div").value = "";
    }


    $(document).ready(function () {




        document.getElementById('UUIDTOSEND').value = localStorage.getItem('currentuuid');

        var uuid = $("#UUIDTOSEND").val();

        loadingindicator_start();

        $.ajax({
            type: "GET",
            url: "database/Emarps/websrvc/websrvc.php?task=valSTIFill&UUID=" + uuid + "",
            dataType: "JSON",
            success: function (response) {

                var id = response[0].id;

                if (id === "0") {
                    alert("Client is not Eligible for Care and Treatment service. ");
                    loadingindicator_stop();
                    setInterval(function () {
                        var host = document.location.host;
                        var url = "" + host + "/marps/main.php";
                        $(location).attr('href', "http://localhost:8888/marps/main.php");
                    }, 300);
                } else {
                    loadingindicator_stop();
                }


            }, error: function (response) {
                Alert("Error");
            }
        });




        partner_list = '';
        $.ajax({
            type: "GET",
            url: "database/Emarps/websrvc/websrvc.php?task=getPartners",
            dataType: "JSON",
            success: function (response) {
                for (i = 0; i < response.length; i++) {
                    partner_list = '<option value="' + response[i].partner_id + '">' + response[i].partner_name + '</option>';
                    $('#Partnr_Id_list').append(partner_list);
                }


            },
            error: function (response) {

            }
        });






        var ac_config = {
            source: "database/ajax.php",
            select: function (event, ui) {
                $("#Health_Ref_Nme").val(ui.item.facility_name);
                $("#Health_Ref_Nmes").val(ui.item.facility_id);
            },
            minLength: 1
        };

        $("#Health_Ref_Nme").autocomplete(ac_config);


    });
</script>