<?php
include 'database/reports.php';
include 'database/configuration.php';
$reports = new Reports();
$configuration = new Configuration();
$server_url = $configuration->get_server_name();


?>

<script type="text/javascript"  src="<?php echo $server_url;?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo $server_url;?>/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo $server_url;?>/css/jquery.dataTables.css" type="text/css" />


<link rel="stylesheet" href="<?php echo $server_url; ?>/css/jquery-ui.css">
<script src="<?php echo $server_url; ?>/js/jquery-ui.js"></script>

<style type="text/css">
    .user_report {width: 80%;}
    .dataTables_filter input {width:60px}
</style>


<script type="text/javascript">
    var j = jQuery.noConflict();
    j(document).ready(function () {
        j('#estimated_kps_aread_wrk').DataTable({
        });
        j('#estimated_kps_area_wrk_1').DataTable({
        });
        j("#filter_date_from").datepicker({
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });

        j("#filter_date_to").datepicker({
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });
    });
</script>




<h2>Estimated KP area of work Report</h2>

<div class="form animated fadeIn">



    <style type="text/css">
        #searchfilters {
            background-color: #FFFFFF;
            width: 700px;
            box-shadow: 0 0 15px 0px rgba(0,0,0,0.2);
            margin-top: 7px;
            margin-right: 20px;
            padding-top: 10px;
            padding-right: 10px;
            padding-left: 10px;
        }
        #searchfilters select {
            margin-right: 15px;
            margin-bottom: 15px;
        }
        .select_filters {
            transition: all 0.6s;
            background-size: 100% 100%;
            padding-top: 3px;
            padding-right: 5px;
            padding-bottom: 3px;
            padding-left: 5px;
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 10px;
            border: 1px solid #999999;
        }
    </style>

    <div id="searchfilters">
        <!-- KP Type filter start -->


        <?php
        $query = "SELECT * FROM `kp_types`
ORDER BY `kp_types`.`name` ASC";
        if ($result = mysqli_query($link, $query)) {
            ?>
            <?php
            ?>
            <select class="select_with_label" id="filter_kp_type" name="filter_kp_type">
                <option value="">
                    KP Type
                </option>
                <?php
                while ($idresult = mysqli_fetch_row($result)) {
                    $kp_type_id = $idresult[0];
                    $kp_type_name = $idresult[1];
                    ?>
                    <option id="partner_id"  value="<?php echo $kp_type_name; ?>"><?php echo $kp_type_name; ?></option>   <?php
                }
            }
            ?>
        </select>

        <!-- KP Type filter start -->
        <!-- Filter form start -->
        <form class="search_filter_form" id="search_filter_form" method="POST">

            <!-- County filter start -->
            <?php
            $query = "SELECT * FROM `region`
ORDER BY `region`.`region_name` ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>
                <?php
                ?>
                <select class="select_with_label" id="filter_county" name="filter_county">
                    <option value="">
                        County/Sub County
                    </option>
                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $region_id = $idresult[0];
                        $region_name = $idresult[1];
                        ?>
                        <option id="region"  value="<?php echo $region_name; ?>"><?php echo $region_name; ?></option>   <?php
                    }
                }
                ?>
            </select>

            <!-- County filter end -->

            <input type="text" id="filter_date_from" class="select_filters" placeholder="Date From :" name="filter_date_from"/>
            <input type="text" id="filter_date_to" placeholder="Date To: " class="select_filters" name="filter_date_to"/>


            <input type="submit" name="search_filter_button" id="search_filter_button" class="search_filter_button" value="Filter Report"/>

        </form>
    </div>

    <div id="estimated_kp_area_wrk_div" class="estimated_kp_area_wrk_div">


        <table id="estimated_kps_aread_wrk" class="estimated_kps_aread_wrk">
            <thead>
                <tr>
                    <th>No</th>
                    <th> KP Type</th>
                    <th>KP Target</th>
                    <th>KP Contacted</th>
                    <th>KP Enrolled </th>
                    <th>KP Receiving Services  </th>
                    <th>Region Name </th>


                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th> KP Type</th>
                    <th>KP Target</th>
                    <th>KP Contacted</th>
                    <th>KP Enrolled </th>
                    <th>KP Receiving Services  </th>
                    <th>Region Name </th>


                </tr>
            </tfoot>
            <tbody>


                <?php
                $res = $reports->estimated_kps_area_work();
                if (mysqli_num_rows($res) > 0) {
                    $i = 1;
                    while ($row = mysqli_fetch_array($res, MYSQLI_BOTH)) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['kp_target']; ?></td>
                            <td><?php echo $row['kp_type_contacted']; ?></td>
                            <td><?php echo $row['kp_type_enrolled']; ?></td>
                            <td><?php echo $row['kp_type_rcving_svrces']; ?></td>
                            <td><?php echo $row['region_name']; ?></td>


                        </tr>
                        <?php
                        $i++;
                    }
                } else {
                    ?><tr>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>
                        <td>Nothing here...</td>

                    </tr><?php
                }
                ?>



            </tbody>
        </table>
    </div>





    <div id="estimatd_kps_area_wrk_div_2" class="estimatd_kps_area_wrk_div_2" style="display: none;">

        <table id="estimated_kps_area_wrk_1" class="estimated_kps_area_wrk_1" >
            <thead>
                <tr>
                    <th>No</th>
                    <th> KP Type</th>
                    <th>KP Target</th>
                    <th>KP Contacted</th>
                    <th>KP Enrolled </th>
                    <th>KP Receiving Services  </th>
                    <th>Region Name </th>


                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th> KP Type</th>
                    <th>KP Target</th>
                    <th>KP Contacted</th>
                    <th>KP Enrolled </th>
                    <th>KP Receiving Services  </th>
                    <th>Region Name </th>


                </tr>
            </tfoot>
            <tbody id="tbody_append" class="tbody_append">

            </tbody>
        </table>

    </div>


</div>
