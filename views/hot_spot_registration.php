<?php
if (isset($_REQUEST['save'])) {
    extract($_REQUEST);
    $succes = $websrvc->hot_spot_registration($hot_spot_name, $user_partner_name, $user_clinic_name, $date_added, $region_name);
    if ($succes) {
        ?>
        <script src="js/jquery.min.js"></script>


        <!-- noty -->
        <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                noty({text: 'Hot Spot Added Succesfully...'});
            });
        </script>


        <?php
    } else {

        echo 'Wrong information please try again';
    }
}
?>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>

<h2>User Registration Form</h2>
<script src="js/conditional.js"></script>
<div class="form animated fadeIn" style="width:400px;">

    <center>
        <div>

            <form name="hot_spot_registration" id="hot_spot_registration"  class="hot_spot_registration" accept-charset="UTF-8" method="POST" action=""> 

                <div class="left">


                    <input name="hot_spot_name" placeholder="Hot Spot Name" required="" class="fieldstyle hot_spot_name"  id="hot_spot_name" type="text">

                    <input type="hidden" name="date_added" value="<?php echo date("Y-m-d H:i:s"); ?>"/>

                    <?php
// GET LIST AND DISPLAY IN FORM
// check connection
                    ?>
                    <label>Partner Name : </label>

                    <?php
                    $query = "SELECT partner_id,partner_name,contact_person FROM partner ORDER BY  partner_id ASC";
                    if ($result = mysqli_query($link, $query)) {
                        ?>
                    <select onchange="document.getElementById('clinic_name_holder').style.display = 'block';" class="partner_name_hot_spot fieldstyle_with_label" id="partner_name_hot_spot" required="" name="user_partner_name">
                            <option id="" value="">Please select a partner</option>
                            <?php
                            while ($idresult = mysqli_fetch_row($result)) {
                                $partner_id = $idresult[0];
                                $partner_name = $idresult[1];
                                $contact_person = $idresult[2];
                                ?>
                                <option id="partner_name_hot_spot"  value="<?php echo $partner_id; ?>"><?php echo $partner_name . '&nbsp;(' . $contact_person . ')'; ?></option>   <?php
                            }
                            ?>
                        </select>

                        <div style="clear:both"></div>

                        <?php
                    }
                    ?>
                    <br>

                    <div id="clinic_name_holder" style="display:none;">

                        <label>Clinic Name : </label>
                        <select class="clinic_name_hot_spot fieldstyle_with_label" required="" name="user_clinic_name" id="clinic_name_hot_spot">

                        </select>

                    </div>

                    <div style="clear:both"></div>

                    <label>Region Name : </label>

                    <?php
                    $query = "SELECT * FROM `region` ORDER BY region_name ASC";
                    if ($result = mysqli_query($link, $query)) {
                        ?>
                        <select  class="region_name fieldstyle_with_label"  required="" name="region_name" id="region_name">

                            <?php
                            while ($idresult = mysqli_fetch_row($result)) {
                                $region_id = $idresult[0];
                                $region_name = $idresult[1];
                                ?>
                                <option id="partner_id"  value="<?php echo $region_id; ?>"><?php echo $region_name . '&nbsp;'; ?></option>   <?php
                            }
                            ?>
                        </select>

                        <div style="clear:both"></div>

                        <?php
                    }
                    ?>





                    <div class="clearafter"></div>

                </div>

                <div class="right">



                </div>

                <div class="next_button_container">

                    <input class="large_button" type="submit" value="Add Hot Spot " name="save" />
                </div>  
            </form>





        </div></center>

</div>