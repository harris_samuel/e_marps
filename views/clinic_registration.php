<?php
if (isset($_REQUEST['save'])) {
    extract($_REQUEST);
    $success = $websrvc->clinic_registration($clinic_name, $county, $facility_code, $facility_id, $partner_name, $clinic_region_name, $date_today, $kp_target, $kp_typess, $kp_targetss);
    if ($success) {
        ?>


        <script src="js/jquery.min.js"></script>


        <!-- noty -->
        <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                noty({text: 'Clinic Added Succesfully...'});
            });
        </script>


        <?php
    } else {



        echo '<div class="animated bounceIn warning">Wrong information please try again. </div>';
    }
}
?>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>





<h2>Clinic Registration Form</h2>
<script src="js/conditional.js"></script>
<div class="form animated fadeIn">

    <form name="clinic_registration" id="clinic_registration"  class="clinic_registration" accept-charset="UTF-8" method="POST" action=""> 
        <div class="left">

            <label>Clinic/Facility Name </label>



            <input type="text" class="clinic_name fieldstyle_with_label" required="" placeholder="Clinic Name" name="clinic_name" id="clinic_name"/>

            <input type="hidden" class="facility_id" name="facility_id" id="facility_id"/>

            <label>County</label>
            <input class="fieldstyle_white_with_label county" id="county" required="" name="county" placeholder="County" readonly="" type="text">

        </div>
        <div class="right">
            <label>Facility/M.F.L Code</label>
            <input class="fieldstyle_white_with_label facility_code" name="facility_code" required="" placeholder="Facility/MFL Code" readonly="" id="facility_code" type="text">

            <input name="date_today" type="hidden" value="<?php
            $my_date = date("Y-m-d H:i:s");
            echo $my_date;
            ?>"/>

            <label>Partner Name : </label>

            <?php
            $query = "SELECT partner_id,partner_name,contact_person FROM partner ORDER BY  partner_id ASC";
            if ($result = mysqli_query($link, $query)) {

                //
                //SELECT THE REFERAL SOURCE
                ?>
                <select  onchange="document.getElementById('region_name_holder').style.display = 'block';"  class="partner_name fieldstyle_with_label" required="" name="partner_name">
                    <option>Please select one : </option>
                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $partner_id = $idresult[0];
                        $partner_name = $idresult[1];
                        $contact_person = $idresult[2];
                        ?>
                        <option value="<?php echo $partner_id; ?>"><?php echo $partner_name . '&nbsp;(' . $contact_person . ')'; ?></option>   <?php
                    }
                    ?>
                </select>
                <?php
            }
            ?>



        </div>

        <br>

        <div id="region_name_holder" style="display:none;">


            <div class="notice">Hold down <img align="absmiddle" src="images/ctrl.png" height="30" /> to select multiple answers</div>


            <label>Region Name : </label>
            <select class="clinic_region_name fieldstyle_with_label" required="" multiple="multiple" name="clinic_region_name[]" id="clinic_region_name">

            </select>

        </div>
        <br>
        <div>
            <label>Key Population Target </label>
            <input type="text" name="kp_target" required="" class="fieldstyle_white_with_label kp-target" id="kp_target" placeholder="KP Target" />
        </div>


        <script type="text/javascript">

            $(document).ready(function () {
                var rowCount = 1;
                $(".add_more_rows").click(function () {

                    rowCount++;

                    var recRow = '<li id="rowCount' + rowCount + '"><tr><td><label>Key Population Type :</label><?php
            $query = "SELECT * FROM `kp_types` ORDER BY Name ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>\n\<select  class="kp_types fieldstyle_with_label"  required="" name="kp_typess[]" id="kp_types">\n\<?php
                while ($idresult = mysqli_fetch_row($result)) {
                    $kp_type_id = $idresult[0];
                    $kp_type_name = $idresult[1];
                    ?>\n\<option id="partner_id"  value="<?php echo $kp_type_id; ?>"><?php echo $kp_type_name . '&nbsp;'; ?></option>\n\<?php }
                ?>\n\ </select>\n\<?php }
            ?></td>\n\
<td><input name="kp_targetss[]" type="text"  maxlength="120" style="margin: 4px 5px 0 5px;"/></td>\n\
\n\
<td> <span id="remove_row' + rowCount + '" onClick="javascript:fnRemove(this);" class="remove_row' + rowCount + '" style="font:normal 12px agency, arial; color:blue; text-decoration:underline; cursor:pointer;">Delete Entry</span> </td></tr> </li>';


                    $('.remove_row' + rowCount + '').click(function () {
                        alert('.remove_row' + rowCount + '');
                        $('#rowCount' + rowCount).remove();
                        alert('#rowCount' + rowCount);
                    });
                    $('#addedRows').append(recRow);


                });

            });

            function fnRemove(t) {

                $(t).parent('li').remove();

            }
        </script>

        <div style="margin-top:120px ;">
            <table rules="all" style="background:#fff;">
                <tr>
                    <td style="font-size:14px;">KP Type</td>
                    <td style="font-size:14px;">KP Target</td>
                    <td><span class="add_more_rows" id="add_more_rows" style="font:normal 12px agency, arial; color:blue; text-decoration:underline; cursor:pointer;">
                            Add More
                        </span>
                    </td>
                </tr>
                <tr id="rowId">
                    <td>
                        <label>Key Population Types : </label>

                        <?php
                        $query = "SELECT * FROM `kp_types` ORDER BY Name ASC";
                        if ($result = mysqli_query($link, $query)) {
                            ?>
                            <select  class="kp_types fieldstyle_with_label"  required="" name="kp_typess[]" id="kp_types">
                                <option></option>
                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $kp_type_id = $idresult[0];
                                    $kp_type_name = $idresult[1];
                                    ?>
                                    <option id="partner_id"  value="<?php echo $kp_type_id; ?>"><?php echo $kp_type_name . '&nbsp;'; ?></option>   <?php
                                }
                                ?>
                            </select>

                            <div style="clear:both"></div>

                            <?php
                        }
                        ?>
                    </td>
                    <td>
                        <input name="kp_targetss[]" type="text" value="" />
                    </td>

                </tr>
            </table>
            <div id="addedRows"></div>

        </div>


        <div class="next_button_container">

            <input class="large_button" type="submit" value="Register Clinic" name="save" />
        </div>  
    </form>







</div>