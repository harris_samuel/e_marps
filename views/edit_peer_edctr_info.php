<?php
include 'database/class.admin.php';
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$admin = new ADMIN();
if (isset($_GET['edit_id'])) {
    $edit_id = $_GET['edit_id'];

    $select_query = "SELECT * FROM manage_peer_educator where id='$edit_id'";
    $res = mysqli_query($link, $select_query);
    if ($res) {
        $row = mysqli_fetch_array($res, MYSQLI_BOTH);
    } else {

        echo 'Error' . mysqli_errno($link);
    }
}
if (isset($_REQUEST['update_peer_educator'])) {
    extract($_REQUEST);
    // echo 'Details ' . '<br>' . $user_name . '<br>' . $edit_id . '<br>' . $e_mail . '<br>' . $phone_no . '<br>' . $status . '<br>' . $role_name . '<br>';
    $success = $admin->update_peer_educator($peer_educator_id, $peer_educator_nme, $nick_name, $phone_no, $status);
    if ($success) {
        echo 'User Information Updated Successfully';
    } else {
        echo 'Error please try again ...';
    }
}
?> 
<div class="form animated fadeIn">
    <script type="text/javascript">
        function goBack() {
            window.history.back()
        }

    </script>

    <form method="post" autocomplete="off" name="update_peer_educator_form" class="update_peer_educator_form" id="update_peer_educator_form">
        <table id="dataview">
            <?php
            $count_row = mysqli_num_rows($res);
            if ($count_row === 1) {
                ?>   
            <input type="hidden" name="peer_educator_id" value="<?php echo $edit_id; ?>"/>
                <tr><td><input type="text" name="peer_educator_nme" placeholder="Peer Educator Name" value="<?php echo $row['peer_educator_nme'] ?>" /><br /></td></tr>
                <tr><td><input type="text" name="nick_name" placeholder="Nick Name" value="<?php echo $row['nick_name'] ?>" /></td></tr>
                <tr><td><input type="text" name="phone_no" placeholder="Phone Number" value="<?php echo $row['phone_no'] ?>" /></td></tr>
                <tr><td>
                        <select name="status" id="" class="status">
                            <option value="<?php echo $row['status'] ?>"><?php echo $row['status'] ?></option>
                            <option value="Active">Active</option>
                            <option value="Dead">Dead</option>
                            <option value="Deleted">Deleted</option>
                            <option value="Discontinued">Discontinued</option>
                            <option value="Lost">Lost</option>
                            <option value="On Going">On Going</option>
                            <option value="Pending">Pending</option>
                            <option value="Transfer" >Transfer</option>
                            <option value="#N/A" >#N/A</option>

                        </select>
                    </td></tr>
           
                <tr><td>
                        <input type="submit" name="update_peer_educator" value="Update Peer Educator Details" class="update_peer_educator button" id="update_peer_educator"/>
                    </td><td>
                        <button class="button" onclick="goBack()">Cancel</button>
                    </td></tr>
                <?php
            } else {
                
            }
            ?>
        </table>
    </form>
</table>
</div>