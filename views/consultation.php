<style type="text/css">
    .clinic_visit_list_entry {
        font-size: 13px;
        background-color: #FFF;
        padding: 5px;
        margin-top: 5px;
        margin-bottom: 5px;
        border: 1px solid #CCC;
        display: block;
    }
</style>

<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>


<script>

    $(document).ready(function () {

        document.getElementById('UUIDTOSEND').value = localStorage.getItem('currentuuid');
        document.getElementById('UUIDTOSEND2').value = localStorage.getItem('currentuuid');
        //Get UUID 
        var uuid = $('#UUIDTOSEND').val();
        //set UUID 

        $('#UUIDTOSEND3').val(uuid);
        $("#add_to_list_btn").click(function () {
            Clinical_Visit();
        });
        $("#add_to_list_2_btn").click(function () {
            additional_service();
        });
        var currentuuid = localStorage.getItem('currentuuid');





        var ac_config = {
            source: "database/ajax.php",
            select: function (event, ui) {
                $("#txt_enterhospitalreferredto").val(ui.item.facility_name);
            },
            minLength: 1
        };
        $("#txt_enterhospitalreferredto").autocomplete(ac_config);
        $("#select_service").change(function () {
            var value = $(this).val();
            $(".service_div_list div").each(function () {

                if ($(this).text().match(value)) {
                    //Text found in div 
                    alert("The Selected service has already been added!!");
                    $("#select_service option:selected").remove();
                }
            });
        });
        $("#additional_service_select").change(function () {
            var value = $(this).val();
            $(".additional_clnc_vst_entry div").each(function () {

                if ($(this).text().match(value)) {
                    //Text found in div 
                    alert("The Selected service has already been added!!");
                    $("#additional_service_select option:selected").remove();
                }
            });
        });







        $.ajax({
            type: "GET",
            url: "database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=hivscrn",
            dataType: "JSON",
            success: function (response) {
                $.each(response, function (i, resp) {


                    //SELF REPORTED STATUS GAPS CHECKER
                    $("input[name=SRS]").prop('checked', false); //uncheck all
                    $("input[name=SRS][value=" + resp.self_rptd_status + "]").prop('checked', true);

                    //COUNSELLED STATUS GAPS CHECKER
                    $("input[name=Counseled]").prop('checked', false); //uncheck all
                    $("input[name=Counseled][value=" + resp.counselled + "]").prop('checked', true);


                    //TESTED STATUS GAPS CHECKER
                    $("input[name=Tested]").prop('checked', false); //uncheck all
                    $("input[name=Tested][value=" + resp.tested + "]").prop('checked', true);


                    //RECEIVED RESULTS STATUS GAPS CHECKER
                    $("input[name=Recvd_Rslts]").prop('checked', false); //uncheck all
                    $("input[name=Recvd_Rslts][value=" + resp.received_rslts + "]").prop('checked', true);


                    //ANTI RETROVIRAL STATUS GAPS CHECKER
                    $("input[name=Arv]").prop('checked', false); //uncheck all
                    $("input[name=Arv][value=" + resp.anti_ret + "]").prop('checked', true);

                    //TESTING RESULTS STATUS GAPS CHECKER
                    $("input[name=Testn_Rslts]").prop('checked', false); //uncheck all
                    $("input[name=Testn_Rslts][value=" + resp.testing_rslts + "]").prop('checked', true);


                    //NEXT APPOINTMENT DATE GAPS CHECKER 
                    $("input[name=Nxt_Appntmnt]").val(resp.next_appntmnt_date);


                    //Get value of the  insert or update
                    var one = "1";
                    $("input[name=Edit]").val(one);
                    $("#Edit1").val(one);
                    $("#Edit2").val(one);


                    //Row ID to be Updated GAPS CHECKER
                    $("input[name=Rid]").val(resp.id);
                    $("#Rid1").val(resp.id);
                    $("#Rid2").val(resp.id);



                });

            }
        });



    });


    function get_cv_id() {
        var currentuuid = localStorage.getItem('currentuuid');
        $.ajax({
            type: "GET",
            url: "views/get_cvid.php?UUID=" + currentuuid,
            dataType: "JSON",
            success: function (response) {

                $('#cv_id_1').val(response[0].cvid);
                $('#cv_id_2').val(response[0].cvid);

            }
        });

    }
    function Clinical_Visit() {

        var thestring = $("#Clinical_Visit").serialize();
        console.log(thestring);
        loadingindicator_start();
        $.post("database/Emarps/websrvc/websrvc.php", $("#Clinical_Visit").serialize())
                .done(function (data) {
                    loadingindicator_stop();
                    addtolist1();
                    get_cv_id();


                    noty({
                        text: " Clinic Visit Data Updated Successfully!",
                        type: 'success',
                        dismissQueue: true,
                        timeout: 5000,
                        closeWith: ['click'],
                        layout: 'center',
                        theme: 'defaultTheme',
                        maxVisible: 10

                    });


                });
    }
    function additional_service() {

        var thestring = $("#additional_service_form").serialize();
        console.log(thestring);
        loadingindicator_start();
        $.post("database/Emarps/websrvc/websrvc.php", $("#additional_service_form").serialize())
                .done(function (data) {
                    loadingindicator_stop();
                    addtolist2();

                    noty({
                        text: " Data Added Successfully!",
                        type: 'success',
                        dismissQueue: true,
                        timeout: 5000,
                        closeWith: ['click'],
                        layout: 'center',
                        theme: 'defaultTheme',
                        maxVisible: 10

                    });


                });
    }
    function hiv_screening() {

        var thestring = $("#hiv_screening_form").serialize();
        console.log(thestring);
        loadingindicator_start();
        $.post("database/Emarps/websrvc/websrvc.php", $("#hiv_screening_form").serialize())
                .done(function (data) {
                    loadingindicator_stop();

                    noty({
                        text: "HIV Screening Data Updated Successfully!",
                        type: 'success',
                        dismissQueue: true,
                        timeout: 5000,
                        closeWith: ['click'],
                        layout: 'top',
                        theme: 'defaultTheme',
                        maxVisible: 10

                    });




                });
    }
    $(function () {
        $("#next_appointment_date_input").datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

</script>
<div id="titlebar"><div id="titlebartext">Clinic Visit MOH904</div></div>

<script>
    sidebar4();</script>



<div class="div_1" id="div_1">


    <div id="div_one" class="div_one" >



        <div id="clinic_visit_a" class="animated fadeIn">


            <form id="Clinical_Visit" class="Clinical_Visit" action="javascript:Clinical_Visit();">

                <div style="display:none;">
                    <input  name="task" value="regClncVst">
                    <input id="UUIDTOSEND" name="UUID" value="">
                    <input name="Edit" value="0">

                    <input name="Pid" id="Pid" class="Pid" value="<?php echo $_SESSION["sess_partner_id"] ?>" />
                    <input name="Clinic" id="Clinic" class="Clinic" value="<?php echo $_SESSION["sess_clinic_id"] ?>" />
                    <input type="text" id="CollPnt" class="CollPnt" value="Clinical" name="CollPnt"/>
                    <input type="text" id="Rid" name="Rid" class="Rid" value="0"/>

                </div>

                <table width="700" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="3" valign="top">

                            <label>Select Service</label>
                            <select class="select_with_label" name="Service" id="select_service">
                                <option selected="selected">Please select</option>
                                <option value="STI">STI</option>
                                <option value="TB">TB</option>
                                <option value="Hepatitis B">Hepatitis B</option>
                                <option value="Hepatitis C">Hepatitis C</option>
                                <option value="Overdose management">Overdose Management</option>
                                <option value="Abscess">Abscess</option>
                                <option value="Alcohol & drug abuse">Alcohol & drug abuse</option>
                                <option value=" Cervical cancer" >Cervical cancer</option>
                            </select>


                        </td>
                    </tr>
                    <tr>
                        <td valign="top"><label style="width:100%">Screened?</label></td>
                        <td valign="top"><label style="width:100%">Treated?</label></td>
                        <td valign="top"><label style="width:100%">Referred?</label></td>
                    </tr>
                    <tr>
                        <td width="390" valign="top">




                            <input name="Scrnd"  type="radio" class="wellness_center radiostyle" value="Yes" checked="checked"><div class="radiolabel">Yes</div>
                            <input name="Scrnd" type="radio" class="wellness_center radiostyle" value="No">  <div class="radiolabel">No</div>

                        </td>
                        <td width="390" valign="top">



                            <input name="Trted" type="radio" class="wellness_center radiostyle" value="Treated" checked="checked"><div class="radiolabel">Yes</div>
                            <input name="Trted" type="radio" class="wellness_center radiostyle" value="Not Treated"> <div class="radiolabel">No</div>

                        </td>
                        <td width="390" valign="top">



                            <input name="Refrd" type="radio" class="wellness_center radiostyle" value="Yes" onclick="enterhospitalreferredto_show();"><div class="radiolabel">Yes</div>
                            <input name="Refrd" type="radio" class="wellness_center radiostyle" onclick="enterhospitalreferredto_hide();" value="No" checked="checked">  <div class="radiolabel">No</div>

                            <input id="txt_enterhospitalreferredto" name="Refrd_To" class="fieldstyle" type="text" placeholder="Enter Hospital referred to" style="display:none">

                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="top">
                            <div id="clinic_visit_list1"></div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">&nbsp;</td>
                        <td valign="top">&nbsp;</td>
                        <td valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="top">
                            <button type="button" class="small_button add_to_list_btn" id="add_to_list_btn" style="width:100%">Add to List</button>
                            <!--<div class="small_button" style="width:100%;" onclick="addtolist1();">Add to List</div>-->
                        </td>
                    </tr>
                </table>




            </form>



        </div>





        <form id="additional_service_form" class="additional_service_form" action="javascript:additional_service();">

            <div style="display:none;">
                <input  name="task" value="regClncVst2">
                <input id="UUIDTOSEND2" name="UUID" value="">
                <input name="Edit" id="Edit1" class="Edit1" value="0">
                <input type="text" id="cv_id_1" class="cv_id_1" name="Cvid"/>
                <input name="Pid" id="Pid" class="Pid" value="<?php echo $_SESSION["sess_partner_id"] ?>" />
                <input name="Clinic" id="Clinic" class="Clinic" value="<?php echo $_SESSION["sess_clinic_id"] ?>" />
                <input type="text" id="CollPnt" class="CollPnt" value="Clinical" name="CollPnt"/>
                <input type="text" id="Rid1" name="Rid" class="Rid1" value="0"/>

            </div>


            <table width="700" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td colspan="3" valign="top"><label>Additional Services</label>
                        <select class="select_with_label additional_service_select" name="Addtn_Servce" id="additional_service_select">
                            <option selected="selected">Please select</option>
                            <option value="PHDP">PHDP</option>
                            <option value="Family planning">Family planning</option>
                            <option value="Risk reduction counselling">Risk  reduction counselling</option>
                            <option value="Gender based violence">Gender based violence</option>
                            <option value="HIV care & treatment">HIV care & treatment</option>
                            <option value="Condom education/demonstration and distribution" >Condom education/demonstration and distribution</option>
                            <option value="Post abortal care">Post abortal care</option>
                            <option value="PEP">PEP</option>
                            <option value="Linkage to psychosocial support">Link to psychosocal support</option>
                        </select></td>
                </tr>
                <tr>
                    <td valign="top"><label style="width:100%">Given?</label></td>
                    <td valign="top"></td>
                    <td valign="top"></td>
                </tr>
                <tr>
                    <td width="390" valign="top">
                        <input name="addtn_srvcs_chcked" type="radio" class="wellness_center radiostyle" value="Yes" checked="checked" />
                        <div class="radiolabel">Yes</div>
                        <input name="addtn_srvcs_chcked" type="radio" class="wellness_center radiostyle" value="No" />
                        <div class="radiolabel">No</div></td>
                    <td width="390" valign="top"></td>
                    <td width="390" valign="top"></td>
                </tr>
                <tr>
                    <td colspan="3" valign="top"><div id="additional_service_div"></div></td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" valign="top">
                        <!--<div class="small_button" style="width:100%;" onclick="addtolist2();">Add to List</div>-->
                        <button type="button" class="small_button add_to_list_2_btn" id="add_to_list_2_btn" style="width:100%">Add to List</button>

                    </td>
                </tr>
            </table>


            <table width="700" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td colspan="2" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">

                        <button class="large_button" type="button"  onClick="show_div_2();">Next</button>


                    </td>
                </tr>
            </table>

        </form>


    </div>



</div>

<div class="div_2" id="div_2" style="display: none;">



    <form id="hiv_screening_form" class="hiv_screening_form" action="javascript:hiv_screening();">

        <div style="display:none;">
            <input  name="task" value="regClncVst3">
            <input id="UUIDTOSEND3" name="UUID" value="">
            <input name="Edit" id="Edit2" class="Edit2" value="0">
            <input type="text" id="cv_id_2" class="cv_id_2" name="Cvid"/>
            <input name="Pid" id="Pid" class="Pid" value="<?php echo $_SESSION["sess_partner_id"] ?>" />
            <input name="Clinic" id="Clinic" class="Clinic" value="<?php echo $_SESSION["sess_clinic_id"] ?>" />
            <input type="text" id="CollPnt" class="CollPnt" value="Clinical" name="CollPnt"/>
            <input type="text" id="Rid2" name="Rid" class="Rid2" value="0"/>

        </div>

        <div id="div_two" class="div_two" style="display: inline-block;">
            <table width="100%" border="0">
                <tr>
                    <td width="33%" class="self_reported_status_labels">Self Reported Status</td>
                    <td width="33%" class="counselled_labels">Counselled</td>

                </tr>
                <tr>
                    <td class="self_reported_status_options" valign="top">

                        <input required name="SRS" ID="SRS" type="radio" class="self_reported_status_input SRS radiostyle" value="Positive">
                        <div class="radiolabel">Positive</div>

                        <div style="clear:both;"></div>

                        <input required name="SRS" ID="SRS" type="radio" class="self_reported_status_input SRS radiostyle" value="Negative">
                        <div class="radiolabel">Negative</div>

                        <div style="clear:both;"></div>

                        <input required name="SRS" ID="SRS" type="radio" class="self_reported_status_input SRS radiostyle" value="Unknown">
                        <div class="radiolabel">Unknown</div>



                    </td>
                    <td class="counselled_options" valign="top">    
                        <input required name="Counseled" type="radio" class="wellness_center radiostyle" value="Yes">
                        <div class="radiolabel">Yes</div>

                        <div style="clear:both;"></div>

                        <input required name="Counseled" type="radio" class="wellness_center radiostyle" value="No">
                        <div class="radiolabel">No</div>

                        <div style="clear:both;"></div>


                    </td>

                </tr>


                <tr>
                    <td width="33%" class="tested_labels">Tested</td>
                    <td width="33%" class="received_results_label">Received Results</td>

                </tr>
                <tr>
                    <td class="tested_options" valign="top">

                        <input required name="Tested" type="radio" class="tested_input radiostyle" value="Yes"><div class="radiolabel">Yes</div>

                        <div style="clear:both;"></div>

                        <input required name="Tested" type="radio" class="tested_input radiostyle" value="No">
                        <div class="radiolabel">No</div>

                        <div style="clear:both;"></div>




                    </td>
                    <td class="received_results_options" valign="top">    
                        <input required name="Recvd_Rslts" type="radio" class="received_results_input radiostyle" value="Yes">
                        <div class="radiolabel">Yes</div>

                        <div style="clear:both;"></div>

                        <input required name="Recvd_Rslts" type="radio" class="received_results_input radiostyle" value="No">
                        <div class="radiolabel">No</div>

                        <div style="clear:both;"></div>


                    </td>

                </tr>



                <tr>
                    <td width="33%" class="anti_retroviral_labels">Anti-Retroviral </td>
                    <td width="33%" class="testing_results_labels">Testing Results</td>

                </tr>
                <tr>
                    <td class="anti_retroviral_options" valign="top">

                        <input required name="Arv" type="radio" class="ant_retroviral_input radiostyle" value="Provided">
                        <div class="radiolabel">Provided</div>

                        <div style="clear:both;"></div>

                        <input required name="Arv" type="radio" class="ant_retroviral_input radiostyle" value="Not Provided">
                        <div class="radiolabel">Not Provided</div>

                        <div style="clear:both;"></div>

                        <input required name="Arv" type="radio" class="ant_retroviral_input radiostyle" value="Referred">
                        <div class="radiolabel">Referred</div>
                        <div style="clear:both;"></div>

                        <input required name="Arv" type="radio" class="ant_retroviral_input radiostyle" value="Not Applicable">
                        <div class="radiolabel">Not Applicable</div>



                    </td>
                    <td class="testing_results_options" valign="top">    
                        <input required name="Testn_Rslts" type="radio" class="testing_results_input radiostyle" value="Positive">
                        <div class="radiolabel">Positive</div>

                        <div style="clear:both;"></div>

                        <input required name="Testn_Rslts" type="radio" class="testing_results_input radiostyle" value="Negative">
                        <div class="radiolabel">Negative</div>

                        <div style="clear:both;"></div>
                        <input required name="Testn_Rslts" type="radio" class="testing_results_input radiostyle" value="Unknown">
                        <div class="radiolabel">Unknown</div>

                        <div style="clear:both;"></div>


                    </td>



                </tr>

                <tr>
                    <td class="next_appointment_date" valign="top">

                        <input type="text" name="Nxt_Appntmnt" class="fieldstyle next_appointment_date_input" id="next_appointment_date_input" placeholder="Enter Next Appointment Date : "/>
                    </td> 
                </tr>



            </table>



            <table width="700" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td colspan="2" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">

                        <button class="large_button" type="button"  onClick="show_div_1();">Previous</button>

                        <button class="large_button" > Register Data</button>
                    </td>
                </tr>
            </table>
        </div>


    </form>


</div>






<script>
    function clinic_visit_add_to_list() {

    }
    function clinic_visit_add_to_list2() {

    }
    function enterhospitalreferredto_show() {
        document.getElementById("txt_enterhospitalreferredto").style.display = "block";
    }
    function enterhospitalreferredto_hide() {
        document.getElementById("txt_enterhospitalreferredto").style.display = "none";
        document.getElementById("txt_enterhospitalreferredto").value = "";
    }
    function addtolist1() {
        clinic_visit_service = document.getElementById("select_service").value;
        if (clinic_visit_service == "Please select") {
            alert("Please select a service!");
            return;
        }
        else {
            clinic_visit_enterhospitalreferredto = document.getElementById("txt_enterhospitalreferredto").value;
            clinic_visit_service_screened = $('input:radio[name=Scrnd]:checked').val();
            clinic_visit_service_treated = $('input:radio[name=Trted]:checked').val();
            clinic_visit_service_referred = $('input:radio[name=Refrd]:checked').val();
            $("#clinic_visit_list1").append('<div class="clinic_visit_list_entry service_div_list animated fadeInUp"><div>' + clinic_visit_service + '</div><div>Screened: ' + clinic_visit_service_screened + '</div><div>Treated: ' + clinic_visit_service_treated + '</div><div>Referred: ' + clinic_visit_service_referred + ' ' + clinic_visit_enterhospitalreferredto + '</div></div>');
        }
    }

    function addtolist2() {
        clinic_visit_additional_service = document.getElementById("additional_service_select").value;
        if (clinic_visit_additional_service == "Please select") {
            alert("Please select an additional service!");
            return;
        }
        else {

            clinic_visit_additional_services = $('input:radio[name=addtn_srvcs_chcked]:checked').val();
            $("#additional_service_div").append('<div class="clinic_visit_list_entry animated additional_clnc_vst_entry fadeInUp"><div>' + clinic_visit_additional_service + '</div><div>Service Given: ' + clinic_visit_additional_services + '</div></div>');

        }
    }

    function show_div_1() {
        document.getElementById("div_1").style.display = "block";
        document.getElementById("div_2").style.display = "none";
    }


    function show_div_2() {
        document.getElementById("div_1").style.display = "none";
        document.getElementById("div_2").style.display = "block";
    }
</script>
