<?php
include 'database/reports.php';

$reports = new Reports();
?>


<script type="text/javascript"  src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.7/css/jquery.dataTables.css" type="text/css" />


<style type="text/css">
    .user_report {width: 80%;}
    .dataTables_filter input {width:60px}
</style>



<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
    var j = jQuery.noConflict();
    j(document).ready(function () {

        j('#partners_report').DataTable({
        });

        j('#table_1_data').DataTable({
        });

        j("#filter_date_from").datepicker({
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });

        j("#filter_date_to").datepicker({
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });

    });


    $(document).ready(function () {

        $.ajax({
            type: "GET",
            url: "/marps/database/Emarps/websrvc/websrvc.php?task=getData&UUID=1K-15.35-689&DataGt=hk",
            dataType: "JSON",
            success: function (response) {
                $('#PrevOptns option[value=' + response[0].hiv_prev + ']').attr('selected', true);

                /*  $("#PrevOptns").append('<option value=' + response[0].hiv_prev + '>' + response[0].hiv_prev + '</option>');
                 $("#HivTrans").append('<option value=' + response[0].hiv_trans + '>' + response[0].hiv_trans + '</option>');
                 $('#PrevOptns').trigger("chosen:updated");
                 $('#HivTrans').trigger("chosen:updated");$("#PrevOptns").append('<option value=' + response[0].hiv_prev + '>' + response[0].hiv_prev + '</option>');
                 $("#HivTrans").append('<option value=' + response[0].hiv_trans + '>' + response[0].hiv_trans + '</option>');
                 $('#PrevOptns').trigger("chosen:updated");
                 $("#HivTrans").append('<option value=' + response[0].hiv_trans + '>' + response[0].hiv_trans + '</option>');
                 $('#HivTrans').trigger("chosen:updated");  */


            }
        });
    });
</script>



<h2>K.P's Contacted  Report</h2>

<div class="form animated fadeIn">



    <style type="text/css">
        #searchfilters {
            background-color: #FFFFFF;
            width: 700px;
            box-shadow: 0 0 15px 0px rgba(0,0,0,0.2);
            margin-top: 7px;
            margin-right: 20px;
            padding-top: 10px;
            padding-right: 10px;
            padding-left: 10px;
        }
        #searchfilters select {
            margin-right: 15px;
            margin-bottom: 15px;
        }
        .select_filters {
            transition: all 0.6s;
            background-size: 100% 100%;
            padding-top: 3px;
            padding-right: 5px;
            padding-bottom: 3px;
            padding-left: 5px;
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 10px;
            border: 1px solid #999999;
        }
    </style>

    <div id="searchfilters">

        <!-- Filter form start -->
        <form class="search_filter_form" id="search_filter_form" method="POST">
            <!-- Partner filter start-->
            <?php
            $query = "SELECT partner_id,partner_name,contact_person FROM partner ORDER BY  partner_id ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>
                <?php
                ?>

                <select class="select_filters" id="filter_partner" name="filter_partner">
                    <option value="">
                        Implementing Partner
                    </option>
                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $partner_id = $idresult[0];
                        $partner_name = $idresult[1];
                        $contact_person = $idresult[2];
                        ?>
                        <option id="partner_id"  value="<?php echo $partner_name; ?>"><?php echo $partner_name . '&nbsp;(' . $contact_person . ')'; ?></option>   <?php
                    }
                }
                ?>
            </select>
            <!-- Partner filter end -->
            <!-- County filter start -->
            <?php
            $query = "SELECT * FROM `region`
ORDER BY `region`.`region_name` ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>
                <?php
                ?>
                <select class="select_with_label" id="filter_county" name="filter_county">
                    <option value="">
                        County/Sub County
                    </option>
                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $region_id = $idresult[0];
                        $region_name = $idresult[1];
                        ?>
                        <option id="partner_id"  value="<?php echo $region_name; ?>"><?php echo $region_name; ?></option>   <?php
                    }
                }
                ?>
            </select>

            <!-- County filter end -->
            <!-- KP Type filter start -->


            <?php
            $query = "SELECT * FROM `kp_types`
ORDER BY `kp_types`.`name` ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>
                <?php
                ?>
                <select class="select_with_label" id="filter_kp_type" name="filter_kp_type">
                    <option value="">
                        KP Type
                    </option>
                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $kp_type_id = $idresult[0];
                        $kp_type_name = $idresult[1];
                        ?>
                        <option id="partner_id"  value="<?php echo $kp_type_name; ?>"><?php echo $kp_type_name; ?></option>   <?php
                    }
                }
                ?>
            </select>

            <!-- KP Type filter start -->

            <!-- Date filter start -->
            <input type="text" id="filter_date_from" placeholder="Date From :" class="select_filters" name="filter_date_from"/>
            <input type="text" id="filter_date_to" placeholder="Date To : " class="select_filters" name="filter_date_to"/>
            <!-- Date filter end  -->

            <input type="submit" name="search_filter_button" id="search_filter_button" class="search_filter_button" value="Filter Report"/>

        </form>


        <table>
            <tbody>
                <tr>
                    <td valign="top"><select required name="HivTrans" size="7" multiple class="select_with_label_wide">

                            <option id="Through unprotected sex">Through unprotected sex</option>
                            <option id="From infected mother to child">From infected mother to child</option>
                            <option id="Contact/exposure to HIV infected blood">Contact/exposure to HIV infected blood </option>
                            <option id="Handshake and close body contact">Handshake and close body contact</option>
                            <option id="Mosquito's and other insects">Mosquito's and other insects</option>
                            <option id="Condom burst">Condom burst</option>
                            <option id="Kissing">Kissing</option>
                            <option id="Sharing of needles/Syringes">Sharing of needles/Syringes</option>
                            <option id="Other">Other</option>
                        </select></td>

                    <td valign="top"><select required name="PrevOptns" size="7" multiple class="select_with_label_wide">
                            <option id="Abstain from sexual intercourse">Abstain from sexual intercourse</option>
                            <option id="Use condoms always">Use condoms always</option>
                            <option id="Limit the number of partners">Limit the number of partners</option>
                            <option id="Avoid sex with people with many partners">Avoid sex with people with many partners</option>
                            <option id="Avoid sex with people who inject drugs">Avoid sex with people who inject drugs</option>
                            <option id="Avoid sex with people who had blood transfusions">Avoid sex with people who had blood transfusions</option>
                            <option id="Avoid sex with people suffering from an STI">Avoid sex with people suffering from an STI</option>
                            <option id="Avoid injections">Avoid injections</option>
                            <option id="Avoid kissing/hugging">Avoid kissing/hugging</option>
                            <option id="Avoid mosquito bites">Avoid mosquito bites</option>
                            <option id="Get protection from traditional healer">Get protection from traditional healer</option>
                            <option id="Other">Other</option>
                            <option id="Don't know">Don't know</option>
                        </select></td>

                </tr>
            </tbody>
        </table>










    </div>




    <div id="trial_table_1div" class="trial_table_1div">

        <table id="partners_report" class="partners_report">
            <thead>
                <tr>
                    <th>No</th>
                    <th> KP Types </th>
                    <th>Abbrv</th>
                    <th>Partner Name</th>
                    <th>Facility Name </th>
                    <th>Region Name </th>
                    <th>No of KPs </th>
                    <th>Date </th>

                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th> KP Name</th>
                    <th>Abbrv</th>
                    <th>Partner Name</th>
                    <th>Facility Name </th>
                    <th>Region Name </th>
                    <th>No of KPs </th>
                    <th>Date </th>

                </tr>
            </tfoot>
            <tbody>



                <?php
                $query = "SELECT * FROM `no_individaul_kps_contacted` where 1";
                if ($result = mysqli_query($link, $query)) {
                    if (mysqli_num_rows($result) > 0) {
                        $i = 1;
                        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $row['Abbrv']; ?></td>
                                <td><?php echo $row['partner_name']; ?></td>
                                <td><?php echo $row['facility_name']; ?></td>
                                <td><?php echo $row['county']; ?></td>
                                <td><?php echo $row['no_kps']; ?></td>
                                <td><?php echo $row['activity_stamp']; ?></td>

                            </tr>
                            <?php
                            $i++;
                        }
                    } else {
                        ?><tr>
                            <td>Nothing here...</td>
                            <td>Nothing here...</td>
                            <td>Nothing here...</td>
                            <td>Nothing here...</td>
                            <td>Nothing here...</td>
                            <td>Nothing here...</td>
                            <td>Nothing here...</td>
                            <td>Nothing here...</td>
                        </tr><?php
                    }
                }
                ?>
            </tbody>
        </table>


    </div>


    <div id="trial_table_div" class="trial_table_div" style="display: none;">

        <table id="table_1_data" class="table_1_data" >
            <thead>
                <tr>
                    <th>No</th>
                    <th> KP Types </th>
                    <th>Abbrv</th>
                    <th>Partner Name</th>
                    <th>Facility Name </th>
                    <th>Region Name </th>
                    <th>No of KPs </th>
                    <th>Date </th>

                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th> KP Name</th>
                    <th>Abbrv</th>
                    <th>Partner Name</th>
                    <th>Facility Name </th>
                    <th>Region Name </th>
                    <th>No of KPs </th>
                    <th>Date </th>

                </tr>
            </tfoot>
            <tbody id="tbody_append" class="tbody_append">

            </tbody>
        </table>

    </div>







</div>
