<style type="text/css">
    .select_with_label_wide {
        width: 408px;
    }
    .labelwide {
        width: 400px;
    }
    .notice {
        font-size: 14px;
        font-weight: normal;
        width: 400px;
        padding-top: 10px;
        padding-right: 13px;
        padding-bottom: 10px;
        padding-left: 13px;
    }
    #checkgaps {
        background-color: #FFF;
        width: 600px;
        position: absolute;
        top: 210px;
        z-index: 999999999;
        background-image: url(images/ajax-loader.gif);
        background-position: 20px center;
        box-shadow: 0 0 15px 5px rgba(0,0,0,0.4);
        text-indent: 70px;
        background-repeat: no-repeat;
        cursor: default;
        padding: 15px;
    }
</style>

<style type="text/css">
    .select_with_label_wide {
        width: 408px;
    }
    .negotiateforcondomuse_labels {
        background-color: #CCC;
        padding-top: 3px;
        padding-bottom: 3px;
        padding-left: 6px;
    }
    .labelwide {
        width: 400px;
    }
    .negotiateforcondomuse_options {
        background-color: #FFF;
    }
    .notice {
        font-size: 14px;
        font-weight: normal;
        width: 400px;
        padding-top: 10px;
        padding-right: 13px;
        padding-bottom: 10px;
        padding-left: 13px;
    }
</style>


<div id="titlebar"><div id="titlebartext">HIV Knowledge MOH901,903,915,920</div></div>

<div id="outputtest" style="display:none;">
    Output Test
</div>

<div id="checkgaps" class="animated fadeIn">Locating Counselling Data Gaps for <span id="gapsclient"></span>...</div>

<script>
    sidebar3();</script>

<script type="text/javascript">

    function hidegapssearch() {
        setTimeout(function () {
            document.getElementById('checkgaps').className = 'animated fadeOut';
        }, 4000);
        setTimeout(function () {
            document.getElementById('checkgaps').style.display = 'none';
        }, 5000);
        setTimeout(function () {
            loadingindicator_stop();
        }, 4000);
    }

    $(document).ready(function () {

        hidegapssearch();
        document.getElementById('UUIDTOSEND').value = localStorage.getItem('currentuuid');
        document.getElementById('UUIDTOSEND2').value = localStorage.getItem('currentuuid');
        var gapsclient = localStorage.getItem('currentclientname');
        var currentuuid = localStorage.getItem('currentuuid');
        var currentgender = localStorage.getItem('currentgender');
        document.getElementById('gapsclient').innerHTML = gapsclient;
        var json_obj_load_form;
        $.getJSON("database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=hk", function (data) {
            json_obj_load_form = data;
            counselling_gaps_hk = (data[0].id);
            counselling_gaps_uuid = (data[0].uuid);
            counselling_gaps_hiv_tested = (data[0].hiv_tested);
            counselling_gaps_hiv_trans = (data[0].hiv_trans);
            counselling_gaps_hiv_prev = (data[0].hiv_prev);
            counselling_gaps_sti_known = (data[0].sti_known);
            counselling_gaps_sti_infected = (data[0].sti_infected);
            counselling_gaps_sti_treatmnt_cntr = (data[0].sti_treatmnt_cntr);
            counselling_gaps_visit_id = (data[0].visit_id);
            // alert(counselling_gaps_hiv_tested);

            console.log('Counselling ID: ' + counselling_gaps_hk + '\nCounselling UUID: ' + counselling_gaps_uuid + '\nCounselling hiv_trans: ' + counselling_gaps_hiv_trans + '\nCounselling hiv_prev: ' + counselling_gaps_hiv_prev + '\nCounselling sti_known: ' + counselling_gaps_sti_known + '\nCounselling sti_infected: ' + counselling_gaps_sti_infected + '\nCounselling sti_treatmnt_cntr: ' + counselling_gaps_sti_treatmnt_cntr + '\nCounselling visit_id:  ' + counselling_gaps_visit_id + '');
            if (counselling_gaps_sti_infected != "none") {
                maptofields(counselling_gaps_sti_infected);
            }
            else if (counselling_gaps_sti_infected == "none") {
                alert('No data found for counselling_gaps_sti_infected.');
            }

            if (counselling_gaps_hiv_trans != "none") {
                maptofields(counselling_gaps_hiv_trans);
            }
            else if (counselling_gaps_hiv_trans == "none") {
                alert('No data found for counselling_gaps_hiv_trans.');
            }

            if (counselling_gaps_sti_treatmnt_cntr != "none") {
                maptofields(counselling_gaps_sti_treatmnt_cntr);
            }
            else if (counselling_gaps_sti_treatmnt_cntr == "none") {
                alert('No data found for counselling_gaps_sti_treatmnt_cntr.');
            }

            if (counselling_gaps_hiv_prev != "none") {
                maptofields(counselling_gaps_hiv_prev);
            }
            else if (counselling_gaps_hiv_prev == "none") {
                alert('No data found for counselling_gaps_hiv_prev.');
            }

            if (counselling_gaps_sti_known != "none") {
                maptofields(counselling_gaps_sti_known);
            }
            else if (counselling_gaps_sti_known == "none") {
                alert('No data found counselling_gaps_sti_known.');
            }

        });
        function maptofields(whatfields) {

            var str = whatfields;
            var str_array = str.split(',');
            for (var i = 0; i < str_array.length; i++) {

                str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                jQuery.each(str_array, function (i, val) {



                    document.getElementById('' + val + '').setAttribute("selected", "selected");
                });
            }
        }






        var json_obj;
        $.getJSON("database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=hk", function (data) {
            json_obj = data;
            var counselling_gaps_hk = (data[0].id);
            localStorage.setItem('counselling_gaps_hk', counselling_gaps_hk);
            checkifset();
        });
        var json_obj2;
        $.getJSON("database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=ht", function (data) {
            json_obj2 = data;
            // alert(JSON.stringify(json_obj2));
            // alert(data[0].id);

            var counselling_gaps_ht = (data[0].id);
            localStorage.setItem('counselling_gaps_ht', counselling_gaps_ht);
            checkifset();
        });
        if (currentgender == 'M') {

// alert('Male');

            var json_obj3;
            $.getJSON("database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=cduse", function (data) {
                json_obj3 = data;
                var counselling_gaps_cduse = (data[0].id);
                localStorage.setItem('counselling_gaps_cduse', counselling_gaps_cduse);
                checkifset();
            });
        }
        else if (currentgender == 'F') {

// alert('Female');

            var json_obj4;
            $.getJSON("database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=fcduse", function (data) {
                json_obj4 = data;
                var counselling_gaps_fcduse = (data[0].id);
                localStorage.setItem('counselling_gaps_fcduse', counselling_gaps_fcduse);
                checkifset();
            });
        }

        $.ajax({
            type: "GET",
            url: "database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=hk",
            dataType: "JSON",
            success: function (response) {

                $('#PrevOptns option[value=' + response[0].prev_optns + ']').attr("selected", "selected");
                // $('#HivTrans option[value=' + response[0].hiv_trans + ']').prop('selected', true);
            }
        });
    });
    function checkifset() {
        var counselling_gaps_hk = localStorage.getItem('counselling_gaps_hk');
        var counselling_gaps_ht = localStorage.getItem('counselling_gaps_ht');
        var counselling_gaps_cduse = localStorage.getItem('counselling_gaps_cduse');
        var counselling_gaps_fcduse = localStorage.getItem('counselling_gaps_fcduse');
        // alert('' + counselling_gaps_hk + ' ' + counselling_gaps_ht + ' ' + counselling_gaps_cduse + ' ' + counselling_gaps_fcduse + '');
    }

    function HIV_KNOWLEDGE() {

        var thestring = $("#HIV_KNOWLEDGE").serialize();
        //alert(thestring);
        console.log(thestring);
        loadingindicator_start();
        $.post("database/Emarps/websrvc/websrvc.php", $("#HIV_KNOWLEDGE").serialize())
                .done(function (data) {
                    loadingindicator_stop();
                    noty({
                        text: "Counselling Data Updated Successfully!",
                        type: 'success',
                        dismissQueue: true,
                        timeout: 5000,
                        closeWith: ['click'],
                        layout: 'top',
                        theme: 'defaultTheme',
                        maxVisible: 10

                    });
                    document.getElementById("div_1").style.display = "none";
                    document.getElementById("div_2").style.display = "block";
                });
    }
</script>



<script>
    sidebar3();
    $(document).ready(function () {
        $('#regCDUse').submit(function (event) {
            dataString = $("#regCDUse").serialize();
            $.ajax({
                type: "POST",
                url: "database/Emarps/websrvc/websrvc.php",
                data: dataString,
                success: function (data) {
                }
            });
            event.preventDefault();
            return false;
        });
        $('.condom_suppliers').on("change", function ()
        {
            var str = "";
            $('.condom_suppliers:checked').each(function ()
            {
                str += $(this).val() + " ,";
            });
            $('#CD_Supplr').val(str);
        });
        var currentuuid = localStorage.getItem('currentuuid');
        $.ajax({
            type: "GET",
            url: "database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=cduse",
            dataType: "JSON",
            success: function (response) {
                $.each(response, function (i, resp) {
                    //print(resp)

                });
            }
        });
        $.ajax({
            type: "GET",
            url: "database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=hk",
            dataType: "JSON",
            success: function (response) {
                $.each(response, function (i, resp) {

                    $("input[name=HivTest]").prop('checked', false); //uncheck all
                    $("input[name=HivTest][value=" + resp.hiv_tested + "]").prop('checked', true);


                    //EDIT FORM DATA GAPS CHECKER
                    //Get value of the  insert or update

                    var one = "1";
                    $("#Edit").val(one);


                });
            }
        });
        $.ajax({
            type: "GET",
            url: "database/Emarps/websrvc/websrvc.php?task=getData&UUID=" + currentuuid + "&DataGt=cduse",
            dataType: "JSON",
            success: function (response) {
                $.each(response, function (i, resp) {

                    //EDIT FORM DATA GAPS CHECKER
                    //Get value of the  insert or update

                    var one = "1";

                    $("#Edit_one").val(one);



                    //HIV TEST GAPS CHECKER
                    $("input[name=CdLub]").prop('checked', false); //uncheck all
                    $("input[name=CdLub][value=" + resp.use_lube + "]").prop('checked', true);

                    //MALE CONDOM USE GAPS CHECKER
                    $("input[name=Mcduse]").prop('checked', false); //uncheck all
                    $("input[name=Mcduse][value=" + resp.male_condom_use + "]").prop('checked', true);



                    //FEMALE CONDOM USE GAPS CHECKER
                    $("input[name=Fcduse]").prop('checked', false); //uncheck all
                    $("input[name=Fcduse][value=" + resp.female_condom_use + "]").prop('checked', true);



                    //FEMALE CONDOM USE GAPS CHECKER
                    $("input[name=Fcduse]").prop('checked', false); //uncheck all
                    $("input[name=Fcduse][value=" + resp.female_condom_use + "]").prop('checked', true);




                    //CONSISTENT CONDOM USE GAPS CHECKER
                    $("input[name=Const_CD_Use]").prop('checked', false); //uncheck all
                    $("input[name=Const_CD_Use][value=" + resp.consistent_condom_use + "]").prop('checked', true);



                    //TRAINED CONDOM NEGOTIATION GAPS CHECKER
                    $("input[name=Cd_Nego]").prop('checked', false); //uncheck all
                    $("input[name=Cd_Nego][value=" + resp.trained_condom_nego + "]").prop('checked', true);



                    //CASUAL CLIENT CONDOM USE GAPS CHECKER
                    $("input[name=Casl_CD_Use]").prop('checked', false); //uncheck all
                    $("input[name=Casl_CD_Use][value=" + resp.casual_client_cd_use + "]").prop('checked', true);



                    //CASUAL CLIENT CONDOM USE GAPS CHECKER
                    $("input[name=Reg_CD_Use]").prop('checked', false); //uncheck all
                    $("input[name=Reg_CD_Use][value=" + resp.regular_client_cd_use + "]").prop('checked', true);


                    //CASUAL CLIENT CONDOM USE GAPS CHECKER
                    $("input[name=Bf_CD_Use]").prop('checked', false); //uncheck all
                    $("input[name=Bf_CD_Use][value=" + resp.boyfriend_cd_use + "]").prop('checked', true);



                    //ACTION CLIENT CONDOM USE REFUSAL GAPS CHECKER
                    $("input[name=Cd_Use_Rfsal]").prop('checked', false); //uncheck all
                    $("input[name=Cd_Use_Rfsal][value="+ resp.action_client_condom_use_refusal +"]").prop('checked', true);




                });
            }


        });





    });
    function REG_CDUSE() {

        var thestring = $("#REG_CDUSE").serialize();
        console.log(thestring);
        loadingindicator_start();
        $.post("database/Emarps/websrvc/websrvc.php", $("#REG_CDUSE").serialize())
                .done(function (data) {
                    loadingindicator_stop();
                    noty({
                        text: "Condom Use Data Updated Successfully!",
                        type: 'success',
                        dismissQueue: true,
                        timeout: 5000,
                        closeWith: ['click'],
                        layout: 'top',
                        theme: 'defaultTheme',
                        maxVisible: 10

                    });
                    document.getElementById("div_1").style.display = "none";
                    document.getElementById("div_2").style.display = "block";
                });
    }

</script>

<div id="div_1" class="div_1" >



    <div class="animated fadeIn">

        <div class="notice">Hold down <img align="absmiddle" src="images/ctrl.png" height="30" /> to select multple answers</div>

        <form id="HIV_KNOWLEDGE" action="javascript:HIV_KNOWLEDGE();">

            <div style="display:none;">
                <input type="text" name="task" value="regHivknw">
                <input type="text" id="UUIDTOSEND" name="UUID" value="">
                <input type="text" name="Edit" id="Edit" class="Edit" value="0">
                <input type="text" name="Pid" id="Pid" class="Pid" value="<?php echo $_SESSION["sess_partner_id"] ?>" />
                <input type="text" name="Clinic" id="Clinic" class="Clinic" value="<?php echo $_SESSION["sess_clinic_id"] ?>" />
                <input type="text" id="CollPnt" class="CollPnt" value="Clinical" name="CollPnt"/>
            </div>

            <table border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td valign="top">



                        <label class="labelwide">How is HIV Transmitted?</label></td>
                    <td valign="top">
                        <label class="labelwide">How do people prevent contracting or transmitting HIV?</label></td>
                </tr>
                <tr>
                    <td valign="top">
                        <select required name="HivTrans"id="HivTrans" size="7" multiple class="select_with_label_wide">

                            <option id="Through unprotected sex">Through unprotected sex</option>
                            <option id="From infected mother to child">From infected mother to child</option>
                            <option id="Contact/exposure to HIV infected blood">Contact/exposure to HIV infected blood </option>
                            <option id="Handshake and close body contact">Handshake and close body contact</option>
                            <option id="Mosquitos and other insects">Mosquito's and other insects</option>
                            <option id="Condom burst">Condom burst</option>
                            <option id="Kissing">Kissing</option>
                            <option id="Sharing of needles/Syringes">Sharing of needles/Syringes</option>
                            <option id="Other">Other</option>
                        </select>
                    </td>

                    <td valign="top">
                        <select required name="PrevOptns" id="PrevOptns" size="7" multiple class="select_with_label_wide">
                            <option id="Abstain from sexual intercourse">Abstain from sexual intercourse</option>
                            <option id="Use condoms always">Use condoms always</option>
                            <option id="Limit the number of partners">Limit the number of partners</option>
                            <option id="Avoid sex with people with many partners">Avoid sex with people with many partners</option>
                            <option id="Avoid sex with people who inject drugs">Avoid sex with people who inject drugs</option>
                            <option id="Avoid sex with people who had blood transfusions">Avoid sex with people who had blood transfusions</option>
                            <option id="Avoid sex with people suffering from an STI">Avoid sex with people suffering from an STI</option>
                            <option id="Avoid injections">Avoid injections</option>
                            <option id="Avoid kissing/hugging">Avoid kissing/hugging</option>
                            <option id="Avoid mosquito bites">Avoid mosquito bites</option>
                            <option id="Get protection from traditional healer">Get protection from traditional healer</option>
                            <option id="Other">Other</option>
                            <option id="Don't know">Don't know</option>
                        </select>
                    </td>

                </tr>
                <tr>
                    <td valign="top">

                        <label class="labelwide">Please mention all Sexually transmitted infections you know of </label></td>
                    <td valign="top"><label class="labelwide">Where do you go for treatment when you have a genital problem </label></td>
                </tr>
                <tr>
                    <td valign="top"><select required name="StiOptns" id="StiOptns" size="7" multiple="multiple" class="select_with_label_wide">
                            <option id="Genital ulcer disease">Genital ulcer disease</option>
                            <option id="Foul smelling vagina discharge">Foul smelling vagina discharge</option>
                            <option id="Painless growth in vaginal area">Painless growth in vaginal area</option>
                            <option id="Lower abdominal pain">Lower abdominal pain</option>
                        </select></td>
                    <td valign="top"><select required name="TrtmntOptns" id="TrtmntOptns" size="7" multiple="multiple" class="select_with_label_wide">
                            <option id="Pharmacy">Pharmacy</option>
                            <option id="Private Doctor">Private Doctor</option>
                            <option id="Govermental clinic">Govermental clinic</option>
                            <option id="Herbalist">Herbalist</option>
                            <option id="Other">Other</option>
                        </select></td>
                </tr>
                <tr>
                    <td valign="top">  <label class="labelwide">Have you  heard of one of the following infections?</label></td>
                    <td valign="top">



                    </td>
                </tr>
                <tr>
                    <td valign="top"><select required name="AddInfctOptns" id="AddInfctOptns" size="7" multiple="multiple" class="select_with_label_wide">
                            <option id="Gonorrhea">Gonorrhea</option>
                            <option id="Syphillis">Syphillis</option>
                            <option id="Herpes">Herpes</option>
                            <option id="Trichomonas">Trichomonas</option>
                            <option id="Clamydia">Clamydia</option>
                            <option id="HIV">HIV</option>
                            <option id="Others">Others</option>
                        </select></td>
                    <td valign="top"><label>Have you ever been tested for HIV?</label>

                        <input required id="HivTest" name="HivTest" type="radio" class="wellness_center radiostyle" value="1">
                        <div class="radiolabel">Yes</div>
                        <input required id="HivTest" name="HivTest" type="radio" class="wellness_center radiostyle" value="0">
                        <div class="radiolabel">No</div></td>

                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <button class="large_button"  >Next</button>
                    </td>
                </tr>
            </table>

        </form>

    </div>



</div>

<div id="div_2" class="div_2" style="display: none;"  >



    <form id="REG_CDUSE"  class="REG_CDUSE" action="javascript:REG_CDUSE();" >
        <div style="display:none;">
            <input type="text" name="task" value="regCDUse">
            <input type="text" id="UUIDTOSEND2" name="UUID" value="">
            <input type="text" name="Edit" id="Edit_one" class="Edit_one" value="0">

            <input type="text" name="Pid" id="Pid" class="Pid" value="<?php echo $_SESSION["sess_partner_id"] ?>" />
            <input type="text" name="Clinic" id="Clinic" class="Clinic" value="<?php echo $_SESSION["sess_clinic_id"] ?>" />
            <input type="text" id="CollPnt" class="CollPnt" value="Clinical" name="CollPnt"/>
        </div>
        <div id="condom_use_a" class="animated fadeIn">
            <table width="700" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td width="390" valign="top">

                        <label>Do you lubricate the condom during sex?</label>

                        <input required name="CdLub" type="radio" class="wellness_center radiostyle" value="Yes">
                        <div class="radiolabel">Yes</div>
                        <input required name="CdLub" type="radio" class="wellness_center radiostyle" value="No">
                        <div class="radiolabel">No</div>

                    </td>
                    <td width="281" valign="top">

                        <label>Select Condom Suppliers</label>

                        <input type="checkbox" class="checkboxstyle condom_suppliers" name="condom_suppliers"  id="id_peer_educator" value="Peer Educator"/>Peer Educator<br>
                        <input type="checkbox" class="checkboxstyle  condom_suppliers" name="condom_suppliers"    id="id_chw" value="CHW"/>CHW<br>
                        <input type="checkbox" class="checkboxstyle condom_suppliers" name="condom_suppliers"  id="id_health_provider" value="Health Prvider"/>Health Provider<br>
                        <input type="checkbox" class="checkboxstyle condom_suppliers" name="condom_suppliers"  id="id_purchase" value="Purchase"/>Purchase<br>
                        <input type="hidden" id="CD_Supplr" class="CD_Supplr" name="CD_Supplr" placeholder="Condom suppliers : "/>



                    </td>
                </tr>
                <tr>
                    <td valign="top">

                    </td>
                </tr>
                <tr>
                    <td valign="top">

                        <label>Do you know how to use a male condom?</label>

                        <input required name="Mcduse" type="radio" class="wellness_center radiostyle" value="Yes">
                        <div class="radiolabel">Yes</div>
                        <input required name="Mcduse" type="radio" class="wellness_center radiostyle" value="No">
                        <div class="radiolabel">No</div>

                    </td>
                    <td valign="top">

                        <label>Do you know how to use a female condom?</label>

                        <input required name="Fcduse" type="radio" class="wellness_center radiostyle" value="Yes">
                        <div class="radiolabel">Yes</div>
                        <input required name="Fcduse" type="radio" class="wellness_center radiostyle" value="No">
                        <div class="radiolabel">No</div>

                    </td>
                </tr>
                <tr>
                    <td valign="top"> </td>
                    <td valign="top">



                    </td>
                </tr>
                <tr>
                    <td valign="top"><label>Do you consistently use a condom with your regular sexual partner(s)?</label>

                        <input required name="Const_CD_Use" type="radio" class="wellness_center radiostyle" value="Yes"  >
                        <div class="radiolabel">Yes</div>
                        <input required name="Const_CD_Use" type="radio" class="wellness_center radiostyle" value="No">
                        <div class="radiolabel">No</div></td>
                    <td valign="top"><label>Have you been trained on condom negotiation?</label>

                        <input required name="Cd_Nego" type="radio" class="wellness_center radiostyle" value="Yes"  >
                        <div class="radiolabel">Yes</div>
                        <input required name="Cd_Nego" type="radio" class="wellness_center radiostyle" value="No">
                        <div class="radiolabel">No</div></td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">

                        <p><br />
                            How often do you negotiate for condom use with the following?
                        </p>


                        <table width="100%" border="0">
                            <tr>
                                <td width="33%" class="negotiateforcondomuse_labels">Casual Client</td>
                                <td width="33%" class="negotiateforcondomuse_labels">Regular Client</td>
                                <td width="33%" class="negotiateforcondomuse_labels">Boyfriend</td>
                            </tr>
                            <tr>
                                <td class="negotiateforcondomuse_options" valign="top">

                                    <input required name="Casl_CD_Use" type="radio" class="wellness_center radiostyle" value="Yes">
                                    <div class="radiolabel">Sometimes</div>

                                    <div style="clear:both;"></div>

                                    <input required name="Casl_CD_Use" type="radio" class="wellness_center radiostyle" value="Always">
                                    <div class="radiolabel">Always</div>

                                    <div style="clear:both;"></div>

                                    <input required name="Casl_CD_Use" type="radio" class="wellness_center radiostyle" value="Never">
                                    <div class="radiolabel">Never</div>

                                    <input required name="Casl_CD_Use" type="radio" class="wellness_center radiostyle" value="N/A">
                                    <div class="radiolabel">N/A</div>

                                </td>
                                <td class="negotiateforcondomuse_options" valign="top">           
                                    <input required name="Reg_CD_Use" type="radio" class="wellness_center radiostyle" value="Yes">
                                    <div class="radiolabel">Sometimes</div>

                                    <div style="clear:both;"></div>

                                    <input required name="Reg_CD_Use" type="radio" class="wellness_center radiostyle" value="Always">
                                    <div class="radiolabel">Always</div>

                                    <div style="clear:both;"></div>

                                    <input required name="Reg_CD_Use" type="radio" class="wellness_center radiostyle" value="Never">
                                    <div class="radiolabel">Never</div>

                                    <div style="clear:both;"></div>

                                    <input required name="Reg_CD_Use" type="radio" class="wellness_center radiostyle" value="N/A"/>
                                    <div class="radiolabel">N/A</div></td>
                                <td class="negotiateforcondomuse_options" valign="top">

                                    <input required name="Bf_CD_Use" type="radio" class="wellness_center radiostyle" value="Sometimes"  />
                                    <div class="radiolabel">Sometimes</div>

                                    <div style="clear:both;"></div>

                                    <input required name="Bf_CD_Use" type="radio" class="wellness_center radiostyle" value="Always">
                                    <div class="radiolabel">Always</div>

                                    <div style="clear:both;"></div>

                                    <input required name="Bf_CD_Use" type="radio" class="wellness_center radiostyle" value="Never">
                                    <div class="radiolabel">Never</div>

                                    <div style="clear:both;"></div>

                                    <input required name="Bf_CD_Use" type="radio" class="wellness_center radiostyle" value="N/A">
                                    <div class="radiolabel">N/A</div>

                                </td>
                            </tr>
                        </table>


                    </td>


                    <td colspan="2" valign="top"></td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">&nbsp;</td>
                    <td colspan="2" valign="top"></td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">



                        <button class="large_button" type="button" onClick="hiv_knwledge_form();">&leftarrow; Previous</button>





                        <button class="large_button" type="button"  onClick="condom_use_b();">Next</button>
                    </td>

                    <td colspan="2" valign="top"></td>
                </tr>
            </table>

        </div>


        <div id="condom_use_b" class="animated fadeIn" style="display:none;">
            <table width="700" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td colspan="2" valign="top">

                        <label>Suppose after negotiating for condom use and the client refuses, what do you do?</label>

                        <input required name="Cd_Use_Rfsal" type="radio" class="wellness_center radiostyle" value="Continue Having Sex">
                        <div class="radiolabel">Continue having sex</div>
                        <input required name="Cd_Use_Rfsal" type="radio" class="wellness_center radiostyle" value="Refuse Having Sex">
                        <div class="radiolabel">Refuse having sex</div>
                        <input required name="Cd_Use_Rfsal" type="radio" class="wellness_center radiostyle" value="Charge More">
                        <div class="radiolabel">Charge more</div>


                    </td>
                </tr>
                <tr>
                    <td width="390" valign="top">

                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">

                        <label>Have you ever experienced violence because you requested for the client to use a condom?</label>

                        <input required name="Vio_Typ" type="radio" class="wellness_center radiostyle" value="Yes">
                        <div class="radiolabel">Yes</div>
                        <input required name="Vio_Typ" type="radio" class="wellness_center radiostyle" value="No">
                        <div class="radiolabel">No</div>

                    </td>
                </tr>
                <tr>
                    <td valign="top"> </td>
                    <td width="281" valign="top">



                    </td>
                </tr>

                <tr>
                    <td colspan="2" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">




                        <button type="button" class="large_button" onClick="condom_use_a();">&leftarrow; Previous</button>


                        <button class="large_button" onClick="">Register Data</button>




                    </td>
                </tr>
            </table>
        </div>



    </form>

</div>







<script>
    function condom_use_b() {
        document.getElementById("condom_use_b").style.display = "block";
        document.getElementById("condom_use_a").style.display = "none";
    }
    function condom_use_a() {
        document.getElementById("condom_use_b").style.display = "none";
        document.getElementById("condom_use_a").style.display = "block";
    }

    function hiv_knwledge_form() {
        document.getElementById("div_1").style.display = "block";
        document.getElementById("div_2").style.display = "none";
    }
</script>