<?php
if (isset($_REQUEST['save'])) {
    extract($_REQUEST);
    $succes = $websrvc->user_registration($user_name, $user_email, $user_partner_name, $user_clinic_name, $user_status, $user_roles, $user_phone, $sidebar_permissions, $systembar_permissions, $reports_permissions, $registers_permissions);
    if ($succes) {
        ?>
        <script src="js/jquery.min.js"></script>


        <!-- noty -->
        <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                noty({text: 'User Added Succesfully...'});
            });
        </script>


        <?php
    } else {

        echo 'Wrong information please try again';
    }
}
?>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>

<h2>User Registration Form</h2>
<script src="js/conditional.js"></script>
<div class="form animated fadeIn" style="width:400px;">

    <center>
        <div>

            <form name="user_registration" id="user_registration"  class="user_registration" accept-charset="UTF-8" method="POST" action=""> 

                <div class="left">


                    <input name="user_name" placeholder="Username" required="" class="fieldstyle user_name"  id="user_name" type="text">



                    <input name="user_email" placeholder="User E-Mail" required="" id="user_email" class="fieldstyle" type="email">
                    <input name="user_phone" placeholder="User Phone" required="" id="user_phone" class="fieldstyle" type="text">
                    <?php
// GET LIST AND DISPLAY IN FORM
// check connection
                    ?>
                    <label>Partner Name : </label>

                    <?php
                    $query = "SELECT partner_id,partner_name,contact_person FROM partner ORDER BY  partner_id ASC";
                    if ($result = mysqli_query($link, $query)) {
                        ?>
                        <select onchange="document.getElementById('clinic_name_holder').style.display = 'block';" class="user_partner_name fieldstyle_with_label" required="" name="user_partner_name">
                            <option id="" value="">Please select a partner</option>
                            <?php
                            while ($idresult = mysqli_fetch_row($result)) {
                                $partner_id = $idresult[0];
                                $partner_name = $idresult[1];
                                $contact_person = $idresult[2];
                                ?>
                                <option id="partner_id"  value="<?php echo $partner_id; ?>"><?php echo $partner_name . '&nbsp;(' . $contact_person . ')'; ?></option>   <?php
                            }
                            ?>
                        </select>

                        <div style="clear:both"></div>

                        <?php
                    }
                    ?>
                    <br>

                    <div id="clinic_name_holder" style="display:none;">

                        <label>Clinic Name : </label>
                        <select class="user_clinic_name fieldstyle_with_label" required="" name="user_clinic_name" id="user_clinic_name">

                        </select>

                    </div>

                    <div style="clear:both"></div>

                    <label>Status : </label>
                    <select class="user_status fieldstyle_with_label" required="" name="user_status" id="user_status">
                        <option value="Active">Active</option>
                        <option value="In Active">In Active</option>
                    </select>

                    <div style="clear:both"></div>

                    <label>Role: </label>


                    <?php
// GET LIST AND DISPLAY IN FORM
                    ?>
                    <?php
                    $clinic_id = $_SESSION['sess_clinic_id'];
                    $partner_id = $_SESSION['sess_partner_id'];
                    if (empty($clinic_id) and empty($partner_id)) {

                        $query = "SELECT * FROM roles  ORDER BY  date_added ASC";
                    } elseif (!empty($partner_id) and empty($clinic_id)) {

                        $query = "SELECT * FROM roles where role_name != 'Super User' ORDER BY date_added ";
                    } elseif (!empty($partner_id) and ! empty($clinic_id)) {

                        $query = "SELECT * FROM roles where role_name != 'Super User' and role_name !='Partner Administrator' ORDER BY date_added ";
                    }
                    if ($result = mysqli_query($link, $query)) {
                        ?>
                        <select class="user_roles fieldstyle_with_label" required="" name="user_roles">
                            <option id="" value="">Please select a role</option>
                            <?php
                            while ($idresult = mysqli_fetch_row($result)) {
                                $role_id = $idresult[0];
                                $role_name = $idresult[1];
                                ?>
                                <option id="partner_id"  value="<?php echo $role_id; ?>"><?php echo $role_name . '&nbsp;'; ?></option>   <?php
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>
                    <div style="clear:both"></div>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#sidebar_selecctall').click(function (event) {  //on click 

                                if (this.checked) { // check select status
                                    $('.sidebar_function_checkbox').each(function () { //loop through each checkbox
                                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                                    });
                                } else {
                                    $('.sidebar_function_checkbox').each(function () { //loop through each checkbox
                                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                                    });
                                }
                            });

                        });
                    </script>

                    <label>Side Bar Function List : </label>

                    <ul>

                        <li><input type="checkbox" id="sidebar_selecctall"/> Select/Un-Select All</li>
                        <div style="clear:both"></div>
                        <li>

                            <?php
                            // GET LIST AND DISPLAY IN FORM
                            ?>
                            <?php
                            $query = "SELECT * FROM `functions` where status='Active' and location='sidebar' ORDER BY  date_added ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>

                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $id = $idresult[0];
                                    $name = $idresult[1];
                                    ?>

                                    <input type="checkbox" class="sidebar_function_checkbox" name="sidebar_permissions[]" value="<?php echo $id; ?>"> <?php echo $name; ?> 
                                    <br>

                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>
                        </li>
                        <div style="clear:both"></div>
                    </ul>



                    <div style="clear:both"></div>


                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#systembar_selecctall').click(function (event) {  //on click 

                                if (this.checked) { // check select status
                                    $('.systembar_function_checkbox').each(function () { //loop through each checkbox
                                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                                    });
                                } else {
                                    $('.systembar_function_checkbox').each(function () { //loop through each checkbox
                                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                                    });
                                }
                            });

                        });
                    </script>




                    <label>System Function List : </label>

                    <ul>

                        <li><input type="checkbox" id="systembar_selecctall"/> Select/Un-Select All</li>
                        <div style="clear:both"></div>
                        <li>

                            <?php
                            // GET LIST AND DISPLAY IN FORM
                            ?>
                            <?php
                            $query = "SELECT * FROM `functions` where status='Active' and location='systembar' ORDER BY  date_added ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>

                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $id = $idresult[0];
                                    $name = $idresult[1];
                                    ?>

                                    <input type="checkbox" class="systembar_function_checkbox" name="systembar_permissions[]" value="<?php echo $id; ?>"> <?php echo $name; ?> 
                                    <br>

                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>
                        </li>
                        <div style="clear:both"></div>
                    </ul>



                    <div style="clear:both"></div>



                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#reports_selecctall').click(function (event) {  //on click 

                                if (this.checked) { // check select status
                                    $('.reports_function_checkbox').each(function () { //loop through each checkbox
                                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                                    });
                                } else {
                                    $('.reports_function_checkbox').each(function () { //loop through each checkbox
                                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                                    });
                                }
                            });

                        });
                    </script>




                    <label>Reports Function List : </label>

                    <ul>

                        <li><input type="checkbox" id="reports_selecctall"/> Select/Un-Select All</li>
                        <div style="clear:both"></div>
                        <li>

                            <?php
                            // GET LIST AND DISPLAY IN FORM
                            ?>
                            <?php
                            $query = "SELECT * FROM `functions` where status='Active' and location='reports' ORDER BY  date_added ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>

                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $id = $idresult[0];
                                    $name = $idresult[1];
                                    ?>

                                    <input type="checkbox" class="reports_function_checkbox" name="reports_permissions[]" value="<?php echo $id; ?>"> <?php echo $name; ?> 
                                    <br>

                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>
                        </li>
                        <div style="clear:both"></div>
                    </ul>



                    <div style="clear:both"></div>



                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#register_selecctall').click(function (event) {  //on click 

                                if (this.checked) { // check select status
                                    $('.register_function_checkbox').each(function () { //loop through each checkbox
                                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                                    });
                                } else {
                                    $('.register_function_checkbox').each(function () { //loop through each checkbox
                                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                                    });
                                }
                            });

                        });
                    </script>




                    <label>Registers Function List : </label>

                    <ul>

                        <li><input type="checkbox" id="register_selecctall"/> Select/Un-Select All</li>
                        <div style="clear:both"></div>
                        <li>

                            <?php
                            // GET LIST AND DISPLAY IN FORM
                            ?>
                            <?php
                            $query = "SELECT * FROM `functions` where status='Active' and location='registers' ORDER BY  date_added ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>

                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $id = $idresult[0];
                                    $name = $idresult[1];
                                    ?>

                                    <input type="checkbox" class="register_function_checkbox" name="registers_permissions[]" value="<?php echo $id; ?>"> <?php echo $name; ?> 
                                    <br>

                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>
                        </li>
                        <div style="clear:both"></div>
                    </ul>



                    <div style="clear:both"></div>






                    <div class="clearafter"></div>

                </div>

                <div class="right">



                </div>

                <div class="next_button_container">

                    <input class="large_button" type="submit" value="Register User" name="save" />
                </div>  
            </form>





        </div></center>

</div>