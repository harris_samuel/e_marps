<?php
include 'database/class.admin.php';

$admin = new ADMIN();
?>


<script type="text/javascript"  src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css" type="text/css" />
<script type="text/javascript">

    $(document).ready(function () {
        $('#kp_type_report').DataTable();
    });
</script>
<style type="text/css">
    .user_report {
        width: 80%;
    }
</style>

<h2>Hot Spot Management Form</h2>

<div class="form animated fadeIn">



    <table id="kp_type_report" class="kp_type_report">
        <thead>
            <tr>
                <th>No</th>
                <th>Hot Spot Name</th>
                <th>Region</th>
                <th>Partner Name</th>
                <th>CLinic Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Hot Spot Name</th>
                <th>Region</th>
                <th>Partner Name</th>
                <th>CLinic Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
        <tbody>


            <?php
            $res = $admin->manage_hot_spot_read();
            if (mysqli_num_rows($res) > 0) {
                $i = 1;
                while ($row = mysqli_fetch_array($res, MYSQLI_BOTH)) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['region_name']; ?></td>
                        <td><?php echo $row['partner_name']; ?></td>
                        <td><?php echo $row['facility_name']; ?></td>


                        <td><a href="?currentview=edit_hot_spots_name&edit_id=<?php echo $row['id']; ?>">Edit</a></td>
                        <td><a href="?currentview=delete_hot_spots_name&delete_id=<?php echo $row['id']; ?>">Delete</a>

                        </td>
                    </tr>
                    <?php
                    $i++;
                }
            } else {
                ?><tr>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                </tr><?php
            }
            ?>
        </tbody>
    </table>


</div>
