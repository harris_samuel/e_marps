<?php
include 'database/class.admin.php';
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$admin = new ADMIN();
if (isset($_GET['delete_id'])) {

    $delete_id = $_GET['delete_id'];

    $select_query = "select * from users inner join roles on users.role_id = roles.role_id WHERE users.user_id='$delete_id'";
    $res = mysqli_query($link, $select_query);
    if ($res) {
        $row = mysqli_fetch_array($res, MYSQLI_BOTH);
    } else {

        echo 'Error' . mysqli_errno($link);
    }
}
if (isset($_REQUEST['delete_user'])) {
    extract($_REQUEST);
    // echo 'Details ' . '<br>' . $user_name . '<br>' . $edit_id . '<br>' . $e_mail . '<br>' . $phone_no . '<br>' . $status . '<br>' . $role_name . '<br>';
    $success = $admin->delete_user($user_name, $e_mail, $delete_id);
    if ($success) {
        echo 'User Account Deativated Succesfully ';
    } else {
        echo 'Error please try again ...';
    }
}
?> 
<div class="form animated fadeIn">
    <script type="text/javascript">
        function goBack() {
            window.history.back()
        }

    </script>
    <form method="post" name="delete_user_form" class="delete_user_form" id="delete_user_form">
        <table id="dataview">
            <?php
            $count_row = mysqli_num_rows($res);
            if ($count_row === 1) {
                ?>   
                <input type="hidden" name="user_id" value="<?php echo $delete_id; ?>"/>
                <h6 class="label">Are you sure you want to delete the  following user details : </h6>
                <hr>
                <tr><td><input type="text" readonly="" name="user_name" placeholder="User Name" value="<?php echo $row['user_name'] ?>" /><br /></td>
                    <td><input type="text" readonly="" name="e_mail" placeholder="E-mail" value="<?php echo $row['email'] ?>" /></td>
                    <td><input type="text" readonly="" name="phone_no" placeholder="Phone Number" value="<?php echo $row['phone_no'] ?>" /></td></tr>

                <tr><td>
                        <input type="submit" name="delete_user" value="Yes" class="delete_user button" id="delete_user"/> <br/>
                    </td>
                    <td>
                        <button class="button" onclick="goBack()">No</button>
                    </td></tr>
                <?php
            } else {
                
            }
            ?>
        </table>
    </form>
</table>
</div>