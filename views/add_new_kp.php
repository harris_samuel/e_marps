<script>

    confirmmessage_new = "The client you are adding will be set as active. Continue?";

    function seteditclient_new() {

        var currentdate = new Date();
        var currenttime = " "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + "";

        var month = currentdate.getUTCMonth() + 1; //months from 1-12
        var day = currentdate.getUTCDate();
        var year = currentdate.getUTCFullYear();

        var currentdate = year + "-" + month + "-" + day;

        var clinic_id = localStorage.getItem('sess_clinic_id');
        var partner_id = localStorage.getItem('sess_partner_id');
        var currentgender = localStorage.getItem('currentgender');

        var currentservice = localStorage.getItem('currentservice');
        var currentuuid = localStorage.getItem('currentuuid');
        var currentclientname = localStorage.getItem('currentclientname');
        var currentclientgender = localStorage.getItem('currentgender');

// redirect url?
        gotoloc = localStorage.getItem('gotoloc');

        console.log('/emarps/database/kp_active/update.php?uuid=' + currentuuid + '&clinic_id=' + clinic_id + '&partner_id=' + partner_id + '&gender=' + currentclientgender + '&name=' + currentclientname + '&entrytime=' + currenttime + '&entrydate=' + currentdate + '&status=Counselling');

        $.get('/emarps/database/kp_active/update.php?uuid=' + currentuuid + '&clinic_id=' + clinic_id + '&partner_id=' + partner_id + '&gender=' + currentclientgender + '&name=' + currentclientname + '&entrytime=' + currenttime + '&entrydate=' + currentdate + '&status=Counselling', function (data) {

            alert('Client ' + currentclientname + ' set as active.\nStatus is ' + currentservice + '\nPartner ID: ' + partner_id + ' - Clinic ID: ' + clinic_id + '\nTime: ' + currenttime + ' - Date: ' + currentdate + '');

            window.location.href = '?currentview=' + gotoloc + '';

        });

    }

// movetocounsellingqueue
    function movetocounsellingqueue_new() {
        var confirmclientchange = confirm("" + confirmmessage_new + "");
        if (confirmclientchange == true) {
        }
        else {
            return;
        }

        gotoloc = 'waiting';
        currentservice = 'Counselling - Queue';
        localStorage.setItem('currentservice', currentservice);
        localStorage.setItem('gotoloc', gotoloc);
        seteditclient_new();

    }

// movetoconsultationqueue
    function movetoconsultationqueue_new() {
        var confirmclientchange = confirm("" + confirmmessage_new + "");
        if (confirmclientchange == true) {
        }
        else {
            return;
        }

        gotoloc = 'waiting';
        currentservice = 'Consultation - Queue';
        localStorage.setItem('currentservice', currentservice);
        localStorage.setItem('gotoloc', gotoloc);
        seteditclient_new();

    }

// movetocareandtreatmentqueue
    function movetocareandtreatmentqueue_new() {
        var confirmclientchange = confirm("" + confirmmessage_new + "");
        if (confirmclientchange == true) {
        }
        else {
            return;
        }

        gotoloc = 'waiting';
        currentservice = 'Care and Treatment - Queue';
        localStorage.setItem('currentservice', currentservice);
        localStorage.setItem('gotoloc', gotoloc);
        seteditclient_new();

    }

// movetopharmacyqueue
    function movetopharmacyqueue_new() {
        var confirmclientchange = confirm("" + confirmmessage_new + "");
        if (confirmclientchange == true) {
        }
        else {
            return;
        }

        gotoloc = 'waiting';
        currentservice = 'Pharmacy - Queue';
        localStorage.setItem('currentservice', currentservice);
        localStorage.setItem('gotoloc', gotoloc);
        seteditclient_new();

    }

// addtocounselling
    function movetocounselling_new() {
        var confirmclientchange = confirm("" + confirmmessage_new + "");
        if (confirmclientchange == true) {
        }
        else {
            return;
        }

        gotoloc = 'counselling';
        currentservice = 'Counselling';
        localStorage.setItem('currentservice', currentservice);
        localStorage.setItem('gotoloc', gotoloc);
        seteditclient_new();

    }


// addtoconsultation

    function movetoconsultation_new() {
        var confirmclientchange = confirm("" + confirmmessage_new + "");
        if (confirmclientchange == true) {
        }
        else {
            return;
        }

        gotoloc = 'consultation';
        currentservice = 'Consultation';
        localStorage.setItem('currentservice', currentservice);
        localStorage.setItem('gotoloc', gotoloc);
        seteditclient_new();

    }


// addtocareandtreatment

    function movetocareandtreatment_new() {
        var confirmclientchange = confirm("" + confirmmessage_new + "");
        if (confirmclientchange == true) {
        }
        else {
            return;
        }

        gotoloc = 'careandtreatment';
        currentservice = 'Care and Treatment';
        localStorage.setItem('currentservice', currentservice);
        localStorage.setItem('gotoloc', gotoloc);
        seteditclient_new();

    }

// addtopharmacy

    function movetopharmacy_new() {
        var confirmclientchange = confirm("" + confirmmessage_new + "");
        if (confirmclientchange == true) {
        }
        else {
            return;
        }

        gotoloc = 'pharmacy';
        currentservice = 'Pharmacy';
        localStorage.setItem('currentservice', currentservice);
        localStorage.setItem('gotoloc', gotoloc);
        seteditclient_new();

    }
</script>

<table border="0" align="center" cellpadding="5">
    <tr>
        <td>

            <input onClick="movetocounselling_new();" class="small_button" type="button" value="Counselling">
            <div onClick="movetocounsellingqueue_new();" data-tip="Move to Counselling Queue" class="waiting_button tip"></div>

        </td>
    </tr>
    <tr><td>
            <input onClick="movetoconsultation_new();" class="small_button" type="button" value="Consultation">
            <div onClick="movetoconsultationqueue_new();" data-tip="Move to Consultation Queue" class="waiting_button tip"></div>
        </td>
    </tr>
    <tr><td>
            <input onClick="movetocareandtreatment_new();" class="small_button" type="button" value="Care and Treatment">
            <div onClick="movetocareandtreatmentqueue_new();" data-tip="Move to Care and Treatment Queue" class="waiting_button tip"></div>
        </td>
    </tr>
    <tr><td>
            <input onClick="movetopharmacy_new();" class="small_button" type="button" value="Pharmacy">
            <div onClick="movetopharmacyqueue_new();" data-tip="Move to Pharmacy Queue" class="waiting_button tip"></div>
        </td>
    </tr>
</table>