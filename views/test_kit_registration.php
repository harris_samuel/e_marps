<?php
$counties = $websrvc->select_counties();
if (isset($_REQUEST['save'])) {
    extract($_REQUEST);
        $succes = $websrvc->test_kit_registration($kit_name, $user_id, $datetime, $kit_type, $status);
    if ($succes) {
        ?>
        <script src="js/jquery.min.js"></script>


        <!-- noty -->
        <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                noty({text: 'New Test Kit Added Succesfully...'});
            });
        </script>


        <?php
    } else {

        echo '<div class="animated bounceIn warning">Wrong information. Please try again</div>';
    }
}
?>

<h2>Test Kit  Type Registration</h2>





<div class="form animated fadeIn" style="width:400px;">

    <center>
        <div>

            <form method="POST" autocomplete="off" action="" name="kp_type_registration" id="form_data"  accept-charset="UTF-8">


                <div class="left">


                    <input name="kit_name"  placeholder="Test Kit Name" class="fieldstyle" type="text" required=""/>

                    <input name="user_id" type="hidden" value="<?php
                    $user_id = $_SESSION['uid'];
                    echo $user_id;
                    ?>"/>
                    <input type="hidden" value="<?php
                    $today = date('Y-m-d H:i:s');
                    echo $today;
                    ?>" name="datetime"/>


                    <select name="kit_type" id="" class="status">

                        <option value="HIV Test Kit">HIV Test Kit</option>
                        <option value="Pregnancy Test Kit">Pregnancy Test Kit</option>
                        <option value="Other">Other</option>

                    </select>


                    <select name="status" id="" class="status">

                        <option value="Active">Active</option>
                        <option value="In Active">In Active</option>


                    </select>


                    <div class="next_button_container">
                        <input class="large_button" type="submit" value="Add Peer Educator" name="save" />

                    </div>
                </div>

            </form>




        </div></center>

</div>