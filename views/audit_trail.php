<style type="text/css">
.logentry {
	background-color: #E1F2FF;
	padding: 5px;
	margin-bottom: 5px;
	cursor: pointer;
}
.logentry:hover {
	background-color: #B9E1FF;
}
.datetimelog {
	color: #FFF;
	background-color: #0070C0;
	padding: 3px;
	margin-bottom: 5px;
}
</style>

<script>
function reloadnotifications() {
$( "#audit_trail" ).load( "/emarps/database/audit_trail/log.php" , function() {
});
}

$( document ).ready(function() {
setInterval(function(){
       reloadnotifications();
   }, 5000);
});
</script>

<div id="titlebar"><div id="titlebartext">Audit Trail</div></div>

<div class="form animated fadeIn" style="width:400px;">
<div id="audit_trail">
<?php include'database/audit_trail/log.php'?>
</div>
</div>