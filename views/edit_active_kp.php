<div id="layer_kpbookactiveclient" class="layerwindow animated fadeIn">
	<div style="margin-top:200px !important;" class="inlayerwindow animated bounceIn">
    <div data-tip="Close" class="layer_close_x tip" onclick="layer_kpbookactiveclient_close()">x</div>
    
<script>

confirmmessage = "You will lose all unsaved changes. The client you are transfering will be set as active. Are you sure?";

function seteditclient() {
	
var currentservice = localStorage.getItem('currentservice');

$.get( "database/kp_active/checkuuid.php?&task=checkifroomisbusy&service="+currentservice+"", function( data ) {
  		
		if (data == "Yes") {
			alert(''+currentservice+' is currently occupied.');
			return;
		}
		else if (data == "No") {
			updatenewnotificationstatus2();
		}
		
});

}

function updatenewnotificationstatus2 () {

var currentservice = localStorage.getItem('currentservice');
var currentuuid = localStorage.getItem('currentuuid');
var currentclientname = localStorage.getItem('currentclientname');
var activeclientidstatusupdate = localStorage.getItem('activeclientidstatusupdate');

// alert(activeclientidstatusupdate);
	
$.get( "database/audit_trail/updatelog.php?currentuser=<?php echo $uid ?>&useraction=Client "+currentclientname+" was moved to "+currentservice+"", function( data ) {
var activeclientidstatusupdate = localStorage.getItem('activeclientidstatusupdate');
var currentservice = localStorage.getItem('currentservice');

var statusupdateurl = "database/kp_active/updatestatus.php?statusupdate="+currentservice+"&activeclientidstatusupdate="+activeclientidstatusupdate+"";
// alert(statusupdateurl);

$.get( ""+statusupdateurl+"", function( data ) {
console.log('Status was changed');

$.get( "database/audit_trail/updatelog.php?currentuser=<?php echo $uid ?>&useraction=Client "+currentclientname+" was moved to "+currentservice+"", function( data ) {

  updatenewnotificationstatusaddnew2();
  
 });
});
});
}

function updatenewnotificationstatusaddnew2 () {

// redirect url?
var gotoloc = localStorage.getItem('gotoloc');

$.get( "/emarps/database/audit_trail/notificationstatus.php?task=addnew", function( data ) {
  setTimeout(function(){
window.location.href = '?currentview='+gotoloc+'';
}, 2000);
});

}

// movetocounsellingqueue
function movetocounsellingqueue() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'waiting';
currentservice = 'Counselling - Queue';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient();

layer_kpbookactiveclient_close();
}

// movetoconsultationqueue
function movetoconsultationqueue() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'waiting';
currentservice = 'Consultation - Queue';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient();

layer_kpbookactiveclient_close();
}

// movetocareandtreatmentqueue
function movetocareandtreatmentqueue() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'waiting';
currentservice = 'Care and Treatment - Queue';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient();

layer_kpbookactiveclient_close();
}

// movetopharmacyqueue
function movetopharmacyqueue() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'waiting';
currentservice = 'Pharmacy - Queue';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient();

layer_kpbookactiveclient_close();
}

// addtocounselling
function movetocounselling() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'counselling';
currentservice = 'Counselling';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient();

layer_kpbookactiveclient_close();
}


// addtoconsultation

function movetoconsultation() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'consultation';
currentservice = 'Consultation';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient();

layer_kpbookactiveclient_close();
}


// addtocareandtreatment

function movetocareandtreatment() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'careandtreatment';
currentservice = 'Care and Treatment';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient();

layer_kpbookactiveclient_close();
}

// addtopharmacy

function movetopharmacy() {
var confirmclientchange = confirm(""+confirmmessage+"");
if (confirmclientchange == true) {  
}
else {
    return;
}

gotoloc = 'pharmacy';
currentservice = 'Pharmacy';
localStorage.setItem('currentservice', currentservice);
localStorage.setItem('gotoloc', gotoloc);
seteditclient();

layer_kpbookactiveclient_close();
}
</script>

<h2>Transfer to Service</h2>

 <table border="0" align="center" cellpadding="5">
  <tr>
    <td>

<input onClick="movetocounselling();" class="small_button" type="button" value="Counselling">
<div onClick="movetocounsellingqueue();" data-tip="Move to Counselling Queue" class="waiting_button tip"></div>

</td>
</tr>
<tr><td>
<input onClick="movetoconsultation();" class="small_button" type="button" value="Consultation">
<div onClick="movetoconsultationqueue();" data-tip="Move to Consultation Queue" class="waiting_button tip"></div>
</td>
</tr>
<tr><td>
<input onClick="movetocareandtreatment();" class="small_button" type="button" value="Care and Treatment">
<div onClick="movetocareandtreatmentqueue();" data-tip="Move to Care and Treatment Queue" class="waiting_button tip"></div>
</td>
</tr>
<tr><td>
<input onClick="movetopharmacy();" class="small_button" type="button" value="Pharmacy">
<div onClick="movetopharmacyqueue();" data-tip="Move to Pharmacy Queue" class="waiting_button tip"></div>
</td>
  </tr>
</table>

  </div>
  </div>
</div>