<?php
include 'database/class.admin.php';

$admin = new ADMIN();
?>


<script type="text/javascript"  src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css" type="text/css" />
<script type="text/javascript">

    $(document).ready(function () {
        $('#partners_report').DataTable();
    });
</script>
<style type="text/css">
    .user_report {
        width: 80%;
    }
</style>

<h2>Peer Educators Management Form</h2>

<div class="form animated fadeIn">



    <table id="partners_report" class="partners_report">
        <thead>
            <tr>
                <th>No</th>
                <th>Peer Educator Name</th>
                <th>Nick name</th>
                <th>Phone No</th>
                <th>Partner</th>
                <th>Site/Spot</th>
                <th>Status</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Peer Educator Name</th>
                <th>Nick name</th>
                <th>Phone No</th>
                <th>Partner</th>
                <th>Site/Spot</th>
                <th>Status</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
        <tbody>


            <?php
            $res = $admin->manage_peer_educator_read();
            if (mysqli_num_rows($res) > 0) {
                $i = 1;
                while ($row = mysqli_fetch_array($res, MYSQLI_BOTH)) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['peer_educator_nme']; ?></td>
                        <td><?php echo $row['nick_name']; ?></td>
                        <td><?php echo $row['phone_no']; ?></td>
                        <td><?php echo $row['partner_name']; ?></td>
                        <td><?php echo $row['facility_name']; ?></td>
                        <td><?php echo $row['status']; ?></td>

                        <td><a href="?currentview=edit_peer_edctr_info&edit_id=<?php echo $row['id']; ?>">Edit</a></td>
                        <td><a href="?currentview=delete_peer_edctr_info&delete_id=<?php echo $row['id']; ?>">Delete</a>

                        </td>
                    </tr>
                    <?php
                    $i++;
                }
            } else {
                ?><tr>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                    <td>Nothing here...</td>
                </tr><?php
            }
            ?>
        </tbody>
    </table>


</div>
