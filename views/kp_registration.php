<style type="text/css">
</style>
<div id="titlebar"><div id="titlebartext">Registration/Revisit</div></div>

<script>
    sidebar1();
</script>

<script>
// Check if return from biometrics validation

    $(document).ready(function () {
        if (localStorage.getItem("uuid_from_biometrics") === null) {
            console.log('Not returning from biometrics');
        }
        else {
            document.getElementById('uuid_entry').value = localStorage.getItem('uuid_from_biometrics');
            localStorage.removeItem('uuid_from_biometrics');
            uuid_entry_search();
        }
    });
</script>

<div id="forms">

    <?php include'kp_registration_id.php'; ?>

</div>

<style>
    #layer_kpsearch input[type="radio"] {
        display: none;
    }
</style>

<div id="layer_kpsearch" class="layerwindow animated fadeIn">
    <div class="inlayerwindow_large animated fadeInUp">
        <input class="layerwindowclose" type="button" value="Close" onclick="layer_kpsearch_close();">
        <div class="inlayerwindowscrollarea">

            <h2>KP Client Search</h2>


            <div style="width:50%; float:left;">


                <form id="CLIENT_SEARCH" action="javascript:searchkpoptions();">

                    <div style="clear:both"></div> 

                    <input required class="fieldstyle"  id="txt_client_name" name="txt_client_name" type="text"  placeholder="Name">

                    <div style="clear:both"></div> 

                    <input name="radio_andor1" id="radio_andor1a" type="radio" value="and" /><input name="radio_andor1" type="radio" id="radio_andor1b" value="or" checked="checked" />

                    <div style="clear:both"></div> 

                    <label>Gender </label>
                    <select onfocus="document.getElementById('radio_andor1a').checked = true;" class="select_with_label" id="select_gender" name="select_gender">
                        <option value="M">Male</option>
                        <option value="F" selected="selected">Female</option>
                    </select>

                    <div style="clear:both"></div>

                    <input name="radio_andor2" id="radio_andor2a" type="radio" value="and" /><input name="radio_andor2" type="radio" value="or" checked="checked" />

                    <div style="clear:both"></div>

                    <fieldset class="fieldsets" style="width:130px;">
                        <legend>Optional</legend>

                        <label>Date of birth</label>
                        <input onfocus="document.getElementById('radio_andor2a').checked = true;" class="fieldstyledate" id="txt_client_date_of_birth" name="txt_client_date_of_birth" type="date"  placeholder="Date of Birth">

                        <div style="clear:both"></div>

                        <input name="radio_andor3" id="radio_andor3a" type="radio" value="and" /><input name="radio_andor3" type="radio" value="or" checked="checked" />

                        <div style="clear:both"></div> 

                        <input onfocus="document.getElementById('radio_andor3a').checked = true;" class="fieldstyle" id="txt_client_mobile_number" name="txt_client_mobile_number" type="text"  placeholder="Mobile Number">

                        <div style="clear:both; height:2px;"></div>

                        <input name="radio_andor4" id="radio_andor4a" type="radio" value="and" /><input name="radio_andor4" type="radio" value="or" checked="checked" />

                        <div style="clear:both"></div> 

                        <input onfocus="document.getElementById('radio_andor4a').checked = true;" class="fieldstyle" id="txt_client_residential_area" name="txt_client_residential_area" type="text"  placeholder="Residential Area">

                        <div style="clear:both; height:2px;"></div>

                        <input name="radio_andor5" id="radio_andor5a" type="radio" value="and" /><input name="radio_andor5" type="radio" value="or" checked="checked" />



                        <input onfocus="document.getElementById('radio_andor5a').checked = true;" class="fieldstyle" id="txt_client_home" name="txt_client_home" type="text"  placeholder="Place of birth">

                        <div style="clear:both; height:6px;"></div>

                    </fieldset>

                    <br>

                    <span class="tip" data-tip="Search for clients registered in the database. Try different search criteria<br> to narrow down the search if you get multiple results.">
                        <button style="float:left; width:150px;" class="small_button" onclick="localStorage.setItem('t2s', 'Searching');
                                speak();">Search</button>
                    </span>

                    <span class="tip" data-tip="Clear all fields and start another search.">
                        <input name="Reset" type="reset" class="small_button" style="float:left; margin-left:10px; width:60px;" value="Reset" />
                    </span>

                    <div style="clear:both"></div> 
                </form>
            </div>

            <div style="width:50%; float:right;">

                <div id="layer_kpsearch_kpuuidsearchoutput">
                </div>

                <div id="layer_kpsearch_kpuuidoutput">
                    <div style="height:15px;"></div>

                    <div id="uuid_box">
                        UUID: <span id="layer_kpsearch_uuid_view">Unknown</span>
                    </div>

                    <div style="clear:both"></div>

                    <div id="layer_kpsearch_search_results">
                        <span id="layer_kpsearch_search_results_ajax">
                            <div class="kp_search_results_box animated fadeInUp" style="-webkit-animation-delay:1">
                                No KP Client data to display.
                            </div>
                        </span>
                    </div>

                </div>

                <input id="btn_register_new_kp_client" style="float:right; margin-top:10px; margin-right:2px; width:334px; display:none;" onclick="registernewkpclient();" class="small_button" type="button" value="No Match? Register new KP Client">

            </div>
        </div>
    </div>



</div>


<div id="layer_kpbookforservice_existing_client" class="layerwindow animated fadeIn">
    <div style="margin-top:100px" class="inlayerwindow_medium animated bounceIn">
        <div data-tip="Close" class="layer_close_x tip" onclick="layer_kpbookforservice_existing_client_close()">x</div>

<?php include"kpbookforservice_existing_client.php"; ?>



    </div>
</div>



<div id="layer_kpbookforservice" class="layerwindow animated fadeIn">
    <div style="margin-top:200px !important;" class="inlayerwindow animated bounceIn">
        <div data-tip="Close" class="layer_close_x tip" onclick="layer_kpbookforservice_close()">x</div>

        <script>

            function updatenewnotificationstatusaddnew() {
                $.get("database/audit_trail/notificationstatus.php?task=addnew", function (data) {
                    document.location.href = '?currentview=' + gotoloc + '';
                });
            }
            function updatenewnotificationstatus() {
                var currentuuid = localStorage.getItem('currentuuid');
                var currentclientname = localStorage.getItem('currentclientname');

                $.get("database/audit_trail/updatelog.php?currentuser=<?php echo $uid ?>&useraction=Client " + currentclientname + " was added to " + currentservice + "", function (data) {
                    updatenewnotificationstatusaddnew();
                });
            }

            function preptoactive() {
                var currentclientname = localStorage.getItem('currentclientname_prep');
                var currentgender = localStorage.getItem('currentgender_prep');
                var currentuuid = localStorage.getItem('currentuuid_prep');

                localStorage.setItem('currentclientname', currentclientname);
                localStorage.setItem('currentgender', currentgender);
                localStorage.setItem('currentuuid', currentuuid);

                localStorage.removeItem('currentclientname_prep');
                localStorage.removeItem('currentgender_prep');
                localStorage.removeItem('currentuuid_prep');
            }

            function addtocounselling() {
                loadingindicator_start();
                gotoloc = 'counselling';
                currentservice = 'Counselling';
                addtoservice();
            }

            function addtoconsultation() {
                loadingindicator_start();
                gotoloc = 'consultation';
                currentservice = 'Consultation';
                addtoservice();
            }

            function addtocareandtreatment() {
                loadingindicator_start();
                gotoloc = 'careandtreatment';
                currentservice = 'Care and Treatment';
                addtoservice();
            }

            function addtopharmacy() {
                loadingindicator_start();
                gotoloc = 'pharmacy';
                currentservice = 'Pharmacy';
                addtoservice();
            }

            function addtocounsellingqueue() {
                loadingindicator_start();
                gotoloc = 'waiting';
                currentservice = 'Counselling - Queue';
                addtoservice();
            }

            function addtoconsultationqueue() {
                loadingindicator_start();
                gotoloc = 'waiting';
                currentservice = 'Consultation - Queue';
                addtoservice();
            }

            function addtocareandtreatmentqueue() {
                loadingindicator_start();
                gotoloc = 'waiting';
                currentservice = 'Care and Treatment - Queue';
                addtoservice();
            }

            function addtopharmacyqueue() {
                loadingindicator_start();
                gotoloc = 'waiting';
                currentservice = 'Pharmacy - Queue';
                addtoservice();
            }

            function addtoservice() {

                $.get('database/kp_active/checkuuid.php?&task=checkifroomisbusy&service=' + currentservice + '', function (datainroom) {

                    if (datainroom == 'Yes') {
                        alert('' + currentservice + ' is currently occupied.');
                        loadingindicator_stop();
                        return;
                    }
                    else if (datainroom == 'No') {
                        addtoservicego();
                    }
                    else {
                        alert('Error. Please try again or contact support.');
                    }

                });

            }


            function addtoservicego() {

                preptoactive();

                var currentdate = new Date();
                var currenttime = " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + "";

                var month = currentdate.getUTCMonth() + 1; //months from 1-12
                var day = currentdate.getUTCDate();
                var year = currentdate.getUTCFullYear();

                currentdate = year + "-" + month + "-" + day;

                var currentuuid = localStorage.getItem('currentuuid');
                var currentclientname = localStorage.getItem('currentclientname');

                var clinic_id = localStorage.getItem('sess_clinic_id');
                var partner_id = localStorage.getItem('sess_partner_id');
                var currentgender = localStorage.getItem('currentgender');

                // check if client is in active client list
                var activate_existing_client = localStorage.getItem('activate_existing_client');
                if (activate_existing_client == 'Yes') {
                    // alert(activate_existing_client);
                    localStorage.removeItem('activate_existing_client');

                    $.get('/emarps/database/kp_active/set_active.php?uuid=' + currentuuid + '&clinic_id=' + clinic_id + '&partner_id=' + partner_id + '&entrytime=' + currenttime + '&entrydate=' + currentdate + '&status=' + currentservice + '', function (data) {

                        alert('Client ' + currentclientname + ' set as active.\nStatus is ' + currentservice + '\nPartner ID: ' + partner_id + ' - Clinic ID: ' + clinic_id + '\nTime: ' + currenttime + ' - Date: ' + currentdate + '');
                    });

                    updatenewnotificationstatus();
                    return;

                }

                // if client is not in active client list
                $.get('/marps/database/kp_active/update.php?uuid=' + currentuuid + '&clinic_id=' + clinic_id + '&partner_id=' + partner_id + '&name=' + currentclientname + '&entrytime=' + currenttime + '&entrydate=' + currentdate + '&gender=' + currentgender + '&status=' + currentservice + '', function (data) {

                    updatenewnotificationstatus();
                });
            }

        </script>
        <h2><span id="book_client_name"></span> - Book for Service</h2>
        <table border="0" align="center" cellpadding="5">
            <tr>
                <td><input class="small_button" type="button" value="Counselling" onClick="addtocounselling();"><div onClick="addtocounsellingqueue();" data-tip="Add to Counselling Queue" class="waiting_button tip"></div>
                </td>
            </tr>
        </table>

        <div id="layer_kpbookforservice_more">▼ Other Options</div>
        <div id="layer_kpbookforservice_more_buttons" style="display:none">
            <table border="0" align="center" cellpadding="5">
                <tr><td>
                        <input class="small_button" type="button" value="Consultation" onClick="addtoconsultation();"><div onClick="addtoconsultationqueue();" data-tip="Add to Consultation Queue" class="waiting_button tip"></div>
                    </td>
                </tr>
                <tr><td>
                        <input class="small_button" type="button" value="Care and Treatment" onClick="addtocareandtreatment();"><div onClick="addtocareandtreatmentqueue();" data-tip="Add to Care and Treatment Queue" class="waiting_button tip"></div>
                    </td>
                </tr>
                <tr><td>
                        <input class="small_button" type="button" value="Pharmacy" onClick="addtopharmacy();"><div onClick="addtopharmacyqueue();" data-tip="Add to Pharmacy Queue" class="waiting_button tip"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

<script>
    scancardreturn = localStorage.getItem('scancardreturn');
// alert (scancardreturn);
    if (scancardreturn == 1) {
        var uuidresult = localStorage.getItem('uuid_entry');
        document.getElementById('layer_kpsearch_uuid_view').innerHTML = localStorage.getItem('uuid_entry');
        layer_kpsearch();
// alert(uuidresult);
        $("#layer_kpsearch_search_results_ajax").load("/emarps/database/registration_revisit.php?uuid=" + uuidresult + "");
        document.getElementById('btn_bookforservice').style.display = 'block';
    }
    else {

    }

    function searchkpoptions() {
        document.getElementById('layer_kpsearch_kpuuidoutput').style.display = 'none';
        document.getElementById('btn_register_new_kp_client').style.display = 'block';
        document.getElementById('layer_kpsearch_kpuuidsearchoutput').style.display = 'block';

        var searchkpoptions_name = document.getElementById('txt_client_name').value;
        var searchkpoptions_gender = document.getElementById('select_gender').value;
        var searchkpoptions_date_of_birth = document.getElementById('txt_client_date_of_birth').value;
        var searchkpoptions_client_mobile_number = document.getElementById('txt_client_mobile_number').value;
        var searchkpoptions_client_residential_area = document.getElementById('txt_client_residential_area').value;
        var searchkpoptions_client_home = document.getElementById('txt_client_home').value;

        var radio_andor1 = $("input[name=radio_andor1]:checked").val();
        var radio_andor2 = $("input[name=radio_andor2]:checked").val();
        var radio_andor3 = $("input[name=radio_andor3]:checked").val();
        var radio_andor4 = $("input[name=radio_andor4]:checked").val();
        var radio_andor5 = $("input[name=radio_andor5]:checked").val();

        var uri = "/marps/database/registration_revisit_kpsearch.php?radio_andor1=" + radio_andor1 + "&radio_andor2=" + radio_andor2 + "&radio_andor3=" + radio_andor3 + "&radio_andor4=" + radio_andor4 + "&radio_andor5=" + radio_andor5 + "&name=" + searchkpoptions_name + "&gender=" + searchkpoptions_gender + "&dob=" + searchkpoptions_date_of_birth + "&mobile=" + searchkpoptions_client_mobile_number + "&residential_area=" + searchkpoptions_client_residential_area + "&home_area=" + searchkpoptions_client_home + "";
        var res = encodeURI(uri);

        loadingindicator_start();

        $("#layer_kpsearch_kpuuidsearchoutput").load("" + res + "", function () {
            loadingindicator_stop();
        });
    }

    $("#layer_kpbookforservice_more").click(function () {
        $("#layer_kpbookforservice_more_buttons").slideToggle("slow", function () {

        });
    });

</script>