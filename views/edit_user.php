<?php
include 'database/class.admin.php';
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$admin = new ADMIN();
if (isset($_GET['edit_id'])) {
    $edit_id = $_GET['edit_id'];

    $select_query = "select * from users inner join roles on users.role_id = roles.role_id WHERE users.user_id='$edit_id'";
    $res = mysqli_query($link, $select_query);
    if ($res) {
        $row = mysqli_fetch_array($res, MYSQLI_BOTH);
    } else {

        echo 'Error' . mysqli_errno($link);
    }
}
if (isset($_REQUEST['update_user'])) {
    extract($_REQUEST);
    // echo 'Details ' . '<br>' . $user_name . '<br>' . $edit_id . '<br>' . $e_mail . '<br>' . $phone_no . '<br>' . $status . '<br>' . $role_name . '<br>';
    $success = $admin->update($user_name, $e_mail, $phone_no, $status, $role_name, $edit_id, $permissions, $date_added, $registers_permissions, $reports_permissions, $sidebar_permissions, $systembar_permissions);
    if ($success) {
        echo 'User Information Updated Successfully';
    } else {
        echo 'Error please try again ...';
    }
}
?> 
<div class="form animated fadeIn">
    <script type="text/javascript">
        function goBack() {
            window.history.back()
        }

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#systembar_selecctall').click(function (event) {  //on click 

                if (this.checked) { // check select status
                    $('.systembar_function_checkbox').each(function () { //loop through each checkbox
                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                    });
                } else {
                    $('.systembar_function_checkbox').each(function () { //loop through each checkbox
                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                    });
                }
            });

        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebar_selecctall').click(function (event) {  //on click 

                if (this.checked) { // check select status
                    $('.sidebar_function_checkbox').each(function () { //loop through each checkbox
                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                    });
                } else {
                    $('.sidebar_function_checkbox').each(function () { //loop through each checkbox
                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                    });
                }
            });

        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#reports_selecctall').click(function (event) {  //on click 

                if (this.checked) { // check select status
                    $('.reports_function_checkbox').each(function () { //loop through each checkbox
                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                    });
                } else {
                    $('.reports_function_checkbox').each(function () { //loop through each checkbox
                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                    });
                }
            });

        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#registers_selecctall').click(function (event) {  //on click 

                if (this.checked) { // check select status
                    $('.registers_function_checkbox').each(function () { //loop through each checkbox
                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                    });
                } else {
                    $('.registers_function_checkbox').each(function () { //loop through each checkbox
                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                    });
                }
            });

        });
    </script>

    <form method="post" name="update_user_form" autocomplete="off" class="update_user_form" id="update_user_form">
        <table id="dataview">
            <?php
            $count_row = mysqli_num_rows($res);
            if ($count_row === 1) {
                ?>   
                <input type="hidden" name="user_id" value="<?php echo $edit_id; ?>"/>
                <tr><td><input type="text" name="user_name" placeholder="User Name" value="<?php echo $row['user_name'] ?>" /><br /></td></tr>
                <tr><td><input type="text" name="e_mail" placeholder="E-mail" value="<?php echo $row['email'] ?>" /></td></tr>
                <tr><td><input type="text" name="phone_no" placeholder="Phone Number" value="<?php echo $row['phone_no'] ?>" /></td></tr>
                <tr><td>
                        <input name="date_added" value="<?php echo date('Y-m-d H:i:s'); ?>"/>
                        <select name="status">
                            <option value="<?php echo $row['status']; ?>"><?php echo $row['status']; ?></option>
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select>
                    </td></tr>
                <tr><td>
                        <select name="role_name" id="role_name" class="role_name">
                            <option value="<?php echo $row['role_id'] ?>"><?php echo $row['role_name'] ?></option>
                            <label>Partner Name : </label>

                            <?php
                            $clinic_id = $_SESSION['sess_clinic_id'];
                            $partner_id = $_SESSION['sess_partner_id'];
                            if (empty($clinic_id) and empty($partner_id)) {

                                $query = "SELECT * FROM roles  ORDER BY  date_added ASC";
                            } elseif (!empty($partner_id) and empty($clinic_id)) {

                                $query = "SELECT * FROM roles where role_name != 'Super User' ORDER BY date_added ";
                            } elseif (!empty($partner_id) and ! empty($clinic_id)) {

                                $query = "SELECT * FROM roles where role_name != 'Super User' and role_name !='Partner Administrator' ORDER BY date_added ";
                            }
                            if ($result = mysqli_query($link, $query)) {
                                ?>  <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $role_id = $idresult[0];
                                    $role_name = $idresult[1];
                                    ?>
                                    <option id="partner_id"  value="<?php echo $role_id; ?>"><?php echo $role_name; ?></option>   <?php
                                }
                                ?>
                                <?php
                            }
                            ?>
                        </select>
                    </td></tr>

                <?php
            } else {
                
            }
            ?>

            <tr>


                <td>


                    <label>System Bar Function List : </label>

                    <ul>

                        <li><input type="checkbox" id="systembar_selecctall"/> Select/Un-Select All</li>
                        <li>

                            <?php
                            // GET LIST AND DISPLAY IN FORM
                            ?>
                            <?php
                            $query = "SELECT * FROM `functions` where status='Active' and location='systembar' ORDER BY  date_added ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>

                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $id = $idresult[0];
                                    $name = $idresult[1];
                                    $query = "select * from user_permission_list where user_id='$edit_id' and function_id ='$id' and permissions_status='Active' ORDER BY date_added ASC";
                                    if ($assigned_result = mysqli_query($link, $query)) {
                                        while ($assigned_results = mysqli_fetch_array($assigned_result))
                                            $assigned_function_name = $assigned_results[5];
                                        ?>

                                        <input type="checkbox" class="systembar_function_checkbox" name="systembar_permissions[]" value="<?php echo $id; ?>"     
                                               <?php if ($assigned_function_name === $name) echo 'checked="checked"'; ?>  > <?php echo $name; ?> 
                                               <?php
                                           }
                                           ?>


                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>
                        </li>
                    </ul>



                </td>



                <td>



                    <label>Side Bar Function List : </label>

                    <ul>

                        <li><input type="checkbox" id="sidebar_selecctall"/> Select/Un-Select All</li>
                        <li>

                            <?php
                            // GET LIST AND DISPLAY IN FORM
                            ?>
                            <?php
                            $query = "SELECT * FROM `functions` where status='Active' and location='sidebar' ORDER BY  date_added ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>

                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $id = $idresult[0];
                                    $name = $idresult[1];
                                    $query = "select * from user_permission_list where user_id='$edit_id' and function_id ='$id' and permissions_status='Active' ORDER BY date_added ASC";
                                    if ($assigned_result = mysqli_query($link, $query)) {
                                        while ($assigned_results = mysqli_fetch_array($assigned_result))
                                            $assigned_function_name = $assigned_results[5];
                                        ?>

                                        <input type="checkbox" class="sidebar_function_checkbox" name="sidebar_permissions[]" value="<?php echo $id; ?>"     
                                               <?php if ($assigned_function_name === $name) echo 'checked="checked"'; ?>  > <?php echo $name; ?> 
                                               <?php
                                           }
                                           ?>


                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>
                        </li>
                    </ul>



                </td>




                <td>



                    <label>Reports Function List : </label>

                    <ul>

                        <li><input type="checkbox" id="reports_selecctall"/> Select/Un-Select All</li>
                        <li>

                            <?php
                            // GET LIST AND DISPLAY IN FORM
                            ?>
                            <?php
                            $query = "SELECT * FROM `functions` where status='Active' and location='reports' ORDER BY  date_added ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>

                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $id = $idresult[0];
                                    $name = $idresult[1];
                                    $query = "select * from user_permission_list where user_id='$edit_id' and function_id ='$id' and permissions_status='Active' ORDER BY date_added ASC";
                                    if ($assigned_result = mysqli_query($link, $query)) {
                                        while ($assigned_results = mysqli_fetch_array($assigned_result))
                                            $assigned_function_name = $assigned_results[5];
                                        ?>

                                        <input type="checkbox" class="reports_function_checkbox" name="reports_permissions[]" value="<?php echo $id; ?>"     
                                               <?php if ($assigned_function_name === $name) echo 'checked="checked"'; ?>  > <?php echo $name; ?> 
                                               <?php
                                           }
                                           ?>


                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>
                        </li>
                    </ul>



                </td>




                <td>



                    <label>Registers Function List : </label>

                    <ul>

                        <li><input type="checkbox" id="registers_selecctall"/> Select/Un-Select All</li>
                        <li>

                            <?php
                            // GET LIST AND DISPLAY IN FORM
                            ?>
                            <?php
                            $query = "SELECT * FROM `functions` where status='Active' and location='registers' ORDER BY  date_added ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>

                                <?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $id = $idresult[0];
                                    $name = $idresult[1];
                                    $query = "select * from user_permission_list where user_id='$edit_id' and function_id ='$id' and permissions_status='Active' ORDER BY date_added ASC";
                                    if ($assigned_result = mysqli_query($link, $query)) {
                                        while ($assigned_results = mysqli_fetch_array($assigned_result))
                                            $assigned_function_name = $assigned_results[5];
                                        ?>

                                        <input type="checkbox" class="registers_function_checkbox" name="registers_permissions[]" value="<?php echo $id; ?>"     
                                               <?php if ($assigned_function_name === $name) echo 'checked="checked"'; ?>  > <?php echo $name; ?> 
                                               <?php
                                           }
                                           ?>


                                    <?php
                                }
                                ?>

                                <?php
                            }
                            ?>
                        </li>
                    </ul>



                </td>



            </tr>
            <tr>
            <span style="color: red;">Please note editing user data will overwrite the  users current access rights. </span>
            </tr>
            <tr><td>
                    <input type="submit" name="update_user" value="Update User Details" class="update_user button" id="update_user"/>
                </td><td>
                    <button class="button" onclick="goBack()">Cancel</button>
                </td></tr>
        </table>
    </form>
</table>
</div>