<?php
include 'database/class.admin.php';
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$admin = new ADMIN();
if (isset($_GET['edit_id'])) {
    $edit_id = $_GET['edit_id'];

    $select_query = "select * from partner  WHERE partner.partner_id='$edit_id'";
    $res = mysqli_query($link, $select_query);
    if ($res) {
        $row = mysqli_fetch_array($res, MYSQLI_BOTH);
    } else {

        echo 'Error' . mysqli_errno($link);
    }
}
if (isset($_REQUEST['update_partner'])) {
    extract($_REQUEST);
    // echo 'Details ' . '<br>' . $user_name . '<br>' . $edit_id . '<br>' . $e_mail . '<br>' . $phone_no . '<br>' . $status . '<br>' . $role_name . '<br>';
    $success = $admin->update_partner($partner_id, $partner_name, $contact_person_name, $contact_person_phone, $contact_person_email, $status,$kp_target);
    if ($success) {
        echo 'Partner Information Updated Successfully';
    } else {
        echo 'Error please try again ...';
    }
}
?> 
<div class="form animated fadeIn">
    <script type="text/javascript">
        function goBack() {
            window.history.back()
        }

    </script>

    <form method="post" name="update_partner_form" class="update_partner_form" id="update_partner_form">
        <table id="dataview">
            <?php
            $count_row = mysqli_num_rows($res);
            if ($count_row === 1) {
                ?>   
                <input type="hidden" name="partner_id" value="<?php echo $row['partner_id']; ?>"/>
                <tr><td><input type="text" name="partner_name" placeholder="Partner Name" value="<?php echo $row['partner_name'] ?>" /><br /></td></tr>
                <tr><td><input type="text" name="contact_person_name" placeholder="Contact Person Name " value="<?php echo $row['contact_person'] ?>" /></td></tr>
                <tr><td><input type="text" name="contact_person_phone" placeholder="Contact Phone Number" value="<?php echo $row['contact_person'] ?>" /></td></tr>
                <tr><td><input type="text" name="contact_person_email" placeholder="Contact Phone Email" value="<?php echo $row['contact_person_email'] ?>" /></td></tr>
                <tr><td><input type="text" name="kp_target" placeholder="KP Target " value="<?php echo $row['kp_target'] ?>" /></td></tr>
                <tr><td>
                        <select name="status">
                            <option value="<?php echo $row['status']; ?>"><?php echo $row['status']; ?></option>
                            <option value="Active">Active</option>
                            <option value="In Active">In Active</option>
                        </select>
                    </td></tr>

                <tr><td>
                        <input type="submit" name="update_partner" value="Update Partner Details" class="update_partner button" id="update_partner"/>
                    </td><td>
                        <button class="button" onclick="goBack()">Cancel</button>
                    </td></tr>
                <?php
            } else {
                
            }
            ?>
        </table>
    </form>
</table>
</div>