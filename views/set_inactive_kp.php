<div id="layer_kpsetinactiveclient" class="layerwindow animated fadeIn">
	<div style="margin-top:200px !important;" class="inlayerwindow animated bounceIn">
    <div data-tip="Close" class="layer_close_x tip" onclick="layer_kpsetinactiveclient_close()">x</div>
    
<script>

</script>

<h2>Remove from active clients</h2>

<div class="hint">
Reason for manual removal from the list of active clients?
</div>

<script type="text/javascript">
function client_remove_other_button() {
	document.getElementById('remove_client_options').style.display = 'none';
	document.getElementById('client_remove_other_entry').style.display = 'block';
}
function client_remove_other_button_back() {
	document.getElementById('remove_client_options').style.display = 'block';
	document.getElementById('client_remove_other_entry').style.display = 'none';
}
function client_remove_localstorage() {
	localStorage.removeItem('currentclientname');
	localStorage.removeItem('currentgender');
	localStorage.removeItem('currentuuid');
	localStorage.removeItem('client_remove_reason');
	localStorage.removeItem('client_remove_id');
	
	document.getElementById('currentclientname').innerHTML = '';
	document.getElementById('currentuuid').innerHTML = '';
}
function client_remove_send() {
	
	loadingindicator_start();
	
	var client_remove_reason = localStorage.getItem('client_remove_reason');
	var client_remove_id = localStorage.getItem('activeclientremove');
	
	$.get('/emarps/database/kp_active/remove.php?id=' + client_remove_id + '&unattended=' + client_remove_reason + '', function (data) {
          layer_kpsetinactiveclient_close();
    });
	
	client_remove_localstorage();
	   
}
function client_remove_other_send() {
	
	var client_remove_reason = document.getElementById('client_remove_other_reason').value;
	var client_remove_id = localStorage.getItem('activeclientremove');
	
	$.get('/emarps/database/kp_active/remove.php?id=' + client_remove_id + '&unattended=Other&unattendedother=' + client_remove_reason + '', function (data) {
          layer_kpsetinactiveclient_close();
    });
	
	client_remove_localstorage();
	   
}
</script>

<br>
<table id="remove_client_options" border="0" cellpadding="5" class="animated fadeIn" style="margin-left:10px;">
   <tr>
    <td><input class="small_button" type="button" value="Facility Closing" onclick="localStorage.setItem('client_remove_reason','Facility Closing');client_remove_send();" /></td>
    <td><input class="small_button" type="button" value="Walked out" onclick="localStorage.setItem('client_remove_reason','Walked out');client_remove_send();" /></td>
  </tr>
<tr>
  <td><input class="small_button" type="button" value="Impatience" onclick="localStorage.setItem('client_remove_reason','Impatience');client_remove_send();" /></td>
  <td><input class="small_button" type="button" value="Misunderstanding" onclick="localStorage.setItem('client_remove_reason','Misunderstanding');client_remove_send();" /></td>
</tr>
<tr>
  <td><input class="small_button" type="button" value="Protest" onclick="localStorage.setItem('client_remove_reason','Protest');client_remove_send();" /></td>
  <td><input class="small_button" type="button" value="System Failure" onclick="localStorage.setItem('client_remove_reason','System Failure');client_remove_send();" /></td>
</tr>
<tr>
  <td><input class="small_button" type="button" value="Completed" onclick="localStorage.setItem('client_remove_reason','Completed');client_remove_send();" /></td>
  <td><input class="small_button" type="button" value="Other" onclick="client_remove_other_button();" /></td>
  </tr>
</table>

<div id="client_remove_other_entry" class="animated fadeIn" style="display:none; padding-left:20px; margin-top:20px;">
<input class="fieldstyle" name="client_remove_other_reason" id="client_remove_other_reason" type="text" placeholder="Please specify other reason" value="">
<div style="margin-top:30px;">
<button class="small_button" onclick="client_remove_other_send();">OK</button>
</div>
<div style="margin-top:10px;">
<button class="small_button" onclick="client_remove_other_button_back();">Cancel</button>
</div>
</div>

  </div>
  </div>
</div>