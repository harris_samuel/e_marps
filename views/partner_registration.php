<?php
$counties = $websrvc->select_counties();
if (isset($_REQUEST['save'])) {
    extract($_REQUEST);
    $succes = $websrvc->partner_registration($kp_types, $date_today, $partner_kp_target, $partner_region_name, $partner, $contactpersonname, $contactpersonphone, $contactpersonemail, $kp_typess, $kp_targetss, $region_namess);
    if ($succes) {
        ?>
        <script src="js/jquery.min.js"></script>


        <!-- noty -->
        <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                noty({text: 'Partner Added Succesfully...'});
            });
        </script>


        <?php
    } else {

        echo '<div class="animated bounceIn warning">Wrong information. Please try again</div>';
    }
}
?>

<h2>New Partner Registration</h2>

<div id="form1" class="form animated fadeIn">
    <form method="POST" action="" name="partner_registration" id="form_data"  accept-charset="UTF-8">


        <div class="left">


            <input name="partner" placeholder="Partner's Name" class="fieldstyle" type="text">

            <input name="contactpersonphone" placeholder="Contact Person Phone" class="fieldstyle" type="text">

            <input name="date_today" type="hidden" value="<?php
            $my_date = date("Y-m-d H:i:s");
            echo $my_date;
            ?>"/>


            <div class="notice">Hold down <img align="absmiddle" src="images/ctrl.png" height="30" /> to select multiple answers</div>


            <label>Region Name : </label>

            <?php
            $query = "SELECT * FROM `region` ORDER BY region_name ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>
                <select  class="partner_region_name fieldstyle_with_label" multiple="multiple" required="" name="partner_region_name[]" id="partner_region_name">

                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $region_id = $idresult[0];
                        $region_name = $idresult[1];
                        ?>
                        <option id="partner_id"  value="<?php echo $region_id; ?>"><?php echo $region_name . '&nbsp;'; ?></option>   <?php
                    }
                    ?>
                </select>

                <div style="clear:both"></div>

                <?php
            }
            ?>

            <div class="clearafter"></div>

        </div>

        <div class="right">

            <input name="contactpersonname" placeholder="Contact Person Name" class="fieldstyle" type="text">

            <input name="contactpersonemail" placeholder="Contact Person E-Mail" class="fieldstyle" type="text">

            <input name="partner_kp_target" placeholder="Partner KP Target" class="fieldstyle" type="text"/>


            <div class="notice">Hold down <img align="absmiddle" src="images/ctrl.png" height="30" /> to select multple answers</div>

            <label>Key Population Types : </label>

            <?php
            $query = "SELECT * FROM `kp_types` ORDER BY Name ASC";
            if ($result = mysqli_query($link, $query)) {
                ?>
                <select  class="kp_types fieldstyle_with_label" multiple="multiple" required="" name="kp_types[]" id="kp_types">

                    <?php
                    while ($idresult = mysqli_fetch_row($result)) {
                        $kp_type_id = $idresult[0];
                        $kp_type_name = $idresult[1];
                        ?>
                        <option id="partner_id"  value="<?php echo $kp_type_id; ?>"><?php echo $kp_type_name . '&nbsp;'; ?></option>   <?php
                    }
                    ?>
                </select>

                <div style="clear:both"></div>

                <?php
            }
            ?>


            <div class="clearafter"></div>



            <div style="margin-top:120px; margin-left: -250px; margin-right: 90px; margin-top: 50px;">
                <table rules="all" style="background:#fff;">
                    <tr>
                        <td style="font-size:14px;">KP Type</td>
                        <td style="font-size:14px;">KP Target</td>
                        <td style="font-size:14px;">Region</td>
                        <td><span class="add_more_rows" id="add_more_rows" style="font:normal 12px agency, arial; color:blue; text-decoration:underline; cursor:pointer;">
                                Add More
                            </span>
                        </td>
                    </tr>
                    <tr id="rowId">
                        <td>
                            <label>Key Population Types : </label>

                            <?php
                            $query = "SELECT * FROM `kp_types` ORDER BY Name ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>
                                <select  class="kp_types fieldstyle_with_label"  required="" name="kp_typess[]" id="kp_types">

                                    <?php
                                    while ($idresult = mysqli_fetch_row($result)) {
                                        $kp_type_id = $idresult[0];
                                        $kp_type_name = $idresult[1];
                                        ?>
                                        <option id="partner_id"  value="<?php echo $kp_type_id; ?>"><?php echo $kp_type_name . '&nbsp;'; ?></option>   <?php
                                    }
                                    ?>
                                </select>

                                <div style="clear:both"></div>

                                <?php
                            }
                            ?>
                        </td>
                        <td>
                            <input name="kp_targetss[]" type="text" value="" />
                        </td>
                        <td>
                            <label>Region Name : </label>

                            <?php
                            $query = "SELECT * FROM `region`";
                            if ($result = mysqli_query($link, $query)) {
                                ?>
                                <select  class="region_name fieldstyle_with_label"  required="" name="region_name[]" id="region_name">
                                    <option></option>
                                    <?php
                                    while ($idresult = mysqli_fetch_row($result)) {
                                        $kp_type_id = $idresult[0];
                                        $kp_type_name = $idresult[1];
                                        ?>
                                        <option id="region_id"  value="<?php echo $kp_type_id; ?>"><?php echo $kp_type_name . '&nbsp;'; ?></option>   <?php
                                    }
                                    ?>
                                </select>

                                <div style="clear:both"></div>

                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                </table>
                <div id="addedRows"></div>

            </div>


        </div>

        <style>
            ul#horizontal_list li {
                display:inline;
            }
        </style>

        <script type="text/javascript">

            $(document).ready(function () {
                var rowCount = 1;
                $(".add_more_rows").click(function () {

                    rowCount++;

                    var recRow = '<p id="rowCount' + rowCount + '"><ul class="horizontal_list" id="horizontal_list"><li><label>Key Population Type :</label><?php
                            $query = "SELECT * FROM `kp_types` ORDER BY Name ASC";
                            if ($result = mysqli_query($link, $query)) {
                                ?>\n\<select  class="kp_types fieldstyle_with_label"  required="" name="kp_typess[]" id="kp_types">\n\<?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $kp_type_id = $idresult[0];
                                    $kp_type_name = $idresult[1];
                                    ?>\n\<option id="partner_id"  value="<?php echo $kp_type_id; ?>"><?php echo $kp_type_name . '&nbsp;'; ?></option>\n\<?php }
                                ?>\n\ </select>\n\<?php }
                            ?></li>\n\
<li><input name="kp_targetss[]" type="text"  maxlength="120" style="margin: 4px 5px 0 5px;"/></li>\n\
<li><label> Region Name :</label><?php
                            $query = "SELECT * FROM `region`";
                            if ($result = mysqli_query($link, $query)) {
                                ?>\n\<select  class="kp_types fieldstyle_with_label"  required="" name="region_namess[]" id="region_name"> <option></option>\n\<?php
                                while ($idresult = mysqli_fetch_row($result)) {
                                    $kp_type_id = $idresult[0];
                                    $kp_type_name = $idresult[1];
                                    ?>\n\<option id="partner_id"  value="<?php echo $kp_type_id; ?>"><?php echo $kp_type_name . '&nbsp;'; ?></option>\n\<?php }
                                ?>\n\ </select>\n\<?php }
                            ?></li>\n\
<li> <span id="remove_row' + rowCount + '" onClick="javascript:fnRemove(this);" class="remove_row' + rowCount + '" style="font:normal 12px agency, arial; culor:blue; text-decoration:underline; cursor:pointer;">Delete Entryy</span> </li></ul>  </p>';


                    $('.remove_row' + rowCount + '').click(function () {
                        alert('.remove_row' + rowCount + '');
                        $('#rowCount' + rowCount).remove();
                        alert('#rowCount' + rowCount);
                    });
                    $('#addedRows').append(recRow);


                });

            });

            function fnRemove(t) {

                $(t).parent('p').remove();

            }
        </script>



        <div class="next_button_container">
            <input class="large_button" type="submit" value="Submit" name="save" />

        </div>

    </form>
</div>