<?php
include 'database/class.admin.php';

$admin = new ADMIN();
?>


<script type="text/javascript"  src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css" type="text/css" />
<script type="text/javascript">

    $(document).ready(function () {
        $('#kp_type_report').DataTable();
    });
</script>
<style type="text/css">
    .user_report {
        width: 80%;
    }
</style>

<h2>KP Type Management Form</h2>

<div class="form animated fadeIn">



    <table id="kp_type_report" class="kp_type_report">
        <thead>
            <tr>
                <th>No</th>
                <th>KP Type Name</th>
                <th>Abbrv</th>
                <th>Date Added</th>
                <th>Status</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No</th>
                <th>KP Type Name</th>
                <th>Abbrv</th>
                <th>Date Added</th>
                <th>Status</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
        <tbody>


            <?php
            $res = $admin->manage_kp_type_read();
            if (mysqli_num_rows($res) > 0) {
                $i = 1;
                while ($row = mysqli_fetch_array($res, MYSQLI_BOTH)) {
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['Name']; ?></td>
                        <td><?php echo $row['Abbrv']; ?></td>
                        <td><?php echo $row['date_added']; ?></td>
                        <td><?php echo $row['status']; ?></td>
                       

                        <td><a href="?currentview=edit_kp_type_info&edit_id=<?php echo $row['id']; ?>">Edit</a></td>
                        <td><a href="?currentview=delete_kp_type_info&delete_id=<?php echo $row['id']; ?>">Delete</a>

                        </td>
                    </tr>
                    <?php
                    $i++;
                }
            } else {
                ?><tr>
                    <td colspan="5">Nothing here...</td>
                    <td colspan="5">Nothing here...</td>
                    <td colspan="5">Nothing here...</td>
                    <td colspan="5">Nothing here...</td>
                    <td colspan="5">Nothing here...</td>
                    <td colspan="5">Nothing here...</td>
                    <td colspan="5">Nothing here...</td>
                </tr><?php
            }
            ?>
        </tbody>
    </table>


</div>
