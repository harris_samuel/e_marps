<?php
include 'database/class.admin.php';
$link = mysqli_connect($host_name, $user_name, $password, $database);
// check connection
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$admin = new ADMIN();
if (isset($_GET['delete_id'])) {

    $delete_id = $_GET['delete_id'];

    $select_query = "select * from partner WHERE partner_id='$delete_id'";
    $res = mysqli_query($link, $select_query);
    if ($res) {
        $row = mysqli_fetch_array($res, MYSQLI_BOTH);
    } else {

        echo 'Error' . mysqli_errno($link);
    }
}
if (isset($_REQUEST['delete_partner'])) {
    extract($_REQUEST);
    // echo 'Details ' . '<br>' . $user_name . '<br>' . $edit_id . '<br>' . $e_mail . '<br>' . $phone_no . '<br>' . $status . '<br>' . $role_name . '<br>';
    $success = $admin->delete_partner($partner_id, $delete_id);
    if ($success) {
        echo 'Partner  Deativated Succesfully ';
    } else {
        echo 'Error please try again ...';
    }
}
?> 
<div class="form animated fadeIn">
    <script type="text/javascript">
        function goBack() {
            window.history.back()
        }

    </script>
    <form method="post" name="delete_partner_form" class="delete_partner_form" id="delete_partner_form">
        <table id="dataview">
            <?php
            $count_row = mysqli_num_rows($res);
            if ($count_row === 1) {
                ?>   
                <input type="hidden" name="partner_id" value="<?php echo $row['partner_id']; ?>"/>
                <tr><td><input type="text" readonly="" name="partner_name" placeholder="Partner Name" value="<?php echo $row['partner_name'] ?>" /><br /></td></tr>
                <tr><td><input type="text" readonly="" name="contact_person_name" placeholder="Contact Person Name " value="<?php echo $row['contact_person'] ?>" /></td></tr>
                <tr><td><input type="text" readonly="" name="contact_person_phone" placeholder="Contact Phone Number" value="<?php echo $row['contact_person'] ?>" /></td></tr>
                <tr><td><input type="text" readonly="" name="contact_person_email" placeholder="Contact Phone Email" value="<?php echo $row['contact_person_email'] ?>" /></td></tr>
                <tr><td>
                        <input type="text" readonly="" name="status" placeholder="Partner Status" value="<?php echo $row['status']; ?>"/>
                    </td></tr>

                <tr><td>
                        <input type="submit" value="Delete Partner Entry" name="delete_partner" class="button"/>
                        <button class="button" onclick="goBack()">Cancel</button>
                    </td></tr>
                <?php
            } else {
                
            }
            ?>
        </table>
    </form>
</table>
</div>