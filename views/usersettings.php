<style type="text/css">
.toggleswitch {
	border: 1px solid #0070C0;
	font-size: 13px;
	background-color: #FFF;
	text-align: center;
	color: #000;
	margin-left: 10px;
	display: inline;
	padding-top: 2px;
	padding-right: 10px;
	padding-bottom: 2px;
	padding-left: 10px;
	cursor: pointer;
	transition: all 0.3s;
}
.toggleswitch:hover {
	background-color: #35A9FF;
	color: #FFF;
	transition: all 0.3s;
}
.toggleswitch_active {
	background-color: #0070C0;
	color: #FFF;
}
</style>

<script>
function usersettings_set_notifications() {
	window.location.reload();
}

$( document ).ready(function() {

var usersettings_notifications = localStorage.getItem('usersettings_notifications');

if (usersettings_notifications == 'Yes') {
document.getElementById('usersettings_notifications_yes').className = 'toggleswitch toggleswitch_active';
}
else if (usersettings_notifications == 'No') {
document.getElementById('usersettings_notifications_no').className = 'toggleswitch toggleswitch_active';
}
else {}
// document.getElementById('usersettings_notifications_yes').className = 'toggleswitch toggleswitch_active';
});
</script>

<div id="titlebar"><div id="titlebartext">User Settings/Preferences</div></div>

<div class="form animated fadeIn" style="margin-top:150px;">
Enable Notifications: 
<div id="usersettings_notifications_yes" class="toggleswitch" onclick="localStorage.setItem('usersettings_notifications','Yes');usersettings_set_notifications();">Yes</div> 
<div id="usersettings_notifications_no" class="toggleswitch" onclick="localStorage.setItem('usersettings_notifications','No');usersettings_set_notifications();">No</div>
</div>