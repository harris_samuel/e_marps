<?php
$counties = $websrvc->select_counties();
if (isset($_REQUEST['save'])) {
    extract($_REQUEST);
    $succes = $websrvc->kp_type_registration($kp_type_name, $abbrv);
    if ($succes) {
        ?>
        <script src="js/jquery.min.js"></script>


        <!-- noty -->
        <script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                noty({text: 'New Key Population Added Succesfully...'});
            });
        </script>


        <?php
    } else {

        echo '<div class="animated bounceIn warning">Wrong information. Please try again</div>';
    }
}
?>

<h2>New Key Population Type Registration</h2>





<div class="form animated fadeIn" style="width:400px;">

    <center>
        <div>

            <form method="POST" autocomplete="off" action="" name="kp_type_registration" id="form_data"  accept-charset="UTF-8">


                <div class="left">


                    <input name="kp_type_name"  placeholder="Key Population's Name" class="fieldstyle" type="text" required=""/>

                    <input name="abbrv" placeholder="Key Population's Abbreviation Name" class="fieldstyle" type="text" required=""/>


                    <div class="next_button_container">
                        <input class="large_button" type="submit" value="Add KP Type" name="save" />

                    </div>
                </div>

            </form>




        </div></center>

</div>