<style type="text/css">
.choose_id_method {
	cursor: pointer;
	transition: all 0.5s;
}
.choose_id_method:hover {
	transition: all 0.5s;
	background-color: #BBE2FF;
}
.kpregstep {
	margin-bottom: 6px;
	padding-top: 5px;
	padding-right: 10px;
	padding-bottom: 10px;
	padding-left: 10px;
	box-shadow: 0 0 2px 1px rgba(0,0,0,0.2);
	margin-top: 6px;
}
.notice {
	background-color: #E1F2FF;
	margin-top: 10px;
	margin-bottom: 10px;
	font-size: 14px;
	font-weight: normal;
	width: 400px;
	padding-top: 10px;
	padding-right: 13px;
	padding-bottom: 10px;
	padding-left: 13px;
	display: inline;
	float: right;
	margin-right: 10px;
}
.regstepheadline {
	font-size: 18px;
	margin-top: 5px;
	margin-bottom: 10px;
}
.spacer {
	height: 15px;
}
</style>


<div class="form animated fadeIn">

<div class="kpregstep">

<table width="100%" border="0">
  <tr>
    <td width="50%"><div class="regstepheadline">Validate by Fingerprint</div></td>
    <td width="50%"><div class="small_button" style="float:right; width:100px;" onclick="document.location.href='?currentview=kp_scanbarcode'">Barcode?</div><div class="regstepheadline">Scan Card</div></td>
  </tr>
  <tr>
    <td onclick="document.location.href='efs://ver'" class="choose_id_method" bgcolor="#E1F2FF" align="center"><br><img src="images/fingerprint.png" width="60"><br><br></td>
    <td onclick="document.location.href='?currentview=kp_scancard'" class="choose_id_method" bgcolor="#E1F2FF" align="center"><br><img src="images/qrcard.png" width="230"><br><br></td>
  </tr>
</table>

</div>

<div class="spacer"></div>

<div class="kpregstep">


<div class="notice">
<img src="images/info_button.png" width="30" align="left" style="padding-right:8px;">If the client does not consent to Biometrics & can not produce Card but remembers UUID, please enter it here.
</div>
<div class="regstepheadline">Search by UUID</div>

<input class="fieldstyle" id="uuid_entry" name="uuid_entry" type="text" placeholder="Enter UUID"><div style="float:left" class="small_button" onclick="uuid_entry_search();">Start Search</div><div style="float:left; margin-left:10px;" class="small_button" onclick="layer_kpsearch();document.getElementById('layer_kpsearch_uuid_view').innerHTML = 'Unknown';">Can't remember UUID?</div><div style="clear:both"></div>
</div>

<div class="spacer"></div>

<div class="kpregstep">
<div class="notice"><img src="images/info_button.png" width="30" align="left" style="padding-right:8px;">Client claims to be not registered in the system and does not consent to Biometrics.
</div><div class="regstepheadline">Not registered/New client?</div>

<div style="margin-top:15px;" class="small_button" onclick="layer_kpsearch();document.getElementById('layer_kpsearch_uuid_view').innerHTML = 'Unknown';">Start search</div>
</div>


</div>