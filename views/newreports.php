<style type="text/css">
#searchfilters {
	background-color: #FFFFFF;
	width: 500px;
	box-shadow: 0 0 15px 0px rgba(0,0,0,0.2);
	margin-top: 7px;
	margin-right: 20px;
	padding-top: 10px;
	padding-right: 10px;
	padding-left: 10px;
}
#searchfilters select {
	margin-right: 15px;
	margin-bottom: 15px;
}


#contain {
	width: 600px;
	height: 500px;
	margin: auto;
}

.accordion,.accordion div,.accordion h1,.accordion p,.accordion a,.accordion img,.accordion span,.accordion em,.accordion ul,.accordion li {
	margin: 0;
	padding: 0;
	border: none;
}

/* Accordion Layout Styles */
.accordion {
	width: 600px;
	padding: 1px 5px 5px 5px;
	-webkit-box-shadow: 0px 1px 0px rgba(255,255,255, .05);
	-moz-box-shadow: 0px 1px 0px rgba(255,255,255, .05);
	box-shadow: 0px 1px 0px rgba(255,255,255, .05);
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
}
.accordion .content div {
	color: #000;
	background-color: #EBEBEB;
	margin-bottom: 5px;
	padding-top: 6px;
	padding-right: 12px;
	padding-bottom: 6px;
	padding-left: 12px;
	border: 1px solid #BBB;
	cursor: pointer;
	transition: all 0.5s;
}
.accordion .content div:hover {
	background-color: #FFFFFF;
	transition: all 0.5s;
}
.accordion .tab {
	display: block;
	height: 35px;
	margin-top: 4px;
	padding-left: 20px;
	text-decoration: none;
	color: #FFF;
	text-shadow: 1px 1px 0px rgba(0,0,0, .2);
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px; /* Old browsers */
	background-position: top;
	font-family: Arial, sans-serif;
	font-size: 12px;
	line-height: 35px;
	font-weight: bold;
	background-color: #0070C0;
}

.accordion .tab:hover,.accordion div:target .tab {
	color: #FFFFFF;
	text-shadow: 0px 1px 0px rgba(255,255,255, .15); /* Old browsers */
	background-color: #999999;
}

.accordion div .content {
	display: none;
	margin: 5px 0;
}

.accordion div:target .content {
	display: block;
}

.accordion > div {
	height: 40px;
	overflow: hidden;

	-webkit-transition: all .3s ease-in-out;
	-moz-transition: all .3s ease-in-out;
	-o-transition: all .3s ease-in-out;
	-ms-transition: all .3s ease-in-out;
	transition: all .3s ease-in-out;
}

.accordion > div:target {
	height: 360px;
}

/* Accordion Content Styles */
.accordion .content h1 {
	color: white;
	font: 18px/32px Arial, sans-serif;
}

.accordion .content p {
	margin: 10px 0;
	color: white;
	font: 11px/16px Arial, sans-serif;
}

.accordion .content span {
	font: italic 11px/12px Georgia, Arial, sans-serif;
	color: #4f4f4f;
}

.accordion .content em.bullet {
	width: 5px;
	height: 5px;
	margin: 0 5px;
	display: inline-block;
	-webkit-box-shadow: inset 1px 1px 1px rgba(255,255,255, 0.4);
	-moz-box-shadow: inset 1px 1px 1px rgba(255,255,255, 0.4);
	box-shadow: inset 1px 1px 1px rgba(255,255,255, 0.4);
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	background-color: #999999;
}

.accordion .content ul li {
	list-style: none;
	float: left;
	margin: 5px 10px 5px 0;
}

.accordion .content img {
	-webkit-box-shadow: 2px 2px 6px rgba(0,0,0, .5);
	-moz-box-shadow: 2px 2px 6px rgba(0,0,0, .5);
	box-shadow: 2px 2px 6px rgba(0,0,0, .5);
}
.select_filters {
	transition: all 0.6s;
	background-size: 100% 100%;
	padding-top: 3px;
	padding-right: 5px;
	padding-bottom: 3px;
	padding-left: 5px;
	margin-top: 10px;
	margin-bottom: 10px;
	margin-left: 10px;
	border: 1px solid #999999;
}
</style>

<script>

</script>

<h2>Monitoring &amp; Evaluation Indicators</h2>

<table border="0">
  <tr>
    <td valign="top">
    
    <div id="searchfilters">
    <select class="select_filters">
    <option>
    Implementing Partner
    </option>
    </select>
    
    <select class="select_with_label">
    <option>
    County / Sub County
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    Site
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    Year
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    Month
    </option>
    </select>
    
    <select class="select_filters">
    <option>
    Quarterly
    </option>
    </select>
    
    </div>
    
    <td valign="top">
    <td><div id="contain">
<div class="accordion">
		<div id="tab-1">
			<a onclick="" href="#tab-1" class="tab">Outreach</a>
			<div class="content">
				<div>KP Contacted</div>
                <div>Newly KP Contacted</div>
                <div>No. of group meetings & KP attendants</div>
		  </div>
		</div>
		<div id="tab-2">
		  <a onclick="" href="#tab-2" class="tab">Clinical Services</a>
			<div class="content">
				<div>Biomedical Services uptake</div>
                <div>Counseling Services uptake</div>
                <div>HIV Testing and RAT</div>
                <div>STI Screening and treatment</div>
                <div>Post exposure prophylaxis</div>
		  </div>
		</div>
  <div id="tab-3">
		  <a onclick="" href="#" class="tab">Commodities</a>
			<div class="content">
				
    </div>
		</div>
  <div id="tab-4">
		  <a onclick="" href="#" class="tab">Structual Interventions</a>
			<div class="content">
				
    </div>			
		</div>
  <div id="tab-5">
	  <a onclick="" href="#" class="tab">Programme coverage</a>
			<div class="content">
			
	  </div>			
  </div>
        
      
    <div id="tab-6">
	  <a onclick="" href="#" class="tab">Cohort Register</a>
			<div class="content">
				
	  </div>			
  </div>
        
        <div id="tab-7">
		  <a onclick="" href="#tab-7" class="tab">MOH Registers</a>
			<div class="content">
				<div>Family planing</div>
                <div>Tubercolosis (Yellowcard)</div>
                <div>Health testing and counseling</div>
                <div>MOH 361a</div>
                <div>MOH 361b</div>
                <div>PEP</div>
                <div>MOH 257 (Bluecard)</div>
		  </div>			
		</div>
        
</div>
</td>
  </tr>
</table>



