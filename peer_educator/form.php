<?php
include '../views/websrvc.php';
$websrvc = new Websrvc();

if (isset($_REQUEST['peer_educator_registration_button'])) {
    extract($_REQUEST);
    $succes = $websrvc->peer_educator_members_registration($member_name, $nick_name, $phone_no);
    if ($succes) {
        echo '<div class="animated bounceIn success">Registration Successfull. </div>';
    } else {

        echo '<div class="animated bounceIn warning">Wrong information. Please try again</div>';
    }
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Peer Educator Register Form</title>
            <link rel="stylesheet" type="text/css" href="view.css" media="all">
                <script type="text/javascript" src="view.js"></script>

                </head>
                <body id="main_body" >

                    <img id="top" src="top.png" alt="">
                        <div id="form_container">

                            <h1><a>Peer Educator Register Form</a></h1>
                            <form id="peer_educator_registration_form" class="appnitro peer_educator_registration_form" autocomplete="off"  method="post" action="">
                                <div class="form_description">
                                    <h2>Peer Educator Register Form</h2>
                                    <p>Peer Educator Members Registration Form </p>
                                </div>						
                                <ul >

                                    <li id="li_1" >
                                        <label class="description" for="element_1">Member Name :  </label>
                                        <div>
                                            <input id="element_1" name="member_name" class="element text medium" type="text" maxlength="255" value=""/> 
                                        </div> 
                                    </li>		<li id="li_2" >
                                        <label class="description" for="element_2">Nick Name : </label>
                                        <div>
                                            <input id="element_2" name="nick_name" class="element text medium" type="text" maxlength="255" value=""/> 
                                        </div> 
                                    </li>		<li id="li_3" >
                                        <label class="description" for="element_3">Phone No: </label>
                                        <div>
                                            <input id="element_3" name="phone_no" class="element text medium" type="text" maxlength="255" value=""/> 
                                        </div> 
                                    </li>

                                    <li class="buttons">
                                        <input type="hidden" name="form_id" value="961472" />

                                        <input id="saveForm" class="button_text" type="submit" name="peer_educator_registration_button" value="Save Member" />
                                    </li>
                                </ul>
                            </form>	
                            <div id="footer">
                                 <a href="http://192.163.227.245/e_marps/emarps">E-marps</a>
                            </div>
                        </div>
                        <img id="bottom" src="bottom.png" alt="">
                            </body>
                            </html>